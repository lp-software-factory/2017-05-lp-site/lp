<?php
use Zend\Expressive\Router\FastRouteRouter;

$application = require_once __DIR__.'/../../api/bootstrap/bootstrap-api.php';

$RESTApiApplication = new \Zend\Expressive\Application(new FastRouteRouter(), null, new \Zend\Stratigility\NoopFinalHandler());
$RESTApiApplication->pipe('/backend/api', $application);
$RESTApiApplication->pipeRoutingMiddleware();
$RESTApiApplication->pipeDispatchMiddleware();

$RESTApiApplication->run();
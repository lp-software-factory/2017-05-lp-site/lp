import {Observable} from 'rxjs';

import {TranslateModuleConfig, TranslateLoader} from '@ngx-translate/core';
import {AppFrontendService} from '../module/Lp/LpApp/_module/frontend/service/AppFrontendService';

export const appTranslateConfig: TranslateModuleConfig = {
    loader: {
        provide: TranslateLoader,
        useFactory: (factoryTranslateFrontendLoader),
        deps: [
            AppFrontendService,
        ]
    },
};

export function factoryTranslateFrontendLoader(frontend: AppFrontendService) {
    return new TranslateFrontendLoader(frontend);
}

export class TranslateFrontendLoader implements TranslateLoader
{
    constructor(
        private frontend: AppFrontendService,
    ) {}

    getTranslation(lang: string): Observable<any> {
        return Observable.create(observer => {
            this.frontend.replay.subscribe(next => {
                if (next.translations) {
                    observer.next(next.translations.resources);
                }
            });
        });
    }
}

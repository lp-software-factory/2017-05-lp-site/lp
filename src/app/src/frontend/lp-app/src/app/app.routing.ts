import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {UINotFound} from '../module/Lp/LpApp/_module/ui/routes/UINotFound/index';

import {LandingRoute} from "../module/Lp/LpApp/_module/landing/routes/Landing/index";
import {LandingHomeRoute} from '../module/Lp/LpApp/_module/landing/routes/LandingHomeRoute/index';
import {LandingContactsRoute} from "../module/Lp/LpApp/_module/landing/routes/LandingContactsRoute/index";
import {LandingServicesRoute} from "../module/Lp/LpApp/_module/landing/routes/LandingServicesRoute/index";
import {LandingProcessRoute} from "../module/Lp/LpApp/_module/landing/routes/LandingProcessRoute/index";
import {LandingTechnologiesRoute} from "../module/Lp/LpApp/_module/landing/routes/LandingTechnologiesRoute/index";
import {LandingCollaborationRoute} from "../module/Lp/LpApp/_module/landing/routes/LandingCollaborationRoute/index";
import {LandingWorkWithUsRoute} from "../module/Lp/LpApp/_module/landing/routes/LandingWorkWithUsRoute/index";

export const appRoutes: Routes = [
    {
        path: '',
        pathMatch: 'prefix',
        component: LandingRoute,
        children: [
            {
                path: '',
                pathMatch: 'full',
                component: LandingHomeRoute,
            },
            {
                path: 'contacts',
                pathMatch: 'full',
                component: LandingContactsRoute,
            },
            {
                path: 'process',
                pathMatch: 'full',
                component: LandingProcessRoute,
            },
            {
                path: 'services',
                pathMatch: 'full',
                component: LandingServicesRoute,
            },
            {
                path: 'technologies',
                pathMatch: 'full',
                component: LandingTechnologiesRoute,
            },
            {
                path: 'collaboration',
                pathMatch: 'full',
                component: LandingCollaborationRoute,
            },
            {
                path: 'work-with-us',
                pathMatch: 'full',
                component: LandingWorkWithUsRoute,
            },
        ]
    },
    {
        path: '**',
        pathMatch: 'full',
        component: UINotFound,
    },
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes, {useHash: true});
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {HttpModule} from '@angular/http';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';

import {LpAppModule} from '../module/Lp/LpApp/LpAppModule';

import {appRoutes} from './app.routing';
import {appTranslateConfig} from './app.translate';

import {AppComponent} from './app.component';
import {AppFrontendService} from '../module/Lp/LpApp/_module/frontend/service/AppFrontendService';
import {FrontendService} from "../module/ZEA2Common/src/_module/frontend/FrontendService";

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserAnimationsModule,
        BrowserModule,
        FormsModule,
        HttpModule,

        RouterModule.forRoot(appRoutes),
        TranslateModule.forRoot(appTranslateConfig),

        LpAppModule,
    ],
    providers: [
        {
            provide: FrontendService,
            useExisting: AppFrontendService,
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule
{
}
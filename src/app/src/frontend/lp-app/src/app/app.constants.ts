export const SITE_HOST: { domain: string, protocol: 'http'|'https' , port: number, } = {
    domain: 'lpsoftwarefactory.com',
    protocol: 'http',
    port: 80
};

export const SITE_HOST_URL = SITE_HOST.protocol + '://' + SITE_HOST.domain;
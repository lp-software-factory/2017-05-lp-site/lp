import {Observable} from "rxjs";

import {template} from "../../functions/template";
import {RESTAdapterInterface} from "../../essentials/RESTAdapterInterface";
import {CreateOAuth2ProviderRequest, urlCreateOAuth2Provider, CreateOAuth2ProviderResponse200} from "../../definitions/oauth2/providers/paths/create";
import {DeleteOAuth2ProviderResponse200, urlDeleteOAuth2Provider} from "../../definitions/oauth2/providers/paths/delete";
import {GetAllOAuth2HandlersResponse200, urlGetAllHandlers} from "../../definitions/oauth2/providers/paths/get-all-handlers";
import {GetAllOAuth2ProvidersResponse200, urlGetAllOAuth2Providers} from "../../definitions/oauth2/providers/paths/get-all-providers";
import {GetOAuth2ProviderConfigResponse200, urlGetOAuth2ProviderConfig} from "../../definitions/oauth2/providers/paths/get-config";
import {SetOAuth2ProviderConfigRequest, SetOAuth2ProviderConfigResponse200, urlSetOAuth2Provider} from "../../definitions/oauth2/providers/paths/set-config";

export interface OAuth2ProvidersRESTAdapterInterface
{
	createOAuth2Providers(request: CreateOAuth2ProviderRequest): Observable<CreateOAuth2ProviderResponse200>;
	deleteOAuth2Providers(code: string): Observable<DeleteOAuth2ProviderResponse200>;
	getAllOAuth2Handlers(): Observable<GetAllOAuth2HandlersResponse200>;
	getAllOAuth2Providers(): Observable<GetAllOAuth2ProvidersResponse200>;
	getOAuth2ProviderConfig(code: string): Observable<GetOAuth2ProviderConfigResponse200>;
	setOAuth2ProviderConfig(code: string, request: SetOAuth2ProviderConfigRequest): Observable<SetOAuth2ProviderConfigResponse200>;
}

export class OAuth2ProvidersRESTService implements OAuth2ProvidersRESTAdapterInterface
{
	constructor(private rest: RESTAdapterInterface) {
	}

	createOAuth2Providers(request: CreateOAuth2ProviderRequest): Observable<CreateOAuth2ProviderResponse200> {
		return this.rest.put(urlCreateOAuth2Provider, request);
	}

	deleteOAuth2Providers(code: string): Observable<DeleteOAuth2ProviderResponse200> {
		return this.rest.delete(template(urlDeleteOAuth2Provider, {
			code: code,
		}));
	}

	getAllOAuth2Handlers(): Observable<GetAllOAuth2HandlersResponse200> {
		return this.rest.get(urlGetAllHandlers);
	}

	getAllOAuth2Providers(): Observable<GetAllOAuth2ProvidersResponse200> {
		return this.rest.get(urlGetAllOAuth2Providers);
	}

	getOAuth2ProviderConfig(code: string): Observable<GetOAuth2ProviderConfigResponse200> {
		return this.rest.get(template(urlGetOAuth2ProviderConfig, {
			code: code,
		}));
	}

	setOAuth2ProviderConfig(code: string, request: SetOAuth2ProviderConfigRequest): Observable<SetOAuth2ProviderConfigResponse200> {
		return this.rest.post(template(urlSetOAuth2Provider, {
			code: code,
		}), request);
	}
}
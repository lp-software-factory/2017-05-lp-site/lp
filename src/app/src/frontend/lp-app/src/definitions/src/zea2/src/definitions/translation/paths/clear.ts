import {Success200} from "../../_common/response/Success200";

export const urlTranslationClear = '/backend/api/translation/{projectId}/clear';

export interface TranslationClearResponse200 extends Success200
{}
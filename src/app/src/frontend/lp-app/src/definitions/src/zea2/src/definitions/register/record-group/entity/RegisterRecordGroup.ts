import {IdEntity} from "../../../_common/markers/IdEntity";
import {SIDEntity} from "../../../_common/markers/SIDEntity";
import {VersionEntity} from "../../../_common/markers/VersionEntity";
import {ModificationEntity} from "../../../_common/markers/ModificationEntity";

    export interface RegisterRecordGroupEntity extends IdEntity, SIDEntity, VersionEntity, ModificationEntity
{
    app_name: string,
}
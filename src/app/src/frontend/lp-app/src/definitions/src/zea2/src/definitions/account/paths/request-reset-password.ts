import {Success200} from "../../_common/response/Success200";

export const urlAccountRequestResetPassword = '/backend/api/account/request-reset-password';

export interface RequestResetPasswordResponse200 extends Success200
{
}

export interface RequestResetPasswordRequest
{
	email: string;
}
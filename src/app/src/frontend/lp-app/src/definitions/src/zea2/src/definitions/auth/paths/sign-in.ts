import {Success200} from "../../_common/response/Success200";
import {JWTAuthToken} from "../entity/JWTAuthToken";

export const urlAuthSignIn = "/backend/api/auth/sign-in";

export interface SignInRequest
{
	email: string;
	password: string;
}

export interface SignInResponse200 extends Success200
{
	token: JWTAuthToken;
}
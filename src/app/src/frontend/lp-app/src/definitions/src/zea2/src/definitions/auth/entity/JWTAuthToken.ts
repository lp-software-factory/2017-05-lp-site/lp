import {AccountEntity} from "../../account/entity/AccountEntity";
import {ProfileEntity} from "../../profile/entity/Profile";

export interface JWTAuthToken
{
	jwt: string;
	account: AccountEntity;
	profile: ProfileEntity
}
import {Success200} from "../../../_common/response/Success200";
import {RegisterRecordGroupEntity} from "../entity/RegisterRecordGroup";

export const urlRegisterRecordGroupEdit = '/backend/api/register/group/{recordGroupId}/edit';

export interface RegisterRecordGroupEditRequest
{
    app_name: string;
}

export interface RegisterRecordGroupEditResponse200 extends Success200
{
    register_record_group: RegisterRecordGroupEntity;
}
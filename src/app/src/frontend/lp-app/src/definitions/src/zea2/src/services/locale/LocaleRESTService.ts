import {Observable} from "rxjs";

import {template} from "../../functions/template";
import {RESTAdapterInterface} from "../../essentials/RESTAdapterInterface";
import {CreateLocaleRequest, urlCreateLocale, CreateLocaleResponse200} from "../../definitions/locale/paths/create";
import {DeleteLocaleResponse200, urlDeleteLocale} from "../../definitions/locale/paths/delete";
import {SetLocaleResponse200, urlSetLocale} from "../../definitions/account/paths/set-locale";
import {GetLocaleResponse200, urlGetLocale} from "../../definitions/locale/paths/get";
import {ListLocalesResponse200, urlListLocales} from "../../definitions/locale/paths/list";
import {EditLocaleRequest, EditLocaleResponse200, urlEditLocale} from "../../definitions/locale/paths/locale";

export interface LocaleRESTInterface
{
	create(request: CreateLocaleRequest): Observable<CreateLocaleResponse200>;
	delete(localeId: number): Observable<DeleteLocaleResponse200>;
	edit(localeId: number, request: EditLocaleRequest): Observable<EditLocaleResponse200>;
	get(localeId: number): Observable<GetLocaleResponse200>;
	setCurrentLocale(region): Observable<SetLocaleResponse200>;
	list(): Observable<ListLocalesResponse200>;
}

export class LocaleRESTService implements LocaleRESTInterface
{
	constructor(private rest: RESTAdapterInterface) {
	}

	create(request: CreateLocaleRequest): Observable<CreateLocaleResponse200> {
		return this.rest.put(urlCreateLocale, request);
	}

	edit(localeId: number, request: EditLocaleRequest): Observable<EditLocaleResponse200> {
		return this.rest.post(template(urlEditLocale, {
			localeId: localeId,
		}), request);
	}

	delete(id: number): Observable<DeleteLocaleResponse200> {
		return this.rest.delete(template(urlDeleteLocale, {
			id: id,
		}));
	}

	setCurrentLocale(region): Observable<SetLocaleResponse200> {
		return this.rest.post(template(urlSetLocale, {
			region: region,
		}), '{}');
	}

	get(id: number): Observable<GetLocaleResponse200> {
		return this.rest.get(template(urlGetLocale, {
			id: id
		}));
	}

	list(): Observable<ListLocalesResponse200> {
		return this.rest.get(urlListLocales);
	}
}
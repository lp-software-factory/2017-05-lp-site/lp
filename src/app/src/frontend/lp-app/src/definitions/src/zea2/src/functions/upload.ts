import {Observable} from "rxjs";

export class UploadObservableProgress
{
    public YEP_I_AM_AN_UPLOADED_RESPONSE = true;

    constructor(
        public progress: number,
        public total: number,
        public loaded: number,
    ) {}
}

export interface UploadObservableFactoryOptions
{
    url: string;
    method?: string;
    file: Blob;
}

export class UploadObservableLastOnlyFactory<T>
{
    public create(input: Observable<T|UploadObservableProgress>): Observable<T> {
        return Observable.create(observer => {
            let lastResponse: T;

            input.subscribe(
                next => {
                    if(! next.hasOwnProperty('YEP_I_AM_AN_UPLOADED_RESPONSE')) {
                        lastResponse = <any>next;
                    }
                },
                error => {
                    observer.error(error);
                    observer.complete();
                },
                () => {
                    observer.next(lastResponse);
                    observer.complete();
                }
            )
        });
    }
}

export class UploadObservableFactory<T>
{
    public create(options: UploadObservableFactoryOptions): Observable<UploadObservableProgress|T> {
        if(! options.method) options.method = 'POST';

        return Observable.create(observer => {
            let xhrRequest = new XMLHttpRequest();
            let formData = new FormData();

            formData.append("file", options.file);

            xhrRequest.open(options.method, options.url);
            xhrRequest.send(formData);

            xhrRequest.onprogress = (e) => {
                if(e.lengthComputable) {
                    observer.next(new UploadObservableProgress(Math.floor((e.loaded / e.total) * 100), e.total, e.loaded));
                }
            };

            xhrRequest.onreadystatechange = () => {
                if(xhrRequest.readyState === 4) {
                    try {
                        let json = JSON.parse(xhrRequest.response);

                        if(xhrRequest.status === 200) {
                            observer.next(json);
                            observer.complete();
                        } else {
                            observer.error(json);
                        }
                    } catch(e) {
                        observer.error('Failed to parse JSON');
                    }
                }
            };
        }).share();
    }
}
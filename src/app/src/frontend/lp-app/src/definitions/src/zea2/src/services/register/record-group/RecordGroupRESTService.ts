import {Observable} from "rxjs";

import {template} from "../../../functions/template";
import {RESTAdapterInterface} from "../../../essentials/RESTAdapterInterface";
import {RegisterRecordGroupCreateRequest, RegisterRecordGroupCreateResponse200, urlRegisterRecordGroupCreate} from "../../../definitions/register/record-group/paths/create";
import {RegisterRecordGroupDeleteResponse200, urlRegisterRecordGroupDelete} from "../../../definitions/register/record-group/paths/delete";
import {RegisterRecordGroupGetByIdResponse200, urlRegisterRecordGroupGetById} from "../../../definitions/register/record-group/paths/get-by-id";
import {RegisterRecordGroupEditRequest, RegisterRecordGroupEditResponse200, urlRegisterRecordGroupEdit} from "../../../definitions/register/record-group/paths/edit";
import {RegisterRecordGroupGetAllResponse200, urlRegisterRecordGroupGetAll} from "../../../definitions/register/record-group/paths/get-all";

export interface RegisterRecordGroupRESTInterface
{
    create(request: RegisterRecordGroupCreateRequest): Observable<RegisterRecordGroupCreateResponse200>;
    edit(recordGroupId: number, request: RegisterRecordGroupEditRequest): Observable<RegisterRecordGroupEditResponse200>;
    delete(recordGroupId: number): Observable<RegisterRecordGroupDeleteResponse200>;
    get(recordGroupId: number): Observable<RegisterRecordGroupGetByIdResponse200>;
    all(): Observable<RegisterRecordGroupGetAllResponse200>;
}

export class RegisterRecordGroupRESTService implements RegisterRecordGroupRESTInterface
{
    constructor(private rest: RESTAdapterInterface) {
    }

    create(request: RegisterRecordGroupCreateRequest): Observable<RegisterRecordGroupCreateResponse200> {
        return this.rest.put(urlRegisterRecordGroupCreate, request);
    }

    edit(recordGroupId: number, request: RegisterRecordGroupEditRequest): Observable<RegisterRecordGroupEditResponse200> {
        return this.rest.post(template(urlRegisterRecordGroupEdit, {
            recordGroupId: recordGroupId,
        }), request);
    }

    delete(id: number): Observable<RegisterRecordGroupDeleteResponse200> {
        return this.rest.delete(template(urlRegisterRecordGroupDelete, {
            id: id,
        }));
    }

    get(id: number): Observable<RegisterRecordGroupGetByIdResponse200> {
        return this.rest.get(template(urlRegisterRecordGroupGetById, {
            id: id
        }));
    }

    all(): Observable<RegisterRecordGroupGetAllResponse200> {
        return this.rest.get(urlRegisterRecordGroupGetAll);
    }
}
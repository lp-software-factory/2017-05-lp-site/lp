import {LocalizedString} from "../../../../../zea2/src/definitions/locale/definitions/LocalizedString";

export interface LpInfo
{
    contacts: LpContacts,
    legal: LpLegal,
}

export interface LpContacts {
    email: {
        main: string,
        additional: string[],
    },
    phone: {
        main: string,
        additional: string[],
    },
    skype: string,
    map: string,
}

export interface LpLegal
{
    name: LocalizedString[],
    inn: string,
    kpp: string,
    real_address: LocalizedString[],
    legal_address: LocalizedString[],
    account: string,
    bank: LocalizedString[],
    bik: string,
    account_bank: string,
}
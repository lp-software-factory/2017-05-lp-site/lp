import {IdEntity} from "../../_common/markers/IdEntity";

export interface AttachmentEntity<T extends AttachmentMetadata> extends IdEntity
{
	sid: string;
	date_created_on: string;
	date_attached_on?: string;
	is_attached: boolean;
	title: string;
	description: string;
	type: string;
	name: string;
	link: {
		url: string;
		resource: string;
		version: number;
		source: {
			source: string;
			origURL: string;
		};
		metadata: T;
	}
	owner?: {
		id: number;
		code: string;
	}
}

export interface AttachmentMetadata
{
}
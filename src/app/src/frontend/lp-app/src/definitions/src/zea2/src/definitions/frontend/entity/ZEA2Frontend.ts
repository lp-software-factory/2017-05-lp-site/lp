import {LocaleEntity} from '../../locale/definitions/LocaleEntity';
import {PaletteEntity} from '../../colors/entity/PaletteEntity';
import {TranslationEntity} from '../../translation/entity/Translation';
import {JWTAuthToken} from '../../auth/entity/JWTAuthToken';

export interface ZEA2Frontend
{
    /**
     * Сюда экспортируются данные, которые шаблонизатор на бэкенде хочет экспортировать во фронтенд
     * Может содержать все что угодно.
     */
    exports: { [key: string]: any };

    /**
     * Данные, которые могут присутствовать в каждом запросе
     */
    locale: {
        current: LocaleEntity;
        available: LocaleEntity[];
    };
    palettes: PaletteEntity[];
    translations: {
        region: string,
        fallback: string,
        resources: TranslationEntity[],
    };
    auth?: JWTAuthToken;
}
import {Observable} from "rxjs";

import {RESTAdapterInterface} from "../../../../zea2/src/essentials/RESTAdapterInterface";
import {template} from "../../../../zea2/src/functions/template";
import {FeedbackCreateRequest, FeedbackCreateResponse200, urlFeedbackCreate} from "../../definitions/feedback/paths/create";
import {FeedbackAnswerRequest, FeedbackAnswerResponse200, urlFeedbackAnswer} from "../../definitions/feedback/paths/answer";
import {FeedbackDeleteResponse200, urlFeedbackDelete} from "../../definitions/feedback/paths/delete";
import {FeedbackGetByIdResponse200, urlFeedbackGetById} from "../../definitions/feedback/paths/get-by-id";
import {FeedbackMarkAsReadResponse200, urlFeedbackMarkAsRead} from "../../definitions/feedback/paths/mark-as-read";
import {FeedbackListRequest, FeedbackListResponse200, urlFeedbackList} from "../../definitions/feedback/paths/list";

export interface FeedbackRESTServiceInterface
{
    create(request: FeedbackCreateRequest): Observable<FeedbackCreateResponse200>;
    answer(feedbackId: number, request: FeedbackAnswerRequest): Observable<FeedbackAnswerResponse200>;
    delete(feedbackId: number): Observable<FeedbackDeleteResponse200>,
    getById(feedbackId: number): Observable<FeedbackGetByIdResponse200>,
    markAsRead(feedbackId: number): Observable<FeedbackMarkAsReadResponse200>,
    list(request: FeedbackListRequest): Observable<FeedbackListResponse200>,
}

export class FeedbackRESTService implements FeedbackRESTServiceInterface
{
    constructor(private rest: RESTAdapterInterface) {}

    create(request: FeedbackCreateRequest): Observable<FeedbackCreateResponse200> {
        return this.rest.put(urlFeedbackCreate, request);
    }

    answer(feedbackId: number, request: FeedbackAnswerRequest): Observable<FeedbackAnswerResponse200> {
        return this.rest.put(template(urlFeedbackAnswer, { feedbackId: feedbackId }), request);
    }

    delete(feedbackId: number): Observable<FeedbackDeleteResponse200> {
        return this.rest.delete(template(urlFeedbackDelete, { feedbackId: feedbackId }));
    }

    getById(feedbackId: number): Observable<FeedbackGetByIdResponse200> {
        return this.rest.get(template(urlFeedbackGetById, { feedbackId: feedbackId }));
    }

    markAsRead(feedbackId: number): Observable<FeedbackMarkAsReadResponse200> {
        return this.rest.post(template(urlFeedbackMarkAsRead, { feedbackId: feedbackId }));
    }

    list(request: FeedbackListRequest): Observable<FeedbackListResponse200> {
        return this.rest.post(urlFeedbackList, request);
    }
}
import {AttachmentMetadata} from "../AttachmentEntity";
import {OpenGraphEntity} from "../../../opengraph/entity/og";

export interface UnknownAttachmentMetadata extends AttachmentMetadata
{
	og: OpenGraphEntity;
}
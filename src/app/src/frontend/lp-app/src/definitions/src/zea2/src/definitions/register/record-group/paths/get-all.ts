import {Success200} from "../../../_common/response/Success200";
import {RegisterRecordGroupEntity} from "../entity/RegisterRecordGroup";

export const urlRegisterRecordGroupGetAll = '/backend/api/register/group/all';

export interface RegisterRecordGroupGetAllResponse200 extends Success200
{
    register_record_groups: RegisterRecordGroupEntity[];
}
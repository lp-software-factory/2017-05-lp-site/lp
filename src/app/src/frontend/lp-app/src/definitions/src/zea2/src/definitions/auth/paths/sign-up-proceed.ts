import {Success200} from "../../_common/response/Success200";
import {JWTAuthToken} from "../entity/JWTAuthToken";

export const urlAuthSignUpProceed = '/backend/api/auth/sign-up-proceed/';

export interface SignUpProceedResponse200 extends Success200
{
	token: JWTAuthToken;
}
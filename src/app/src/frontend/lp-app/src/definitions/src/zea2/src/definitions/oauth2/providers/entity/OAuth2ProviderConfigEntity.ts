export interface OAuth2ProviderConfigEntity
{
	clientId: string,
	clientSecret: string,
	redirectUri: string
}
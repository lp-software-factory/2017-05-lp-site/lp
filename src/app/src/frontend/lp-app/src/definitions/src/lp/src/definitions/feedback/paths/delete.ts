import {Success200} from "../../../../../zea2/src/definitions/_common/response/Success200";

export const urlFeedbackDelete = '/backend/api/feedback/{feedbackId}/delete';

export interface FeedbackDeleteResponse200 extends Success200
{
}
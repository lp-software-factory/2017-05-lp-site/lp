import {IdEntity} from "../../_common/markers/IdEntity";
import {SIDEntity} from "../../_common/markers/SIDEntity";
import {ModificationEntity} from "../../_common/markers/ModificationEntity";
import {VersionEntity} from "../../_common/markers/VersionEntity";
import {LocalizedString} from "../../locale/definitions/LocalizedString";

export interface TranslationEntity extends IdEntity, SIDEntity, ModificationEntity, VersionEntity
{
    project_id: number,
    key: string,
    value: LocalizedString[],
}
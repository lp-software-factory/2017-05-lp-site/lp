import {Observable} from "rxjs/Rx";
import {TranslationClearResponse200, urlTranslationClear} from "../../definitions/translation/paths/clear";
import {TranslationGetResponse200, urlTranslationGet} from "../../definitions/translation/paths/get";
import {TranslationSetRequest, TranslationSetResponse200, urlTranslationSet} from "../../definitions/translation/paths/set";
import {TranslationListResponse200, urlTranslationList} from "../../definitions/translation/paths/list";
import {TranslationListRegionResponse200} from "../../definitions/translation/paths/list-region";
import {RESTAdapterInterface} from "../../essentials/RESTAdapterInterface";
import {template} from "../../functions/template";

export interface TranslationRESTServiceInterface
{
    clear(projectId: number, key: string): Observable<TranslationClearResponse200>;
    get(projectId: number, key: string): Observable<TranslationGetResponse200>;
    set(projectId: number, key: string, request: TranslationSetRequest): Observable<TranslationSetResponse200>;
    list(projectId: number): Observable<TranslationListResponse200>;
    listRegion(projectId: number, region: string): Observable<TranslationListRegionResponse200>;
}

export class TranslationRESTService implements TranslationRESTServiceInterface
{
    constructor(private rest: RESTAdapterInterface) {}

    clear(projectId: number, key: string): Observable<TranslationClearResponse200> {
        return this.rest.delete(template(urlTranslationClear, {
            projectId: projectId,
        }), { search: { key: key } });
    }

    get(projectId: number, key: string): Observable<TranslationGetResponse200> {
        return this.rest.get(template(urlTranslationGet, {
            projectId: projectId,
        }), { search: { key: key } });
    }

    set(projectId: number, key: string, request: TranslationSetRequest): Observable<TranslationSetResponse200> {
        return this.rest.post(template(urlTranslationSet, {
            projectId: projectId,
        }), request, { search: { key: key } });
    }

    list(projectId: number): Observable<TranslationListResponse200> {
        return this.rest.get(template(urlTranslationList, {
            projectId: projectId,
        }));
    }

    listRegion(projectId: number, region: string): Observable<TranslationListRegionResponse200> {
        return this.rest.get(template(urlTranslationList, {
            projectId: projectId,
            region: region,
        }));
    }
}

import {Success200} from "../../../../../zea2/src/definitions/_common/response/Success200";
import {Feedback} from "../entity/Feedback";

export const urlFeedbackCreate = '/backend/api/feedback/create';

export interface FeedbackCreateRequest
{
    reference: string,
    from: {
        name: string,
        email: string,
        phone: string,
    },
    message: string,
    message_attachment_ids: number[],
}

export interface FeedbackCreateResponse200 extends Success200
{
    feedback: Feedback<any>,
}
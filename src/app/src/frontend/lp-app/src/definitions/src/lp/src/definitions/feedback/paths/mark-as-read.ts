import {Success200} from "../../../../../zea2/src/definitions/_common/response/Success200";
import {Feedback} from "../entity/Feedback";

export const urlFeedbackMarkAsRead = '/backend/api/feedback/{feedbackId}/mark-as-read';

export interface FeedbackMarkAsReadResponse200 extends Success200
{
    feedback: Feedback<any>,
}
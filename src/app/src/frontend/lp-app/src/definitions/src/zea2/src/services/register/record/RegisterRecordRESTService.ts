import {Observable} from "rxjs";

import {template} from "../../../functions/template";
import {RESTAdapterInterface} from "../../../essentials/RESTAdapterInterface";
import {RegisterRecordCreateRequest, RegisterRecordCreateResponse200, urlRegisterRecordCreate} from "../../../definitions/register/record/paths/create";
import {RegisterRecordEditRequest, RegisterRecordEditResponse200, urlRegisterRecordEdit} from "../../../definitions/register/record/paths/edit";
import {RegisterRecordDeleteResponse200, urlRegisterRecordDelete} from "../../../definitions/register/record/paths/delete";
import {RegisterRecordGetByIdResponse200, urlRegisterRecordGetById} from "../../../definitions/register/record/paths/get-by-id";
import {RegisterRecordGetAllResponse200, urlRegisterRecordGetAll} from "../../../definitions/register/record/paths/get-all";

export interface RegisterRecordRESTServiceInterface
{
    create(request: RegisterRecordCreateRequest): Observable<RegisterRecordCreateResponse200>;
    edit(recordId: number, request: RegisterRecordEditRequest): Observable<RegisterRecordEditResponse200>;
    delete(recordId: number): Observable<RegisterRecordDeleteResponse200>;
    get(recordId: number): Observable<RegisterRecordGetByIdResponse200>;
    all(): Observable<RegisterRecordGetAllResponse200>;
}

export class RegisterRecordRESTService implements RegisterRecordRESTServiceInterface
{
    constructor(private rest: RESTAdapterInterface) {
    }

    create(request: RegisterRecordCreateRequest): Observable<RegisterRecordCreateResponse200> {
        return this.rest.put(urlRegisterRecordCreate, request);
    }

    edit(recordId: number, request: RegisterRecordEditRequest): Observable<RegisterRecordEditResponse200> {
        return this.rest.post(template(urlRegisterRecordEdit, {
            recordId: recordId,
        }), request);
    }

    delete(id: number): Observable<RegisterRecordDeleteResponse200> {
        return this.rest.delete(template(urlRegisterRecordDelete, {
            id: id,
        }));
    }

    get(id: number): Observable<RegisterRecordGetByIdResponse200> {
        return this.rest.get(template(urlRegisterRecordGetById, {
            id: id
        }));
    }

    all(): Observable<RegisterRecordGetAllResponse200> {
        return this.rest.get(urlRegisterRecordGetAll);
    }
}
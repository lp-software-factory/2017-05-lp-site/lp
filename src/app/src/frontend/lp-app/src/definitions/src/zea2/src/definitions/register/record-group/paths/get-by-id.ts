import {Success200} from "../../../_common/response/Success200";
import {RegisterRecordGroupEntity} from "../entity/RegisterRecordGroup";

export const urlRegisterRecordGroupGetById = '/backend/api/register/group/{recordGroupId}/get';

export interface RegisterRecordGroupGetByIdResponse200 extends Success200
{
    register_record_group: RegisterRecordGroupEntity;
}
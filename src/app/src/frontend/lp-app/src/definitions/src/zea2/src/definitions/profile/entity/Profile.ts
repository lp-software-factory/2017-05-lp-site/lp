import {ImageEntityCollection} from "../../avatar/entity/ImageEntityCollection";
import {IdEntity} from "../../_common/markers/IdEntity";

export interface ProfileEntity extends IdEntity
{
	account_id: number;
	date_created_at: string;
	first_name: string;
	last_name: string;
	middle_name: string;
	email: string;
	image: ImageEntityCollection
	last_updated_on: string;
	sid: string
}

export interface Profile
{
	entity: ProfileEntity;
}
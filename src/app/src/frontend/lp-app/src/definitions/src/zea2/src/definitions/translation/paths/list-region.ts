import {Success200} from "../../_common/response/Success200";
import {TranslationByRegion} from "../entity/TranslationByRegion";

export const urlTranslationListRegion = '/backend/api/translation/{projectId}/list-region/{region}';

export interface TranslationListRegionResponse200 extends Success200
{
    translations: TranslationByRegion[],
}

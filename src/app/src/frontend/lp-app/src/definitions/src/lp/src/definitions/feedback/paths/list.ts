import {Success200} from "../../../../../zea2/src/definitions/_common/response/Success200";
import {Feedback} from "../entity/Feedback";
import {SeekCriteria} from "../../../../../zea2/src/definitions/_common/criteria/SeekCriteria";

export const urlFeedbackList = '/backend/api/feedback/list';

export interface FeedbackListRequest
{
    seek: SeekCriteria,
}

export interface FeedbackListResponse200 extends Success200
{
    entities: Feedback<any>[],
}
import {ZEA2Frontend} from '../../../../../zea2/src/definitions/frontend/entity/ZEA2Frontend';
import {LpContacts, LpLegal} from "../../contacts/entity/LpContacts";

export interface LpAppFrontend extends ZEA2Frontend
{
    lp: {
        contacts: LpContacts,
        legal: LpLegal,
    },
}

import {ImageEntity} from "./ImageEntity";

export interface ImageEntityCollection
{
	uid?: string;
	is_auto_generated: boolean;
	variants: { [size: string]: ImageEntity };
}
import {AccountRESTService} from './account/AccountRESTService';
import {AttachmentRESTService} from './attachment/AttachmentRESTService';
import {AuthRESTService} from './auth/AuthRESTService';
import {LocaleRESTService} from './locale/LocaleRESTService';
import {OAuth2ProvidersRESTService} from './oauth2-providers/OAuth2ProvidersRESTService';
import {TranslationRESTService} from './translation/TranslationRESTService';
import {RegisterRecordGroupRESTService} from './register/record-group/RecordGroupRESTService';
import {RegisterRecordRESTService} from './register/record/RegisterRecordRESTService';

export const LIST_SERVICES = [
    AccountRESTService,
    AttachmentRESTService,
    AuthRESTService,
    LocaleRESTService,
    OAuth2ProvidersRESTService,
    TranslationRESTService,
    RegisterRecordGroupRESTService,
    RegisterRecordRESTService,
];
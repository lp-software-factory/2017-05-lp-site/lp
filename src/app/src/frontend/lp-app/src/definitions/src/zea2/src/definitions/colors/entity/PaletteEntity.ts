import {ColorEntity} from "./ColorEntity";

export interface PaletteEntity
{
	code: string;
	background: ColorEntity;
	foreground: ColorEntity;
	border: ColorEntity;
}
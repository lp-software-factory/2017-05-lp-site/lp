import {Success200} from "../../../_common/response/Success200";
import {AttachmentEntity, AttachmentMetadata} from "../AttachmentEntity";

export const urlAttachmentLink = '/backend/api/attachment/link/';

export interface LinkAttachmentResponse200 extends Success200
{
	entity: AttachmentEntity<AttachmentMetadata>;
}
import {Success200} from "../../_common/response/Success200";

export const urlAuthSignUp = "/backend/api/auth/sign-up";

export interface SignUpRequest
{
	email: string
	password: string;
	first_name: string;
	last_name: string;
	phone: {
		has: boolean;
		value?: string;
	},
}

export interface SignUpResponse200 extends Success200
{
}
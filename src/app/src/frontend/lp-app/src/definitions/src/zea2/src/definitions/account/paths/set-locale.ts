import {Success200} from "../../_common/response/Success200";
import {LocaleEntity} from "../../locale/definitions/LocaleEntity";

export const urlSetLocale = '/backend/api/account/set-locale/{region}';

export interface SetLocaleResponse200 extends Success200
{
	locale: LocaleEntity;
}
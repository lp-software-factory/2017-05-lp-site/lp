import {Success200} from "../../_common/response/Success200";
import {TranslationEntity} from "../entity/Translation";

export const urlTranslationList = '/backend/api/translation/{projectId}/list';

export interface TranslationListResponse200 extends Success200
{
    translations: TranslationEntity[],
}

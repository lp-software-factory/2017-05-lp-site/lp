import {Success200} from "../../_common/response/Success200";

export const urlDeleteLocale = "/backend/api/locale/{id}/delete";

export interface DeleteLocaleResponse200 extends Success200
{
}
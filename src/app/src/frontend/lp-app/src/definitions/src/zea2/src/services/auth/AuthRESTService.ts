import {Observable} from "rxjs";

import {CurrentResponse200, urlAuthCurrent} from "../../definitions/auth/paths/current";
import {SignInRequest, SignInResponse200, urlAuthSignIn} from "../../definitions/auth/paths/sign-in";
import {SignUpRequest, SignUpResponse200, urlAuthSignUp} from "../../definitions/auth/paths/sign-up";
import {SignUpProceedResponse200, urlAuthSignUpProceed} from "../../definitions/auth/paths/sign-up-proceed";
import {RESTAdapterInterface} from "../../essentials/RESTAdapterInterface";
import {SignOutResponse200, urlAuthSignOut} from "../../definitions/auth/paths/sign-out";

export interface AuthRESTAdapterInterface
{
	current(): Observable<CurrentResponse200>;
	signIn(request: SignInRequest): Observable<SignInResponse200>;
	signUp(request: SignUpRequest): Observable<SignUpResponse200>;
	signUpProceed(email: string, token: string): Observable<SignUpProceedResponse200>;
	signOut(): Observable<SignOutResponse200>;
}

export class AuthRESTService implements AuthRESTAdapterInterface
{
	constructor(private rest: RESTAdapterInterface) {
	}

	current(): Observable<CurrentResponse200> {
		return this.rest.get(urlAuthCurrent);
	}

	signIn(request: SignInRequest): Observable<SignInResponse200> {
		return this.rest.post(urlAuthSignIn, request, {
			noAuth: true,
		});
	}

	signUp(request: SignUpRequest): Observable<SignUpResponse200> {
		return this.rest.post(urlAuthSignUp, request, {
			noAuth: true,
		});
	}

	signUpProceed(email: string, token: string): Observable<SignUpProceedResponse200> {
		return this.rest.get(urlAuthSignUpProceed, {search: {
			email: email,
			token: token,
		}});
	}

	signOut(): Observable<SignOutResponse200> {
		return this.rest.delete(urlAuthSignOut);
	}
}
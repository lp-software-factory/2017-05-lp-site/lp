import {AttachmentMetadata} from "../AttachmentEntity";
import {OpenGraphEntity} from "../../../opengraph/entity/og";

export interface PageAttachmentMetadata extends AttachmentMetadata
{
	og: OpenGraphEntity;
}
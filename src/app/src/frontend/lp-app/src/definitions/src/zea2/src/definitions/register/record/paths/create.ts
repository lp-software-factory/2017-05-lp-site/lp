import {Success200} from "../../../_common/response/Success200";
import {RegisterRecordEntity} from "../entity/RegisterRecord";

export const urlRegisterRecordCreate = '/backend/api/register/record/create';

export interface RegisterRecordCreateRequest
{
    register_record_group_id: number,
    key: string,
    value: string,
    options: {
        exports_to_frontend: boolean,
    }
}

export interface RegisterRecordCreateResponse200 extends Success200
{
    register_record: RegisterRecordEntity;
}
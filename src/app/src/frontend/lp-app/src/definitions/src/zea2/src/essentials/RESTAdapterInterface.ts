import {Observable} from "rxjs";

export interface RESTAdapterInterfaceOptions
{
    noAuth?: boolean;
    search?: RESTAdapterInterfaceSearch;
    headers?: RESTAdapterInterfaceHeader;
}

export interface RESTAdapterInterfaceHeader { [key: string]: string }
export interface RESTAdapterInterfaceSearch { [key: string]: string }

export interface RESTAdapterInterface
{
    get(url: string, options?: RESTAdapterInterfaceOptions): Observable<any>;
    put(url: string, json?: any, options?: RESTAdapterInterfaceOptions): Observable<any>;
    post(url: string, json?: any, options?: RESTAdapterInterfaceOptions): Observable<any>;
    delete(url: string, options?: RESTAdapterInterfaceOptions): Observable<any>;
}
import {Success200} from "../../../_common/response/Success200";
import {RegisterRecordGroupEntity} from "../entity/RegisterRecordGroup";

export const urlRegisterRecordGroupCreate = '/backend/api/register/group/create';

export interface RegisterRecordGroupCreateRequest
{
    app_name: string;
}

export interface RegisterRecordGroupCreateResponse200 extends Success200
{
    register_record_group: RegisterRecordGroupEntity;
}
import {Success200} from "../../../_common/response/Success200";
import {RegisterRecordEntity} from "../entity/RegisterRecord";

export const urlRegisterRecordGetAll = '/backend/api/register/record/all';

export interface RegisterRecordGetAllResponse200 extends Success200
{
    register_records: RegisterRecordEntity[];
}
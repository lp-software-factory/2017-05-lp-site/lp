import {trigger, state, animate, style, transition, group, keyframes} from '@angular/animations';

export function slideToLeft() {
    return trigger('routeTransition', [
        state('void', style({position:'fixed', "overflow-y": 'auto', height: '100%', width:'100%'}) ),
        state('*', style({}) ),
        transition(':enter', [
            style({transform: 'translateX(100%)'}),
            animate('0.3s ease-in-out', style({transform: 'translateX(0%)'}))
        ]),
        /*transition(':leave', [
            style({transform: 'translateX(0%)'}),
            animate('0.5s ease-in-out', style({transform: 'translateX(-100%)'}))
        ])*/
    ]);
}

export function slideToRight() {
    return trigger('routeTransition', [
        state('void', style({position:'fixed', "overflow-y": 'auto', height: '100%', width:'100%'}) ),
        state('*', style({}) ),
        transition(':enter', [
            style({transform: 'translateX(-100%)'}),
            animate('0.3s ease-in-out', style({transform: 'translateX(0%)'}))
        ]),
        /*transition(':leave', [
            style({transform: 'translateX(0%)'}),
            animate('0.5s ease-in-out', style({transform: 'translateX(100%)'}))
        ])*/
    ]);
}

//ToDo: summon lulu for help
export function animateModal() {
    return trigger('modalState', [
        state('active', style({}) ),
        state('inactive', style({}) ),
        transition('inactive => active', [
            style({transform: 'translateY(0%)'}),
            animate('0.3s ease-in-out', style({transform: 'translateY(50%)'}))
        ]),
        transition('active => inactive', [
            style({transform: 'translateY(50%)'}),
            animate('0.3s ease-in-out', style({transform: 'translateY(0%)'}))
        ])
    ]);
}

import {NgModule} from "@angular/core";

import {
    factoryAccountRESTService, factoryAttachmentRESTService,
    factoryAuthRESTService, factoryLocaleRESTService, factoryOAuth2ProvidersRESTService, factoryTranslationRESTService
} from "./_module/app.rest.services";

import {JWTService} from "./_module/auth/service/JWTService";
import {RESTAdapter} from "./_module/common/service/RESTAdapter";
import {LocaleService} from "./_module/locale/service/LocaleService";
import {LocalizePipe} from "./_module/locale/pipe/LocalizePipe";
import {FrontendService} from "./_module/frontend/FrontendService";
import {TranslateService} from "@ngx-translate/core";

import {ZEA2Frontend} from "../../../definitions/src/zea2/src/definitions/frontend/entity/ZEA2Frontend";

import {AccountRESTService} from "../../../definitions/src/zea2/src/services/account/AccountRESTService";
import {AttachmentRESTService} from "../../../definitions/src/zea2/src/services/attachment/AttachmentRESTService";
import {AuthRESTService} from "../../../definitions/src/zea2/src/services/auth/AuthRESTService";
import {LocaleRESTService} from "../../../definitions/src/zea2/src/services/locale/LocaleRESTService";
import {OAuth2ProvidersRESTService} from "../../../definitions/src/zea2/src/services/oauth2-providers/OAuth2ProvidersRESTService";
import {TranslationRESTService} from "../../../definitions/src/zea2/src/services/translation/TranslationRESTService";
import {UIScrollService} from "./_module/ui/service/UIScrollService";
import {UITitleService} from "./_module/ui/service/UITitleService";

@NgModule({
    declarations: [
        LocalizePipe,
    ],
    exports: [
        LocalizePipe,
    ],
    providers: [
        JWTService,
        RESTAdapter,

        // Locale
        LocaleService,

        // UI
        UIScrollService,
        UITitleService,

        // REST Services
        RESTAdapter,

        {
            provide: AccountRESTService,
            useFactory: factoryAccountRESTService,
            deps: [RESTAdapter],
        },
        {
            provide: AttachmentRESTService,
            useFactory: factoryAttachmentRESTService,
            deps: [RESTAdapter],
        },
        {
            provide: AuthRESTService,
            useFactory: factoryAuthRESTService,
            deps: [RESTAdapter],
        },
        {
            provide: LocaleRESTService,
            useFactory: factoryLocaleRESTService,
            deps: [RESTAdapter],
        },
        {
            provide: OAuth2ProvidersRESTService,
            useFactory: factoryOAuth2ProvidersRESTService,
            deps: [RESTAdapter],
        },
        {
            provide: TranslationRESTService,
            useFactory: factoryTranslationRESTService,
            deps: [RESTAdapter],
        },
    ],
})
export class ZEA2CommonModule
{
    constructor(
        private frontend: FrontendService<any>,
        private translate: TranslateService,
        private locale: LocaleService,
        private auth: JWTService,
    ) {
        this.initFrontend();
        this.initLocale();
        this.initTranslations();
        this.initAuth();
    }

    private initFrontend(): void {
        if (window && window['app-frontend']) {
            this.frontend.emit(window['app-frontend']);
        }
    }

    private initLocale(): void {
        this.frontend.replay.subscribe((next: ZEA2Frontend) => {
            this.locale.current = next.locale.current;
            this.locale.locales = next.locale.available;
        });
    }

    private initTranslations(): void {
        this.translate.setDefaultLang('en_US');

        this.frontend.replay.subscribe(next => {
            if (next.translations) {
                this.translate.getTranslation(next.translations.region);
                this.translate.use(next.translations.region);
            }
        });
    }

    private initAuth(): void {
        this.frontend.replay.subscribe(next => {
            if (next.auth && next.auth.jwt) {
                this.auth.setJWT(next.auth);
            }
        });
    }
}
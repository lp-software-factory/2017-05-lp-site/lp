import {Injectable} from "@angular/core";

import {LocaleEntity} from "../../../../../../definitions/src/zea2/src/definitions/locale/definitions/LocaleEntity";
import {LocalizedString} from "../../../../../../definitions/src/zea2/src/definitions/locale/definitions/LocalizedString";

@Injectable()
export class LocaleService
{
    private fallback: string[] = ['en_US', 'en_GB'];

    public current: LocaleEntity;
    public locales: LocaleEntity[] = [];

    getLocaleByRegion(region: string): LocaleEntity {
        let result = this.locales.filter(compare => compare.region === region);

        if(result.length === 0) {
            throw new Error(`Locale with region "${region}" not found`);
        }

        return result[0];
    }

    public getLocalization(input: LocalizedString[]): LocalizedString {
        if(input.length === 0) {
            return {
                region: 'en_GB',
                value: ''
            };
        }
        for(let i = 0; i < input.length; i++) {
            if(input[i].region === this.current.region)
                return input[i];
        }


        for(let fallback of this.fallback) {
            if(input.hasOwnProperty(fallback)) {
                return input[fallback];
            }
        }
        return input[0];
    }
}

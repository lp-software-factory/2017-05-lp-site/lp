import {Injectable} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {Title} from "@angular/platform-browser";
import {Observable, Subscription} from "rxjs";

export interface UITitleSegment
{
    title: string,
    translate?: boolean,
}

@Injectable()
export class UITitleService
{
    public static DELIMITER = ' / ';
    public static DEFAULT_PREFIX = '[ZEA2]';

    private lastTranslateSubscription: Subscription;
    private lastTranslatePrefixSubscription: Subscription;

    private prefix: string = UITitleService.DEFAULT_PREFIX;
    private current: UITitleSegment[] = [];

    constructor(
        private translate: TranslateService,
        private browserTitle: Title,
    ) {}

    private update(): void {
        let result: string[] = [];

        if(this.prefix && this.prefix.length) {
            result.push(this.prefix);
        }

        for(let segment of this.current) {
            result.push(segment.title);
        }

        this.browserTitle.setTitle(result.join(UITitleService.DELIMITER));
    }

    public usePrefix(prefix: string, options: { translate?: boolean }): void {
        this.prefix = prefix;

        if(this.lastTranslateSubscription) {
            this.lastTranslateSubscription.unsubscribe();
        }

        if (options.translate) {
            this.lastTranslatePrefixSubscription = this.translate.get(prefix).subscribe(n => this.prefix = n);
        }

        this.update();
    }

    public setTitle(segments: UITitleSegment[]): void {
        segments = JSON.parse(JSON.stringify(segments));

        if(this.lastTranslateSubscription) {
            this.lastTranslateSubscription.unsubscribe();
        }

        let toTranslate = segments.filter(s => s.translate);

        if(toTranslate.length > 0) {
            this.lastTranslateSubscription = this.translate.get(toTranslate.map(s => s.title)).subscribe(next => {
                for(let key in next) {
                    if(next.hasOwnProperty(key)) {
                        segments.filter(s => s.title === key)[0].title = next[key];
                    }
                }

                this.current = segments;
                this.update();
            });
        }else{
            this.current = segments;
            this.update();
        }

        this.update();
    }
}

import {Injectable} from "@angular/core";
import {Subject} from "rxjs";

@Injectable()
export class UIScrollService
{
    public onScrollTopSubject: Subject<void> = new Subject<void>();
    public onScrollBottomSubject: Subject<void> = new Subject<void>();

    scrollTop(): void {
        this.onScrollTopSubject.next(undefined);
    }

    scrollBottom(): void {
        this.onScrollBottomSubject.next(undefined);
    }
}
export function randomString(length: number, possible: string = "abcdef0123456789") {
    let text = "";

    for(let i = 0; i < length; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}
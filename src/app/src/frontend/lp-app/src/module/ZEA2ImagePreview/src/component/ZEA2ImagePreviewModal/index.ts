import {Component, Input, Output, EventEmitter, HostListener} from "@angular/core";
import {Subscription, Subject} from "rxjs";

export interface ZEA2ImagePreviewModalQuery
{
    current: string,
    images: string[],
}

@Component({
    selector: 'zea2-image-preview',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss'
    ],
})
export class ZEA2ImagePreviewModal
{
    @Input('subject') subject: Subject<ZEA2ImagePreviewModalQuery>;

    @Output('close') closeEvent: EventEmitter<ZEA2ImagePreviewModalQuery> = new EventEmitter<ZEA2ImagePreviewModalQuery>();

    private subscription: Subscription;

    public visible: boolean = false;

    private lastQuery: ZEA2ImagePreviewModalQuery;

    public images: string[] = [];
    private current: string;

    ngOnInit(): void {
        this.subscription = this.subject.subscribe(next => {
            this.visible = true;

            this.images = next.images;
            this.current = next.current;

            this.lastQuery = next;
        });
    }

    ngOnDestroy(): void {
        if(this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    @HostListener('document:keyup', ['$event']) keyUp($event: KeyboardEvent) {
        if($event.key === 'ArrowLeft') {
            if(this.hasPrev()) {
                this.prev();
            }
        }else if($event.key === 'ArrowRight') {
            if(this.hasNext()) {
                this.next();
            }
        }else if($event.key === 'Escape') {
            this.close();
        }
    }

    close(): void {
        this.visible = false;
        this.closeEvent.emit(this.lastQuery);
    }

    cursor(): number {
        for(let i = 0; i < this.images.length; i++) {
            if(this.images.hasOwnProperty(i) && this.images[i] === this.current) {
                return i;
            }
        }

        return 0;
    }

    length(): number {
        return this.images.length;
    }

    hasNext(): boolean {
        return (this.cursor() + 1) < this.length();
    }

    next(): void {
        this.current = this.images[this.cursor() + 1];
    }

    hasPrev(): boolean {
        return this.cursor() > 0;
    }

    prev(): void {
        if(this.hasPrev()) {
            this.current = this.images[this.cursor() - 1];
        }
    }

    isCurrent(input: string): boolean {
        return this.current === input;
    }
}
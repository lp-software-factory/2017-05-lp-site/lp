import {NgModule} from "@angular/core";

import {ZEA2ImagePreviewModal} from "./component/ZEA2ImagePreviewModal/index";
import {CommonModule} from "@angular/common";

@NgModule({
    imports: [
        CommonModule,
    ],
    declarations: [
        ZEA2ImagePreviewModal,
    ],
    exports: [
        ZEA2ImagePreviewModal,
    ],
})
export class ZEA2ImagePreviewModule {}
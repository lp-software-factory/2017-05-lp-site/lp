import {Component} from "@angular/core";
import {Subscription} from "rxjs";

import {AppFrontendService} from "../../../../../frontend/service/AppFrontendService";

@Component({
    selector: 'lp-landing-home-contact-now',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class LandingHomeContactNow
{
    public phone: string;
    public email: string;

    private subscription: Subscription;

    constructor(
        private frontend: AppFrontendService,
    ) {}

    ngOnInit(): void {
        this.subscription = this.frontend.replay.subscribe(next => {
            this.phone = next.lp.contacts.phone.main;
            this.email = next.lp.contacts.email.main;
        });
    }

    ngOnDestroy(): void {
        if(this.subscription) {
            this.subscription.unsubscribe();
        }
    }
}
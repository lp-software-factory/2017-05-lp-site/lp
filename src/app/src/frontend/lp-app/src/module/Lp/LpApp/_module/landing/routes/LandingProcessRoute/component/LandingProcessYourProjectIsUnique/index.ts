import {Component} from "@angular/core";

@Component({
    selector: 'lp-landing-process-your-project-is-unique',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class LandingProcessYourProjectIsUnique
{}
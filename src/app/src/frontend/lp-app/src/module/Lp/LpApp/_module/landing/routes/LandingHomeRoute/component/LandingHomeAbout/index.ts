import {Component} from "@angular/core";

@Component({
    selector: 'lp-landing-home-about',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ],
})
export class LandingHomeAbout
{}
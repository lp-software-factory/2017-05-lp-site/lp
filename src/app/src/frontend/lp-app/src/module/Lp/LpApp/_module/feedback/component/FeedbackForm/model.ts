import {Injectable} from "@angular/core";
import {FeedbackCreateRequest} from "../../../../../../../definitions/src/lp/src/definitions/feedback/paths/create";


export enum FeedbackFormStateENUM
{
    Form = <any>"form",
    Fail = <any>"fail",
    Success = <any>"success",
}

@Injectable()
export class FeedbackFormModel
{
    public state: FeedbackFormStateENUM = FeedbackFormStateENUM.Form;

    public static DEFAULT_REFERENCE: string = 'global';

    public reference: string;
    public name: string;
    public phone: string;
    public email: string;
    public message: string;

    constructor() {
        this.reset();
    }

    createFeedbackRequest(): FeedbackCreateRequest {
        return {
            reference: this.reference,
            from: {
                name: this.name,
                email: this.email,
                phone: this.phone,
            },
            message: this.message,
            message_attachment_ids: [],
        };
    }

    reset() {
        this.reference = FeedbackFormModel.DEFAULT_REFERENCE;
        this.name = '';
        this.phone = '';
        this.email = '';
        this.message = '';
    }
}
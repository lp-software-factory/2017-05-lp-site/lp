import {Injectable} from "@angular/core";

import {ModalControl} from "../../../../../../ZEA2Common/src/_module/common/classes/ModalControl";

export enum UIAlertModalQueryLevelENUM
{
    Default = <any>"default",
    Warning = <any>"is-warning",
    Danger = <any>"is-danger",
    Success = <any>"is-success",
}

export interface UIAlertString
{
    value: string,
    translate: boolean,
}

export interface UIAlertModalQuery
{
    title: UIAlertString,
    message: UIAlertString,
    level: UIAlertModalQueryLevelENUM,
}

@Injectable()
export class UIAlertModalService
{
    public controls: ModalControl<UIAlertModalQuery> = new ModalControl<UIAlertModalQuery>();
}
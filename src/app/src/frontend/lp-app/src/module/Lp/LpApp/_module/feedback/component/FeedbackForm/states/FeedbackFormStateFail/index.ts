import {Component, Output, EventEmitter} from "@angular/core";

import {FeedbackFormModel} from "../../model";

@Component({
    selector: 'lp-feedback-form-state-fail',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class FeedbackFormStateFail
{
    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    constructor(
        public model: FeedbackFormModel,
    ) {}

    repeat() {

    }

    back() {

    }

    close() {
        this.closeEvent.emit(undefined);
    }
}
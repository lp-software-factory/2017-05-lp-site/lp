import {trigger, state, animate, style, transition} from '@angular/animations';

export function routerTransition() {
    return slideToLeft();
}

export function slideToRight() {
    return trigger('routerTransition', [
        state('void', style({position:'absolute', width:'100%'}) ),
        state('*', style({position:'absolute', width:'100%'}) ),
        transition(':enter', [
            style({transform: 'translateX(-100%)'}),
            animate('0.2s ease-in-out', style({transform: 'translateX(0%)'}))
        ]),
        transition(':leave', [
            style({transform: 'translateX(0%)'}),
            animate('0.2s ease-in-out', style({transform: 'translateX(100%)'}))
        ])
    ]);
}

export function slideToLeft() {
    return trigger('routerTransition', [
        state('void', style({position:'relative', width:'100%'}) ),
        state('*', style({position:'relative', width:'100%'}) ),
        transition(':enter', [
            style({left: '100%'}),
            animate('0.2s ease-in-out', style({left: '0%'}))
        ]),
        transition(':leave', [
            style({left: '0%'}),
            animate('0.2s ease-in-out', style({left: '-100%'}))
        ])
    ]);
}

export function slideToBottom() {
    return trigger('routerTransition', [
        state('void', style({position:'absolute', width:'100%'}) ),
        state('*', style({position:'absolute', width:'100%'}) ),
        transition(':enter', [
            style({transform: 'translateY(-100%)'}),
            animate('0.2s ease-in-out', style({transform: 'translateY(0%)'}))
        ]),
        transition(':leave', [
            style({transform: 'translateY(0%)'}),
            animate('0.2s ease-in-out', style({transform: 'translateY(100%)'}))
        ])
    ]);
}

export function slideToTop() {
    return trigger('routerTransition', [
        state('void', style({position:'absolute', width:'100%'}) ),
        state('*', style({position:'absolute', width:'100%'}) ),
        transition(':enter', [
            style({transform: 'translateY(100%)'}),
            animate('0.2s ease-in-out', style({transform: 'translateY(0%)'}))
        ]),
        transition(':leave', [
            style({transform: 'translateY(0%)'}),
            animate('0.2s ease-in-out', style({transform: 'translateY(-100%)'}))
        ])
    ]);
}
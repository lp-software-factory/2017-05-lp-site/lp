import {Component, Input} from "@angular/core";
import {Subscription} from "rxjs";
import {TranslateService} from "@ngx-translate/core";

@Component({
    selector: 'lp-landing-title',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class LandingTitle
{
    @Input('value') value: string;
    @Input('animate') animate: boolean = false;

    public cursor: string = '_';
    public message: string = '';

    private cursorTimer: any;
    private subscription: Subscription;

    constructor(
        private translate: TranslateService,
    ) {}

    ngOnChanges(): void {
        if(this.subscription) {
            this.subscription.unsubscribe();
        }

        if(this.cursorTimer) {
            clearInterval(this.cursorTimer);
        }

        this.subscription = this.translate.get(this.value).subscribe(next => {
            if(this.animate) {
                setTimeout(() => {
                    this.typeMessage(next + ' ');
                }, 500);
            }else{
                this.message = next;
                this.cursor = '';
            }
        });

        if(this.animate) {
            this.cursorTimer = setInterval(() => {
                this.cursor = this.cursor === '_' ? ' ' : '_';
            }, 800);
        }
    }

    ngOnDestroy(): void {
        if(this.subscription) {
            this.subscription.unsubscribe();
        }

        if(this.cursorTimer) {
            clearInterval(this.cursorTimer);
        }
    }

    typeMessage(input: string, typed: number = 0): void {
        if(typed <= input.length) {
            let delay = 30;

            if(Math.random() < 0.2) {
                delay = 250;
            }

            setTimeout(() => {
                this.message = input.slice(0, typed);
                this.typeMessage(input, typed + 1);
            }, Math.floor(Math.random()*delay));
        }
    }
}
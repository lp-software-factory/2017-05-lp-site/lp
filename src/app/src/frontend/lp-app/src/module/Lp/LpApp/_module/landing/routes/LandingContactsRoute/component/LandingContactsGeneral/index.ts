import {Component} from "@angular/core";
import {Subscription} from "rxjs";

import {AppFrontendService} from "../../../../../frontend/service/AppFrontendService";
import {LpInfo} from "../../../../../../../../../definitions/src/lp/src/definitions/contacts/entity/LpContacts";

@Component({
    selector: 'lp-landing-contacts-general',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class LandingContactsGeneral
{
    private subscription: Subscription;

    public lp: LpInfo;

    constructor(
        private frontend: AppFrontendService,
    ) {}

    ngOnInit(): void {

        this.subscription = this.frontend.last.subscribe(next => {
            this.lp = next.lp;
        });
    }

    ngOnDestroy(): void {
        if(this.subscription) {
            this.subscription.unsubscribe();
        }
    }
}
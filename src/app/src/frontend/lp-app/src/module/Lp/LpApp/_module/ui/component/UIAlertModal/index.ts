import {Component, HostListener} from "@angular/core";
import {Subscription} from "rxjs";

import {UIAlertModalService, UIAlertModalQueryLevelENUM, UIAlertString} from "./service";

@Component({
    selector: 'lp-ui-alert-modal',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class UIAlertModal
{
    private subscription: Subscription;

    public title: UIAlertString;
    public message: UIAlertString;
    public level: UIAlertModalQueryLevelENUM;

    constructor(
        private service: UIAlertModalService,
    ) {}

    ngOnInit(): void {
        this.subscription = this.service.controls.notify.subscribe(next => {
            this.title = next.title;
            this.message = next.message;
            this.level = next.level;
        });
    }

    ngOnDestroy(): void {
        if(this.subscription) {
            this.subscription.unsubscribe();
        }
    }


    @HostListener('document:keyup', ['$event']) keyUp($event: KeyboardEvent) {
        if($event.key === 'Escape') {
            $event.stopPropagation();
            this.close();
        }
    }

    getCSSClasses(): string[] {
        switch(this.level) {
            default:
                return [];

            case UIAlertModalQueryLevelENUM.Warning:
                return ['is-warning'];

            case UIAlertModalQueryLevelENUM.Danger:
                return ['is-danger'];

            case UIAlertModalQueryLevelENUM.Success:
                return ['is-success'];
        }
    }

    isOpened(): boolean {
        return this.service.controls.isOpened();
    }

    close(): void {
        this.service.controls.close();
    }
}
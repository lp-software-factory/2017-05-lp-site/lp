import {Injectable} from "@angular/core";
import {Meta} from '@angular/platform-browser';
import {TranslateService} from "@ngx-translate/core";

import {SITE_HOST_URL} from "../../../../../../app/app.constants";
import {Subscription} from "rxjs";

export interface SetMetaQuery
{
    name: string,
    description: string,
    uri: string,
    image?: string,
}

@Injectable()
export class UILandingMetaService
{
    public static DEFAULT_IMAGE: string = SITE_HOST_URL + '/dist/app/images/landing/meta-image.png';

    private latestTranslate: Subscription;

    constructor(
        private service: Meta,
        private translate: TranslateService,
    ) {}

    public init(): void {
        this.translate.get([
            'lp.m.ui.meta.defaults.title',
            'lp.m.ui.meta.defaults.description',
        ]).subscribe(translations => {
            this.service.addTags([
                { name: 'name', content: translations['lp.m.ui.meta.defaults.title'] },
                { name: 'description', content: translations['lp.m.ui.meta.defaults.description'] },
                { name: 'image', content: UILandingMetaService.DEFAULT_IMAGE },
                { name: 'url', content: SITE_HOST_URL },

                { name: 'twitter:card', content: 'summary' },
                { name: 'twitter:title', content: translations['lp.m.ui.meta.defaults.title'] },
                { name: 'twitter:description', content: translations['lp.m.ui.meta.defaults.description'] },
                { name: 'twitter:image', content: UILandingMetaService.DEFAULT_IMAGE },
                { name: 'twitter:url', content: SITE_HOST_URL },

                { name: 'og:title', content: translations['lp.m.ui.meta.defaults.title'] },
                { name: 'og:description', content: translations['lp.m.ui.meta.defaults.description'] },
                { name: 'og:type', content: 'article' },
                { name: 'og:url', content: SITE_HOST_URL },
                { name: 'og:image', content: UILandingMetaService.DEFAULT_IMAGE },
            ]);
        });
    }

    public setMeta(meta: SetMetaQuery): void {
        if(this.latestTranslate) {
            this.latestTranslate.unsubscribe();
        }

        this.latestTranslate = this.translate.get([meta.name, meta.description]).subscribe(next => {
            let cpMeta: SetMetaQuery;

            cpMeta = JSON.parse(JSON.stringify(meta));
            cpMeta.name = next[meta.name];
            cpMeta.description = next[meta.description];

            this.setGenericMeta(cpMeta);
            this.setTwitterCard(cpMeta);
            this.setOGMeta(cpMeta);
        });
    }

    private setGenericMeta(meta: SetMetaQuery): void {
        this.service.updateTag({ name: 'name', content: meta.name });
        this.service.updateTag({ name: 'description', content: meta.description });
        this.service.updateTag({ name: 'image', content: meta.image || UILandingMetaService.DEFAULT_IMAGE });
        this.service.updateTag({ name: 'url', content: SITE_HOST_URL + '/' + meta.uri });
    }

    private setTwitterCard(meta: SetMetaQuery): void {
        this.service.updateTag({ name: 'twitter:title', content: meta.name });
        this.service.updateTag({ name: 'twitter:description', content: meta.description });
        this.service.updateTag({ name: 'twitter:image', content: meta.image || UILandingMetaService.DEFAULT_IMAGE });
        this.service.updateTag({ name: 'twitter:url', content: SITE_HOST_URL + '/' + meta.uri });
    }

    private setOGMeta(meta: SetMetaQuery): void {
        this.service.updateTag({ name: 'og:title', content: meta.name });
        this.service.updateTag({ name: 'og:description', content: meta.description });
        this.service.updateTag({ name: 'og:image', content: meta.image || UILandingMetaService.DEFAULT_IMAGE });
        this.service.updateTag({ name: 'og:url', content: SITE_HOST_URL + '/' + meta.uri });
    }
}
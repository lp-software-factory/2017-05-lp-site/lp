import {Component} from "@angular/core";

import {LandingScrollService} from "../../service/LandingScrollService";
import {UITitleService} from "../../../../../../ZEA2Common/src/_module/ui/service/UITitleService";
import {routerTransition} from "../../animation/app.routing.animations";
import {UILandingMetaService} from "../../service/LandingMetaService";

@Component({
    host: {'[@routerTransition]': ''},
    animations: [routerTransition()],
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class LandingHomeRoute
{
    constructor(
        private uiTitle: UITitleService,
        private uiScroll: LandingScrollService,
        private uiMeta: UILandingMetaService,
    ) {}

    ngOnInit(): void {
        this.uiTitle.setTitle([]);
        this.uiScroll.scrollTop();

        this.uiMeta.setMeta({
            name: 'lp.m.landing.home.title',
            description: 'lp.m.landing.home.description',
            uri: ''
        });
    }
}
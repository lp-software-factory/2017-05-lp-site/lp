import {Injectable} from '@angular/core';
import {ReplaySubject, BehaviorSubject} from 'rxjs';

import {LpAppFrontend} from "../../../../../../definitions/src/lp/src/definitions/frontend/entity/LpAppFrontend";

@Injectable()
export class AppFrontendService
{
    public last: BehaviorSubject<LpAppFrontend> = new BehaviorSubject<LpAppFrontend>(undefined);
    public replay: ReplaySubject<LpAppFrontend> = new ReplaySubject<LpAppFrontend>();

    emit(next: LpAppFrontend): void {
        this.last.next(next);
        this.replay.next(next);
    }
}

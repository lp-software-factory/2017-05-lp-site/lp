import {Component} from "@angular/core";

@Component({
    selector: 'lp-landing-work-with-us-modern-technologies',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class LandingWorkWithUsModernTechnologies
{}
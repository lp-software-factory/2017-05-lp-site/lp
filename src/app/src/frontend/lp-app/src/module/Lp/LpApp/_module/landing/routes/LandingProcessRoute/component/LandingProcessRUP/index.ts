import {Component} from "@angular/core";

@Component({
    selector: 'lp-landing-process-rup',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class LandingProcessRUP
{}

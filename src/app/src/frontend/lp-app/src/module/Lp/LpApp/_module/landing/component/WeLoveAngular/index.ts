import {Component} from "@angular/core";

@Component({
    selector: 'lp-landing-we-love-angular',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ],
})
export class WeLoveAngular
{}
import {Component, Output, EventEmitter} from "@angular/core";

import {FeedbackFormModel, FeedbackFormStateENUM} from "../../model";
import {FeedbackRESTService} from "../../../../../../../../../definitions/src/lp/src/services/feedback/FeedbackRESTService";
import {Feedback} from "../../../../../../../../../definitions/src/lp/src/definitions/feedback/entity/Feedback";
import {LoadingManager} from "../../../../../../../../ZEA2Common/src/_module/common/classes/LoadingStatus";

@Component({
    selector: 'lp-feedback-form-state-form',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class FeedbackFormStateForm
{
    @Output('success') successEvent: EventEmitter<Feedback<any>> = new EventEmitter<Feedback<any>>();
    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    public status: LoadingManager = new LoadingManager();

    constructor(
        public model: FeedbackFormModel,
        private rest: FeedbackRESTService,
    ) {}

    submit() {
        let loading = this.status.addLoading();

        this.rest.create(this.model.createFeedbackRequest())
            .retry(3)
            .subscribe(
                next => {
                    loading.is = false;

                    this.model.state = FeedbackFormStateENUM.Success;
                    this.successEvent.emit(next.feedback);
                },
                error => {
                    loading.is = false;

                    this.model.state = FeedbackFormStateENUM.Fail;
                }
            )
        ;
    }

    canBeSubmitted(): boolean {
        let hasName = (typeof this.model.name === 'string') && (this.model.name.length > 0);
        let hasEmail = (typeof this.model.email === 'string') && (this.model.email.length > 0);
        let hasPhone = (typeof this.model.phone === 'string') && (this.model.phone.length > 0);
        let hasMessage = (typeof this.model.message === 'string') && (this.model.message.length > 0);

        return hasName && hasPhone && hasEmail && hasMessage;
    }


    close() {
        this.closeEvent.emit(undefined);
    }
}
import {Component} from "@angular/core";

@Component({
    selector: 'lp-landing-contacts',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class LandingContacts
{}
import {Component} from "@angular/core";

@Component({
    selector: 'lp-landing-process-support',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class LandingProcessSupport
{}
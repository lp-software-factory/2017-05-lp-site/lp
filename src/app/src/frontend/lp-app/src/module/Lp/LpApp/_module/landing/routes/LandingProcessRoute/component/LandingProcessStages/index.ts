import {Component} from "@angular/core";

@Component({
    selector: 'lp-landing-process-stages',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class LandingProcessStages
{}
import {Component} from "@angular/core";

@Component({
    selector: 'lp-landing-work-with-us-professionalism',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class LandingWorkWithUsProfessionalism
{}
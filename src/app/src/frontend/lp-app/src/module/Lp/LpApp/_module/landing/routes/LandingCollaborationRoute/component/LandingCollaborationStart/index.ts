import {Component} from "@angular/core";
import {AppFrontendService} from "../../../../../frontend/service/AppFrontendService";
import {Subscription} from "rxjs";

@Component({
    selector: 'lp-landing-collaboration-start',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class LandingCollaborationStart
{
    public phone: string = '';

    private subscription: Subscription;

    constructor(
        private frontend: AppFrontendService,
    ) {}

    ngOnInit(): void {
        this.subscription = this.frontend.last.subscribe(next => {
            this.phone = next.lp.contacts.phone.main;
        });
    }

    ngOnDestroy(): void {
        if(this.subscription) {
            this.subscription.unsubscribe();
        }
    }
}

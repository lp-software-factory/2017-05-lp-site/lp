import {Injectable} from "@angular/core";

@Injectable()
export class LandingScrollService
{
    scrollTop(): void {
        if(document && document.body) {
            document.body.scrollTop = 0;
        }
    }

    scrollPostIntro(): void {
        if(document && document.body && document.getElementsByClassName) {
            if(document.body.clientWidth < 740) {
                document.body.scrollTop = document.getElementsByClassName('js-scroll-top-intro')[0].clientHeight;

                console.log(document.body.scrollTop);
            }else{
                let elemIntro = document.getElementsByClassName('js-scroll-top-intro')[0].clientHeight;
                let elemMenu = document.getElementsByClassName('js-scroll-top-menu')[0].clientHeight;

                document.body.scrollTop = Math.max(0, elemIntro - elemMenu);
            }
        }
    }
}
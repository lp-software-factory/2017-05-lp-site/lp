import {Component} from "@angular/core";

@Component({
    selector: 'lp-landing-home-out-software-is-the-best',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class LandingHomeOurSoftwareIsTheBest
{
}
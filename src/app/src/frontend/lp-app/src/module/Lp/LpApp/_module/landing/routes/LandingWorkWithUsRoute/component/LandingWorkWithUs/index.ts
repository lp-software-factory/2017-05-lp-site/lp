import {Component} from "@angular/core";

@Component({
    selector: 'lp-landing-work-with-us',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ],
})
export class LandingWorkWithUs
{}

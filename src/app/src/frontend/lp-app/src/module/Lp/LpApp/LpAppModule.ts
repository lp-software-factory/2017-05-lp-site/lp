import {NgModule} from '@angular/core';

import {appRoutes} from "../../../app/app.routing";
import {appTranslateConfig} from "../../../app/app.translate";

import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';

import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {BrowserModule} from '@angular/platform-browser';
import {CommonModule} from '@angular/common';
import {HttpModule} from '@angular/http';
import {FormsModule} from "@angular/forms";

import {AppFrontendService} from './_module/frontend/service/AppFrontendService';

import {ZEA2CommonModule} from "../../ZEA2Common/src/ZEA2CommonModule";
import {LpRESTServicesModule} from "../LpRESTServices/LpRESTServicesModule";

import {UINotFound} from './_module/ui/routes/UINotFound/index';
import {UIMainMenu} from './_module/ui/component/UIMainMenu/index';
import {FeedbackForm} from "./_module/feedback/component/FeedbackForm/index";
import {FeedbackFormStateForm} from "./_module/feedback/component/FeedbackForm/states/FeedbackFormStateForm/index";
import {FeedbackFormStateSuccess} from "./_module/feedback/component/FeedbackForm/states/FeedbackFormStateSuccess/index";
import {FeedbackFormStateFail} from "./_module/feedback/component/FeedbackForm/states/FeedbackFormStateFail/index";
import {UIAlertModal} from "./_module/ui/component/UIAlertModal/index";
import {UIAlertModalService} from "./_module/ui/component/UIAlertModal/service";
import {UITitleService} from "../../ZEA2Common/src/_module/ui/service/UITitleService";
import {UIScrollService} from "../../ZEA2Common/src/_module/ui/service/UIScrollService";
import {LandingHomeRoute} from "./_module/landing/routes/LandingHomeRoute/index";
import {LandingContactsRoute} from "./_module/landing/routes/LandingContactsRoute/index";
import {LandingProcessRoute} from "./_module/landing/routes/LandingProcessRoute/index";
import {LandingServicesRoute} from "./_module/landing/routes/LandingServicesRoute/index";
import {LandingTechnologiesRoute} from "./_module/landing/routes/LandingTechnologiesRoute/index";
import {LandingRoute} from "./_module/landing/routes/Landing/index";
import {LandingIntro} from "./_module/landing/component/Intro/index";
import {LandingTitle} from "./_module/landing/component/Title/index";
import {LandingContactsMap} from "./_module/landing/routes/LandingContactsRoute/component/LandingContactsMap/index";
import {LandingContactsGeneral} from "./_module/landing/routes/LandingContactsRoute/component/LandingContactsGeneral/index";
import {LandingContacts} from "./_module/landing/routes/LandingContactsRoute/component/LandingContacts/index";
import {LandingServices} from "./_module/landing/routes/LandingServicesRoute/component/LandingServices/index";
import {LandingHome} from "./_module/landing/routes/LandingHomeRoute/component/LandingHome/index";
import {LandingHomeAbout} from "./_module/landing/routes/LandingHomeRoute/component/LandingHomeAbout/index";
import {LandingHomeSoftwareTypes} from "./_module/landing/routes/LandingHomeRoute/component/LandingHomeSoftwareTypes/index";
import {LandingHomeContactNow} from "./_module/landing/routes/LandingHomeRoute/component/LandingHomeContactNow/index";
import {LandingCollaborationRoute} from "./_module/landing/routes/LandingCollaborationRoute/index";
import {WeLoveAngular} from "./_module/landing/component/WeLoveAngular/index";
import {LandingCollaboration} from "./_module/landing/routes/LandingCollaborationRoute/component/LandingCollaboration/index";
import {LandingCollaborationForms} from "./_module/landing/routes/LandingCollaborationRoute/component/LandingCollaborationForms/index";
import {LandingCollaborationStart} from "./_module/landing/routes/LandingCollaborationRoute/component/LandingCollaborationStart/index";
import {LandingProcess} from "./_module/landing/routes/LandingProcessRoute/component/LandingProcess/index";
import {LandingProcessRUP} from "./_module/landing/routes/LandingProcessRoute/component/LandingProcessRUP/index";
import {LandingWorkWithUsRoute} from "./_module/landing/routes/LandingWorkWithUsRoute/index";
import {LandingWorkWithUs} from "./_module/landing/routes/LandingWorkWithUsRoute/component/LandingWorkWithUs/index";
import {LandingTechnologies} from "./_module/landing/routes/LandingTechnologiesRoute/component/LandingTechnologies/index";
import {LandingScrollService} from "./_module/landing/service/LandingScrollService";
import {UIMainSlideMenu} from "./_module/ui/component/UIMainSlideMenu/index";
import {LandingProcessStages} from "./_module/landing/routes/LandingProcessRoute/component/LandingProcessStages/index";
import {LandingProcessYourProjectIsUnique} from "./_module/landing/routes/LandingProcessRoute/component/LandingProcessYourProjectIsUnique/index";
import {LandingProcessSupport} from "./_module/landing/routes/LandingProcessRoute/component/LandingProcessSupport/index";
import {LandingWorkWithUsModernTechnologies} from "./_module/landing/routes/LandingWorkWithUsRoute/component/LandingWorkWithUsModernTechnologies/index";
import {LandingWorkWithUsProfessionalism} from "./_module/landing/routes/LandingWorkWithUsRoute/component/LandingWorkWithUsProfessionalism/index";
import {LandingWorkWithUsNoStupidHR} from "./_module/landing/routes/LandingWorkWithUsRoute/component/LandingWorkWithUsNoStupidHR/index";
import {UIMobileBottomMenu} from "./_module/ui/component/UIMobileBottomMenu/index";
import {LandingHomeOurSoftwareIsTheBest} from "./_module/landing/routes/LandingHomeRoute/component/LandingHomeOurSoftwareIsTheBest/index";
import {UILandingMetaService} from "./_module/landing/service/LandingMetaService";

@NgModule({
    imports: [
        BrowserAnimationsModule,
        BrowserModule,
        CommonModule,
        HttpModule,
        FormsModule,

        ZEA2CommonModule,
        LpRESTServicesModule,

        RouterModule.forChild(appRoutes),
        TranslateModule.forChild(appTranslateConfig),
    ],
    declarations: [
        // ui
        UINotFound,
        UIMainMenu,
        UIAlertModal,

        // feedback
        FeedbackForm,
        FeedbackFormStateForm,
        FeedbackFormStateSuccess,
        FeedbackFormStateFail,

        // landing
        LandingIntro,
        LandingRoute,
        LandingHome,
        LandingHomeRoute,
        LandingHomeAbout,
        LandingHomeSoftwareTypes,
        LandingHomeContactNow,
        LandingContactsRoute,
        LandingProcessRoute,
        LandingServicesRoute,
        LandingTechnologiesRoute,
        LandingContactsMap,
        LandingTitle,
        LandingContactsGeneral,
        LandingContacts,
        LandingServices,
        LandingCollaborationRoute,
        WeLoveAngular,
        LandingCollaboration,
        LandingCollaborationForms,
        LandingCollaborationStart,
        LandingProcess,
        LandingProcessRUP,
        LandingTechnologies,
        LandingWorkWithUs,
        LandingWorkWithUsRoute,
        UIMainSlideMenu,
        LandingProcessStages,
        LandingProcessYourProjectIsUnique,
        LandingProcessSupport,
        LandingWorkWithUsModernTechnologies,
        LandingWorkWithUsProfessionalism,
        LandingWorkWithUsNoStupidHR,
        UIMobileBottomMenu,
        LandingHomeOurSoftwareIsTheBest,
    ],
    providers: [
        // frontend
        AppFrontendService,

        // ui
        UIAlertModalService,

        // landing
        LandingScrollService,
        UILandingMetaService,
    ],
    exports: [
        UIAlertModal,
        UIMainMenu,
        UIMainSlideMenu,
        UIMobileBottomMenu,

        LandingRoute,
        LandingHomeRoute,
        LandingContactsRoute,
        LandingProcessRoute,
        LandingServicesRoute,
        LandingTechnologiesRoute,
        LandingCollaborationRoute,
    ]
})
export class LpAppModule
{
    constructor(
        private uiTitle: UITitleService,
        private uiScrollService: UIScrollService,
        private uiMetaService: UILandingMetaService,
    ) {
        this.initTitle();
        this.initMetaTags();
        this.initOnScrollTopSubject();
    }

    initTitle(): void {
        this.uiTitle.usePrefix('lp.m.ui.title.prefix', {
            translate: true,
        });
    }

    initMetaTags(): void {
        this.uiMetaService.init();
    }

    initOnScrollTopSubject(): void {
        this.uiScrollService.onScrollTopSubject.subscribe(next => {
            if(document && document.body) {
                setTimeout(() => {
                    document.body.scrollTop = 0;
                });
            }

            if(document && document.querySelectorAll) {
                setTimeout(() => {
                    [].forEach.call(document.querySelectorAll('.ui-scroll-service-scroll-me-top'), elem => {
                        elem.scrollTop = 0;
                    });
                });
            }
        });
    }
}

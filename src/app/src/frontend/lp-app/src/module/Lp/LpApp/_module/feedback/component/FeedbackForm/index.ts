import {Component, Input, Output, EventEmitter, HostListener} from "@angular/core";

import {FeedbackFormModel} from "./model";
import {Feedback} from "../../../../../../../definitions/src/lp/src/definitions/feedback/entity/Feedback";

@Component({
    selector: 'lp-feedback-form',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ],
    providers: [
        FeedbackFormModel,
    ]
})
export class FeedbackForm
{
    @Output('success') successEvent: EventEmitter<Feedback<any>> = new EventEmitter<Feedback<any>>();
    @Output('close') closeEvent: EventEmitter<void  > = new EventEmitter<void>();

    @Input('reference') reference: string = FeedbackFormModel.DEFAULT_REFERENCE;

    constructor(
        public model: FeedbackFormModel,
    ) {}

    ngOnInit(): void {}

    @HostListener('document:keyup', ['$event']) keyUp($event: KeyboardEvent) {
        if($event.key === 'Escape') {
            this.close();
        }
    }

    is(state: any): boolean {
        return this.model.state === state;
    }

    success($event: Feedback<any>): void {
        this.successEvent.emit($event);
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }
}
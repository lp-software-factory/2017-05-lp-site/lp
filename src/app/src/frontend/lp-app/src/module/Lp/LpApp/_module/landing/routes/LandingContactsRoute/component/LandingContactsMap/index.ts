import {Component} from "@angular/core";
import {DomSanitizer} from "@angular/platform-browser";
import {Subscription} from "rxjs";

import {AppFrontendService} from "../../../../../frontend/service/AppFrontendService";

export const landingContactsMapPreviewImage = '/dist/app/images/landing/map-preview.png';

@Component({
    selector: 'lp-landing-contacts-map',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss'
    ]
})
export class LandingContactsMap
{
    public mapUrl: any;
    public locked: boolean = true;
    public loaded: boolean = false;

    public previewCssClasses: string[] = [];
    public preview: any = {
        'background-image': `url(${landingContactsMapPreviewImage})`
    };

    private subscription: Subscription;

    constructor(
        private frontend: AppFrontendService,
        private sanitize: DomSanitizer,
    ) {}

    ngOnInit(): void {
        this.subscription = this.frontend.last.subscribe(next => {
            this.mapUrl = this.sanitize.bypassSecurityTrustResourceUrl(next.lp.contacts.map);
        });
    }

    ngOnDestroy(): void {
        if(this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    onLoad($event): void {
        if ($event.target.src != '') {
            setTimeout(() => {
                this.loaded = true;
            }, 2100);

            this.previewCssClasses.push('loaded');
        }
    }

    mousedown(): void {
        this.locked = false;
    }

    mouseleave(): void {
        this.locked = true;
    }

    click(): void {
        this.locked = false;
    }
}
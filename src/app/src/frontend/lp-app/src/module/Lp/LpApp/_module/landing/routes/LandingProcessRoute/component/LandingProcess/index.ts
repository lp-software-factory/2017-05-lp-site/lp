import {Component} from "@angular/core";

@Component({
    selector: 'lp-landing-process',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class LandingProcess
{}
import {NgModule} from "@angular/core";

import {RESTAdapter} from "./service/RESTAdapter";

import {
    factoryFeedbackRESTService,
} from "./factory/app.rest.services";

import {FeedbackRESTService} from "../../../definitions/src/lp/src/services/feedback/FeedbackRESTService";

@NgModule({
    providers: [
        RESTAdapter,
        {
            provide: FeedbackRESTService,
            useFactory: factoryFeedbackRESTService,
            deps: [RESTAdapter],
        },
    ]
})
export class LpRESTServicesModule
{}
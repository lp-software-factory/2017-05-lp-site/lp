import {Component, HostListener} from "@angular/core";

import {UIMainMenuConfig, UIMainMenuItem} from "../UIMainMenu/index";
import {Routes, Router} from "@angular/router";

@Component({
    selector: 'lp-ui-main-slide-menu',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class UIMainSlideMenu
{
    public static TIME_ANIMATION = 200;

    public visible: boolean = false;
    public opened: boolean = false;

    public items: UIMainMenuItem[] = UIMainMenuConfig;

    constructor(
        private router: Router,
    ) {}

    @HostListener('document:keyup', ['$event']) keyUp($event: KeyboardEvent) {
        if($event.key === 'Escape') {
            $event.stopPropagation();
            this.close();
        }
    }

    go(url: Routes) {
        this.router.navigate(url);
        this.close();
    }

    open(): void {
        this.visible = true;
        this.opened = true;
    }

    close(): void {
        this.opened = false;

        setTimeout(() => {
            this.visible = false;
        }, UIMainSlideMenu.TIME_ANIMATION);
    }
}
import {Component} from '@angular/core';
import {Routes} from "@angular/router";

declare let Sticky;

export interface UIMainMenuItem
{
    title: string,
    alt: string,
    route: Routes,
    order: number,
}

export const UIMainMenuConfig: UIMainMenuItem[] = [
    {
        title: 'lp.m.ui.main-menu.home.title',
        alt: 'lp.m.ui.main-menu.home.alt',
        route: ['/'],
        order: 1,
    },
    {
        title: 'lp.m.ui.main-menu.collaboration.title',
        alt: 'lp.m.ui.main-menu.collaboration.alt',
        route: ['/collaboration'],
        order: 2,
    },
    {
        title: 'lp.m.ui.main-menu.process.title',
        alt: 'lp.m.ui.main-menu.process.alt',
        route: ['/process'],
        order: 3,
    },
    {
        title: 'lp.m.ui.main-menu.work-with-us.title',
        alt: 'lp.m.ui.main-menu.work-with-us.alt',
        route: ['/work-with-us'],
        order: 4,
    },
    {
        title: 'lp.m.ui.main-menu.contacts.title',
        alt: 'lp.m.ui.main-menu.contacts.alt',
        route: ['/contacts'],
        order: 5,
    },
];

@Component({
    selector: 'lp-ui-main-menu',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class UIMainMenu
{
    private sticky: any;

    public items: UIMainMenuItem[] = UIMainMenuConfig;

    ngOnInit() {
        setTimeout(() => {
            this.sticky = new Sticky('.lp-main-menu-container');
        }, 100);
    }

    ngOnDestroy() {
        if (this.sticky) {
            this.sticky.destroy();
        }
    }
}
import {Component} from "@angular/core";

@Component({
    selector: 'lp-landing-services',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class LandingServices
{}
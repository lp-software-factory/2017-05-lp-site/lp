import {Component} from "@angular/core";

@Component({
    selector: 'lp-landing-collaboration-forms',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class LandingCollaborationForms
{}
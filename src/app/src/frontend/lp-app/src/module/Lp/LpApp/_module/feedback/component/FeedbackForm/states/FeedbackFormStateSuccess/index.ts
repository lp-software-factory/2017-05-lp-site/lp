import {Component, Output, EventEmitter} from "@angular/core";

import {FeedbackFormModel, FeedbackFormStateENUM} from "../../model";

@Component({
    selector: 'lp-feedback-form-state-success',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class FeedbackFormStateSuccess
{
    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    constructor(
        public model: FeedbackFormModel,
    ) {}

    next(): void {
        this.model.state = FeedbackFormStateENUM.Form;
        this.model.reset();
    }

    close() {
        this.closeEvent.emit(undefined);
    }
}

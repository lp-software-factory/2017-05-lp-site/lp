import {Component} from "@angular/core";

import {landingContactsMapPreviewImage} from "../LandingContactsRoute/component/LandingContactsMap/index";
import {LandingScrollService} from "../../service/LandingScrollService";
import {UITitleService} from "../../../../../../ZEA2Common/src/_module/ui/service/UITitleService";

declare let Image;

@Component({
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class LandingRoute
{
    constructor(
        private uiTitle: UITitleService,
        private uiScroll: LandingScrollService,
    ) {}

    ngOnInit(): void {
        this.uiTitle.setTitle([]);
        this.uiScroll.scrollTop();

        this.preLoadContactsMapImage();
    }

    preLoadContactsMapImage(): void {
        let image: any;

        image = new Image();
        image.src = landingContactsMapPreviewImage;
    }
}
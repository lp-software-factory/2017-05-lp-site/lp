import {Component} from '@angular/core';

import {UITitleService} from "../../../../../../ZEA2Common/src/_module/ui/service/UITitleService";
import {UIScrollService} from "../../../../../../ZEA2Common/src/_module/ui/service/UIScrollService";

@Component({
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class UINotFound
{
    constructor(
        private uiTitle: UITitleService,
        private uiScroll: UIScrollService,
    ) {}

    ngOnInit(): void {
        this.uiTitle.setTitle([]);
        this.uiScroll.scrollTop();
    }
}
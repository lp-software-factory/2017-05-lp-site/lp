import {Component} from "@angular/core";

import {LandingScrollService} from "../../service/LandingScrollService";
import {UITitleService} from "../../../../../../ZEA2Common/src/_module/ui/service/UITitleService";
import {routerTransition} from "../../animation/app.routing.animations";
import {UILandingMetaService} from "../../service/LandingMetaService";

@Component({
    host: {'[@routerTransition]': ''},
    animations: [routerTransition()],
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class LandingServicesRoute
{
    constructor(
        private uiTitle: UITitleService,
        private uiScroll: LandingScrollService,
        private uiMeta: UILandingMetaService,
    ) {}

    ngOnInit(): void {
        this.uiTitle.setTitle([{ title: 'lp.m.landing.services.title', translate: true }]);
        this.uiScroll.scrollPostIntro();

        this.uiMeta.setMeta({
            name: 'lp.m.landing.services.title',
            description: 'lp.m.landing.services.description',
            uri: 'services'
        });
    }

    ngAfterViewInit() {
        setTimeout(() => {
            this.uiScroll.scrollPostIntro();
        }, 255);
    }
}
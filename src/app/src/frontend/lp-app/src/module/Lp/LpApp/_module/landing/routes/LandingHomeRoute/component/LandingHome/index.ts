import {Component} from "@angular/core";

@Component({
    selector: 'lp-landing-home',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class LandingHome
{}
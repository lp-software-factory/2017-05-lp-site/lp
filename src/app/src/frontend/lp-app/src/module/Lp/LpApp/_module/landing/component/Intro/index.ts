import {Component} from "@angular/core";

@Component({
    selector: 'lp-landing-intro',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class LandingIntro
{}
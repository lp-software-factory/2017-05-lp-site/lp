import {Component} from "@angular/core";

@Component({
    selector: 'lp-landing-collaboration',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class LandingCollaboration
{}
import {RESTAdapter} from "../service/RESTAdapter";

import {FeedbackRESTService} from "../../../../definitions/src/lp/src/services/feedback/FeedbackRESTService";

export function factoryFeedbackRESTService(adapter: RESTAdapter) {
	return new FeedbackRESTService(adapter);
}

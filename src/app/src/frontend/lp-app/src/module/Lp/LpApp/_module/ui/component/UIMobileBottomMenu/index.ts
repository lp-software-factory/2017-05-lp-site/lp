import {Component} from "@angular/core";

import {UIMainMenuItem, UIMainMenuConfig} from "../UIMainMenu/index";

@Component({
    selector: 'lp-ui-mobile-bottom-menu',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class UIMobileBottomMenu
{
    public items: UIMainMenuItem[] = UIMainMenuConfig;
}
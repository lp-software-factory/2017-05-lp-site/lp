let proxy = require('express-http-proxy');
let app = require('express')();

app.use('/', proxy('localhost:3000'));

app.listen(80, function () {
  console.log('Example app listening on port 80!');
});
<?php
namespace ZEA2\Platform;

use function \DI\object;
use function \DI\get;

use ZEA2\Platform\Service\EnvironmentService;

return [
    EnvironmentService::class => object()
        ->constructorParameter('current', get('config.environment')),
];
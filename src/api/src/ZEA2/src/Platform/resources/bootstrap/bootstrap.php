<?php
namespace ZEA2\REST;

use DI\ContainerBuilder;
use Doctrine\Common\Cache\ApcuCache;
use ZEA2\Platform\Bootstrap\Scripts\CreateDIContainer;
use ZEA2\Platform\Bootstrap\Scripts\LoadBundlesConfiguration;
use ZEA2\Platform\Constants\Environment;
use ZEA2\Platform\Service\BundlesService;
use ZEA2\Platform\Service\ConfigService;
use ZEA2\Platform\Builder\ZEApplicationBuilder;
use Zend\Expressive\Application;

require_once __DIR__.'/../../../../../../vendor/autoload.php';

if(file_exists($configure = __DIR__.'/../../../../../provide/provide.config.php'))
    /** @noinspection PhpIncludeInspection */
    require_once $configure;

return function(array $options): Application
{
    $options = array_merge([
        'environment' => null,
        'cache' => [
            'enabled' => defined('ZEA2_ACPU_ENABLED') ? ZEA2_ACPU_ENABLED : false,
            'key' => 'config.zea2.platform.cache',
            'namespace' => 'default',
        ]
    ], $options);

    if(is_string($options['environment']) && ! in_array($options['environment'], Environment::LIST_ENVIRONMENTS)) {
        throw new \Exception(sprintf('Unknown environment `%s`', $options['environment']));
    }

    $configService = new ConfigService();
    $bundleService = new BundlesService();

    $loaded = false;
    $keyCache = $options['cache']['key'];
    $useCache = $options['cache']['enabled'];

    $apcuCache = new ApcuCache();
    $apcuCache->setNamespace($options['cache']['namespace']);

    if($useCache) {
        if($apcuCache->contains($keyCache)) {
            $configService->merge(unserialize($apcuCache->fetch($keyCache)));

            $loaded = true;
        }
    }

    $bundleService->addBundlesRecursive($options['bundles']);

    if(! $loaded) {
        (new LoadBundlesConfiguration)(
        /* environment => */ $options['environment'],
            /* provide config =>  */ __DIR__.'/../../../../../../provide',
            /* config service => */ $configService,
            /* bundle service => */ $bundleService
        );

        if($useCache) {
            $apcuCache->save($keyCache, serialize($configService->all()));
        }
    }

    $containerBuilder = new ContainerBuilder();

    if($useCache) {
        $containerBuilder->setDefinitionCache($apcuCache);
    }

    $application = (new ZEApplicationBuilder())
        ->addInitScriptsFromBundles($bundleService->listBundles())
        ->build((new CreateDIContainer())(
            $containerBuilder,
            $bundleService,
            $configService
        ));

    return $application;
};
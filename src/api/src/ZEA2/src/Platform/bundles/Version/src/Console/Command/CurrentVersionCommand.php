<?php
namespace ZEA2\Platform\Bundles\Version\Console\Command;

use ZEA2\Platform\Bundles\Version\Service\VersionService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class CurrentVersionCommand extends Command
{
    /** @var VersionService */
    private $versionService;

    public function __construct(VersionService $versionService)
    {
        $this->versionService = $versionService;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('version:current')
            ->setDescription('Show current REST API version');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln($this->versionService->getCurrentVersion());
    }
}
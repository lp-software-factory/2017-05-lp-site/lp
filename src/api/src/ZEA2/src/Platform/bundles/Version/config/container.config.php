<?php
namespace ZEA2\Platform\Bundles\Version;

use function DI\get;
use function DI\object;

use ZEA2\Platform\Bundles\Version\Service\VersionService;

return [
    VersionService::class => object()
        ->constructorParameter('composer', get('config.zea2.platform.composer.json')),
];
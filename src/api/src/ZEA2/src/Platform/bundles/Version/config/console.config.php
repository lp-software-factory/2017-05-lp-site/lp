<?php
namespace ZEA2\Platform\Bundles\Version;

use ZEA2\Platform\Bundles\Version\Console\Command\CurrentVersionCommand;

return [
    'config.zea2.platform.console' => [
        'commands' => [
            'Version' => [
                CurrentVersionCommand::class,
            ]
        ]
    ]
];
<?php
namespace ZEA2\Platform\Bundles\Frontend\Service;

use ZEA2\Platform\Bundles\Bundle;
use ZEA2\Platform\Bundles\Frontend\Markers\FrontendBundleInjectable\FrontendBundleInjectable;
use ZEA2\Platform\Bundles\Frontend\Markers\FrontendBundleInjectable\FrontendScript;
use ZEA2\Platform\Service\BundlesService;
use Cocur\Chain\Chain;
use DI\Container;

class HasExporterWithSameIdException extends \Exception {}

class FrontendService
{
    /** @var Container */
    private $container;
    
    /** @var BundlesService */
    private $bundlesService;

    /** @var array */
    private $exported = [];

    public function __construct(
        Container $container,
        BundlesService $bundlesService)
    {
        $this->container = $container;
        $this->bundlesService = $bundlesService;
    }

    public function export(string $key, $data): self
    {
        $this->exported[$key] = $data;

        return $this;
    }

    public function fetch(): array {
        $result = [];
        $scripts = Chain::create($this->bundlesService->listBundles())
            ->filter(function(Bundle $bundle) {
                return $bundle instanceof FrontendBundleInjectable;
            })
            ->map(function(FrontendBundleInjectable $bundle) {
                return $bundle->getFrontendScripts();
            })
            ->reduce(function(array $carry, array $scripts) {
                return array_merge($carry, $scripts);
            }, [])
        ;

        $scripts = array_map(function(string $script) {
            return $this->container->get($script);
        }, $scripts);

        Chain::create($scripts)
            ->map(function(FrontendScript $script) use (&$result) {
                $result = array_merge_recursive($result, $script());
            });

        $result['exports'] = $this->exported;

        return $result;
    }
}
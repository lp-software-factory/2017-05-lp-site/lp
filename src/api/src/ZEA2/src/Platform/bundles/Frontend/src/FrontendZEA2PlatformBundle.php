<?php
namespace ZEA2\Platform\Bundles\Frontend;

use ZEA2\Platform\Bundles\ZEA2Bundle;

class FrontendZEA2PlatformBundle extends ZEA2Bundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}
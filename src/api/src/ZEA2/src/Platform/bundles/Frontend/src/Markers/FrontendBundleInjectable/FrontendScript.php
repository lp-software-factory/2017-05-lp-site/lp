<?php
namespace ZEA2\Platform\Bundles\Frontend\Markers\FrontendBundleInjectable;

interface FrontendScript
{
    public function __invoke(): array;
}
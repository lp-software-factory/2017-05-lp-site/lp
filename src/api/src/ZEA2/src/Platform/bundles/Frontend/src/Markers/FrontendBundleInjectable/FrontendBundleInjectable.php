<?php
namespace ZEA2\Platform\Bundles\Frontend\Markers\FrontendBundleInjectable;

interface FrontendBundleInjectable
{
    public function getFrontendScripts(): array;
}
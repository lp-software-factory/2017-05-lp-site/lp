<?php
namespace ZEA2\Platform\Bundles\APIDocs;

use ZEA2\Platform\Bundles\ZEA2Bundle;

final class APIDocsZEA2PlatformBundle extends ZEA2Bundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}
<?php
namespace ZEA2\Platform\Bundles\APIDocs\Schema\Exceptions;

final class InvalidJSONSchemaException extends \Exception {}
<?php
namespace ZEA2\Platform\Bundles\APIDocs\Generator\Impl;

use Cocur\Chain\Chain;
use ZEA2\Platform\Bundles\APIDocs\APIBuilder\LocalSwaggerAPIBuilder;
use ZEA2\Platform\Bundles\APIDocs\Bundle\ZEA2APIDocsBundle;
use ZEA2\Platform\Bundles\APIDocs\Generator\APIDocsGeneratorInterface;
use ZEA2\Platform\Bundles\Bundle;
use ZEA2\Platform\Service\BundlesService;

final class BundlesAPIDocsGenerator implements APIDocsGeneratorInterface
{
    /** @var BundlesService */
    private $bundlesService;

    public function __construct(BundlesService $bundlesService)
    {
        $this->bundlesService = $bundlesService;
    }

    public function generateAPIDocs(): array
    {

        $sources = Chain::create($this->bundlesService->listBundles())
            ->filter(function(Bundle $bundle) {
                return $bundle instanceof ZEA2APIDocsBundle;
            })
            ->filter(function(ZEA2APIDocsBundle $bundle) {
                return $bundle->hasAPIDocs();
            })
            ->map(function(ZEA2APIDocsBundle $bundle) {
                return $bundle->getAPIDocsDir();
            })
            ->array;

        return (new LocalSwaggerAPIBuilder($sources))->build();
    }
}
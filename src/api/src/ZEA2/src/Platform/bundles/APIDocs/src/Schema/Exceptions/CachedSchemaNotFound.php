<?php
namespace ZEA2\Platform\Bundles\APIDocs\Schema\Exceptions;

class CachedSchemaNotFound extends \Exception
{
}
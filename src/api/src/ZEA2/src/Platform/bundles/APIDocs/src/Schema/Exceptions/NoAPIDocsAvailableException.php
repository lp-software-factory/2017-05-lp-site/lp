<?php
namespace ZEA2\Platform\Bundles\APIDocs\Schema\Exceptions;

class NoAPIDocsAvailableException extends \Exception
{
}
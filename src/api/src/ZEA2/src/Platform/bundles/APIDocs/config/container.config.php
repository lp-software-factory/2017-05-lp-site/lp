<?php
namespace ZEA2\Platform\Bundles\APIDocs;

use function DI\object;
use function DI\get;

use ZEA2\Platform\Bundles\APIDocs\Config\APCUConfig;

return [
    APCUConfig::class => object()->constructorParameter('config', get('config.zea2.platform.api-docs.apcu')),
];
<?php
namespace ZEA2\Platform\Bundles\APIDocs\Service;

use ZEA2\Platform\Bundles\APIDocs\Schema\JSONSchema;

class SchemaService
{
    /** @var APIDocsService */
    private $apiDocsService;

    public function __construct(APIDocsService $apiDocsService)
    {
        $this->apiDocsService = $apiDocsService;
    }

    public function getDefinition(string $definition): JSONSchema
    {
        return new JSONSchema($this->apiDocsService->getDefinition($definition));
    }
}
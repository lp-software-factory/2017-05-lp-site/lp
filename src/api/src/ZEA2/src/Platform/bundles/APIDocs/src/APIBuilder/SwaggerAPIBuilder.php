<?php
namespace ZEA2\Platform\Bundles\APIDocs\APIBuilder;

interface SwaggerAPIBuilder
{
    public function build(): array;
}
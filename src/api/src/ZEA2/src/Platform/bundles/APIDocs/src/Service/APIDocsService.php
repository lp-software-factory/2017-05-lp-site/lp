<?php
namespace ZEA2\Platform\Bundles\APIDocs\Service;

use Doctrine\Common\Cache\ApcuCache;
use ZEA2\Platform\Bundles\APIDocs\Config\APCUConfig;
use ZEA2\Platform\Bundles\APIDocs\Exceptions\APIDocsDefinitionNotFoundException;
use ZEA2\Platform\Bundles\APIDocs\Generator\APIDocsGeneratorInterface;

final class APIDocsService
{
    /** @var array */
    private $apiDocs;

    /** @var ApcuCache */
    private $apcuCache;

    /** @var APCUConfig */
    private $apcuConfig;

    /** @var APIDocsGeneratorInterface */
    private $generator;

    public function __construct(
        ApcuCache $apcuCache,
        APCUConfig $apcuConfig,
        APIDocsGeneratorInterface $generator
    ) {
        $this->apcuCache = $apcuCache;
        $this->apcuConfig = $apcuConfig;
        $this->generator = $generator;
        $this->cache = new ApcuCache();
    }

    public function getAPIDocs(array $options = []): array
    {
        $options = array_merge([
            'use_cache' => true
        ], $options);

        if($options['use_cache']) {
            $key = $this->apcuConfig->getKey();
            $cache = $this->apcuCache;

            if($this->apiDocs === null) {
                if($cache->contains($key)) {
                    return $cache->fetch($key);
                }else{
                    $this->apiDocs = $this->generator->generateAPIDocs();
                    $this->apcuCache->save($key, $this->apiDocs, $this->apcuConfig->getLifeTime());
                }
            }

            return $this->apiDocs;
        }else{
            $this->apiDocs = $this->generator->generateAPIDocs();

            return $this->apiDocs;
        }
    }

    public function getDefinition(string $definition): array
    {
        $apiDocsDefinitions = $this->getAPIDocs()['definitions'];

        if(isset($apiDocsDefinitions[$definition])) {
            return $apiDocsDefinitions[$definition];
        }else{
            throw new APIDocsDefinitionNotFoundException(sprintf('Definition `%s` not found', $definition));
        }
    }
}
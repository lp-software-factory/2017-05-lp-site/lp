<?php
namespace ZEA2\Platform\Bundles\APIDocs\Schema;

use JsonSchema\Validator;

class JSONSchema
{
    /** @var array */
    private $definition;

    /** @var Validator */
    private $validator;

    public function __construct($definition)
    {
        $this->definition = $definition;
        $this->validator = new Validator();
    }

    public function getDefinition()
    {
        return $this->definition;
    }

    public function getValidator(): Validator
    {
        return $this->validator;
    }

    public function validate($data): Validator
    {
        $this->validator->check($data, json_decode(json_encode($this->definition)));

        return $this->validator;
    }
}
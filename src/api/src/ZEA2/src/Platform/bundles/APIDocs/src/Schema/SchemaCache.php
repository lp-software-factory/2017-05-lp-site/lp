<?php
namespace ZEA2\Platform\Bundles\APIDocs\Schema;

interface SchemaCache
{
    public function generateCacheToken(string $bundle, string $path);
    public function putSchemaToCache(string $token, JSONSchema $schema);
    public function hasCachedSchema(string $token): bool;
    public function getCachedSchema(string $token): JSONSchema;
    public function reset();
}
<?php
namespace ZEA2\Platform\Bundles\APIDocs;

use ZEA2\Platform\Bundles\APIDocs\Middleware\APIDocsMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'get',
                'path' => '/api-docs.json',
                'middleware' => APIDocsMiddleware::class,
            ],
        ]
    ]
];
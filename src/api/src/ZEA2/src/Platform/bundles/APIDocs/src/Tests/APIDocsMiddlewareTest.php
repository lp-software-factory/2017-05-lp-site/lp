<?php
namespace ZEA2\Platform\Bundles\APIDocs\Tests;

use ZEA2\Platform\Bundles\PHPUnit\RESTRequest\RESTRequest;
use ZEA2\Platform\Bundles\PHPUnit\TestCase\MiddlewareTestCase;

final class APIDocsMiddlewareTest extends MiddlewareTestCase
{
    public function test200()
    {
        $this->requestAPIDocs()
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'swagger' => $this->expectString(),
                'basePath' => '/backend/api',
                'info' => [
                    'version' => $this->expectString(),
                    'title' => $this->expectString(),
                ],
            ]);
    }

    private function requestAPIDocs(): RESTRequest
    {
        return $this->request('GET', '/api-docs.json');
    }
}
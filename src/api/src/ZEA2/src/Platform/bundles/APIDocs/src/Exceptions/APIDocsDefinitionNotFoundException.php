<?php
namespace ZEA2\Platform\Bundles\APIDocs\Exceptions;

final class APIDocsDefinitionNotFoundException extends \Exception {}
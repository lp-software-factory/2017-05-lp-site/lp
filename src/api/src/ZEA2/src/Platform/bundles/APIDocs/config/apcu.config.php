<?php
namespace ZEA2\Platform\Bundles\APIDocs;

/** @noinspection SpellCheckingInspection */
return [
    'config.zea2.platform.api-docs.apcu' => [
        'api-docs' => [
            'key' => 'config.zea2.platform.api-docs.json',
            'lifetime' => 3 /* hours */ * 60 /* minutes */ * 60 /* seconds */,
        ]
    ]
];
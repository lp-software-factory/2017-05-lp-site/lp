<?php
namespace ZEA2\Platform\Bundles\APIDocs\Bundle;

interface ZEA2APIDocsBundle
{
    public function hasAPIDocs(): bool;
    public function getAPIDocsDir(): string;
}
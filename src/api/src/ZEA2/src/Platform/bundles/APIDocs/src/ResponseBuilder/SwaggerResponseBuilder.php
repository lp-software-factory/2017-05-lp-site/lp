<?php
namespace ZEA2\Platform\Bundles\APIDocs\ResponseBuilder;

use ZEA2\Platform\Response\JSON\ResponseBuilder;
use ZEA2\Platform\Response\JSON\ResponseDecoratorsManager;

final class SwaggerResponseBuilder extends ResponseBuilder
{
    protected function initDecorators(ResponseDecoratorsManager $decoratorsManager)
    {
    }
}
<?php
namespace ZEA2\Platform\Bundles\APIDocs\Schema;

use ZEA2\Platform\Bundles\APIDocs\Service\SchemaService;
use ZEA2\Platform\Bundles\APIDocs\Schema\Exceptions\InvalidJSONSchemaException;
use Psr\Http\Message\ServerRequestInterface;

abstract class SchemaRequest
{
    /** @var SchemaService */
    private static $schemaService;

    /** @var array */
    private $data;

    /** @var ServerRequestInterface */
    private $request;

    public final function __construct(ServerRequestInterface $request)
    {
        $this->request = $request;

        if($request->getParsedBody()) {
            $this->data = json_decode(json_encode($request->getParsedBody()), true);
        }else{
            $this->data = json_decode($request->getBody(), true);
        }

        if($this->data === null) {
            $this->data = [];
        }

        $this->validateSchema();
    }

    abstract public function getParameters();

    private function validateSchema()
    {
        $data = $this->getData();

        $schema = $this->getSchema();
        $validator = $schema->validate(json_decode(json_encode($data)));

        if (!($validator->isValid())) {
            throw new InvalidJSONSchemaException('Invalid JSON request');
        }
    }

    public static function injectSchemaService(SchemaService $schemaService)
    {
        self::$schemaService = $schemaService;
    }

    public final static function getSchemaService(): SchemaService
    {
        return self::$schemaService;
    }

    abstract protected function getSchema(): JSONSchema;

    protected final function getData(): array
    {
        return $this->data;
    }

    protected final function getRequest(): ServerRequestInterface
    {
        return $this->request;
    }
}
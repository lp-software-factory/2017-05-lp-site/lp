<?php
namespace ZEA2\Platform\Bundles\APIDocs\Generator;

interface APIDocsGeneratorInterface
{
    public function generateAPIDocs(): array;
}
<?php
namespace ZEA2\Platform\Bundles\Console;

use ZEA2\Platform\Bundles\ZEA2Bundle;

final class ConsoleZEA2PlatformBundle extends ZEA2Bundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}
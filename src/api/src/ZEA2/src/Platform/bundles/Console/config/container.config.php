<?php
namespace ZEA2\Platform\Bundles\Company;

use function DI\factory;

use ZEA2\Platform\Bundles\Console\Factory\ConsoleApplicationFactory;
use Symfony\Component\Console\Application;

return [
    Application::class => factory(new ConsoleApplicationFactory()),
];
<?php
namespace ZEA2\Platform\Bundles\MongoDB;

use ZEA2\Platform\Bundles\ZEA2Bundle;

final class MongoDBZEA2PlatformBundle extends ZEA2Bundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}
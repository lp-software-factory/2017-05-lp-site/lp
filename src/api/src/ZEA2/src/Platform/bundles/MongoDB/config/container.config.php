<?php
namespace ZEA2\Platform\Bundles\Doctrine;

use function DI\factory;

use ZEA2\Platform\Bundles\MongoDB\Factory\MongoDBFactory;
use MongoDB\Database;

return [
    Database::class => factory(MongoDBFactory::class),
];
<?php
namespace ZEA2\Platform\Bundles\DoctrineORM\Type\LocalizedString\Value;

use ZEA2\Platform\Bundles\DoctrineORM\Type\LocalizedString\Exceptions\InvalidRegionException;
use ZEA2\Platform\Bundles\DoctrineORM\Type\LocalizedString\Exceptions\TranslationNotFoundException;
use ZEA2\Platform\Bundles\DoctrineORM\Type\LocalizedString\LocalizedString;
use ZEA2\Platform\Markers\JSONSerializable;

final class MutableLocalizedString implements JSONSerializable, LocalizedString
{
    /** @var string[] */
    private $translations = [];

    public function __construct(array $translations = [])
    {
        foreach($translations as $entity) {
            $this->setTranslation($entity['region'], $entity['value']);
        }
    }

    public function toJSON(): array
    {
        return $this->getDefinition();
    }

    public function toImmutable(): ImmutableLocalizedString
    {
        return new ImmutableLocalizedString($this->getDefinition());
    }

    public function getDefinition(): array
    {
        $result = [];

        foreach($this->translations as $region => $value) {
            $result[] = [
                'region' => $region,
                'value' => $value
            ];
        }

        return $result;
    }

    public function setTranslation(string $region, string $value): self
    {
        $this->validateRegion($region);
        $this->translations[$region] = $value;

        return $this;
    }

    public function countTranslation(): int
    {
        return count($this->translations);
    }

    public function hasTranslation(string $region): bool
    {
        $this->validateRegion($region);

        return isset($this->translations[$region]) && is_string($this->translations[$region]);
    }

    public function getTranslation(string $region): string
    {
        $this->validateRegion($region);

        if($this->hasTranslation($region)) {
            return $this->translations[$region];
        }else{
            throw new TranslationNotFoundException(sprintf('Translation for region `%s` not found'));
        }
    }

    private function validateRegion(string $region)
    {
        $regex = '/^[a-z]{2}_[A-Z]{2}$/';

        if(! preg_match($regex, $region)) {
            throw new InvalidRegionException(sprintf('Invalid region identifier `%s`', $region));
        }
    }
}
<?php
namespace ZEA2\Platform\Bundles\DoctrineORM\Type\LocalizedString\Exceptions;

final class InvalidRegionException extends \Exception {}
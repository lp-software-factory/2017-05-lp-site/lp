<?php
namespace ZEA2\Platform\Bundles\DoctrineORM\Factory;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use ZEA2\Platform\Factory\ContainerFactoryInterface;
use Interop\Container\ContainerInterface;

abstract class DoctrineRepositoryFactory implements ContainerFactoryInterface
{
    abstract protected function getEntityClassName(): string;

    public function __invoke(ContainerInterface $container): EntityRepository
    {
        $em = $container->get(EntityManager::class); /** @var EntityManager $em */

        return $em->getRepository($this->getEntityClassName());
    }
}
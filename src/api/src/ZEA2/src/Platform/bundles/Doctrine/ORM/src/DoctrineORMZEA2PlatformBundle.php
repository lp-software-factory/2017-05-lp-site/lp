<?php
namespace ZEA2\Platform\Bundles\DoctrineORM;

use ZEA2\Platform\Bundles\DoctrineORM\InitScripts\DoctrineTypesInitScript;
use ZEA2\Platform\Bundles\ZEA2Bundle;
use ZEA2\Platform\Init\InitScriptInjectableBundle;

final class DoctrineORMZEA2PlatformBundle extends ZEA2Bundle implements InitScriptInjectableBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getInitScripts(): array
    {
        return [
            DoctrineTypesInitScript::class,
        ];
    }
}
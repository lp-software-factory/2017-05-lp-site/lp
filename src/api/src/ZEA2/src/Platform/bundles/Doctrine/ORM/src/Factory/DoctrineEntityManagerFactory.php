<?php
namespace ZEA2\Platform\Bundles\DoctrineORM\Factory;

use Doctrine\Common\Cache\ApcuCache;
use Doctrine\Common\Cache\ArrayCache;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use ZEA2\Platform\Bundles\Bundle;
use ZEA2\Platform\Constants\Environment;
use ZEA2\Platform\Service\BundlesService;
use ZEA2\Platform\Service\EnvironmentService;
use Interop\Container\ContainerInterface;

final class DoctrineEntityManagerFactory
{
    /** @var array */
    private $config;

    public function __construct(array $config)
    {
        $this->config = $config;
    }

    public function __invoke(ContainerInterface $container, EnvironmentService $environmentService, ApcuCache $apcuCache)
    {
        $bundleService =  $container->get(BundlesService::class); /** @var BundlesService $bundleService */
        $entitySourceDirs = [];

        /** @var Bundle $bundle */
        foreach($bundleService->listBundles() as $bundle){
            $bundleEntityDir = $bundle->getDir()."/Entity";

            if(is_dir($bundleEntityDir)){
                $entitySourceDirs[] = $bundleEntityDir;
            }
        }

        if(in_array($environmentService->getCurrent(), [Environment::DEVELOPMENT, Environment::TESTING])) {
            $cache = new ArrayCache();
        }else{
            $cache = new ApcuCache();
            $cache->setNamespace(sprintf('doctrine2orm_%s', $environmentService->getCurrent()));
        }

        $config = new Configuration();
        $config->setMetadataCacheImpl($cache);
        $driverImpl = $config->newDefaultAnnotationDriver($entitySourceDirs);
        $config->setMetadataDriverImpl($driverImpl);
        $config->setQueryCacheImpl($cache);
        $config->setProxyDir(__DIR__.'/../Proxies');
        $config->setProxyNamespace('ZEA2\Platform\Bundles\DoctrineORM\Proxies');

        $config = $this->config;
        $doctrineConfig = Setup::createAnnotationMetadataConfiguration($entitySourceDirs, true);

        return EntityManager::create($config['connection_options'], $doctrineConfig);
    }
}
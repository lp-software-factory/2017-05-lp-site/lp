<?php
namespace ZEA2\Platform\Bundles\Doctrine;

use function DI\get;
use function DI\object;
use function DI\factory;

use Doctrine\ORM\EntityManager;
use ZEA2\Platform\Bundles\DoctrineORM\Factory\DoctrineEntityManagerFactory;

return [
    EntityManager::class => factory(DoctrineEntityManagerFactory::class),
    DoctrineEntityManagerFactory::class => object()->constructorParameter('config', get('config.zea2.platform.doctrine2orm')),
];
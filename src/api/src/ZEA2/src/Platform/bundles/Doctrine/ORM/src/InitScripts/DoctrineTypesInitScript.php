<?php
namespace ZEA2\Platform\Bundles\DoctrineORM\InitScripts;

use Doctrine\DBAL\Types\Type;
use ZEA2\Platform\Bundles\DoctrineORM\Type\LocalizedString\LocalizedStringType;
use ZEA2\Platform\Init\InitScript;

final class DoctrineTypesInitScript implements InitScript
{
    public function __invoke()
    {
        Type::addType('i18n', LocalizedStringType::class);
    }
}
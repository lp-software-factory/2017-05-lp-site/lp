<?php
namespace ZEA2\Platform\Bundles\DoctrineORM\Type\LocalizedString\Exceptions;

final class TranslationNotFoundException extends \Exception {}
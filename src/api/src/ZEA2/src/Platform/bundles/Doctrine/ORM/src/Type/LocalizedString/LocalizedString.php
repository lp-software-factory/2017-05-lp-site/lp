<?php
namespace ZEA2\Platform\Bundles\DoctrineORM\Type\LocalizedString;

interface LocalizedString
{
    public function toJSON() : array;
    public function getDefinition() : array;
    public function hasTranslation(string $region) : bool;
    public function getTranslation(string $region) : string;
    public function countTranslation(): int;
}
<?php
namespace ZEA2\Platform\Bundles\DoctrineORM\Type\LocalizedString;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use ZEA2\Platform\Bundles\DoctrineORM\Type\LocalizedString\Value\ImmutableLocalizedString;

final class LocalizedStringType extends Type
{
    public const TYPE_NAME = 'i18n';

    public function getName()
    {
        return self::TYPE_NAME;
    }

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return $platform->getJsonTypeDeclarationSQL($fieldDeclaration);
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if($value instanceof LocalizedString) {
            return json_encode($value->getDefinition());
        }else{
            return '[]';
        }
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === null || $value === '') {
            return new ImmutableLocalizedString();
        }

        $value = (is_resource($value)) ? stream_get_contents($value) : $value;

        return new ImmutableLocalizedString(json_decode($value, true));
    }
}
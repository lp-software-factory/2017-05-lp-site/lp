<?php
namespace ZEA2\Platform\Constants;

interface Environment
{
    public const LIST_ENVIRONMENTS = [
        self::DEVELOPMENT,
        self::PRODUCTION,
        self::TESTING,
        self::STAGE,
    ];

    public const DEVELOPMENT = 'development';
    public const PRODUCTION = 'production';
    public const TESTING = 'testing';
    public const STAGE = 'stage';
}
<?php
namespace ZEA2\Platform\Exception;

final class BundleNotFoundException extends \Exception {}
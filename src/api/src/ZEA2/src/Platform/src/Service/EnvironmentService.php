<?php
namespace ZEA2\Platform\Service;

final class EnvironmentService
{
    /** @var string */
    private $current;

    public function __construct(string $current)
    {
        $this->current = $current;
    }

    public function getCurrent(): string
    {
        return $this->current;
    }
}
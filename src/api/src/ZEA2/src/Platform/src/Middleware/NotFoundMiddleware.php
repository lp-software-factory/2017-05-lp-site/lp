<?php
namespace ZEA2\Platform\Middleware;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;
use Zend\Stratigility\MiddlewareInterface;

final class NotFoundMiddleware implements MiddlewareInterface
{
    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new JSONResponseBuilder($response);

        $responseBuilder
            ->setJSON([
                'error' => '404 NOT FOUND',
            ])
            ->setStatusNotFound();

        return $responseBuilder->build();
    }
}
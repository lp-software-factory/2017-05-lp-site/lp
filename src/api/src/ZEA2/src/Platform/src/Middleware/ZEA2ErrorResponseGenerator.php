<?php
namespace ZEA2\Platform\Middleware;

use ZEA2\Platform\Response\JSON\JSONResponseBuilder;
use ZEA2\Platform\StatusExceptions\StatusException400;
use ZEA2\Platform\StatusExceptions\StatusException403;
use ZEA2\Platform\StatusExceptions\StatusException404;
use ZEA2\Platform\StatusExceptions\StatusException409;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Zend\Diactoros\Response;

final class ZEA2ErrorResponseGenerator
{
    public function __invoke(\Throwable $e, ServerRequestInterface $request, ResponseInterface $response)
    {
        $responseBuilder = new JSONResponseBuilder(new Response());

        if($e instanceof StatusException400) {
            $responseBuilder->setStatusBadRequest();
        }else if($e instanceof StatusException403) {
            $responseBuilder->setStatusNotAllowed();
        }else if($e instanceof StatusException404) {
            $responseBuilder->setStatusNotFound();
        }else if($e instanceof StatusException409) {
            $responseBuilder->setStatusConflict();
        }else{
            $responseBuilder->setStatusInternalError();
        }

        return $responseBuilder
            ->setJSON([
                'trace' => $e->getTrace(),
                'last' => $e->getTrace()[0],
                'error' => $e->getMessage(),
            ])
            ->build();
    }
}
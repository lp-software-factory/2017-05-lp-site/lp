<?php
namespace ZEA2\Platform\Init;

interface InitScript
{
    public function __invoke();
}
<?php
namespace ZEA2\Platform\Init\InitScripts;

use ZEA2\Platform\Init\InitScript;
use ZEA2\Platform\Middleware\NotFoundMiddleware;
use Zend\Expressive\Application;

final class NotFoundPipeInitScript implements InitScript
{
    private $application;

    public function __construct(Application $application)
    {
        $this->application = $application;
    }

    public function __invoke()
    {
        $this->application->pipe(new NotFoundMiddleware());
    }
}
<?php
namespace ZEA2\Platform\Init;

interface InitScriptInjectableBundle
{
    public function getInitScripts(): array;
}
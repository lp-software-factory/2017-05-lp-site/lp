<?php
namespace ZEA2\Platform\Init\InitScripts;

use Cocur\Chain\Chain;
use ZEA2\Platform\Bundles\Bundle;
use ZEA2\Platform\Bundles\Markers\ZEA2EventSubscriptionsBundle\ZEA2EventSubscriptionsBundle;
use ZEA2\Platform\Event\ZEA2EventEmitter;
use ZEA2\Platform\Event\ZEA2EventScript;
use ZEA2\Platform\Service\BundlesService;
use ZEA2\Platform\Init\InitScript;
use Interop\Container\ContainerInterface;

final class EventsInitScript implements InitScript
{
    /** @var ZEA2EventEmitter */
    private $eventEmitter;

    /** @var BundlesService */
    private $bundlesService;

    /** @var ContainerInterface */
    private $container;

    public function __construct(
        ZEA2EventEmitter $eventEmitter,
        BundlesService $bundlesService,
        ContainerInterface $container
    ) {
        $this->eventEmitter = $eventEmitter;
        $this->bundlesService = $bundlesService;
        $this->container = $container;
    }

    public function __invoke()
    {
        foreach($this->bundlesService->listBundles() as $bundle) {
            $this->bootstrapEvents($bundle);
        }
    }

    private function bootstrapEvents(Bundle $bundle)
    {
        if($bundle instanceof ZEA2EventSubscriptionsBundle) {
            Chain::create($bundle->getEventScripts())
                ->map(function(string $className) {
                    return $this->container->get($className);
                })
                ->map(function(ZEA2EventScript $script) {
                    $script->__invoke($this->eventEmitter);
                });
        }
    }
}
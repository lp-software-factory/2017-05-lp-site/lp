<?php
namespace ZEA2\Platform\Init\InitScripts;

use ZEA2\Platform\Bundles\APIDocs\Schema\SchemaRequest;
use ZEA2\Platform\Bundles\APIDocs\Service\SchemaService;
use ZEA2\Platform\Init\InitScript;

final class InjectSchemaServiceInitScript implements InitScript
{
    /** @var SchemaService */
    private $schemaService;

    public function __construct(SchemaService $schemaService)
    {
        $this->schemaService = $schemaService;
    }

    public function __invoke()
    {
        SchemaRequest::injectSchemaService($this->schemaService);
    }
}
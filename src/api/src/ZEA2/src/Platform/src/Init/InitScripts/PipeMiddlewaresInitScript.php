<?php
namespace ZEA2\Platform\Init\InitScripts;

use ZEA2\Platform\Init\InitScript;
use Zend\Expressive\Application;

final class PipeMiddlewaresInitScript implements InitScript
{
    /** @var Application */
    private $application;

    public function __construct(Application $application)
    {
        $this->application = $application;
    }

    public function __invoke()
    {
        $application = $this->application;

        $application->pipeRoutingMiddleware();
        $application->pipeDispatchMiddleware();
        $application->raiseThrowables();
    }
}
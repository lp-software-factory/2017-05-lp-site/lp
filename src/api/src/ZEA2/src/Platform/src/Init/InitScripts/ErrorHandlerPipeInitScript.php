<?php
namespace ZEA2\Platform\Init\InitScripts;

use ZEA2\Platform\Init\InitScript;
use ZEA2\Platform\Middleware\ZEA2ErrorResponseGenerator;
use Zend\Diactoros\Response;
use Zend\Expressive\Application;
use Zend\Stratigility\Middleware\ErrorHandler;

final class ErrorHandlerPipeInitScript implements InitScript
{
    /** @var Application */
    private $application;

    public function __construct(Application $application)
    {
        $this->application = $application;
    }

    public function __invoke()
    {
        $this->application->pipe(new ErrorHandler(new Response(), new ZEA2ErrorResponseGenerator()));
    }
}
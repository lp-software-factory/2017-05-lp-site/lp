<?php
namespace ZEA2\Platform\Event\Markers\EventAwareService;

use Evenement\EventEmitterInterface;

interface EventAwareService
{
    public function getEventEmitter(): EventEmitterInterface;
}
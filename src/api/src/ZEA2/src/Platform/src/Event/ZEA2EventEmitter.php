<?php
namespace ZEA2\Platform\Event;

use Evenement\EventEmitter;

final class ZEA2EventEmitter extends EventEmitter {}
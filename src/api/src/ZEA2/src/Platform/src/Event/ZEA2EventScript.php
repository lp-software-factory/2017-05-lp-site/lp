<?php
namespace ZEA2\Platform\Event;

interface ZEA2EventScript
{
    public function __invoke(ZEA2EventEmitter $eventEmitter);
}
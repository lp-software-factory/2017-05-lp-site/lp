<?php
namespace ZEA2\Platform\Event\Markers\EventAwareService;

use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;

trait EventAwareServiceTrait
{
    /** @var EventEmitterInterface */
    private $eventEmitter;

    public function getEventEmitter(): EventEmitterInterface {
        if(! $this->eventEmitter) {
            $this->eventEmitter = new EventEmitter();
        }

        return $this->eventEmitter;
    }
}
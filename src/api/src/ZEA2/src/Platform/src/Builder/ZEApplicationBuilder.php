<?php
namespace ZEA2\Platform\Builder;

use Cocur\Chain\Chain;
use DI\Container;
use Interop\Container\ContainerInterface;
use ZEA2\Platform\Bundles\Bundle;
use ZEA2\Platform\Init\InitScript;
use ZEA2\Platform\Init\InitScriptInjectableBundle;
use Zend\Diactoros\Response\SapiEmitter;
use Zend\Expressive\Application;
use Zend\Expressive\Router\FastRouteRouter;
use Zend\Expressive\Router\RouterInterface;
use Zend\Stratigility\NoopFinalHandler;

final class ZEApplicationBuilder
{
    /** @var string[] */
    private $initScripts = [];

    /** @var RouterInterface */
    private $router;

    /** @var SapiEmitter */
    private $SAPIEmitter;

    public function __construct()
    {
        $this->router = new FastRouteRouter();
        $this->SAPIEmitter = new SapiEmitter();
    }

    public function addInitScripts(array $initScripts): self
    {
        foreach($initScripts as $is) {
            $this->initScripts[] = $is;
        }

        return $this;
    }

    public function addInitScriptsFromBundles(array $bundles): self
    {
        array_map(function(Bundle $bundle) {
            if($bundle instanceof InitScriptInjectableBundle) {
                $this->addInitScripts($bundle->getInitScripts());
            }
        }, $bundles);

        return $this;
    }

    public function setRouter(RouterInterface $router): self
    {
        $this->router = $router;

        return $this;
    }

    public function setSAPIEmitter(SapiEmitter $SAPIEmitter): self
    {
        $this->SAPIEmitter = $SAPIEmitter;

        return $this;
    }

    public function build(Container $container): Application
    {
        $application = new Application(
            $this->router,
            $container,
            new NoopFinalHandler(),
            $this->SAPIEmitter
        );

        $container->set(Application::class, $application);
        $container->set(ContainerInterface::class, $container);
        $container->set(Container::class, $container);

        Chain::create($this->initScripts)
            ->map(function(string $input) use ($container) {
                return $container->get($input);
            })
            ->map(function(InitScript $initScript) {
                $initScript->__invoke();
            });

        return $application;
    }
}
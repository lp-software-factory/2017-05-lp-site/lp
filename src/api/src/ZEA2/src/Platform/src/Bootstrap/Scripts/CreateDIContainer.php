<?php
namespace ZEA2\Platform\Bootstrap\Scripts;

use DI\Container;
use DI\ContainerBuilder;
use ZEA2\Platform\Bundles\Bundle;
use ZEA2\Platform\Service\BundlesService;
use ZEA2\Platform\Service\ConfigService;
use Interop\Container\ContainerInterface;

final class CreateDIContainer
{
    public function __invoke(
        ContainerBuilder $containerBuilder,
        BundlesService $bundlesService,
        ConfigService $configService
    ): Container
    {
        $containerBuilder->addDefinitions($configService->all());

        $containerBuilder->addDefinitions([
            BundlesService::class => $bundlesService,
            ConfigService::class => $configService,
        ]);

        $container = $containerBuilder->build();
        $container->set(Container::class, $container);
        $container->set(ContainerInterface::class, $container);

        array_map(function(Bundle $bundle) use ($container) {
            $this->bindBundles($bundle, $container);
        }, $bundlesService->listBundles());

        return $container;
    }

    private function bindBundles(Bundle $bundle, Container $container)
    {
        $container->set(get_class($bundle), $bundle);

        if($bundle->hasBundles()) {
            foreach($bundle->getBundles() as $sub) {
                $this->bindBundles($sub, $container);
            }
        }
    }
}
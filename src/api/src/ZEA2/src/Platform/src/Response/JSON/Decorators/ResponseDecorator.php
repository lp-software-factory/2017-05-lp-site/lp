<?php
namespace ZEA2\Platform\Response\JSON\Decorators;

use ZEA2\Platform\Response\JSON\ResponseBuilder;

interface ResponseDecorator
{
    public function decorate(ResponseBuilder $builder, array &$response): void;
}
<?php
namespace ZEA2\Platform\Response\JSON;

use ZEA2\Platform\Response\JSON\Decorators\ResponseDecorator;

final class ResponseDecoratorsManager implements ResponseDecorator
{
    /** @var ResponseDecorator[] */
    private $decorators = [];

    public function attach(ResponseDecorator $decorator): self
    {
        $this->decorators[get_class($decorator)] = $decorator;

        return $this;
    }

    public function detach(ResponseDecorator $decorator): self
    {
        if(isset($this->decorators[get_class($decorator)])) {
            unset($this->decorators[get_class($decorator)]);
        }

        return $this;
    }

    public function detachByClassName(string $decorator): self
    {
        if(isset($this->decorators[$decorator])) {
            unset($this->decorators[$decorator]);
        }

        return $this;
    }

    public function has(ResponseDecorator $decorator): bool
    {
        return isset($this->decorators[get_class($decorator)]);
    }


    public function clear()
    {
        $this->decorators = [];
    }

    public function decorate(ResponseBuilder $builder, array &$response): void
    {
        foreach($this->decorators as $decorator) {
            $decorator->decorate($builder, $response);
        }
    }
}
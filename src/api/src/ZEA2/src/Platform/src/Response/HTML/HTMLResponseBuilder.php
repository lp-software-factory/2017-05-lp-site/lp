<?php
namespace ZEA2\Platform\Response\HTML;

use Psr\Http\Message\ResponseInterface;

final class HTMLResponseBuilder
{
    const CODE_SUCCESS = 200;
    const CODE_BAD_REQUEST = 400;
    const CODE_NOT_FOUND = 404;
    const CODE_NOT_ALLOWED = 403;
    const CODE_CONFLICT = 409;
    const CODE_UN_PROCESSABLE = 422;
    const CODE_INTERNAL_ERROR = 500;

    /** @var ResponseInterface */
    private $response;

    /** @var int */
    private $status = null;

    /** @var string */
    private $content;

    /** @var string */
    private $contentType = 'text/html';

    /** @var string|null|\Exception */
    private $error;

    public final function __construct(ResponseInterface $response)
    {
        $this->response = $response;
    }

    public function getContentType(): string
    {
        return $this->contentType;
    }

    public function setContentType(string $contentType)
    {
        $this->contentType = $contentType;
    }

    public final function setStatus($status): self
    {
        $this->status = $status;

        return $this;
    }

    public final function getStatus(): int
    {
        return $this->status;
    }

    public function setStatusSuccess(): self
    {
        $this->setStatus(self::CODE_SUCCESS);

        return $this;
    }

    public function setStatusNotFound(): self
    {
        $this->setStatus(self::CODE_NOT_FOUND);

        return $this;
    }

    public function setStatusNotProcessable(): self
    {
        $this->setStatus(self::CODE_UN_PROCESSABLE);

        return $this;
    }

    public function setStatusNotAllowed(): self
    {
        $this->setStatus(self::CODE_NOT_ALLOWED);

        return $this;
    }

    public function setStatusBadRequest(): self
    {
        $this->setStatus(self::CODE_BAD_REQUEST);

        return $this;
    }

    public function setStatusConflict(): self
    {
        $this->setStatus(self::CODE_CONFLICT);

        return $this;
    }

    public function setStatusInternalError(): self
    {
        $this->setStatus(self::CODE_INTERNAL_ERROR);

        return $this;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content)
    {
        $this->content = $content;
    }

    public final function getError()
    {
        return $this->error;
    }

    public function hasError()
    {
        return $this->error !== null;
    }

    public final function setError($error): self
    {
        $this->error = $error;

        return $this;
    }

    public final function build(): ResponseInterface
    {
        $this->checkRequirements();

        $newResponse = $this->response->withStatus($this->status);

        if($this->hasError()) {
            $newResponse->getBody()->write(json_encode([
                'success' => false,
                'trace' => $this->getError()->getTrace(),
                'error' => $this->getError()->getMessage(),
            ]));

            return $newResponse
                ->withStatus($this->status)
                ->withHeader('Content-Type', 'application/json');
        }else{
            $newResponse->getBody()->write($this->getContent());

            return $newResponse
                ->withStatus($this->status)
                ->withHeader('Content-Type', $this->getContentType());
        }
    }

    public function checkRequirements()
    {
        if($this->status === null) {
            throw new \Exception('No status code provided');
        }
    }
}
<?php
namespace ZEA2\Platform\Response\JSON;

use ZEA2\Platform\Response\JSON\Decorators\Lib\ErrorResponseDecorator;
use ZEA2\Platform\Response\JSON\Decorators\Lib\NotBadResponseDecorator;
use ZEA2\Platform\Response\JSON\Decorators\Lib\SuccessResponseDecorator;
use ZEA2\Platform\Response\JSON\Decorators\Lib\TimeResponseDecorator;

final class JSONResponseBuilder extends ResponseBuilder
{
    protected function initDecorators(ResponseDecoratorsManager $decoratorsManager)
    {
        $decoratorsManager
            ->attach(new SuccessResponseDecorator())
            ->attach(new TimeResponseDecorator())
            ->attach(new ErrorResponseDecorator())
            ->attach(new NotBadResponseDecorator())
        ;
    }
}
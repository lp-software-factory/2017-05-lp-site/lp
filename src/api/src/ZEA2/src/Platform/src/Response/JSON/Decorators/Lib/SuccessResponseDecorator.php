<?php
namespace ZEA2\Platform\Response\JSON\Decorators\Lib;

use ZEA2\Platform\Response\JSON\Decorators\ResponseDecorator;
use ZEA2\Platform\Response\JSON\ResponseBuilder;
use ZEA2\Platform\Response\StatusCode;

final class SuccessResponseDecorator implements ResponseDecorator
{
    public function decorate(ResponseBuilder $builder, array &$response): void
    {
        $success =
                 $builder->getStatus() === StatusCode::HTTP_200_OK
            && (! $builder->hasError());

        $response['success'] = $success;
    }
}
<?php
namespace ZEA2\Platform;

use ZEA2\Platform\Bundles\ZEA2Bundle;

final class ZEPlatformBundle extends ZEA2Bundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}
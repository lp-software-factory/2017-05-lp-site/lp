<?php
namespace ZEA2\Platform;

use ZEA2\Platform\Bundles\APIDocs\APIDocsZEA2PlatformBundle;
use ZEA2\Platform\Bundles\Console\ConsoleZEA2PlatformBundle;
use ZEA2\Platform\Bundles\DoctrineORM\DoctrineORMZEA2PlatformBundle;
use ZEA2\Platform\Bundles\Frontend\FrontendZEA2PlatformBundle;
use ZEA2\Platform\Bundles\MongoDB\MongoDBZEA2PlatformBundle;
use ZEA2\Platform\Bundles\Version\VersionZEA2PlatformBundle;
use ZEA2\Platform\Bundles\ZEA2Bundle;

final class ZEA2PlatformBundle extends ZEA2Bundle
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getBundles(): array
    {
        return [
            new APIDocsZEA2PlatformBundle(),
            new ConsoleZEA2PlatformBundle(),
            new DoctrineORMZEA2PlatformBundle(),
            new FrontendZEA2PlatformBundle(),
            new MongoDBZEA2PlatformBundle(),
            new VersionZEA2PlatformBundle(),
        ];
    }
}
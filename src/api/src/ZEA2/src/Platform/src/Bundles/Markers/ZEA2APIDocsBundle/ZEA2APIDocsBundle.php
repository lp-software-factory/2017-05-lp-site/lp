<?php
namespace ZEA2\Platform\Bundles\Markers\ZEA2APIDocsBundle;

interface ZEA2APIDocsBundle
{
    public function hasAPIDocs(): bool;
    public function getAPIDocsDir(): string;
}
<?php
namespace ZEA2\Platform\Bundles\Markers\ZEA2EventSubscriptionsBundle;

interface ZEA2EventSubscriptionsBundle
{
    public function getEventScripts(): array;
}
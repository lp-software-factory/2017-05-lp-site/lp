<?php
namespace ZEA2\Platform\Factory;

use Interop\Container\ContainerInterface;

interface ContainerFactoryInterface
{
    public function __invoke(ContainerInterface $container);
}
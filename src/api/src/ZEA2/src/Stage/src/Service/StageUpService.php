<?php
namespace ZEA2\Stage\Service;

use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use Interop\Container\ContainerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use ZEA2\Stage\Fixtures\StageFixture;

final class StageUpService
{
    public const EVENT_STAGE_UP_STARTS = 'zea2.stage.stage-up.starts';
    public const EVENT_UP_FIXTURE = 'zea2.stage.stage-up.up-fixture';
    public const EVENT_UP_FIXTURE_COMPLETE = 'zea2.stage.stage-up.up-fixture.complete';
    public const EVENT_STAGE_UP_ENDS = 'zea2.stage.stage-up.ends';

    /** @var string[] */
    private $fixtures;

    /** @var EventEmitterInterface */
    private $eventEmitter;

    /** @var ContainerInterface */
    private $container;

    public function __construct(array $fixtures, ContainerInterface $container)
    {
        $this->fixtures = $fixtures;
        $this->eventEmitter = new EventEmitter();
        $this->container = $container;
    }

    public function getEventEmitter(): EventEmitterInterface
    {
        return $this->eventEmitter;
    }

    public function up(OutputInterface $output, InputInterface $input)
    {
        $this->getEventEmitter()->emit(self::EVENT_STAGE_UP_STARTS, [$output, $input]);

        array_map(function(StageFixture $fixture) use ($output, $input) {
            $output->writeln(sprintf('[*] Up %s fixture', get_class($fixture)));

            $this->eventEmitter->emit(self::EVENT_UP_FIXTURE, [$fixture, $output, $input]);
            $fixture->up($output, $input);
            $this->eventEmitter->emit(self::EVENT_UP_FIXTURE_COMPLETE, [$fixture, $output, $input]);
        }, $this->getFixtures());

        $this->getEventEmitter()->emit(self::EVENT_STAGE_UP_ENDS, [$output, $input]);

        $output->writeln('');
        $output->writeln('Done!');
    }

    private function getFixtures(): array
    {
        return array_map(function(string $fixture) {
            return $this->container->get($fixture);
        }, $this->fixtures);
    }
}
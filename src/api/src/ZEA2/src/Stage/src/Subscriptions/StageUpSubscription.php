<?php
namespace ZEA2\Stage\Subscriptions;

use ZEA2\Domain\Bundles\Mail\Service\MailService;
use ZEA2\Stage\Service\StageUpService;
use ZEA2\Platform\Event\ZEA2EventEmitter;
use ZEA2\Platform\Event\ZEA2EventScript;

final class StageUpSubscription implements ZEA2EventScript
{
    /** @var MailService */
    private $mailService;

    /** @var StageUpService */
    private $stageUpService;

    public function __construct(MailService $mailService, StageUpService $stageUpService)
    {
        $this->mailService = $mailService;
        $this->stageUpService = $stageUpService;
    }

    public function __invoke(ZEA2EventEmitter $eventEmitter)
    {
        $this->stageUpService->getEventEmitter()->on(StageUpService::EVENT_STAGE_UP_STARTS, function() {
            $this->mailService->disable();
        });

        $this->stageUpService->getEventEmitter()->on(StageUpService::EVENT_STAGE_UP_ENDS, function() {
            $this->mailService->enable();
        });
    }
}
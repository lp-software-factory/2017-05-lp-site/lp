<?php
namespace ZEA2\Stage\Fixtures;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

interface StageFixture
{
    public function up(OutputInterface $output, InputInterface $input): void;
}
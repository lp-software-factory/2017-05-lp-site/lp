<?php
namespace ZEA2\Stage;

use ZEA2\Stage\Subscriptions\StageUpSubscription;
use ZEA2\Platform\Bundles\Markers\ZEA2EventSubscriptionsBundle\ZEA2EventSubscriptionsBundle;
use ZEA2\Platform\Bundles\ZEA2Bundle;

final class ZEA2StageBundle extends ZEA2Bundle implements ZEA2EventSubscriptionsBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getEventScripts(): array
    {
        return [
            StageUpSubscription::class,
        ];
    }
}
<?php
namespace ZEA2\Stage\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use ZEA2\Stage\Service\StageUpService;

final class StageUpCommand extends Command
{
    /** @var StageUpService */
    private $script;

    public function __construct(StageUpService $script)
    {
        $this->script = $script;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('stage:up')
            ->setDescription('Up stage')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->script->up($output, $input);
    }
}
<?php
namespace ZEA2\Stage;

use function DI\object;
use function DI\get;

use ZEA2\Stage\Service\StageUpService;

return [
    StageUpService::class => object()->constructorParameter('fixtures', get('config.zea2.stage.fixtures')),
];
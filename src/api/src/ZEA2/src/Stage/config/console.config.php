<?php
namespace ZEA2\Stage;

use ZEA2\Stage\Console\Command\StageUpCommand;

return [
    'config.zea2.platform.console' => [
        'commands' => [
            'Stage' => [
                StageUpCommand::class,
            ],
        ],
    ],
];
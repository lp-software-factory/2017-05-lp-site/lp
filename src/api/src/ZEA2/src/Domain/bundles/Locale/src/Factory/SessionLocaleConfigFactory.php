<?php
namespace ZEA2\Domain\Bundles\Locale\Factory;

use ZEA2\Domain\Bundles\Locale\Config\SessionLocaleConfig;
use Interop\Container\ContainerInterface;

final class SessionLocaleConfigFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $config = $container->get('config.zea2.domain.locale');

        return new SessionLocaleConfig(
            $config['session']['key'],
            $config['session']['default']
        );
    }
}
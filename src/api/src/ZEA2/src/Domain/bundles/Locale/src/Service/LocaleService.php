<?php
namespace ZEA2\Domain\Bundles\Locale\Service;

use ZEA2\Domain\Bundles\Locale\Config\LocaleConfig;
use ZEA2\Domain\Bundles\Locale\Exceptions\DuplicateLocaleException;
use ZEA2\Domain\Bundles\Locale\Entity\Locale;
use ZEA2\Domain\Bundles\Locale\Parameters\CreateLocaleParameters;
use ZEA2\Domain\Bundles\Locale\Parameters\EditLocaleParameters;
use ZEA2\Domain\Bundles\Locale\Repository\LocaleRepository;
use League\Flysystem\Exception;

final class LocaleService
{
    /** @var LocaleRepository */
    private $localeRepository;

    /** @var Locale */
    private $currentLocale;

    /** @var array */
    private $defaultLocale;

    public function __construct(LocaleRepository $localeRepository, LocaleConfig $config)
    {
        $this->localeRepository = $localeRepository;
        $this->defaultLocale = $config->getDefault();
    }

    private function touchDefaultLocales()
    {
        if(count($this->localeRepository->getAllLocales()) === 0) {
            $defaultLocales = [
                ['language' => 'ru', 'region' => 'ru_RU'],
                ['language' => 'en', 'region' => 'en_GB'],
                ['language' => 'en', 'region' => 'en_US'],
            ];

            foreach($defaultLocales as $defaultLocale) {
                if(! $this->hasLocale($defaultLocale['language'], $defaultLocale['region'])) {
                    $this->localeRepository->createLocale(new Locale($defaultLocale['language'], $defaultLocale['region']));
                }
            }
        }
    }

    public function setCurrentLocale(Locale $currentLocale)
    {
        $this->currentLocale = $currentLocale;
    }

    public function getCurrentLocale(): Locale
    {
        if($this->currentLocale === null) {
            $this->currentLocale = $this->getFallbackLocale();
        }

        return $this->currentLocale;
    }

    public function getAllLocales(): array
    {
        return $this->localeRepository->getAllLocales();
    }

    public function createLocale(CreateLocaleParameters $parameters): Locale
    {
        $language = $parameters->getLanguage();
        $region = $parameters->getRegion();

        if($this->hasLocale($language, $region)) {
            throw new DuplicateLocaleException(sprintf('Locale %s(%s) already exists', $language, $region));
        }

        $locale = new Locale($language, $region);

        $this->localeRepository->createLocale($locale);

        if($this->currentLocale === null) {
            $this->currentLocale = $locale;
        }

        return $locale;
    }

    public function editLocale(int $localeId, EditLocaleParameters $parameters): Locale
    {
        $locale = $this->getLocaleById($localeId);

        $locale
            ->setLanguage($parameters->getLanguage())
            ->setRegion($parameters->getRegion());

        $this->localeRepository->saveLocale($locale);

        return $locale;
    }

    public function getLocaleByRegion(string $region): Locale
    {
        $this->touchDefaultLocales();

        return $this->localeRepository->getLocale($region);
    }

    public function getLocaleById(int $localeId): Locale
    {
        $this->touchDefaultLocales();

        return $this->localeRepository->getById($localeId);
    }

    public function deleteLocale(int $localeId)
    {
        $this->localeRepository->deleteLocale(
            $this->localeRepository->getById($localeId)
        );
    }

    public function hasLocale(string $language, string $region): bool
    {
        return $this->localeRepository->hasLocale($language, $region);
    }

    public function getFallbackLocale(): Locale
    {
        $this->touchDefaultLocales();

        $locale = $this->defaultLocale;

        if($this->localeRepository->hasLocale($locale['language'], $locale['region'])) {
            return $this->localeRepository->getLocale($locale['region']);
        }else{
            throw new Exception("You don't have any locales in system. Please run hi-console.sh ru ru_RU to add one");
        }
    }
}
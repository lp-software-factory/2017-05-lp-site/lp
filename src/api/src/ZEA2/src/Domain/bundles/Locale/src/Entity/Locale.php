<?php
namespace ZEA2\Domain\Bundles\Locale\Entity;

use ZEA2\Domain\Markers\IdEntity\IdEntity;
use ZEA2\Domain\Markers\IdEntity\IdEntityTrait;
use ZEA2\Domain\Markers\JSONMetadataEntity\JSONMetadataEntity;
use ZEA2\Domain\Markers\JSONMetadataEntity\JSONMetadataEntityTrait;
use ZEA2\Domain\Markers\JSONSerializable;
use ZEA2\Domain\Markers\VersionEntity\VersionEntity;
use ZEA2\Domain\Markers\VersionEntity\VersionEntityTrait;
use ZEA2\Platform\Bundles\DoctrineORM\Type\LocalizedString\Value\ImmutableLocalizedString;

/**
 * @Entity(repositoryClass="ZEA2\Domain\Bundles\Locale\Repository\LocaleRepository")
 * @Table(name="locale")
 */
class Locale implements JSONSerializable, IdEntity, JSONMetadataEntity, VersionEntity
{
    public const LATEST_VERSION = '1.0.0';

    use IdEntityTrait, JSONMetadataEntityTrait, VersionEntityTrait;

    /**
     * @Column(name="language", type="string")
     * @var string
     */
    private $language;

    /**
     * @Column(name="region", type="string")
     * @var string
     */
    private $region;

    /**
     * @Column(name="title", type="i18n")
     * @var ImmutableLocalizedString
     */
    private $title;

    public function __construct(string $language, string $region)
    {
        $this->setCurrentVersion(self::LATEST_VERSION);

        $this->language = $language;
        $this->region = $region;
        $this->title = new ImmutableLocalizedString([[
            'region' => $region,
            'value' => sprintf('%s(%s)', $language, $region),
        ]]);
    }

    public function toJSON(array $options = []): array
    {
        return [
            'id' => $this->getIdNoFall(),
            'metadata' => array_merge($this->getMetadata(), [
                'version' => $this->getMetadataVersion()
            ]),
            'language' => $this->getLanguage(),
            'region' => $this->getRegion(),
            'title' => $this->getTitle()->toJSON($options),
        ];
    }

    public function getMetadataVersion(): string
    {
        return "1.0.0";
    }

    public function getLanguage(): string
    {
        return $this->language;
    }

    public function getRegion(): string
    {
        return $this->region;
    }

    public function setLanguage(string $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function setRegion(string $region): self
    {
        $this->region = $region;

        return $this;
    }

    public function getTitle(): ImmutableLocalizedString
    {
        return $this->title;
    }

    public function setTitle(ImmutableLocalizedString $title)
    {
        $this->title = $title;
    }
}
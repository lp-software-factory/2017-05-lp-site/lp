<?php
namespace ZEA2\Domain\Bundles\Locale\Service;

use ZEA2\Domain\Bundles\Locale\Config\SessionLocaleConfig;
use ZEA2\Domain\Bundles\Locale\Entity\Locale;

final class SessionLocaleService
{
    /** @var SessionLocaleConfig */
    private $config;

    /** @var LocaleService */
    private $service;

    public function __construct(SessionLocaleConfig $config, LocaleService $service)
    {
        $this->config = $config;
        $this->service = $service;
    }

    public function setCurrentLocale(Locale $locale)
    {
        $_SESSION[$this->config->getSessionKey()] = $locale->getRegion();
    }

    public function getCurrentLocale(): Locale
    {
        return $this->service->getLocaleByRegion($this->getCurrentRegion());
    }

    public function getCurrentRegion(): string
    {
        if(! isset($_SESSION[$this->config->getSessionKey()])) {
            return $this->config->getDefaultRegion();
        }else{
            return $_SESSION[$this->config->getSessionKey()];
        }
    }
}
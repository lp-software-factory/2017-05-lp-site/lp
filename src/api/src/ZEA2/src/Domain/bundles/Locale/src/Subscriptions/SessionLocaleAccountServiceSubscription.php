<?php
namespace ZEA2\Domain\Bundles\Locale\Subscriptions;

use ZEA2\Domain\Bundles\Account\Entity\Account;
use ZEA2\Domain\Bundles\Account\Service\AccountService;
use ZEA2\Domain\Bundles\Locale\Service\SessionLocaleService;
use ZEA2\Platform\Event\ZEA2EventEmitter;
use ZEA2\Platform\Event\ZEA2EventScript;

final class SessionLocaleAccountServiceSubscription implements ZEA2EventScript
{
    /** @var AccountService */
    private $accountService;

    /** @var SessionLocaleService */
    private $sessionLocaleService;

    public function __construct(AccountService $accountService, SessionLocaleService $sessionLocaleService)
    {
        $this->accountService = $accountService;
        $this->sessionLocaleService = $sessionLocaleService;
    }

    public function __invoke(ZEA2EventEmitter $eventEmitter)
    {
        $this->accountService->getEventEmitter()->on(AccountService::EVENT_SET_LOCALE, function(Account $account) {
            $this->sessionLocaleService->setCurrentLocale($account->getLocale());
        });
    }
}
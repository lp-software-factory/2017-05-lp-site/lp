<?php
namespace ZEA2\Domain\Bundles\Locale\Exceptions;

final class DuplicateLocaleException extends \Exception {}
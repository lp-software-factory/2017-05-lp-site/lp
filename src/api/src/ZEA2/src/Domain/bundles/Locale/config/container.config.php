<?php
namespace ZEA2\Domain\Bundles\Frontend;

use function DI\object;
use function DI\factory;
use function DI\get;

use ZEA2\Domain\Bundles\Locale\Config\LocaleConfig;
use ZEA2\Domain\Bundles\Locale\Config\SessionLocaleConfig;
use ZEA2\Domain\Bundles\Locale\Factory\Doctrine\LocaleRepositoryFactory;
use ZEA2\Domain\Bundles\Locale\Factory\LocaleConfigFactory;
use ZEA2\Domain\Bundles\Locale\Factory\SessionLocaleConfigFactory;
use ZEA2\Domain\Bundles\Locale\Repository\LocaleRepository;

return [
    SessionLocaleConfig::class => object()->constructorParameter('config', get('config.zea2.domain.locale.session')),
    LocaleRepository::class => factory(LocaleRepositoryFactory::class),
    LocaleConfig::class => factory(LocaleConfigFactory::class),
    SessionLocaleConfig::class => factory(SessionLocaleConfigFactory::class),
];
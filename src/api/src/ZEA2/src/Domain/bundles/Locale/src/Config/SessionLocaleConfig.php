<?php
namespace ZEA2\Domain\Bundles\Locale\Config;

final class SessionLocaleConfig
{
    /** @var string */
    private $sessionKey;

    /** @var string */
    private $defaultRegion;

    public function __construct($sessionKey, $defaultRegion)
    {
        $this->sessionKey = $sessionKey;
        $this->defaultRegion = $defaultRegion;
    }

    public function getSessionKey(): string
    {
        return $this->sessionKey;
    }

    public function getDefaultRegion(): string
    {
        return $this->defaultRegion;
    }
}
<?php
namespace ZEA2\Domain\Bundles\Locale;

use ZEA2\Domain\Bundles\Locale\Console\Command\CreateLocaleCommand;

return [
    'config.zea2.platform.console' => [
        'commands' => [
            'Locale' => [
                CreateLocaleCommand::class,
            ],
        ],
    ],
];
<?php
namespace ZEA2\Domain\Bundles\Locale\Functions;

use Cocur\Chain\Chain;

function localizedString(string $region, array $input): string
{
    return Chain::create($input)
        ->reduce(function(string $carry, array $next) use ($region) {
            return ($next['region'] === $region)
                ? $next['value']
                : $carry;
        }, '');
}
<?php
namespace ZEA2\Domain\Bundles\Locale\Factory\Doctrine;

use ZEA2\Domain\Bundles\Locale\Entity\Locale;
use ZEA2\Platform\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;

final class LocaleRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return Locale::class;
    }
}
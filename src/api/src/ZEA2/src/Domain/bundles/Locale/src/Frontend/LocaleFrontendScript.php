<?php
namespace ZEA2\Domain\Bundles\Locale\Frontend;

use ZEA2\Domain\Bundles\Locale\Entity\Locale;
use ZEA2\Domain\Bundles\Locale\Service\LocaleService;
use ZEA2\Domain\Bundles\Locale\Service\SessionLocaleService;
use ZEA2\Platform\Bundles\Frontend\Markers\FrontendBundleInjectable\FrontendScript;

final class LocaleFrontendScript implements FrontendScript
{
    /** @var LocaleService */
    private $localeService;

    /** @var SessionLocaleService */
    private $sessionLocaleService;

    public function __construct(LocaleService $localeService, SessionLocaleService $sessionLocaleService)
    {
        $this->localeService = $localeService;
        $this->sessionLocaleService = $sessionLocaleService;
    }

    public function __invoke(): array
    {
        return [
            'locale' => [
                'current' => $this->sessionLocaleService->getCurrentLocale()->toJSON(),
                'available' => array_map(function(Locale $locale) {
                    return $locale->toJSON();
                }, $this->localeService->getAllLocales()),
            ],
        ];
    }
}
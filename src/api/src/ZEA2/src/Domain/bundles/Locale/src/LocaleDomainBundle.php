<?php
namespace ZEA2\Domain\Bundles\Locale;

use ZEA2\Domain\Bundles\Locale\Frontend\LocaleFrontendScript;
use ZEA2\Domain\Bundles\Locale\Subscriptions\SessionLocaleAccountServiceSubscription;
use ZEA2\Domain\Bundles\Locale\Subscriptions\SessionLocaleAuthServiceSubscription;
use ZEA2\Platform\Bundles\Frontend\Markers\FrontendBundleInjectable\FrontendBundleInjectable;
use ZEA2\Platform\Bundles\Markers\ZEA2EventSubscriptionsBundle\ZEA2EventSubscriptionsBundle;
use ZEA2\Platform\Bundles\ZEA2Bundle;

final class LocaleDomainBundle extends ZEA2Bundle implements ZEA2EventSubscriptionsBundle, FrontendBundleInjectable
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getEventScripts(): array
    {
        return [
            SessionLocaleAccountServiceSubscription::class,
            SessionLocaleAuthServiceSubscription::class,
        ];
    }

    public function getFrontendScripts(): array
    {
        return [
            LocaleFrontendScript::class,
        ];
    }
}
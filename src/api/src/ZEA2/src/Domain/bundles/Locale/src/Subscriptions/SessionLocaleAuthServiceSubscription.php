<?php
namespace ZEA2\Domain\Bundles\Locale\Subscriptions;

use ZEA2\Domain\Bundles\Account\Entity\Account;
use ZEA2\Domain\Bundles\Auth\Service\AuthService;
use ZEA2\Domain\Bundles\Locale\Service\SessionLocaleService;
use ZEA2\Platform\Event\ZEA2EventEmitter;
use ZEA2\Platform\Event\ZEA2EventScript;

final class SessionLocaleAuthServiceSubscription implements ZEA2EventScript
{
    /** @var AuthService */
    private $authService;

    /** @var SessionLocaleService */
    private $sessionLocaleService;

    public function __construct(AuthService $authService, SessionLocaleService $sessionLocaleService)
    {
        $this->authService = $authService;
        $this->sessionLocaleService = $sessionLocaleService;
    }

    public function __invoke(ZEA2EventEmitter $eventEmitter)
    {
        $this->authService->getEventEmitter()->on(AuthService::EVENT_SIGN_IN, function(Account $account) {
            $this->sessionLocaleService->setCurrentLocale($account->getLocale());
        });
    }
}
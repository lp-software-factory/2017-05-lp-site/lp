<?php
namespace ZEA2\Domain\Bundles\Locale\Config;

final class LocaleConfig
{
    /** @var array */
    private $default;

    /** @var string[] */
    private $fallback;

    public function __construct(array $default, array $fallback)
    {
        $this->default = $default;
        $this->fallback = $fallback;
    }

    public function getDefault(): array
    {
        return $this->default;
    }

    public function getFallback(): array
    {
        return $this->fallback;
    }
}
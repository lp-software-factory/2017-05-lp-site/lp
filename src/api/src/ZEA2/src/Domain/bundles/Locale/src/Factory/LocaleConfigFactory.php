<?php
namespace ZEA2\Domain\Bundles\Locale\Factory;

use ZEA2\Domain\Bundles\Locale\Config\LocaleConfig;
use Interop\Container\ContainerInterface;

final class LocaleConfigFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $config = $container->get('config.zea2.domain.locale');

        return new LocaleConfig($config['default'], $config['fallback']);
    }
}
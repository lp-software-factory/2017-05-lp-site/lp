<?php
namespace ZEA2\Domain\Bundles\Locale\Exceptions;

final class LocaleNotFoundException extends \Exception {}
<?php
namespace ZEA2\Domain\Bundles\Avatar;

use function DI\factory;

use ZEA2\Domain\Bundles\Avatar\Config\AvatarConfig;
use ZEA2\Domain\Bundles\Avatar\Factory\AvatarConfigFactory;

return [
    AvatarConfig::class => factory(AvatarConfigFactory::class),
];
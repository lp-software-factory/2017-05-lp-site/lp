<?php
namespace ZEA2\Domain\Bundles\Avatar\Strategy;

use ZEA2\Domain\Bundles\Avatar\Exception\ImageTooSmallException;
use ZEA2\Domain\Bundles\Avatar\Exception\InvalidRatioException;
use Intervention\Image\Image;

abstract class SquareImageStrategy implements ImageStrategy
{
    const MIN_SIZE = 64;

    public function getDefaultSize(): int {
        return 64;
    }

    public function getSizes(): array {
        return [
            512,
            256,
            128,
            64,
            32,
            16
        ];
    }

    public function getRatio(): float {
        return 1;
    }

    public function validate(Image $origImage)
    {
        if($origImage->getWidth() !== $origImage->getHeight()) {
            throw new InvalidRatioException('Image should be a square');
        }

        if($origImage->getWidth() < self::MIN_SIZE) {
            throw new ImageTooSmallException(sprintf('Image should be more than %s px', self::MIN_SIZE));
        }
    }
}
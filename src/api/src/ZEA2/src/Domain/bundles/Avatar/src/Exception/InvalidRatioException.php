<?php
namespace ZEA2\Domain\Bundles\Avatar\Exception;

class InvalidRatioException extends ImageServiceException {}
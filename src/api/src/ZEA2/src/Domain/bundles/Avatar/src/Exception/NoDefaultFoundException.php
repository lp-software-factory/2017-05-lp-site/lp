<?php
namespace ZEA2\Domain\Bundles\Avatar\Exception;

class NoDefaultFoundException extends ImageServiceException {}
<?php
namespace ZEA2\Domain\Bundles\Avatar\Exception;

final class FileNotUploadedException extends \Exception {}
<?php
namespace ZEA2\Domain\Bundles\Avatar;

use function DI\object;
use function DI\get;

use ZEA2\Domain\Bundles\Avatar\Service\AvatarService;
use ZEA2\Domain\Bundles\Avatar\Service\Strategy\FileAvatarStrategy;

return [
    AvatarService::class => object()->constructorParameter('strategy', get(FileAvatarStrategy::class))
];
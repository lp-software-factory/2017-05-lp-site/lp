<?php
namespace ZEA2\Domain\Bundles\Avatar;

use ZEA2\Platform\Bundles\ZEA2Bundle;

final class AvatarDomainBundle extends ZEA2Bundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}
<?php
namespace ZEA2\Domain\Bundles\Avatar;

return [
    'config.zea2.domain.avatar' => [
        'font' => '%s/fonts/Roboto/Roboto-Medium.ttf',
    ]
];
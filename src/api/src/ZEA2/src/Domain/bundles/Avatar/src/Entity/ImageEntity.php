<?php
namespace ZEA2\Domain\Bundles\Avatar\Entity;

use ZEA2\Domain\Bundles\Avatar\Image\ImageCollection;

interface ImageEntity
{
    public function getImages(): ImageCollection;
    public function hasImages(): bool;
    public function setImages(ImageCollection $images);
}
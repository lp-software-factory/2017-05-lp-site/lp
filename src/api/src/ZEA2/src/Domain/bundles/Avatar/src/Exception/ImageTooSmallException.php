<?php
namespace ZEA2\Domain\Bundles\Avatar\Exception;

class ImageTooSmallException extends ImageServiceException {}
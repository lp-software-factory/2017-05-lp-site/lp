<?php
namespace ZEA2\Domain\Bundles\Avatar\Exception;

class InvalidCropException extends ImageServiceException {}
<?php
namespace ZEA2\Domain\Bundles\Avatar\Exception;

class ImageTooBigException extends ImageServiceException {}
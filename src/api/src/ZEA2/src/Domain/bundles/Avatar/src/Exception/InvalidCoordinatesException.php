<?php
namespace ZEA2\Domain\Bundles\Avatar\Exception;

final class InvalidCoordinatesException extends \Exception {}
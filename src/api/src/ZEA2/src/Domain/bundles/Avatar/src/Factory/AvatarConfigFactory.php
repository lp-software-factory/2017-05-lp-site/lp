<?php
namespace ZEA2\Domain\Bundles\Avatar\Factory;

use ZEA2\Domain\Bundles\Avatar\AvatarDomainBundle;
use ZEA2\Domain\Bundles\Avatar\Config\AvatarConfig;
use Interop\Container\ContainerInterface;

final class AvatarConfigFactory
{
    public function __invoke(AvatarDomainBundle $bundle, ContainerInterface $container)
    {
        $config = $container->get('config.zea2.domain.avatar');

        return new AvatarConfig(
            sprintf($config['font'], $bundle->getResourcesDir())
        );
    }
}
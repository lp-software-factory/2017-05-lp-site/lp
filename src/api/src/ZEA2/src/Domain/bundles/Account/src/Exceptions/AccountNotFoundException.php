<?php
namespace ZEA2\Domain\Bundles\Account\Exceptions;

final class AccountNotFoundException extends \Exception {}
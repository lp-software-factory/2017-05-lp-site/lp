<?php
namespace ZEA2\Domain\Bundles\Account\Exceptions;

final class DuplicateAccountException extends \Exception {}
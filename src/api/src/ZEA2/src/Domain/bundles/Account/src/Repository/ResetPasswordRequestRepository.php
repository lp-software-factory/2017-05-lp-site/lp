<?php
namespace ZEA2\Domain\Bundles\Account\Repository;

use Doctrine\ORM\EntityRepository;
use ZEA2\Domain\Bundles\Account\Entity\ResetPasswordRequest;
use ZEA2\Domain\Bundles\Auth\Exceptions\ResetPasswordRequestNotFoundException;

final class ResetPasswordRequestRepository extends EntityRepository
{
    public function createResetPasswordRequest(ResetPasswordRequest $resetPasswordRequest): int
    {
        while($this->hasWithSID($resetPasswordRequest->getSID())) {
            $resetPasswordRequest->regenerateSID();
        }

        $this->getEntityManager()->persist($resetPasswordRequest);
        $this->getEntityManager()->flush($resetPasswordRequest);

        return $resetPasswordRequest->getId();
    }

    public function saveResetPasswordRequest(ResetPasswordRequest $resetPasswordRequest)
    {
        $this->getEntityManager()->flush($resetPasswordRequest);
    }

    public function deleteResetPasswordRequest(ResetPasswordRequest $resetPasswordRequest)
    {
        $this->getEntityManager()->remove($resetPasswordRequest);
        $this->getEntityManager()->flush($resetPasswordRequest);
    }

    public function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

    public function getBySID(string $sid): ResetPasswordRequest
    {
        $result = $this->findOneBy(['sid' => $sid]);

        if($result instanceof ResetPasswordRequest) {
            return $result;
        }else{
            throw new ResetPasswordRequestNotFoundException(sprintf('ResetPasswordRequest `SID(%s)` not found', $sid));
        }
    }

    public function hasResetPasswordRequestsWithAcceptToken(string $acceptToken): bool
    {
        return $this->findOneBy(['acceptToken' => $acceptToken]) !== null;
    }

    public function getResetPasswordRequestsWithAcceptToken(string $acceptToken): ResetPasswordRequest
    {
        $result = $this->findOneBy(['acceptToken' => $acceptToken]);

        if($result instanceof ResetPasswordRequest) {
            return $result;
        }else{
            throw new ResetPasswordRequestNotFoundException(sprintf('Invalid access token'));
        }
    }

    public function getResetPasswordRequestsByEmail(string $email): array
    {
        return $this->findBy(['email' => $email]);
    }
}
<?php
namespace ZEA2\Domain\Bundles\Account\Entity;

use ZEA2\Domain\Markers\IdEntity\IdEntity;
use ZEA2\Domain\Markers\IdEntity\IdEntityTrait;
use ZEA2\Domain\Markers\JSONMetadataEntity\JSONMetadataEntity;
use ZEA2\Domain\Markers\JSONMetadataEntity\JSONMetadataEntityTrait;
use ZEA2\Domain\Markers\ModificationEntity\ModificationEntity;
use ZEA2\Domain\Markers\ModificationEntity\ModificationEntityTrait;
use ZEA2\Domain\Markers\SIDEntity\SIDEntity;
use ZEA2\Domain\Markers\SIDEntity\SIDEntityTrait;
use ZEA2\Domain\Markers\VersionEntity\VersionEntity;
use ZEA2\Domain\Markers\VersionEntity\VersionEntityTrait;
use ZEA2\Domain\Util\GenerateRandomString;

/**
 * @Entity(repositoryClass="ZEA2\Domain\Bundles\Account\Repository\ResetPasswordRequestRepository")
 * @Table(name="reset_password_request")
 */
class ResetPasswordRequest implements IdEntity, SIDEntity, ModificationEntity, JSONMetadataEntity,VersionEntity
{
    public const RESET_PASSWORD_TOKEN_LENGTH = 32;
    public const LATEST_VERSION = '1.0.0';

    use IdEntityTrait, SIDEntityTrait, ModificationEntityTrait, JSONMetadataEntityTrait, VersionEntityTrait;

    /**
     *@Column(name="accept_token", type="string")
     * @var string
     */
    private $acceptToken;

    /**
     * @OneToOne(targetEntity="ZEA2\Domain\Bundles\Account\Entity\Account")
     * @JoinColumn(name="account_id", referencedColumnName="id")
     * @var Account
     */
    private $account;

    public function __construct(Account $account)
    {
        $this->setCurrentVersion(self::LATEST_VERSION);
        $this->acceptToken = GenerateRandomString::generate(12);
        $this->account = $account;

        $this->regenerateSID();
        $this->initModificationEntity();
        $this->acceptToken = GenerateRandomString::generate(self::RESET_PASSWORD_TOKEN_LENGTH);
    }

    public function getMetadataVersion(): string
    {
        return "1.0.0";
    }

    public function getAccount(): Account
    {
        return $this->account;
    }

    public function getAcceptToken(): string
    {
        return $this->acceptToken;
    }

    public function isAcceptTokenValid(string $token): bool
    {
        return $this->acceptToken === $token;
    }
}
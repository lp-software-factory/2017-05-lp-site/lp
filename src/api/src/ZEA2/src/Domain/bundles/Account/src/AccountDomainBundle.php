<?php
namespace ZEA2\Domain\Bundles\Account;

use ZEA2\Platform\Bundles\ZEA2Bundle;

final class AccountDomainBundle extends ZEA2Bundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}
<?php
namespace ZEA2\Domain\Bundles\Account\Service;

use ZEA2\Domain\Bundles\Account\Entity\ResetPasswordRequest;
use ZEA2\Domain\Bundles\Mail\Service\MailService;

final class AccountMailService
{
    public const MAIL_TYPE_RESET_PASSWORD = 'hi.domain.account.reset-password';

    /** @var MailService */
    private $mailService;

    public function __construct(MailService $mailService)
    {
        $this->mailService = $mailService;
    }

    public function sendResetPasswordEmail(ResetPasswordRequest $request)
    {
        $context = [
            'reset_password_token' => $request->getAcceptToken(),
            'reset_password_email' => $request->getAccount()->getEmail(),
            'reset_password_profile' => $request->getAccount()->getCurrentProfile(),
        ];

        $message = \Swift_Message::newInstance()
            ->setTo($request->getAccount()->getEmail())
            ->setContentType('text/html');

        $message->setBody($this->mailService->getTemplateLocalized('@mail/api/zea2/domain/account/reset-password', $context));

        $this->mailService->sendNoReply($message, self::MAIL_TYPE_RESET_PASSWORD);

        return $message;
    }
}
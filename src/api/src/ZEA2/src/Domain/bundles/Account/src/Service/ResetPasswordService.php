<?php
namespace ZEA2\Domain\Bundles\Account\Service;

use Evenement\EventEmitter;
use ZEA2\Domain\Bundles\Account\Entity\Account;
use ZEA2\Domain\Bundles\Account\Entity\ResetPasswordRequest;
use ZEA2\Domain\Bundles\Account\Repository\ResetPasswordRequestRepository;

final class ResetPasswordService
{
    public const EVENT_RESET_PASSWORD = 'hi.domain.account.reset-password';
    public const EVENT_SEND_RESET_PASSWORD = 'hi.domain.account.send-reset-password';

    /** @var EventEmitter */
    private $eventEmitter;

    /** @var AccountService */
    private $accountService;

    /** @var ResetPasswordRequestRepository */
    private $resetPasswordRepository;

    /** @var PasswordHashService */
    private $passwordHashService;

    /** @var AccountMailService */
    private $mailService;

    public function __construct(
        AccountService $accountService,
        ResetPasswordRequestRepository $resetPasswordRepository,
        PasswordHashService $passwordHashService,
        AccountMailService $mailService
    ) {
        $this->eventEmitter = new EventEmitter();
        $this->accountService = $accountService;
        $this->resetPasswordRepository = $resetPasswordRepository;
        $this->passwordHashService = $passwordHashService;
        $this->mailService = $mailService;
    }

    public function getEventEmitter(): EventEmitter
    {
        return $this->eventEmitter;
    }

    public function resetPassword(string $acceptToken, string $newPassword): Account
    {
        $request = $this->resetPasswordRepository->getResetPasswordRequestsWithAcceptToken($acceptToken);
        $account = $request->getAccount();

        $this->accountService->setPassword($account, $newPassword);
        $this->eventEmitter->emit(self::EVENT_RESET_PASSWORD, [$account]);

        return $account;
    }

    public function sendResetPasswordRequest(string $email): ResetPasswordRequest
    {
        $account = $this->accountService->getAccountWithEmail($email);
        $request = new ResetPasswordRequest($account);

        $this->resetPasswordRepository->createResetPasswordRequest($request);
        $this->mailService->sendResetPasswordEmail($request);
        $this->eventEmitter->emit(self::EVENT_SEND_RESET_PASSWORD, [$account]);

        return $request;
    }
}
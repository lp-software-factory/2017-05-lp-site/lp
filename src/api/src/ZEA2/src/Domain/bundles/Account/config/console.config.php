<?php
namespace ZEA2\Domain\Bundles\Account;

use ZEA2\Domain\Bundles\Account\Console\Command\CreateAdminAccountCommand;
use ZEA2\Domain\Bundles\Account\Console\Command\PasswdAccountCommand;

return [
    'config.zea2.platform.console' => [
        'commands' => [
            'Account' => [
                CreateAdminAccountCommand::class,
                PasswdAccountCommand::class,
            ],
        ],
    ],
];
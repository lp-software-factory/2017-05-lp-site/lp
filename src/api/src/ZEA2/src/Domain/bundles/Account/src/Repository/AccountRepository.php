<?php
namespace ZEA2\Domain\Bundles\Account\Repository;

use Doctrine\ORM\EntityRepository;
use ZEA2\Domain\Bundles\Account\Entity\Account;
use ZEA2\Domain\Bundles\Account\Exceptions\AccountNotFoundException;
use ZEA2\Domain\Criteria\IdsCriteria;
use ZEA2\Domain\Criteria\SeekCriteria;

final class AccountRepository extends EntityRepository
{
    public function createAccount(Account $account): int
    {
        while($this->hasWithSID($account->getSID())) {
            $account->regenerateSID();
        }

        $this->getEntityManager()->persist($account);
        $this->getEntityManager()->flush($account);

        return $account->getId();
    }

    public function saveAccount(Account $account)
    {
        $this->getEntityManager()->flush($account);
    }

    public function deleteAccount(Account $account)
    {
        $this->getEntityManager()->remove($account);
        $this->getEntityManager()->flush($account);
    }

    public function listAccounts(SeekCriteria $seek): array
    {
        $query = $this->createQueryBuilder('c');
        $query->setFirstResult($seek->getOffset());
        $query->setMaxResults($seek->getLimit());

        return $query->getQuery()->execute();
    }

    public function getById(int $id): Account
    {
        $result = $this->find($id);

        if($result instanceof Account) {
            return $result;
        }else{
            throw new AccountNotFoundException(sprintf('Account `ID(%d)` not found', $id));
        }
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        $ids = $idsCriteria->getIds();
        $results = $this->findBy(['id' => $ids]);

        if(count($ids) !== count($results)) {
            throw new \Exception('Accounts with IDs (%s) not found', array_diff($ids, array_map(function(Account $account) {
                return $account->getId();
            }, $results)));
        }

        return $results;
    }

    public function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

    public function getBySID(string $sid): Account
    {
        $result = $this->findOneBy(['sid' => $sid]);

        if($result instanceof Account) {
            return $result;
        }else{
            throw new AccountNotFoundException(sprintf('Account `SID(%s)` not found', $sid));
        }
    }

    public function getAccountWithEmail(string $email): Account
    {
        $result = $this->findOneBy(['email' => $email]);

        if($result instanceof Account) {
            return $result;
        }else{
            throw new AccountNotFoundException(sprintf('Account `Email(%s)` not found', $email));
        }
    }

    public function hasAccountWithEmail(string $email): bool
    {
        return $this->findOneBy(['email' => $email]) !== null;
    }
}
<?php
namespace ZEA2\Domain\Bundles\Account\Console\Command;

use ZEA2\Domain\Bundles\Account\Service\AccountService;
use ZEA2\Domain\Bundles\Profile\Service\ProfileService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class PasswdAccountCommand extends Command
{
    /** @var AccountService */
    private $accountService;

    /** @var ProfileService */
    private $profileService;

    public function __construct(AccountService $accountService, ProfileService $profileService)
    {
        $this->accountService = $accountService;
        $this->profileService = $profileService;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('account:passwd')
            ->setDescription('Change account password')
            ->setDefinition(
                new InputDefinition([
                    new InputArgument('email', InputArgument::REQUIRED, 'Email'),
                    new InputArgument('password', InputArgument::REQUIRED, 'Password'),
                ])
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $email = $input->getArgument('email');
        $pass = $input->getArgument('password');

        $account = $this->accountService->getAccountWithEmail($email);

        $this->accountService->setPassword($account, $pass);

        $output->writeln(sprintf(
            'Account `%s %s` updated',
            $account->getCurrentProfile()->getFirstName(),
            $account->getCurrentProfile()->getLastName()
        ));
    }
}
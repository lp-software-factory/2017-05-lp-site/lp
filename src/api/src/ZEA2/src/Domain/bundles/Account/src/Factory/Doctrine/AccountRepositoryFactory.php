<?php
namespace ZEA2\Domain\Bundles\Account\Factory\Doctrine;

use ZEA2\Domain\Bundles\Account\Entity\Account;
use ZEA2\Platform\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;

final class AccountRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return Account::class;
    }
}
<?php
namespace ZEA2\Domain\Bundles\Account;

use function DI\factory;

use ZEA2\Domain\Bundles\Account\Factory\Doctrine\AccountRepositoryFactory;
use ZEA2\Domain\Bundles\Account\Factory\Doctrine\ResetPasswordRequestRepositoryFactory;
use ZEA2\Domain\Bundles\Account\Repository\AccountRepository;
use ZEA2\Domain\Bundles\Account\Repository\ResetPasswordRequestRepository;

return [
    AccountRepository::class => factory(AccountRepositoryFactory::class),
    ResetPasswordRequestRepository::class => factory(ResetPasswordRequestRepositoryFactory::class),
];
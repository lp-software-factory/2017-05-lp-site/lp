<?php
namespace ZEA2\Domain\Bundles\Account\Factory\Doctrine;

use ZEA2\Domain\Bundles\Account\Entity\ResetPasswordRequest;
use ZEA2\Platform\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;

final class ResetPasswordRequestRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return ResetPasswordRequest::class;
    }
}
<?php
namespace ZEA2\Domain\Bundles\Account\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use ZEA2\Domain\Bundles\Account\Exceptions\AccountHasNoRequestedProfileException;
use ZEA2\Domain\Bundles\Locale\Entity\Locale;
use ZEA2\Domain\Bundles\Profile\Entity\Profile;
use ZEA2\Domain\Markers\IdEntity\IdEntity;
use ZEA2\Domain\Markers\IdEntity\IdEntityTrait;
use ZEA2\Domain\Markers\JSONMetadataEntity\JSONMetadataEntity;
use ZEA2\Domain\Markers\JSONMetadataEntity\JSONMetadataEntityTrait;
use ZEA2\Domain\Markers\JSONSerializable;
use ZEA2\Domain\Markers\ModificationEntity\ModificationEntity;
use ZEA2\Domain\Markers\ModificationEntity\ModificationEntityTrait;
use ZEA2\Domain\Markers\SIDEntity\SIDEntity;
use ZEA2\Domain\Markers\SIDEntity\SIDEntityTrait;
use ZEA2\Domain\Bundles\OAuth2\Entity\OAuth2Account;
use ZEA2\Domain\Bundles\OAuth2\Entity\OAuth2Provider;
use ZEA2\Domain\Markers\VersionEntity\VersionEntity;
use ZEA2\Domain\Markers\VersionEntity\VersionEntityTrait;
use Zend\Validator\ValidatorChain;

/**
 * @Entity(repositoryClass="ZEA2\Domain\Bundles\Account\Repository\AccountRepository")
 * @Table(name="account")
 */
class Account implements JSONSerializable, IdEntity, SIDEntity, ModificationEntity, JSONMetadataEntity,VersionEntity
{
    public const LATEST_VERSION = '1.0.0';

    public const APP_ACCESS_PROTECTED = 'protected';
    public const APP_ACCESS_ADMIN = 'admin';

    use IdEntityTrait, SIDEntityTrait, ModificationEntityTrait, JSONMetadataEntityTrait, VersionEntityTrait;

    /**
     * @OneToMany(targetEntity="ZEA2\Domain\Bundles\OAuth2\Entity\OAuth2Account", mappedBy="account", cascade={"all"})
     * @var Collection
     */
    private $oAuth2Accounts;

    /**
     * @OneToOne(targetEntity="ZEA2\Domain\Bundles\Locale\Entity\Locale")
     * @JoinColumn(name="locale_id", referencedColumnName="id")
     * @var Locale
     */
    private $locale;

    /**
     * @Column(type="json_array", name="app_access")
     * @var array
     */
    private $appAccess = [];

    /**
     * @OneToMany(targetEntity="ZEA2\Domain\Bundles\Profile\Entity\Profile", mappedBy="account", cascade={"all"})
     * @var Collection
     */
    private $profiles;

    /**
     * @Column(type="string", name="email")
     * @var string
     */
    private $email = '';

    /**
     * @Column(type="string", name="phone")
     * @var string|null
     */
    private $phone;

    /**
     * @Column(type="string", name="password")
     * @var string
     */
    private $password;

    public function __construct(string $email, array $appAccess = [self::APP_ACCESS_PROTECTED], Locale $locale)
    {
        $this->setCurrentVersion(self::LATEST_VERSION);

        $emailValidator = (new ValidatorChain())
            ->attach(new \Zend\Validator\NotEmpty())
            ->attach(new \Zend\Validator\StringLength(['min' => 3, 'max' => '127']))
            ->attach(new \Zend\Validator\EmailAddress());

        if(! $emailValidator->isValid($email)) {
            throw new \Exception(sprintf('Invalid email `%s`', $email));
        }

        array_map(function(string $app) {
            $this->grantAccess($app);
        }, $appAccess);

        $this->email = $email;
        $this->oAuth2Accounts = new ArrayCollection();
        $this->profiles = new ArrayCollection();
        $this->regenerateSID();
        $this->locale = $locale;
        $this->initModificationEntity();
    }

    public function toJSON(array $options = []): array
    {
        $result = [
            'id' => $this->getId(),
            'sid' => $this->getSID(),
            'date_created_at' => $this->getDateCreatedAt()->format(\DateTime::RFC2822),
            'last_updated_on' => $this->getLastUpdatedOn()->format(\DateTime::RFC2822),
            'metadata' => array_merge($this->getMetadata(), [
                'version' => $this->getMetadataVersion(),
            ]),
            'version' => $this->getVersion(),
            'app_access' => $this->appAccess,
            'email' => $this->getEmail(),
            'phone' => [
                'has' => $this->hasPhone(),
            ],
        ];

        if($this->hasPhone()) {
            $result['phone']['value'] = $this->getPhone();
        }

        return $result;
    }

    public function getMetadataVersion(): string
    {
        return "1.0.0";
    }

    public function getOAuth2Accounts(): Collection
    {
        return $this->oAuth2Accounts;
    }

    public function hasOAuth2AccountOfProvider(OAuth2Provider $provider): bool
    {
        /** @var OAuth2Account $oAuth2Account */
        foreach($this->oAuth2Accounts->toArray() as $oAuth2Account) {
            if($oAuth2Account->getProvider() === $provider) {
                return true;
            }
        }

        return false;
    }

    public function getLocale(): Locale
    {
        return $this->locale;
    }

    public function setLocale(Locale $locale): self
    {
        $this->locale = $locale;
        $this->markAsUpdated();

        return $this;
    }

    public function attachProfile(Profile $profile): self
    {
        $this->profiles->add($profile);
        $this->markAsUpdated();

        return $this;
    }

    public function detachProfile(Profile $profile): self
    {
        $this->profiles->removeElement($profile);
        $this->markAsUpdated();

        return $this;
    }

    public function getProfileWithId(int $id): Profile
    {
        /** @var Profile $profile */
        foreach($this->getProfiles() as $profile) {
            if($profile->isPersisted()) {
                if($profile->getId() === $id) {
                    return $profile;
                }
            }
        }

        throw new AccountHasNoRequestedProfileException(sprintf('Profile with id `%d` not found', $id));
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function isPasswordEquals(string $compare): bool
    {
        return password_verify($compare, $this->password);
    }

    /**
     * @return Profile[]
     */
    public function getProfiles(): array
    {
        return $this->profiles->getValues();
    }

    public function getCurrentProfile(): Profile
    {
        return $this->profiles->first();
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function hasPhone(): bool
    {
        return $this->phone !== null;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function changePhone(string $phone = null): self
    {
        $this->phone = $phone;
        $this->markAsUpdated();

        return $this;
    }

    public function hasAccess(string $app): bool
    {
        return in_array($app, $this->appAccess);
    }

    public function grantAccess(string $app): self
    {
        if(! in_array($app, $this->appAccess)) {
            $this->appAccess[] = $app;
        }

        $this->markAsUpdated();

        return $this;
    }

    public function withdrawAccess(string $app): self
    {
        $this->appAccess = array_filter($this->appAccess, function($input) use ($app) {
            return $input !== $app;
        });

        $this->markAsUpdated();

        return $this;
    }
}
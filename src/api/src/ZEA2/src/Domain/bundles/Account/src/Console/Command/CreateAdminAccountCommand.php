<?php
namespace ZEA2\Domain\Bundles\Account\Console\Command;

use ZEA2\Domain\Bundles\Account\Parameters\AccountRegisterParameters;
use ZEA2\Domain\Bundles\Account\Service\AccountRegistrationService;
use ZEA2\Domain\Bundles\Profile\Parameters\CreateProfileParameters;
use ZEA2\Domain\Bundles\Profile\Service\ProfileService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class CreateAdminAccountCommand extends Command
{
    /** @var AccountRegistrationService */
    private $accountRegistrationService;

    /** @var ProfileService */
    private $profileService;

    public function __construct(AccountRegistrationService $accountRegistrationService, ProfileService $profileService)
    {
        $this->accountRegistrationService = $accountRegistrationService;
        $this->profileService = $profileService;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('account:create:admin')
            ->setDescription('Create new admin account')
            ->setDefinition(
                new InputDefinition([
                    new InputArgument('email', InputArgument::REQUIRED, 'Email for new admin account'),
                    new InputArgument('password', InputArgument::REQUIRED, 'Password'),
                    new InputArgument('firstName', InputArgument::OPTIONAL, 'First Name', 'ROOT'),
                    new InputArgument('lastName', InputArgument::OPTIONAL, 'Last Name', 'Admin'),
                ])
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $email = $input->getArgument('email');
        $pass = $input->getArgument('password');

        $account = $this->accountRegistrationService->registerAdminAccount(new AccountRegisterParameters($email, $pass, new CreateProfileParameters(
            $input->getArgument('firstName'),
            $input->getArgument('lastName')
        )));

        $output->writeln(sprintf(
            'Admin account `%s %s` created',
            $account->getCurrentProfile()->getFirstName(),
            $account->getCurrentProfile()->getLastName()
        ));
    }
}
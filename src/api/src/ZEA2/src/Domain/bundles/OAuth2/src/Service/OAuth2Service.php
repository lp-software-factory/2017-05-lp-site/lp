<?php
namespace ZEA2\Domain\Bundles\OAuth2\Service;

use ZEA2\Domain\Bundles\Account\Entity\Account;
use ZEA2\Domain\Bundles\Account\Parameters\AccountRegisterParameters;
use ZEA2\Domain\Bundles\Account\Service\AccountRegistrationService;
use ZEA2\Domain\Bundles\Account\Service\AccountService;
use ZEA2\Domain\Bundles\OAuth2\Entity\OAuth2Account;
use ZEA2\Domain\Bundles\OAuth2\Entity\OAuth2Provider;
use ZEA2\Domain\Bundles\OAuth2\Exceptions\DuplicateOAth2AccountException;
use ZEA2\Domain\Bundles\OAuth2\Factory\LeagueProviderFactory;
use ZEA2\Domain\Bundles\OAuth2\Request\OAuth2Request;
use ZEA2\Domain\Bundles\OAuth2\Repository\OAuth2AccountRepository;
use ZEA2\Domain\Bundles\OAuth2\Repository\OAuth2ProviderRepository;
use ZEA2\Domain\Bundles\OAuth2\Request\OAuth2RegistrationRequest;
use ZEA2\Domain\Bundles\Profile\Service\ProfileService;
use ZEA2\Domain\Util\GenerateRandomString;
use League\OAuth2\Client\Provider\AbstractProvider;

final class OAuth2Service
{
    private const OAUTH2_PASSWORD_LENGTH = 32;

    /** @var OAuth2AccountRepository */
    private $repository;

    /** @var AccountService */
    private $accountService;

    /** @var AccountRegistrationService */
    private $accountRegistrationService;

    /** @var OAuth2ProviderService */
    private $oAuth2ProviderRepository;

    /** @var LeagueProviderFactory */
    private $leagueProviderFactory;

    /** @var ProfileService */
    private $profileService;

    public function __construct(
        AccountService $accountService,
        AccountRegistrationService $accountRegistrationService,
        OAuth2AccountRepository $repository,
        OAuth2ProviderRepository $providerRepository,
        LeagueProviderFactory $leagueProviderFactory,
        ProfileService $profileService
    ) {
        $this->repository = $repository;
        $this->accountService = $accountService;
        $this->accountRegistrationService = $accountRegistrationService;
        $this->providerRepository = $providerRepository;
        $this->leagueProviderFactory = $leagueProviderFactory;
        $this->profileService = $profileService;
    }

    public function getOAuth2Provider(string $provider): OAuth2Provider
    {
        return $this->oAuth2ProviderRepository->getProviderByCode($provider);
    }

    public function getOAuth2LeagueProvider(string $provider): AbstractProvider
    {
        return $this->leagueProviderFactory->createLeagueProviderFromCode(
            $provider
        );
    }

    public function signIn(OAuth2Request $request): OAuth2Account
    {
        $resourceOwnerId = $request->getResourceOwnerId();
        $oAuth2Provider = $this->providerRepository->getByProviderCode($request->getCode());

        if($this->hasOAuth2AccountWithResourceOwnerId($oAuth2Provider, $resourceOwnerId)) {
            $oauth2Account = $this->getOAuth2AccountWithResourceOwnerId($oAuth2Provider, $resourceOwnerId);
        }else{
            $registrationRequest = $request->createRegistrationRequest();

            $account = (function(OAuth2RegistrationRequest $registrationRequest, OAuth2Request $request) : Account {
                if(! $this->accountService->hasAccountWithEmail($registrationRequest->getEmail())) {
                    return $this->accountRegistrationService->registerUserAccount(new AccountRegisterParameters(
                        $registrationRequest->getEmail(),
                        GenerateRandomString::generate(self::OAUTH2_PASSWORD_LENGTH),
                        $request->getCreateProfileParameters()
                    ));
                }else{
                    return $this->accountService->getAccountWithEmail($registrationRequest->getEmail());
                }
            })($registrationRequest, $request);

            $oauth2Account = $this->createOAuth2Account(
                $resourceOwnerId,
                $account,
                $oAuth2Provider,
                $request->getAccessToken()
            );
        }

        return $oauth2Account;
    }

    public function createOAuth2Account(string $resourceOwnerId, Account $account, OAuth2Provider $provider, string $refreshToken): OAuth2Account
    {
        $oAuth2Account = new OAuth2Account($account, $provider, $refreshToken, $resourceOwnerId);

        if($this->hasOAuth2AccountWithResourceOwnerId($provider, $resourceOwnerId)) {
            throw new DuplicateOAth2AccountException(sprintf('Duplicate OAuth2 account attempt'));
        }

        $this->repository->createOAuth2Account($oAuth2Account);

        return $oAuth2Account;
    }

    public function hasOAuth2AccountWithResourceOwnerId(OAuth2Provider $provider, string $resourceOwnerId): bool
    {
        return $this->repository->hasOAuth2AccountWithResourceOwnerId($provider, $resourceOwnerId);
    }

    public function getOAuth2AccountWithResourceOwnerId(OAuth2Provider $provider, string $resourceOwnerId): OAuth2Account
    {
        return $this->repository->getOAuth2AccountWithResourceOwnerId($provider, $resourceOwnerId);
    }
}
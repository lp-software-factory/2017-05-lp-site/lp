<?php
namespace ZEA2\Domain\Bundles\OAuth2\Request\Types;

use Aego\OAuth2\Client\Provider\Yandex;
use Aego\OAuth2\Client\Provider\YandexResourceOwner;
use ZEA2\Domain\Bundles\OAuth2\Request\OAuth2RegistrationRequest;
use ZEA2\Domain\Bundles\Profile\Entity\Profile;
use League\OAuth2\Client\Provider\AbstractProvider;
use ZEA2\Domain\Bundles\Profile\Parameters\CreateProfileParameters;
use ZEA2\Domain\Exceptions\NotImplementedException;

final class YandexOAuth2Request extends AbstractOAuth2Request
{
    public const PROVIDER_CODE = 'yandex';

    public function getCode(): string
    {
        return self::PROVIDER_CODE;
    }

    public function getLeagueProviderClassName(): string
    {
        return Yandex::class;
    }

    public static function createLeagueProvider(array $options = [], array $collaborators = []): AbstractProvider
    {
        return new Yandex($options, $collaborators);
    }

    public function createRegistrationRequest(): OAuth2RegistrationRequest
    {
        $resourceOwner = $this->getResourceOwner();

        if($resourceOwner instanceof YandexResourceOwner) {
            return new OAuth2RegistrationRequest($resourceOwner, $resourceOwner->getEmail());
        }else{
            throw new \Exception('Invalid resourceOwner');
        }
    }

    public function getCreateProfileParameters(): CreateProfileParameters
    {
        throw new NotImplementedException();
    }

    public function setupProfile(Profile $profile)
    {
        $resourceOwner = $this->getResourceOwner();

        if($resourceOwner instanceof YandexResourceOwner) {
            $profile->changeName(
                $resourceOwner->getFirstName(),
                $resourceOwner->getLastName()
            );
        }else{
            throw new \Exception('Invalid resourceOwner');
        }
    }
}
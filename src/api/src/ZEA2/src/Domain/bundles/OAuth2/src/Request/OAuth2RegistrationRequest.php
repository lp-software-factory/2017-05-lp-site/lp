<?php
namespace ZEA2\Domain\Bundles\OAuth2\Request;

use League\OAuth2\Client\Provider\ResourceOwnerInterface;

final class OAuth2RegistrationRequest
{
    /** @var string */
    private $email;

    /** @var  ResourceOwnerInterface */
    private $resourceOwner;

    public function __construct(ResourceOwnerInterface $resourceOwner, string $email)
    {
        $this->email = $email;
        $this->resourceOwner = $resourceOwner;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getResourceOwner():ResourceOwnerInterface
    {
        return $this->resourceOwner;
    }
}
<?php
namespace ZEA2\Domain\Bundles\OAuth2;

use ZEA2\Platform\Bundles\ZEA2Bundle;

final class OAuth2DomainBundle extends ZEA2Bundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}
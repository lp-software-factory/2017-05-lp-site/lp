<?php
namespace ZEA2\Domain\Bundles\OAuth2\Factory\Doctrine;

use ZEA2\Domain\Bundles\OAuth2\Entity\OAuth2Account;
use ZEA2\Platform\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;

final class OAuth2AccountRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return OAuth2Account::class;
    }
}
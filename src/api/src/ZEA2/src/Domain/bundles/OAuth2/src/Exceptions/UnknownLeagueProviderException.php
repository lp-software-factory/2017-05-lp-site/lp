<?php
namespace ZEA2\Domain\Bundles\OAuth2\Exceptions;

final class UnknownLeagueProviderException extends \Exception {}
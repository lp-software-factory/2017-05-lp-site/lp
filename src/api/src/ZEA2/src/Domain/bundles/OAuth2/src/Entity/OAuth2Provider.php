<?php
namespace ZEA2\Domain\Bundles\OAuth2\Entity;

use ZEA2\Domain\Markers\IdEntity\IdEntity;
use ZEA2\Domain\Markers\IdEntity\IdEntityTrait;
use ZEA2\Domain\Markers\JSONMetadataEntity\JSONMetadataEntity;
use ZEA2\Domain\Markers\JSONMetadataEntity\JSONMetadataEntityTrait;
use ZEA2\Domain\Markers\JSONSerializable;
use ZEA2\Domain\Markers\ModificationEntity\ModificationEntity;
use ZEA2\Domain\Markers\ModificationEntity\ModificationEntityTrait;
use ZEA2\Domain\Markers\SIDEntity\SIDEntity;
use ZEA2\Domain\Markers\SIDEntity\SIDEntityTrait;
use ZEA2\Domain\Markers\VersionEntity\VersionEntity;
use ZEA2\Domain\Markers\VersionEntity\VersionEntityTrait;

/**
 * @Entity(repositoryClass="ZEA2\Domain\Bundles\OAuth2\Repository\OAuth2ProviderRepository")
 * @Table(name="oauth2_provider")
 */
class OAuth2Provider implements JSONSerializable, IdEntity, SIDEntity, JSONMetadataEntity, ModificationEntity, VersionEntity
{
    public const LATEST_VERSION = '1.0.0';

    use IdEntityTrait, SIDEntityTrait, JSONMetadataEntityTrait, ModificationEntityTrait, VersionEntityTrait;

    /**
     * @Column(name="code", type="string")
     * @var string
     */
    private $code;

    /**
     * @Column(name="handler", type="string")
     * @var string
     */
    private $handler;

    /**
     * @Column(name="config", type="json_array")
     * @var array
     */
    private $config;

    public function __construct(string $code, string $handler, array $config)
    {
        $this->setCurrentVersion(self::LATEST_VERSION);

        $this->code = $code;
        $this->handler = $handler;
        $this->config = $config;

        $this->regenerateSID();
        $this->initModificationEntity();
    }

    public function toJSON(array $options = []): array
    {
        return [
            'id' => $this->getIdNoFall(),
            'sid' => $this->getSID(),
            'metadata' => array_merge($this->getMetadata(), [
                'version' => $this->getMetadataVersion(),
            ]),
            'date_created_at' => $this->getDateCreatedAt()->format(\DateTime::RFC2822),
            'last_updated_on' => $this->getLastUpdatedOn()->format(\DateTime::RFC2822),
            'code' => $this->getCode(),
            'handler' => $this->getHandler(),
            'config' => $this->getConfig(),
        ];
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getHandler(): string
    {
        return $this->handler;
    }

    public function getConfig(): array
    {
        return $this->config;
    }

    public function setConfig(array $config): self
    {
        $this->config = $config;
        $this->markAsUpdated();

        return $this;
    }

    public function getMetadataVersion(): string
    {
        return "1.0.0";
    }
}
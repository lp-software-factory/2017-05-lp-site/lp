<?php
namespace ZEA2\Domain\Bundles\OAuth2\Exceptions;

final class OAuth2ProviderNotFoundException extends \Exception
{}
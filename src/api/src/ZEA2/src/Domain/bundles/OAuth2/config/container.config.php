<?php
namespace ZEA2\Domain\Bundles\Mail;

use function DI\object;
use function DI\factory;
use function DI\get;

use ZEA2\Domain\Bundles\OAuth2\Config\ListAvailableLeagueProviders;
use ZEA2\Domain\Bundles\OAuth2\Factory\Doctrine\OAuth2AccountRepositoryFactory;
use ZEA2\Domain\Bundles\OAuth2\Factory\Doctrine\OAuth2ProviderRepositoryFactory;
use ZEA2\Domain\Bundles\OAuth2\Repository\OAuth2AccountRepository;
use ZEA2\Domain\Bundles\OAuth2\Repository\OAuth2ProviderRepository;

return [
    ListAvailableLeagueProviders::class => object()->constructorParameter('available', get('config.zea2.oauth2.league.available')),
    OAuth2AccountRepository::class => factory(OAuth2AccountRepositoryFactory::class),
    OAuth2ProviderRepository::class => factory(OAuth2ProviderRepositoryFactory::class),
];
<?php
namespace ZEA2\Domain\Bundles\OAuth2\Request\Types;

use ZEA2\Domain\Bundles\OAuth2\Request\OAuth2RegistrationRequest;
use ZEA2\Domain\Bundles\Profile\Entity\Profile;
use J4k\OAuth2\Client\Provider\User as VkontakteUser;
use J4k\OAuth2\Client\Provider\Vkontakte;
use League\OAuth2\Client\Provider\AbstractProvider;
use ZEA2\Domain\Bundles\Profile\Parameters\CreateProfileParameters;
use ZEA2\Domain\Exceptions\NotImplementedException;

final class VkontakteOAuth2Request extends AbstractOAuth2Request
{
    public const PROVIDER_CODE = 'vkontakte';

    public function getCode(): string
    {
        return self::PROVIDER_CODE;
    }

    public function getLeagueProviderClassName(): string
    {
        return Vkontakte::class;
    }

    public static function createLeagueProvider(array $options = [], array $collaborators = []): AbstractProvider
    {
        return new Vkontakte($options, $collaborators);
    }

    public function createRegistrationRequest(): OAuth2RegistrationRequest
    {
        $resourceOwner = $this->getResourceOwner();

        if($resourceOwner instanceof VkontakteUser) {
            return new OAuth2RegistrationRequest($resourceOwner, $this->getAccessToken()->getValues()['email']);
        }else{
            throw new \Exception('Invalid resourceOwner');
        }
    }

    public function getCreateProfileParameters(): CreateProfileParameters
    {
        throw new NotImplementedException();
    }

    public function setupProfile(Profile $profile)
    {
        $resourceOwner = $this->getResourceOwner();

        if($resourceOwner instanceof VkontakteUser) {
            $profile->changeName(
                $resourceOwner->getFirstName(),
                $resourceOwner->getLastName()
            );
        }else{
            throw new \Exception('Invalid resourceOwner');
        }
    }
}
<?php
namespace ZEA2\Domain\Bundles\OAuth2\Request\Types;

use ZEA2\Domain\Bundles\OAuth2\Request\OAuth2Request;
use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Provider\ResourceOwnerInterface;
use League\OAuth2\Client\Token\AccessToken;

abstract class AbstractOAuth2Request implements OAuth2Request
{
    /** @var AbstractProvider */
    protected $leagueProvider;

    /** @var AccessToken */
    protected $accessToken;

    /** @var ResourceOwnerInterface */
    protected $resourceOwner;

    public function __construct(AbstractProvider $leagueProvider, AccessToken $accessToken)
    {
        $this->leagueProvider = $leagueProvider;
        $this->accessToken = $accessToken;
    }

    public function getResourceOwner(): ResourceOwnerInterface
    {
        $this->fetchResourceOwner();

        return $this->resourceOwner;
    }

    public function getResourceOwnerId(): string
    {
        $this->fetchResourceOwner();

        return $this->resourceOwner->getId();
    }

    public function getAccessToken(): AccessToken
    {
        return $this->accessToken;
    }

    private function fetchResourceOwner()
    {
        if ($this->resourceOwner === null) {
            $this->resourceOwner = $this->leagueProvider->getResourceOwner($this->accessToken);
        }
    }
}
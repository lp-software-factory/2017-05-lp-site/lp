<?php
namespace ZEA2\Domain\Bundles\OAuth2\Entity;

use ZEA2\Domain\Bundles\Account\Entity\Account;
use ZEA2\Domain\Markers\IdEntity\IdEntity;
use ZEA2\Domain\Markers\IdEntity\IdEntityTrait;
use ZEA2\Domain\Markers\JSONMetadataEntity\JSONMetadataEntity;
use ZEA2\Domain\Markers\JSONMetadataEntity\JSONMetadataEntityTrait;
use ZEA2\Domain\Markers\JSONSerializable;
use ZEA2\Domain\Markers\ModificationEntity\ModificationEntity;
use ZEA2\Domain\Markers\ModificationEntity\ModificationEntityTrait;
use ZEA2\Domain\Markers\SIDEntity\SIDEntity;
use ZEA2\Domain\Markers\SIDEntity\SIDEntityTrait;
use ZEA2\Domain\Markers\VersionEntity\VersionEntity;
use ZEA2\Domain\Markers\VersionEntity\VersionEntityTrait;

/**
 * @Entity(repositoryClass="ZEA2\Domain\Bundles\OAuth2\Repository\OAuth2AccountRepository")
 * @Table(name="oauth2_account")
 */
class OAuth2Account implements JSONSerializable, IdEntity, SIDEntity, ModificationEntity, JSONMetadataEntity, VersionEntity
{
    public const LATEST_VERSION = '1.0.0';

    use IdEntityTrait, SIDEntityTrait, ModificationEntityTrait, JSONMetadataEntityTrait, VersionEntityTrait;

    /**
     * @ManyToOne(targetEntity="ZEA2\Domain\Bundles\Account\Entity\Account")
     * @JoinColumn(name="account_id", referencedColumnName="id")
     * @var Account
     */
    private $account;

    /**
     * @ManyToOne(targetEntity="ZEA2\Domain\Bundles\OAuth2\Entity\OAuth2Provider")
     * @JoinColumn(name="provider_id", referencedColumnName="id")
     * @var OAuth2Provider
     */
    private $provider;

    /**
     * @Column(name="refresh_token", type="string")
     * @var string
     */
    private $refreshToken;

    /**
     * @Column(name="resource_owner_id", type="string")
     * @var string
     */
    private $resourceOwnerId;

    public function __construct(Account $account, OAuth2Provider $provider, $refreshToken, $resourceOwnerId)
    {
        $this->setCurrentVersion(self::LATEST_VERSION);

        $this->account = $account;
        $this->provider = $provider;
        $this->refreshToken = $refreshToken;
        $this->resourceOwnerId = $resourceOwnerId;

        $this->regenerateSID();
        $this->initModificationEntity();
    }

    public function getMetadataVersion(): string
    {
        return "1.0.0";
    }

    public function toJSON(array $options = []): array
    {
        return [
            'id' => $this->getIdNoFall(),
            'sid' => $this->getSID(),
            'metadata' => array_merge($this->getMetadata(), [
                'version' => $this->getMetadataVersion(),
            ]),
            'date_created_at' => $this->getDateCreatedAt()->format(\DateTime::RFC2822),
            'last_updated_on' => $this->getLastUpdatedOn()->format(\DateTime::RFC2822),
            'account_id' => $this->getAccount()->getId(),
            'provider' => $this->getProvider()->toJSON($options, $options),
            'resource_owner_id' => $this->getResourceOwnerId(),
        ];
    }

    public function getAccount(): Account
    {
        return $this->account;
    }

    public function getProvider(): OAuth2Provider
    {
        return $this->provider;
    }

    public function getRefreshToken(): string
    {
        return $this->refreshToken;
    }

    public function getResourceOwnerId(): string
    {
        return $this->resourceOwnerId;
    }
}
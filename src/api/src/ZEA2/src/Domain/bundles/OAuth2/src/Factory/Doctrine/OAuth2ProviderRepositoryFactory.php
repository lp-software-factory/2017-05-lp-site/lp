<?php
namespace ZEA2\Domain\Bundles\OAuth2\Factory\Doctrine;

use ZEA2\Platform\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;
use ZEA2\Domain\Bundles\OAuth2\Entity\OAuth2Provider;

final class OAuth2ProviderRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return OAuth2Provider::class;
    }
}
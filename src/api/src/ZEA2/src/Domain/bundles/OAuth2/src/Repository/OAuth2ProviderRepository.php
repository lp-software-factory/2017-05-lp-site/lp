<?php
namespace ZEA2\Domain\Bundles\OAuth2\Repository;

use Doctrine\ORM\EntityRepository;
use ZEA2\Domain\Criteria\IdsCriteria;
use ZEA2\Domain\Bundles\OAuth2\Entity\OAuth2Provider;
use ZEA2\Domain\Bundles\OAuth2\Exceptions\OAuth2ProviderNotFoundException;

final class OAuth2ProviderRepository extends EntityRepository
{
    public function createOAuth2Provider(OAuth2Provider $oAuth2Provider): int
    {
        while($this->hasWithSID($oAuth2Provider->getSID())) {
            $oAuth2Provider->regenerateSID();
        }

        $this->getEntityManager()->persist($oAuth2Provider);
        $this->getEntityManager()->flush($oAuth2Provider);

        return $oAuth2Provider->getId();
    }

    public function saveOAuth2Provider(OAuth2Provider $oAuth2Provider)
    {
        $this->getEntityManager()->flush($oAuth2Provider);
    }

    public function saveOAuth2ProviderEntities(array $entities)
    {
        foreach($entities as $entity) {
            $this->saveOAuth2Provider($entity);
        }
    }

    public function deleteOAuth2Provider(OAuth2Provider $oAuth2Provider)
    {
        $this->getEntityManager()->remove($oAuth2Provider);
        $this->getEntityManager()->flush($oAuth2Provider);
    }

    public function getAll(): array
    {
        return $this->findAll();
    }

    public function getById(int $id): OAuth2Provider
    {
        $result = $this->find($id);

        if($result instanceof OAuth2Provider) {
            return $result;
        }else{
            throw new OAuth2ProviderNotFoundException(sprintf('OAuth2Provider `ID(%d)` not found', $id));
        }
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        $ids = $idsCriteria->getIds();
        $results = $this->findBy(['id' => $ids]);

        if(count($ids) !== count($results)) {
            throw new \Exception('OAuth2Providers with IDs (%s) not found',
                array_diff($ids, array_map(function(OAuth2Provider $oAuth2Provider) {
                    return $oAuth2Provider->getId();
                }, $results)));
        }

        return $results;
    }

    public function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

    public function getBySID(string $sid): OAuth2Provider
    {
        $result = $this->findOneBy(['sid' => $sid]);

        if($result instanceof OAuth2Provider) {
            return $result;
        }else{
            throw new OAuth2ProviderNotFoundException(sprintf('OAuth2Provider `SID(%s)` not found', $sid));
        }
    }

    public function getByProviderCode(string $code): OAuth2Provider
    {
        $result = $this->findOneBy(['code' => $code]);

        if($result instanceof OAuth2Provider) {
            return $result;
        }else{
            throw new OAuth2ProviderNotFoundException(sprintf('OAuth2Provider `Code(%s)` not found', $code));
        }
    }

    public function hasProviderWithCode(string $code): bool
    {
        return $this->findOneBy(['code' => $code]) !== null;
    }
}
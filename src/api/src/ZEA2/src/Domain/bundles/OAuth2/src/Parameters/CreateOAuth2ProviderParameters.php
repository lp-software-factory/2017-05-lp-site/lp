<?php
namespace ZEA2\Domain\Bundles\OAuth2\Parameters;

final class CreateOAuth2ProviderParameters
{
    /** @var string */
    private $code;

    /** @var string */
    private $handler;

    /** @var array */
    private $config;


    public function __construct(string $code, string $handler, array $config)
    {
        $this->code = $code;
        $this->handler = $handler;
        $this->config = $config;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getHandler(): string
    {
        return $this->handler;
    }

    public function getConfig(): array
    {
        return $this->config;
    }
}
<?php
namespace ZEA2\Domain\Bundles\Twig;

use ZEA2\Platform\Bundles\ZEA2Bundle;

final class TwigDomainBundle extends ZEA2Bundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}
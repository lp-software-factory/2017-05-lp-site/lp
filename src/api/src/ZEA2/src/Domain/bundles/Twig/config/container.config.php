<?php
namespace ZEA2\Domain\Bundles\Designer;

use function DI\get;
use function DI\object;
use function DI\factory;

use ZEA2\Domain\Bundles\Twig\Config\TwigConfig;

return [
    TwigConfig::class => object()->constructorParameter('config', get('config.zea2.domain.twig')),
];
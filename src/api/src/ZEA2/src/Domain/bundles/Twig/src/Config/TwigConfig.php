<?php
namespace ZEA2\Domain\Bundles\Twig\Config;

final class TwigConfig
{
    /** @var string[] */
    private $paths;

    /** @var array */
    private $options;

    /** @var array */
    private $exports;

    public function __construct(array $config)
    {
        $this->paths = $config['paths'];
        $this->options = $config['options'];
        $this->exports = $config['exports'];
    }

    public function getPaths(): array
    {
        return $this->paths;
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    public function getExports(): array
    {
        return $this->exports;
    }
}
<?php
namespace ZEA2\Domain\Bundles\Register\Util;

final class ImmutableRegisterKeyValuePair
{
    /** @var string */
    private $key;

    /** @var string */
    private $value;

    public function __construct($key, $value)
    {
        $this->key = $key;
        $this->value = $value;
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function getValue(): string
    {
        return $this->value;
    }
}
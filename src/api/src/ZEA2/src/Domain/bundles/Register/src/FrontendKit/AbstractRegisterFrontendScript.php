<?php
namespace ZEA2\Domain\Bundles\Register\FrontendKit;

use ZEA2\Domain\Bundles\Register\Entity\RegisterRecord;
use ZEA2\Domain\Bundles\Register\Formatter\RegisterRecordFormatter;
use ZEA2\Domain\Bundles\Register\Service\RegisterRecord\RegisterRecordService;
use ZEA2\Domain\Bundles\Register\Service\RegisterRecordGroup\RegisterRecordGroupService;
use ZEA2\Platform\Bundles\Frontend\Markers\FrontendBundleInjectable\FrontendScript;

abstract class AbstractRegisterFrontendScript implements FrontendScript
{
    /** @var RegisterRecordService */
    private $registerRecordService;

    /** @var RegisterRecordGroupService */
    private $registerRecordGroupService;

    /** @var RegisterRecordFormatter */
    private $registerRecordFormatter;

    public function __construct(
        RegisterRecordService $registerRecordService,
        RegisterRecordGroupService $registerRecordGroupService,
        RegisterRecordFormatter $registerRecordFormatter
    ) {
        $this->registerRecordService = $registerRecordService;
        $this->registerRecordGroupService = $registerRecordGroupService;
        $this->registerRecordFormatter = $registerRecordFormatter;
    }

    abstract protected function getRegisterGroups(): array ;

    public function __invoke(): array
    {
        $register = [];

        foreach($this->getRegisterGroups() as $groupName) {
            $group = $this->registerRecordGroupService->getByName($groupName);
            $records = $this->registerRecordService->getAllExportedToFrontendRegisterRecordsWithGroup($group->getId());

            /** @var RegisterRecord $record */
            foreach($records as $record) {
                $register[$record->getId()] = $this->registerRecordFormatter->formatOne($record);
            }
        }

        return [
            'register' => $register,
        ];
    }
}
<?php
namespace ZEA2\Domain\Bundles\Register\Parameters\RegisterRecordGroup;

abstract class AbstractRegisterRecordGroupParameters
{
    /** @var string */
    private $appName;

    public function __construct($appName)
    {
        $this->appName = $appName;
    }

    public function getAppName(): string
    {
        return $this->appName;
    }
}
<?php
namespace ZEA2\Domain\Bundles\Register\Exceptions;

final class InvalidAppNameException extends \Exception {}
<?php
namespace ZEA2\Domain\Bundles\Register\Parameters\RegisterRecord;

use ZEA2\Domain\Bundles\Register\Util\ImmutableRegisterKeyValuePair;

final class EditBulkRegisterRecordParameters
{
    /** @var ImmutableRegisterKeyValuePair[] */
    private $pairs;

    public function __construct(array $pairs)
    {
        $this->pairs = $pairs;
    }

    public function getPairs(): array
    {
        return $this->pairs;
    }
}
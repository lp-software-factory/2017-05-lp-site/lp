<?php
namespace ZEA2\Domain\Bundles\Register\Factory\Doctrine;

use ZEA2\Domain\Bundles\Register\Entity\RegisterRecord;
use ZEA2\Platform\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;

final class RegisterRecordRepositoryDoctrineFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return RegisterRecord::class;
    }
}
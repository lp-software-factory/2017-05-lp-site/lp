<?php
namespace ZEA2\Domain\Bundles\Register\StageKit;

use Interop\Container\ContainerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use ZEA2\Domain\Bundles\Register\Entity\RegisterRecordGroup;
use ZEA2\Domain\Bundles\Register\Parameters\RegisterRecord\CreateRegisterRecordParameters;
use ZEA2\Domain\Bundles\Register\Parameters\RegisterRecordGroup\CreateRegisterRecordGroupParameters;
use ZEA2\Domain\Bundles\Register\Service\RegisterRecord\RegisterRecordService;
use ZEA2\Domain\Bundles\Register\Service\RegisterRecordGroup\RegisterRecordGroupService;
use ZEA2\Stage\Fixtures\StageFixture;

abstract class AbstractZEA2RegisterStageFixture implements StageFixture
{
    /** @var array */
    private $bootstrap;

    /** @var RegisterRecordService */
    private $registerRecordService;

    /** @var RegisterRecordGroupService */
    private $registerRecordGroupService;

    public function __construct(
        ContainerInterface $container,
        RegisterRecordService $registerRecordService,
        RegisterRecordGroupService $registerRecordGroupService
    ) {
        $this->bootstrap = $container->get('zea2.stage.register.bootstrap');
        $this->registerRecordService = $registerRecordService;
        $this->registerRecordGroupService = $registerRecordGroupService;
    }

    abstract protected function getJSONConfig(): array ;

    public function up(OutputInterface $output, InputInterface $input): void
    {
        $config = array_merge_recursive($this->getJSONConfig(), $this->bootstrap);

        $output->writeln('[*] Register Fixture');

        foreach($config as $appName => $jsonRecords) {
            $output->writeln(sprintf(' + GROUP: %s', $appName));

            $group = $this->registerRecordGroupService->create(new CreateRegisterRecordGroupParameters($appName));

            foreach($jsonRecords as $key => $jsonRecord) {
                $output->writeln(sprintf(' * KEY: %s', $key));

                if(is_array($jsonRecord)) {
                    $this->registerRecordService->create(new CreateRegisterRecordParameters(
                        $group,
                        $appName.'.'.$key,
                        $jsonRecord['value'],
                        $jsonRecord['options'] ?? []
                    ));
                }else{
                    $this->registerRecordService->create(new CreateRegisterRecordParameters(
                        $group,
                        $appName.'.'.$key,
                        (string) $jsonRecord,
                        []
                    ));
                }
            }
        }
    }
}
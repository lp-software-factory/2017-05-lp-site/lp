<?php
namespace ZEA2\Domain\Bundles\Register\Exceptions;

final class DuplicateRecordGroupException extends \Exception {}
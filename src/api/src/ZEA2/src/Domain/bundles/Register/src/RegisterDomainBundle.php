<?php
namespace ZEA2\Domain\Bundles\Register;

use ZEA2\Platform\Bundles\ZEA2Bundle;

final class RegisterDomainBundle extends ZEA2Bundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}
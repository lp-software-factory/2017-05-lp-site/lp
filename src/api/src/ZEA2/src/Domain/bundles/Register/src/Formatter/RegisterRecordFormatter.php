<?php
namespace ZEA2\Domain\Bundles\Register\Formatter;

use ZEA2\Domain\Bundles\Register\Entity\RegisterRecord;

final class RegisterRecordFormatter
{
    public function formatOne(RegisterRecord $record): array
    {
        return $record->toJSON();
    }

    public function formatMany(array $records): array
    {
        return array_map(function(RegisterRecord $record) {
            return $this->formatOne($record);
        }, $records);
    }
}
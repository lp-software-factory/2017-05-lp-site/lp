<?php
namespace ZEA2\Domain\Bundles\Register\Entity;

use ZEA2\Domain\Bundles\Register\Exceptions\InvalidAppNameException;
use ZEA2\Domain\Markers\IdEntity\IdEntity;
use ZEA2\Domain\Markers\IdEntity\IdEntityTrait;
use ZEA2\Domain\Markers\JSONMetadataEntity\JSONMetadataEntity;
use ZEA2\Domain\Markers\JSONMetadataEntity\JSONMetadataEntityTrait;
use ZEA2\Domain\Markers\JSONSerializable;
use ZEA2\Domain\Markers\ModificationEntity\ModificationEntity;
use ZEA2\Domain\Markers\ModificationEntity\ModificationEntityTrait;
use ZEA2\Domain\Markers\SIDEntity\SIDEntity;
use ZEA2\Domain\Markers\SIDEntity\SIDEntityTrait;
use ZEA2\Domain\Markers\VersionEntity\VersionEntity;
use ZEA2\Domain\Markers\VersionEntity\VersionEntityTrait;

/**
 * @Entity(repositoryClass="ZEA2\Domain\Bundles\Register\Repository\RegisterRecordGroupRepository")
 * @Table(name="register_record_group")
 */
class RegisterRecordGroup implements JSONSerializable, IdEntity, SIDEntity, VersionEntity, ModificationEntity, JSONMetadataEntity
{
    public const LATEST_VERSION = '1.0.0';
    public const REGEX_APP_NAME = '/^([a-z0-9\.\_\-]+)$/';

    use IdEntityTrait, SIDEntityTrait, VersionEntityTrait, ModificationEntityTrait, JSONMetadataEntityTrait;

    /**
     * @Column(name="app_name", type="string")
     * @var string
     */
    private $appName;

    public function __construct(string $appName)
    {
        $this->setCurrentVersion(self::LATEST_VERSION);

        $this->setAppName($appName);

        $this->regenerateSID();
        $this->initModificationEntity();
    }

    public function getMetadataVersion(): string
    {
        return '1.0.0';
    }

    public function toJSON(array $options = []): array
    {
        return [
            'id' => $this->getIdNoFall(),
            'sid' => $this->getSID(),
            'metadata' => array_merge($this->getMetadata(), [
                'version' => $this->getMetadataVersion(),
            ]),
            'date_created_at' => $this->getDateCreatedAt()->format(\DateTime::RFC2822),
            'last_updated_on' => $this->getLastUpdatedOn()->format(\DateTime::RFC2822),
            'version' => $this->getVersion(),
            'app_name' => $this->getAppName(),
        ];
    }

    public function getAppName(): string
    {
        return $this->appName;
    }

    public function setAppName(string $appName): self
    {
        $appName = trim($appName);

        if(! (strlen($appName) && preg_match(self::REGEX_APP_NAME, $appName))) {
            throw new InvalidAppNameException(sprintf('App name `%s` is invalid, app name should match regex `%s`', $appName, self::REGEX_APP_NAME));
        }

        $this->appName = $appName;
        $this->markAsUpdated();

        return $this;
    }
}
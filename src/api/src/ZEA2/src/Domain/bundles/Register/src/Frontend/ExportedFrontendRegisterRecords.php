<?php
namespace ZEA2\Domain\Bundles\Register\Frontend;

use ZEA2\Domain\Bundles\Register\Service\RegisterRecord\RegisterRecordService;
use ZEA2\Domain\Bundles\Register\Formatter\RegisterRecordFormatter;
use ZEA2\Platform\Bundles\Frontend\Markers\FrontendBundleInjectable\FrontendScript;

abstract class ExportedFrontendRegisterRecords implements FrontendScript
{
    /** @var RegisterRecordService */
    private $registerRecordService;

    /** @var RegisterRecordFormatter */
    private $registerRecordFormatter;

    public function __construct(RegisterRecordService $registerRecordService, RegisterRecordFormatter $registerRecordFormatter)
    {
        $this->registerRecordService = $registerRecordService;
        $this->registerRecordFormatter = $registerRecordFormatter;
    }

    public function __invoke(): array
    {
        return [
            'register' => [
                'records' => $this->registerRecordFormatter->formatMany(
                    $this->registerRecordService->getAllExportedToFrontendRegisterRecordsWithGroup($this->getRecordGroupId())
                ),
            ],
        ];
    }

    abstract protected function getRecordGroupId(): int;
}
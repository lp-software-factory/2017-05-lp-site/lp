<?php
namespace ZEA2\Domain\Bundles\Register\Parameters\RegisterRecord;

use ZEA2\Domain\Bundles\Register\Entity\RegisterRecordGroup;

abstract class AbstractRegisterRecordParameters
{
    public const OPTION_EXPORTS_TO_FRONTEND = 'exports_to_frontend';

    /** @var RegisterRecordGroup */
    private $group;

    /** @var string */
    private $key;

    /** @var string */
    private $value;

    /** @var boolean */
    private $exportsToFrontend = false;

    public function __construct(RegisterRecordGroup $recordGroup, string $key, string $value, array $options = [])
    {
        $options = array_merge([
            self::OPTION_EXPORTS_TO_FRONTEND => false,
        ], $options);

        $this->group = $recordGroup;
        $this->key = $key;
        $this->value = $value;

        if($options[self::OPTION_EXPORTS_TO_FRONTEND]) {
            $this->exportsToFrontend = true;
        }
    }

    public function getGroup(): RegisterRecordGroup
    {
        return $this->group;
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function isExportedToFrontend(): bool
    {
        return $this->exportsToFrontend;
    }
}
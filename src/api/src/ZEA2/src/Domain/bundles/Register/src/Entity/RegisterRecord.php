<?php
namespace ZEA2\Domain\Bundles\Register\Entity;

use ZEA2\Domain\Bundles\Register\Exceptions\InvalidKeyNameException;
use ZEA2\Domain\Markers\IdEntity\IdEntity;
use ZEA2\Domain\Markers\IdEntity\IdEntityTrait;
use ZEA2\Domain\Markers\JSONMetadataEntity\JSONMetadataEntity;
use ZEA2\Domain\Markers\JSONMetadataEntity\JSONMetadataEntityTrait;
use ZEA2\Domain\Markers\JSONSerializable;
use ZEA2\Domain\Markers\ModificationEntity\ModificationEntity;
use ZEA2\Domain\Markers\ModificationEntity\ModificationEntityTrait;
use ZEA2\Domain\Markers\SIDEntity\SIDEntity;
use ZEA2\Domain\Markers\SIDEntity\SIDEntityTrait;
use ZEA2\Domain\Markers\VersionEntity\VersionEntity;
use ZEA2\Domain\Markers\VersionEntity\VersionEntityTrait;

/**
 * @Entity(repositoryClass="ZEA2\Domain\Bundles\Register\Repository\RegisterRecordRepository")
 * @Table(name="register_record")
 */
class RegisterRecord implements JSONSerializable, IdEntity, SIDEntity, JSONMetadataEntity, ModificationEntity, VersionEntity
{
    public const LATEST_VERSION = '1.0.0';
    public const KEY_REGEX = '/^([a-z0-9\.\_\-]+)$/';

    use IdEntityTrait, SIDEntityTrait, JSONMetadataEntityTrait, ModificationEntityTrait, VersionEntityTrait;

    /**
     * @ManyToOne(targetEntity="ZEA2\Domain\Bundles\Register\Entity\RegisterRecordGroup")
     * @JoinColumn(name="register_record_group_id", referencedColumnName="id")
     * @var RegisterRecordGroup
     */
    private $group;

    /**
     * @Column(name="key", type="string")
     * @var string
     */
    private $key;

    /**
     * @Column(name="value", type="string")
     * @var string
     */
    private $value;

    /**
     * @Column(name="exports_to_frontend", type="boolean")
     * @var boolean
     */
    private $exportsToFrontend = false;

    public function __construct(RegisterRecordGroup $recordGroup, string $key, string $value)
    {
        $this->setCurrentVersion(self::LATEST_VERSION);

        $this
            ->setGroup($recordGroup)
            ->setKey($key)
            ->setValue($value)
        ;

        $this->regenerateSID();
        $this->initModificationEntity();
    }

    public function getMetadataVersion(): string
    {
        return '1.0.0';
    }

    public function toJSON(array $options = []): array
    {
        return [
            'id' => $this->getIdNoFall(),
            'sid' => $this->getSID(),
            'metadata' => array_merge($this->getMetadata(), [
                'version' => $this->getMetadataVersion(),
            ]),
            'date_created_at' => $this->getDateCreatedAt()->format(\DateTime::RFC2822),
            'last_updated_on' => $this->getLastUpdatedOn()->format(\DateTime::RFC2822),
            'version' => $this->getVersion(),
            'register_record_group_id' => $this->getGroup()->getId(),
            'key' => $this->getKey(),
            'value' => $this->getValue(),
            'options' => [
                'exports_to_frontend' => $this->isExportedToFrontend(),
            ],
        ];
    }

    public function getGroup(): RegisterRecordGroup
    {
        return $this->group;
    }

    public function setGroup(RegisterRecordGroup $group): self
    {
        $this->group = $group;
        $this->markAsUpdated();

        return $this;
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function setKey(string $key): self
    {
        if(! preg_match(self::KEY_REGEX, $key)) {
            throw new InvalidKeyNameException(sprintf('Invalid key'));
        }

        $this->key = $key;
        $this->markAsUpdated();

        return $this;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;
        $this->markAsUpdated();

        return $this;
    }

    public function enableExportsToFrontend(): self
    {
        $this->exportsToFrontend = true;
        $this->markAsUpdated();

        return $this;
    }

    public function disableExportsToFrontend(): self
    {
        $this->exportsToFrontend = true;
        $this->markAsUpdated();

        return $this;
    }

    public function isExportedToFrontend(): bool
    {
        return $this->exportsToFrontend;
    }
}
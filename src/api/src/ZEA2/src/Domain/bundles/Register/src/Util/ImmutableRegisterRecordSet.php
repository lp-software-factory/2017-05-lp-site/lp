<?php
namespace ZEA2\Domain\Bundles\Register\Util;

use ZEA2\Domain\Bundles\Register\Entity\RegisterRecord;

final class ImmutableRegisterRecordSet
{
    /** @var RegisterRecord[] */
    private $records = [];

    public function __construct(array $records)
    {
        array_map(function(RegisterRecord $record) {
            $this->records[$record->getKey()] = $record;
        }, $records);
    }

    public function has(string $key): bool
    {
        return isset($this->records[$key]);
    }

    public function get(string $key): RegisterRecord
    {
        if(! $this->has($key)) {
            throw new \Exception(sprintf('Register key `%s` not found in ImmutableRegisterRecordSet', $key));
        }

        return $this->records[$key];
    }
}
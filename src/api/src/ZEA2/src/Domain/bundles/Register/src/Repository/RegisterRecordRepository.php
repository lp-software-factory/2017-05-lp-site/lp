<?php
namespace ZEA2\Domain\Bundles\Register\Repository;

use Doctrine\ORM\EntityRepository;
use ZEA2\Domain\Bundles\Register\Entity\RegisterRecord;
use ZEA2\Domain\Bundles\Register\Exceptions\RegisterRecordNotFoundException;
use ZEA2\Domain\Criteria\IdsCriteria;

final class RegisterRecordRepository extends EntityRepository
{
    public function createRegisterRecord(RegisterRecord $registerRecord): int
    {
        while($this->hasWithSID($registerRecord->getSID())) {
            $registerRecord->regenerateSID();
        }

        $this->getEntityManager()->persist($registerRecord);
        $this->getEntityManager()->flush($registerRecord);

        return $registerRecord->getId();
    }

    public function saveRegisterRecord(RegisterRecord $registerRecord)
    {
        $this->getEntityManager()->flush($registerRecord);
    }

    public function saveRegisterRecords(array $registerRecords)
    {
        $this->getEntityManager()->flush($registerRecords);
    }

    public function deleteRegisterRecord(RegisterRecord $registerRecord)
    {
        $this->getEntityManager()->remove($registerRecord);
        $this->getEntityManager()->flush($registerRecord);
    }

    public function getById(int $id): RegisterRecord
    {
        $result = $this->find($id);

        if($result instanceof RegisterRecord) {
            return $result;
        }else{
            throw new RegisterRecordNotFoundException(sprintf('RegisterRecord `ID(%d)` not found', $id));
        }
    }

    public function getAll(): array
    {
        return $this->findAll();
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        $ids = $idsCriteria->getIds();
        $results = $this->findBy(['id' => $ids]);

        if(count($ids) !== count($results)) {
            throw new RegisterRecordNotFoundException(sprintf(
                'RegisterRecords with IDs (%s) not found',
                join(', ', array_diff($ids, array_map(function(RegisterRecord $registerRecord) {
                    return $registerRecord->getId();
                }, $results)))
            ));
        }

        return $results;
    }

    public function getByKey(string $key): RegisterRecord
    {
        /** @var RegisterRecord $result */
        $result = $this->findOneBy(['key' => $key]);

        if($result === null) {
            throw new RegisterRecordNotFoundException(sprintf('RegisterRecord with key `%s` not found', $key));
        }

        return $result;
    }

    public function getByKeys(array $keys): array
    {
        $result = $this->findBy([
            'key' => $keys,
        ]);

        if(count($keys) !== count($result)) {
            throw new RegisterRecordNotFoundException('Some of keys not found');
        }

        return $result;
    }

    public function hasWithKey(string $key): bool
    {
        return $this->findOneBy(['key' => $key]) !== null;
    }

    public function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

    public function hasRegisterRecordWithId(string $id): bool
    {
        return $this->findOneBy(['id' => $id]) !== null;
    }

    public function getBySID(string $sid): RegisterRecord
    {
        $result = $this->findBy(['sid' => $sid]);

        if($result instanceof RegisterRecord) {
            return $result;
        }else{
            throw new RegisterRecordNotFoundException(sprintf('RegisterRecord `SID(%s)` not found', $sid));
        }
    }

    public function getAllExportedToFrontendRegisterRecords(): array
    {
        return $this->findBy([
            'exportsToFrontend' => true,
        ]);
    }

    public function getAllExportedToFrontendRegisterRecordGroupsWithGroup(int $recordGroupId): array
    {
        return $this->findBy([
            'group' => $recordGroupId,
            'exportsToFrontend' => true,
        ]);
    }

    public function getRegisterRecordsByKeys(array $keys): array
    {
        return $this->findBy([
            'key' => $keys,
        ]);
    }
}
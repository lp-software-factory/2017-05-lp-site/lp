<?php
namespace ZEA2\Domain\Bundles\Register;

use ZEA2\Domain\Bundles\Register\Factory\Doctrine\RegisterRecordGroupRepositoryDoctrineFactory;
use ZEA2\Domain\Bundles\Register\Factory\Doctrine\RegisterRecordRepositoryDoctrineFactory;
use ZEA2\Domain\Bundles\Register\Repository\RegisterRecordGroupRepository;
use ZEA2\Domain\Bundles\Register\Repository\RegisterRecordRepository;

return [
    RegisterRecordRepository::class => \DI\factory(RegisterRecordRepositoryDoctrineFactory::class),
    RegisterRecordGroupRepository::class => \DI\factory(RegisterRecordGroupRepositoryDoctrineFactory::class),
];
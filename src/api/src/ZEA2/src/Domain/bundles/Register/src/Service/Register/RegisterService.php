<?php
namespace ZEA2\Domain\Bundles\Register\Service\Register;

use ZEA2\Domain\Bundles\Register\Service\RegisterRecord\RegisterRecordService;
use ZEA2\Domain\Bundles\Register\Service\RegisterRecordGroup\RegisterRecordGroupService;
use ZEA2\Domain\Bundles\Register\Util\ImmutableRegisterRecordSet;

final class RegisterService
{
    /** @var RegisterRecordService */
    private $registerRecordService;

    /** @var RegisterRecordGroupService */
    private $registerRecordGroupService;

    public function __construct(
        RegisterRecordService $registerRecordService,
        RegisterRecordGroupService $registerRecordGroupService
    ) {
        $this->registerRecordService = $registerRecordService;
        $this->registerRecordGroupService = $registerRecordGroupService;
    }

    public function fetch(array $keys): ImmutableRegisterRecordSet
    {
        return new ImmutableRegisterRecordSet($this->registerRecordService->getRegisterRecordsByKeys($keys));
    }

    public function with(array $callbacks): ImmutableRegisterRecordSet
    {
        $set = new ImmutableRegisterRecordSet($this->registerRecordService->getRegisterRecordsByKeys(array_keys($callbacks)));

        foreach($callbacks as $key => $callback) {
            $callback($set->get($key)->getValue(), $key);
        }

        return $set;
    }
}
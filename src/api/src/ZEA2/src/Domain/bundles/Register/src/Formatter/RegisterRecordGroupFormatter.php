<?php
namespace ZEA2\Domain\Bundles\Register\Formatter;

use ZEA2\Domain\Bundles\Register\Entity\RegisterRecordGroup;

final class RegisterRecordGroupFormatter
{
    public function formatOne(RegisterRecordGroup $record): array
    {
        return $record->toJSON();
    }

    public function formatMany(array $records): array
    {
        return array_map(function(RegisterRecordGroup $record) {
            return $this->formatOne($record);
        }, $records);
    }
}
<?php
namespace ZEA2\Domain\Bundles\Register\Service\RegisterRecord;

use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use ZEA2\Domain\Bundles\Register\Entity\RegisterRecord;
use ZEA2\Domain\Bundles\Register\Exceptions\DuplicateKeyException;
use ZEA2\Domain\Bundles\Register\Parameters\RegisterRecord\CreateRegisterRecordParameters;
use ZEA2\Domain\Bundles\Register\Parameters\RegisterRecord\EditBulkRegisterRecordParameters;
use ZEA2\Domain\Bundles\Register\Parameters\RegisterRecord\EditRegisterRecordParameters;
use ZEA2\Domain\Bundles\Register\Parameters\RegisterRecord\GetBulkRegisterRecordParameters;
use ZEA2\Domain\Bundles\Register\Repository\RegisterRecordRepository;
use ZEA2\Domain\Bundles\Register\Util\ImmutableRegisterKeyValuePair;

final class RegisterRecordService
{
    public const EVENT_CREATED = 'zea2.domain.register-record.created';
    public const EVENT_EDITED_BULK = 'zea2.domain.register-record.edited.bulk';
    public const EVENT_EDITED_SINGLE = 'zea2.domain.register-record.edited.single';
    public const EVENT_DELETE = 'zea2.domain.register-record.delete';
    public const EVENT_DELETED = 'zea2.domain.register-record.deleted';

    /** @var EventEmitterInterface */
    private $eventEmitter;

    /** @var RegisterRecordRepository */
    private $registerRecordRepository;

    public function __construct(RegisterRecordRepository $registerRecordRepository)
    {
        $this->eventEmitter = new EventEmitter();
        $this->registerRecordRepository = $registerRecordRepository;
    }

    public function create(CreateRegisterRecordParameters $parameters): RegisterRecord
    {
        if($this->registerRecordRepository->hasWithKey($parameters->getKey())) {
            throw new DuplicateKeyException(sprintf('RegisterRecord with key `%s` already exists!', $parameters->getKey()));
        }

        $registerRecord = new RegisterRecord(
            $parameters->getGroup(),
            $parameters->getKey(),
            $parameters->getValue()
        );

        if($parameters->isExportedToFrontend()) {
            $registerRecord->enableExportsToFrontend();
        }

        $this->registerRecordRepository->createRegisterRecord($registerRecord);
        $this->eventEmitter->emit(self::EVENT_CREATED, [$registerRecord]);

        return $registerRecord;
    }

    public function edit(int $registerRecordId, EditRegisterRecordParameters $parameters): RegisterRecord
    {
        $registerRecord = $this->getById($registerRecordId);

        if($this->registerRecordRepository->hasWithKey($parameters->getKey())) {
            $found = $this->registerRecordRepository->getByKey($parameters->getKey());

            if($found->getId() !== $registerRecord->getId()) {
                throw new DuplicateKeyException(sprintf('RegisterRecord with key `%s` already exists!', $parameters->getKey()));
            }
        }

        $registerRecord
            ->setGroup($parameters->getGroup())
            ->setKey($parameters->getKey())
            ->setValue($parameters->getValue());

        if($parameters->isExportedToFrontend()) {
            $registerRecord->enableExportsToFrontend();
        }else{
            $registerRecord->disableExportsToFrontend();
        }

        $this->registerRecordRepository->saveRegisterRecord($registerRecord);
        $this->eventEmitter->emit(self::EVENT_EDITED_SINGLE, [$registerRecord, $registerRecordId]);

        return $registerRecord;
    }

    public function editBulk(EditBulkRegisterRecordParameters $parameters): array
    {
        return array_map(function(ImmutableRegisterKeyValuePair $pair): RegisterRecord {
            $registerRecord = $this->getByKey($pair->getKey());
            $registerRecord->setValue($pair->getValue());

            $this->registerRecordRepository->saveRegisterRecord($registerRecord);
            $this->eventEmitter->emit(self::EVENT_EDITED_BULK, [$registerRecord]);

            return $registerRecord;
        }, $parameters->getPairs());
    }

    public function delete(int $registerRecordId): void
    {
        $registerRecord = $this->getById($registerRecordId);

        $this->eventEmitter->emit(self::EVENT_DELETE, [$registerRecord, $registerRecordId]);
        $this->registerRecordRepository->deleteRegisterRecord($registerRecord);
        $this->eventEmitter->emit(self::EVENT_DELETED, [$registerRecord, $registerRecordId]);
    }

    public function getById(int $registerRecordId): RegisterRecord
    {
        return $this->registerRecordRepository->getById($registerRecordId);
    }

    public function getByKey(string $key): RegisterRecord
    {
        return $this->registerRecordRepository->getByKey($key);
    }

    public function getBulk(GetBulkRegisterRecordParameters $parameters): array
    {
        return $this->registerRecordRepository->getByKeys($parameters->getKeys());
    }

    public function getAll(): array
    {
        return $this->registerRecordRepository->getAll();
    }

    public function getAllExportedToFrontendRegisterRecords(): array
    {
        return $this->registerRecordRepository->getAllExportedToFrontendRegisterRecords();
    }

    public function getAllExportedToFrontendRegisterRecordsWithGroup(int $recordGroupId): array
    {
        return $this->registerRecordRepository->getAllExportedToFrontendRegisterRecordGroupsWithGroup($recordGroupId);
    }

    public function getRegisterRecordsByKeys(array $keys): array
    {
        return $this->registerRecordRepository->getRegisterRecordsByKeys($keys);
    }
}
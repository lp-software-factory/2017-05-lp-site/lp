<?php
namespace ZEA2\Domain\Bundles\Register\Service\RegisterRecordGroup;

use Cocur\Chain\Chain;
use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use ZEA2\Domain\Bundles\Register\Entity\RegisterRecordGroup;
use ZEA2\Domain\Bundles\Register\Exceptions\DuplicateRecordGroupException;
use ZEA2\Domain\Bundles\Register\Parameters\RegisterRecordGroup\CreateRegisterRecordGroupParameters;
use ZEA2\Domain\Bundles\Register\Parameters\RegisterRecordGroup\EditRegisterRecordGroupParameters;
use ZEA2\Domain\Bundles\Register\Repository\RegisterRecordGroupRepository;

final class RegisterRecordGroupService
{
    public const EVENT_CREATED = 'zea2.domain.register-record-group.created';
    public const EVENT_EDITED = 'zea2.domain.register-record-group.edited';
    public const EVENT_DELETE = 'zea2.domain.register-record-group.delete';
    public const EVENT_DELETED = 'zea2.domain.register-record-group.deleted';

    /** @var EventEmitterInterface */
    private $eventEmitter;

    /** @var RegisterRecordGroupRepository */
    private $registerRecordGroupRepository;

    public function __construct(RegisterRecordGroupRepository $registerRecordGroupRepository)
    {
        $this->eventEmitter = new EventEmitter();
        $this->registerRecordGroupRepository = $registerRecordGroupRepository;
    }

    public function create(CreateRegisterRecordGroupParameters $parameters): RegisterRecordGroup
    {
        $appName = $parameters->getAppName();

        if($this->hasRegisterRecordGroupWithAppName($appName)) {
            throw new DuplicateRecordGroupException(sprintf('Register record group with app `%s` already exists', $appName));
        }

        $registerRecordGroup = new RegisterRecordGroup($appName);

        $this->registerRecordGroupRepository->createRegisterRecordGroup($registerRecordGroup);
        $this->eventEmitter->emit(self::EVENT_CREATED, [$registerRecordGroup]);

        return $registerRecordGroup;
    }

    public function edit(int $registerRecordGroupId, EditRegisterRecordGroupParameters $parameters): RegisterRecordGroup
    {
        $appName = $parameters->getAppName();

        if($this->hasRegisterRecordGroupWithAppName($appName, $registerRecordGroupId)) {
            throw new DuplicateRecordGroupException(sprintf('Register record group with app `%s` already exists', $appName));
        }

        $registerRecordGroup = $this->getById($registerRecordGroupId);

        $registerRecordGroup
            ->setAppName($appName);

        $this->registerRecordGroupRepository->saveRegisterRecordGroup($registerRecordGroup);
        $this->eventEmitter->emit(self::EVENT_DELETE, [$registerRecordGroup, $registerRecordGroupId]);

        return $registerRecordGroup;
    }

    public function delete(int $registerRecordGroupId): void
    {
        $registerRecordGroup = $this->getById($registerRecordGroupId);

        $this->eventEmitter->emit(self::EVENT_DELETE, [$registerRecordGroup, $registerRecordGroupId]);
        $this->registerRecordGroupRepository->deleteRegisterRecordGroup($registerRecordGroup);
        $this->eventEmitter->emit(self::EVENT_DELETED, [$registerRecordGroup, $registerRecordGroupId]);
    }

    public function getById(int $registerRecordGroupId): RegisterRecordGroup
    {
        return $this->registerRecordGroupRepository->getById($registerRecordGroupId);
    }

    public function getByName(string $groupName): RegisterRecordGroup
    {
        $result = array_filter($this->getAll(), function(RegisterRecordGroup $group) use ($groupName) {
            return $group->getAppName() === $groupName;
        });

        if(!count($result)) {
            throw new \Exception(sprintf('Register group with name `%s` not found', $groupName));
        }

        $result = array_pop($result);

        return $result;
    }

    public function getAll(): array
    {
        return $this->registerRecordGroupRepository->getAll();
    }

    public function hasRegisterRecordGroupWithAppName(string $appName, int $excludeId = null): bool
    {
        return Chain::create($this->getAll())
            ->filter(function(RegisterRecordGroup $group) use ($appName, $excludeId) {
                return $group->getAppName() === $appName && ($excludeId === null || $excludeId !== $group->getId());
            })
            ->count() > 0;
    }
}
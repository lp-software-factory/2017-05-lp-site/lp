<?php
namespace ZEA2\Domain\Bundles\Register\Repository;

use Doctrine\ORM\EntityRepository;
use ZEA2\Domain\Bundles\Register\Entity\RegisterRecordGroup;
use ZEA2\Domain\Bundles\Register\Exceptions\RegisterRecordGroupNotFoundException;
use ZEA2\Domain\Criteria\IdsCriteria;

final class RegisterRecordGroupRepository extends EntityRepository
{
    public function createRegisterRecordGroup(RegisterRecordGroup $registerRecordGroup): int
    {
        while($this->hasWithSID($registerRecordGroup->getSID())) {
            $registerRecordGroup->regenerateSID();
        }

        $this->getEntityManager()->persist($registerRecordGroup);
        $this->getEntityManager()->flush($registerRecordGroup);

        return $registerRecordGroup->getId();
    }

    public function saveRegisterRecordGroup(RegisterRecordGroup $registerRecordGroup)
    {
        $this->getEntityManager()->flush($registerRecordGroup);
    }

    public function saveRegisterRecordGroups(array $registerRecordGroups)
    {
        $this->getEntityManager()->flush($registerRecordGroups);
    }

    public function deleteRegisterRecordGroup(RegisterRecordGroup $registerRecordGroup)
    {
        $this->getEntityManager()->remove($registerRecordGroup);
        $this->getEntityManager()->flush($registerRecordGroup);
    }

    public function getById(int $id): RegisterRecordGroup
    {
        $result = $this->find($id);

        if($result instanceof RegisterRecordGroup) {
            return $result;
        }else{
            throw new RegisterRecordGroupNotFoundException(sprintf('RegisterRecordGroup `ID(%d)` not found', $id));
        }
    }

    public function getAll(): array
    {
        return $this->findAll();
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        $ids = $idsCriteria->getIds();
        $results = $this->findBy(['id' => $ids]);

        if(count($ids) !== count($results)) {
            throw new RegisterRecordGroupNotFoundException(sprintf(
                'RegisterRecordGroups with IDs (%s) not found',
                join(', ', array_diff($ids, array_map(function(RegisterRecordGroup $registerRecordGroup) {
                    return $registerRecordGroup->getId();
                }, $results)))
            ));
        }

        return $results;
    }

    public function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

    public function hasRegisterRecordGroupWithId(string $id): bool
    {
        return $this->findOneBy(['id' => $id]) !== null;
    }

    public function getBySID(string $sid): RegisterRecordGroup
    {
        $result = $this->findBy(['sid' => $sid]);

        if($result instanceof RegisterRecordGroup) {
            return $result;
        }else{
            throw new RegisterRecordGroupNotFoundException(sprintf('RegisterRecordGroup `SID(%s)` not found', $sid));
        }
    }
}
<?php
namespace ZEA2\Domain\Bundles\Register\Parameters\RegisterRecord;

use ZEA2\Domain\Bundles\Register\Util\ImmutableRegisterKeyValuePair;

final class GetBulkRegisterRecordParameters
{
    /** @var string[] */
    private $keys;

    public function __construct(array $keys)
    {
        $this->keys = $keys;
    }

    public function getKeys()
    {
        return $this->keys;
    }
}
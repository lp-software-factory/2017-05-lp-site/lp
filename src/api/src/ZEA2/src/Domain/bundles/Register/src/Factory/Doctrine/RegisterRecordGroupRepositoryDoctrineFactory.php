<?php
namespace ZEA2\Domain\Bundles\Register\Factory\Doctrine;

use ZEA2\Domain\Bundles\Register\Entity\RegisterRecordGroup;
use ZEA2\Platform\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;

final class RegisterRecordGroupRepositoryDoctrineFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return RegisterRecordGroup::class;
    }
}
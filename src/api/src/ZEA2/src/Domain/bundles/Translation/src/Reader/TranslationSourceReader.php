<?php
namespace ZEA2\Domain\Bundles\Translation\Reader;

use ZEA2\Domain\Bundles\Translation\Entity\TranslationProject;

interface TranslationSourceReader
{
    public function read(string $region, TranslationProject $project): array;
}
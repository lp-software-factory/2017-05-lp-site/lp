<?php
namespace ZEA2\Domain\Bundles\Translation\Reader\Readers;

use ZEA2\Domain\Bundles\Translation\Entity\Translation;
use ZEA2\Domain\Bundles\Translation\Entity\TranslationProject;
use ZEA2\Domain\Bundles\Translation\Entity\TranslationProjectSource;
use ZEA2\Domain\Bundles\Translation\Reader\TranslationSourceReader;
use ZEA2\Platform\Bundles\DoctrineORM\Type\LocalizedString\Value\ImmutableLocalizedString;

final class PHPTranslationSourceReader implements TranslationSourceReader
{
    public function read(string $dir, TranslationProject $project): array
    {
        $results = [];
        $translations = [];

        /** @var TranslationProjectSource $source */
        foreach($project->getSources() as $source) {
            foreach($source->getDirs() as $dir) {
                foreach(glob(sprintf('%s/*.php', $dir)) as $current) {
                    $pi = pathinfo($current);

                    /** @noinspection PhpIncludeInspection */
                    $input = require($current);

                    $this->readSubTree($input, $pi['filename'], $results);
                }
            }
        }

        foreach($results as $key => $value) {
            $translations[] = new Translation(
                $project->getId(),
                $key,
                new ImmutableLocalizedString($value)
            );
        }

        return $translations;
    }


    private function readSubTree(array $input, string $region, &$results, array $carry = []) {
        foreach($input as $key => $value) {
            $next = array_values($carry);
            $next[] = $key;

            if(is_array($value)) {
                $this->readSubTree($value, $region, $results, $next);
            }else if(is_string($value)) {
                $key = implode('.', $next);

                if(! isset($results[$key])) {
                    $results[$key] = [];
                }

                $results[$key][] = [
                    'region' => $region,
                    'value' => $value,
                ];
            }
        }
    }
}
<?php
namespace ZEA2\Domain\Bundles\Translation\Reader;

use ZEA2\Domain\Bundles\Translation\Entity\TranslationProjectSource;
use ZEA2\Domain\Bundles\Translation\Reader\Readers\JSONTranslationSourceReader;
use ZEA2\Domain\Bundles\Translation\Reader\Readers\PHPTranslationSourceReader;
use Interop\Container\ContainerInterface;

final class TranslationSourceReaderFactory
{
    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function createTranslationSourceReader(string $sourceType): TranslationSourceReader
    {
        switch($sourceType) {
            default:
                throw new \Exception(sprintf('Source type `%s` is unknown', $sourceType));

            case TranslationProjectSource::TYPE_JSON:
                return $this->container->get(JSONTranslationSourceReader::class);

            case TranslationProjectSource::TYPE_PHP:
                return $this->container->get(PHPTranslationSourceReader::class);
        }
    }
}
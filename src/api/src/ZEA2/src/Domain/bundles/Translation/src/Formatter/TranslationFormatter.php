<?php
namespace ZEA2\Domain\Bundles\Translation\Formatter;

use ZEA2\Domain\Bundles\Translation\Entity\Translation;
use ZEA2\Domain\Bundles\Translation\Entity\TranslationByRegion;

final class TranslationFormatter
{
    public function formatOne(Translation $entity): array
    {
        return $entity->toJSON();
    }

    public function formatMany(array $entities): array
    {
        return array_map(function(Translation $entity) {
            return $this->formatOne($entity);
        }, $entities);
    }

    public function formatManyRegion(array $entities): array
    {
        $map = [];

        array_map(function(TranslationByRegion $entity) use (&$map) {
            $map[$entity->getKey()] = $entity->getValue();
        }, $entities);

        return $map;
    }
}
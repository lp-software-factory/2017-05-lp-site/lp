<?php
namespace ZEA2\Domain\Bundles\Translation\Factory\Doctrine;

use ZEA2\Domain\Bundles\Translation\Entity\Translation;
use ZEA2\Platform\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;

final class TranslationDoctrineRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return Translation::class;
    }
}
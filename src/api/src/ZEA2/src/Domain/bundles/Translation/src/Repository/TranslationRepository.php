<?php
namespace ZEA2\Domain\Bundles\Translation\Repository;

use Doctrine\ORM\EntityRepository;
use ZEA2\Domain\Bundles\Translation\Entity\Translation;
use ZEA2\Domain\Bundles\Translation\Exceptions\TranslationNotFoundException;
use ZEA2\Domain\Criteria\IdsCriteria;

final class TranslationRepository extends EntityRepository
{
    public function createEntity(Translation $entity): int
    {
        while ($this->hasWithSID($entity->getSID())) {
            $entity->regenerateSID();
        }

        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush($entity);

        return $entity->getId();
    }

    public function saveEntity(Translation $entity)
    {
        $this->getEntityManager()->flush($entity);
    }

    public function saveEntityEntities(array $entities)
    {
        foreach ($entities as $entity) {
            $this->saveEntity($entity);
        }
    }

    public function deleteEntity(Translation $entity)
    {
        $this->getEntityManager()->remove($entity);
        $this->getEntityManager()->flush($entity);
    }

    public function hasEntity(int $projectId, string $key): bool
    {
        return $this->findOneBy([
                'projectId' => $projectId,
                'key' => $key
            ]) !== null;
    }

    public function getEntityByKey(int $projectId, string $key): Translation
    {
        $result = $this->findOneBy([
            'projectId' => $projectId,
            'key' => $key
        ]);

        if($result instanceof Translation) {
            return $result;
        }else{
            throw new TranslationNotFoundException(sprintf('Translation (projectId: %d, key: %s) not found', $projectId, $key));
        }
    }

    public function getAllByProject(int $projectId): array
    {
        return $this->findBy([
            'projectId' => $projectId,
        ]);
    }

    public function getById(int $id): Translation
    {
        $result = $this->find($id);

        if ($result instanceof Translation) {
            return $result;
        } else {
            throw new TranslationNotFoundException(sprintf('Translation `ID(%d)` not found', $id));
        }
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        $ids = $idsCriteria->getIds();
        $results = $this->findBy(['id' => $ids]);

        if (count($ids) !== count($results)) {
            throw new \Exception('Translations with IDs (%s) not found', array_diff($ids, array_map(function (Translation $entity) {
                return $entity->getId();
            }, $results)));
        }

        return $results;
    }

    public function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

    public function getBySID(string $sid): Translation
    {
        $result = $this->findOneBy(['sid' => $sid]);

        if ($result instanceof Translation) {
            return $result;
        } else {
            throw new TranslationNotFoundException(sprintf('Translation `SID(%s)` not found', $sid));
        }
    }
}
<?php
namespace ZEA2\Domain\Bundles\Translation;

use function DI\get;
use function DI\object;
use function DI\factory;

use DI\Container;
use ZEA2\Domain\Bundles\Translation\Factory\Doctrine\TranslationDoctrineRepositoryFactory;
use ZEA2\Domain\Bundles\Translation\Repository\TranslationProjectRepository;
use ZEA2\Domain\Bundles\Translation\Repository\TranslationRepository;

return [
    TranslationRepository::class => factory(TranslationDoctrineRepositoryFactory::class),
    TranslationProjectRepository::class => object()
        ->constructorParameter('projects', get('config.zea2.domain.translation.projects')),
];
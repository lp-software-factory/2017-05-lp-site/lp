<?php
namespace ZEA2\Domain\Bundles\Translation\Parameters;

use ZEA2\Platform\Bundles\DoctrineORM\Type\LocalizedString\Value\ImmutableLocalizedString;

final class SetTranslationParameters
{
    /** @var int */
    private $projectId;

    /** @var string */
    private $key;

    /** @var ImmutableLocalizedString */
    private $value;

    public function __construct(int $projectId, string $key, ImmutableLocalizedString $value)
    {
        $this->projectId = $projectId;
        $this->key = $key;
        $this->value = $value;
    }

    public function getProjectId(): int
    {
        return $this->projectId;
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function getValue(): ImmutableLocalizedString
    {
        return $this->value;
    }
}
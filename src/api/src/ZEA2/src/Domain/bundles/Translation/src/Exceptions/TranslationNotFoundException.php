<?php
namespace ZEA2\Domain\Bundles\Translation\Exceptions;

final class TranslationNotFoundException extends \Exception {}
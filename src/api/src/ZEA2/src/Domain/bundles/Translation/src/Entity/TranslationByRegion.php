<?php
namespace ZEA2\Domain\Bundles\Translation\Entity;

use ZEA2\Domain\Markers\JSONSerializable;

final class TranslationByRegion implements JSONSerializable
{
    /** @var string */
    private $key;

    /** @var string */
    private $value;

    public function __construct($key, $value)
    {
        $this->key = $key;
        $this->value = $value;
    }

    public function toJSON(array $options = []): array
    {
        return [
            'key' => $this->getKey(),
            'value' => $this->getValue(),
        ];
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function getValue(): string
    {
        return $this->value;
    }
}
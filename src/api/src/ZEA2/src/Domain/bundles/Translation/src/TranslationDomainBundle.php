<?php
namespace ZEA2\Domain\Bundles\Translation;

use ZEA2\Platform\Bundles\ZEA2Bundle;

final class TranslationDomainBundle extends ZEA2Bundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}
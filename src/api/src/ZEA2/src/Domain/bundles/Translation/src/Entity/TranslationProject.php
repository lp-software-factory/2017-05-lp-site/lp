<?php
namespace ZEA2\Domain\Bundles\Translation\Entity;

final class TranslationProject
{
    /** @var int */
    private $id;

    /** @var string */
    private $title;

    /** @var int */
    private $position;

    /** @var string[] */
    private $sources = [];

    public function __construct(int $id, string $title, int $position, array $sources)
    {
        $this->id = $id;
        $this->title = $title;
        $this->position = $position;
        $this->sources = $sources;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getPosition(): int
    {
        return $this->position;
    }

    /** @return TranslationProjectSource[] */
    public function getSources(): array
    {
        return $this->sources;
    }
}
<?php
namespace ZEA2\Domain\Bundles\Translation\Entity;

use ZEA2\Domain\Markers\IdEntity\IdEntity;
use ZEA2\Domain\Markers\IdEntity\IdEntityTrait;
use ZEA2\Domain\Markers\JSONMetadataEntity\JSONMetadataEntity;
use ZEA2\Domain\Markers\JSONMetadataEntity\JSONMetadataEntityTrait;
use ZEA2\Domain\Markers\JSONSerializable;
use ZEA2\Domain\Markers\ModificationEntity\ModificationEntity;
use ZEA2\Domain\Markers\ModificationEntity\ModificationEntityTrait;
use ZEA2\Domain\Markers\SerialEntity\SerialEntityTrait;
use ZEA2\Domain\Markers\SIDEntity\SIDEntity;
use ZEA2\Domain\Markers\SIDEntity\SIDEntityTrait;
use ZEA2\Domain\Markers\VersionEntity\VersionEntity;
use ZEA2\Domain\Markers\VersionEntity\VersionEntityTrait;
use ZEA2\Platform\Bundles\DoctrineORM\Type\LocalizedString\Value\ImmutableLocalizedString;

/**
 * @Entity(repositoryClass="ZEA2\Domain\Bundles\Translation\Repository\TranslationRepository")
 * @Table(name="translation")
 */
class Translation implements JSONSerializable, IdEntity, SIDEntity, JSONMetadataEntity, ModificationEntity, VersionEntity
{
    public const LATEST_VERSION = '1.0.0';

    use IdEntityTrait, SIDEntityTrait, JSONMetadataEntityTrait, VersionEntityTrait, ModificationEntityTrait, SerialEntityTrait;

    /**
     * @Column(name="project_id", type="integer")
     * @var int
     */
    private $projectId;

    /**
     * @Column(name="key", type="string")
     * @var string
     */
    private $key;

    /**
     * @Column(name="title", type="i18n")
     * @var ImmutableLocalizedString
     */
    private $value;

    public function __construct(int $projectId, string $key, ImmutableLocalizedString $value)
    {
        $this->setCurrentVersion(self::LATEST_VERSION);

        $this->projectId = $projectId;
        $this->key = $key;
        $this->value = $value;

        $this->regenerateSID();
        $this->initModificationEntity();
    }

    public function toJSON(array $options = []): array
    {
        return [
            "id" => $this->isPersisted() ? $this->getId() : null,
            "sid" => $this->getSID(),
            "metadata" => array_merge($this->getMetadata(), [
                'version' => $this->getMetadataVersion(),
            ]),
            'date_created_at' => $this->getDateCreatedAt()->format(\DateTime::RFC2822),
            'last_updated_on' => $this->getLastUpdatedOn()->format(\DateTime::RFC2822),
            'position' => $this->getPosition(),
            'version' => $this->getVersion(),
            'project_id' => $this->getProjectId(),
            'key' => $this->getKey(),
            'value' => $this->getValue()->toJSON($options),
        ];
    }

    public function getMetadataVersion(): string
    {
        return "1.0.0";
    }

    public function getProjectId(): int
    {
        return $this->projectId;
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function getValue(): ImmutableLocalizedString
    {
        return $this->value;
    }

    public function setValue(ImmutableLocalizedString $value): self
    {
        $this->value = $value;
        $this->markAsUpdated();

        return $this;
    }
}
<?php
namespace ZEA2\Domain\Bundles\Translation\Repository;

use ZEA2\Domain\Bundles\Translation\Entity\TranslationProject;
use ZEA2\Domain\Bundles\Translation\Entity\TranslationProjectSource;

final class TranslationProjectRepository
{
    public const PROJECT_ID_TEST = -1;

    public const PROJECT_ID_ZEA2_FRONTEND = 1;
    public const PROJECT_ID_ZEA2_ADMIN = 2;

    /** @var TranslationProject[] */
    private $projects = [];

    public function __construct(array $projects)
    {
        $this->projects = array_map(function(array $input) {
            return new TranslationProject(
                $input['id'],
                $input['title'],
                $input['position'],
                array_map(function(array $source) {
                    return new TranslationProjectSource($source['type'], $source['dirs']);
                }, $input['sources'])
            );
        }, $projects);
    }

    public function listProjects(): array
    {
        return $this->projects;
    }

    public function getProjectById(int $projectId): TranslationProject
    {
        $result = array_values(array_filter($this->listProjects(), function(TranslationProject $input) use ($projectId) {
            return $input->getId() === $projectId;
        }));

        if(count($result) !== 1) {
            throw new \Exception(sprintf('Translation project with ID `%d` does not exists or has any duplicates', $projectId));
        }

        return $result[0];
    }
}
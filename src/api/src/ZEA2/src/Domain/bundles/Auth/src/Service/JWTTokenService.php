<?php
namespace ZEA2\Domain\Bundles\Auth\Service;

use Firebase\JWT\BeforeValidException;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use Firebase\JWT\SignatureInvalidException;
use ZEA2\Domain\Service\ServerConfig;
use ZEA2\Domain\Bundles\Account\Entity\Account;
use ZEA2\Domain\Bundles\Auth\Exceptions\JWTTokenFailException;
use ZEA2\Domain\Bundles\Profile\Entity\Profile;
use ZEA2\Domain\Util\GenerateRandomString;
use UnexpectedValueException;
use ZEA2\Platform\Constants\Environment;

final class JWTTokenService
{
    const JWT_DEVELOPMENT_KEY = 'zea2-invite-dev';
    const EXPIRES = 7 /* days */ * 24 /* h */ * 60 /* min */ * 60 /* sec */;

    /** @var string */
    private $jwtSecret;

    /** @var ServerConfig */
    private $serverConfig;

    public function __construct(string $env, string $jwtSecret, ServerConfig $serverConfig)
    {
        if($env === Environment::PRODUCTION && $jwtSecret  === self::JWT_DEVELOPMENT_KEY) {
            throw new \Exception(
                "Do not use development JWT key `%s` for production. Override key `config.auth.jwt.secret` in provide.config.php",
                self::JWT_DEVELOPMENT_KEY
            );
        }

        $this->jwtSecret = $jwtSecret;
        $this->serverConfig = $serverConfig;
    }

    public function generateJWTFor(Account $account, Profile $profile): string
    {
        $time = time();

        $token = array_combine([
            'iat',  // Issued at: time when the token was generated
            'jti',  //   Json Token Id: an unique identifier for the token
            'iss',  // Issuer
            'nbf',  // Not before
            'exp',  // Expire
            'data', // Data related to the signer user
        ], [
            $time,
            GenerateRandomString::generate(32),
            $this->serverConfig->getHost(),
            $time,
            $time + self::EXPIRES,
            [
                'accountId' => $account->getId(),
                'profileId' => $profile->getId(),
            ]
        ]);

        return JWT::encode($token, $this->jwtSecret, 'HS512');
    }

    public function decodeJWT(string $jwt): array
    {
        try {
            return json_decode(json_encode(JWT::decode($jwt, $this->jwtSecret, ['HS512'])), true);
        }catch(UnexpectedValueException | SignatureInvalidException | BeforeValidException | ExpiredException $e) {
            throw new JWTTokenFailException($e->getMessage());
        }
    }
}
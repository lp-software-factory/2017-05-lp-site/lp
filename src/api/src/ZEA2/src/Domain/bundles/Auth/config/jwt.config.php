<?php
namespace ZEA2\Domain\Bundles\Auth;

use ZEA2\Domain\Bundles\Auth\Service\JWTTokenService;

return [
    /* you'll be warned if you try to use this key in production */
    'config.auth.jwt.secret' => JWTTokenService::JWT_DEVELOPMENT_KEY,
];
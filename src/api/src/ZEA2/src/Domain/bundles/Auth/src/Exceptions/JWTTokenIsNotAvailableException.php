<?php
namespace ZEA2\Domain\Bundles\Auth\Exceptions;

final class JWTTokenIsNotAvailableException extends \Exception {}
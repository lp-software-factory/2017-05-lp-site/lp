<?php
namespace ZEA2\Domain\Bundles\Auth\Service;

use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use ZEA2\Domain\Bundles\Account\Entity\Account;
use ZEA2\Domain\Bundles\Account\Parameters\AccountRegisterParameters;
use ZEA2\Domain\Bundles\Account\Service\AccountRegistrationService;
use ZEA2\Domain\Bundles\Account\Service\AccountService;
use ZEA2\Domain\Bundles\Account\Service\PasswordHashService;
use ZEA2\Domain\Bundles\Auth\Entity\SignUpRequest;
use ZEA2\Domain\Bundles\Account\Exceptions\DuplicateAccountException;
use ZEA2\Domain\Bundles\Auth\Exceptions\InvalidPasswordException;
use ZEA2\Domain\Bundles\Auth\Inject\AuthToken;
use ZEA2\Domain\Bundles\Auth\Parameters\SignInParameters;
use ZEA2\Domain\Bundles\Auth\Parameters\SignUpParameters;
use ZEA2\Domain\Bundles\Auth\Repository\SignUpRequestRepository;
use ZEA2\Domain\Bundles\OAuth2\Factory\OAuth2RequestFactory;
use ZEA2\Domain\Bundles\Profile\Parameters\CreateProfileParameters;
use ZEA2\Domain\Bundles\Profile\Service\ProfileService;
use ZEA2\Domain\Bundles\OAuth2\Service\OAuth2Service;
use League\OAuth2\Client\Provider\AbstractProvider;

final class AuthService
{
    public const EVENT_SIGN_IN = 'hi.domain.auth.sign-in';
    public const EVENT_SIGN_IN_OAUTH2 = 'hi.domain.auth.sign-in.oauth2';
    public const EVENT_SIGN_UP = 'hi.domain.auth.sign-in';
    public const EVENT_SIGN_OUT = 'hi.domain.auth.sign-in';

    /** @var EventEmitterInterface */
    private $eventEmitter;

    /** @var SignUpRequestRepository */
    private $repository;

    /** @var AuthToken */
    private $authToken;

    /** @var JWTTokenService */
    private $jwtTokenService;

    /** @var PasswordHashService */
    private $passwordHashService;

    /** @var OAuth2Service */
    private $oauth2Service;

    /** @var AccountService */
    private $accountService;

    /** @var AccountRegistrationService */
    private $accountRegistrationService;

    /** @var ProfileService */
    private $profileService;

    /** @var AuthMailService */
    private $authMailService;

    /** @var OAuth2RequestFactory */
    private $oAuth2RequestFactory;

    public function __construct(
        AuthToken $authToken,
        JWTTokenService $jwtTokenService,
        PasswordHashService $passwordHashService,
        OAuth2Service $oAuth2Service,
        AccountService $accountService,
        AccountRegistrationService $accountRegistrationService,
        ProfileService $profileService,
        AuthMailService $authMailService,
        SignUpRequestRepository $repository,
        OAuth2RequestFactory $oAuth2RequestFactory
    ) {
        $this->eventEmitter = new EventEmitter();
        $this->authToken = $authToken;
        $this->jwtTokenService = $jwtTokenService;
        $this->passwordHashService = $passwordHashService;
        $this->oauth2Service = $oAuth2Service;
        $this->accountService = $accountService;
        $this->accountRegistrationService = $accountRegistrationService;
        $this->profileService = $profileService;
        $this->authMailService = $authMailService;
        $this->oAuth2RequestFactory = $oAuth2RequestFactory;
        $this->repository = $repository;
    }

    public function getEventEmitter(): EventEmitterInterface
    {
        return $this->eventEmitter;
    }

    public function signUp(SignUpParameters $parameters): SignUpRequest
    {
        $request = new SignUpRequest(
            $parameters->getEmail(),
            $this->passwordHashService->hash($parameters->getPassword()),
            $parameters->getFirstName(),
            $parameters->getLastName(),
            $parameters->hasPhone() ? $parameters->getPhone() : null,
            $parameters->isIsCompanyProfile(),
            $parameters->isIsCompanyProfile() ? $parameters->getCompanyName() : null
        );

        $email = $request->getEmail();

        if($this->accountService->hasAccountWithEmail($email)) {
            throw new DuplicateAccountException(sprintf('Account with email `%s` already exists', $email));
        }

        $this->repository->createSignUpRequest($request);
        $this->authMailService->sendTokenVerificationEmail($request);

        return $request;
    }

    public function auth(Account $account): AuthToken
    {
        $jwtToken = $this->jwtTokenService->generateJWTFor($account, $account->getCurrentProfile());

        $this->authToken->auth($jwtToken);

        return $this->authToken;
    }

    public function signIn(SignInParameters $parameters): AuthToken
    {
        $email = $parameters->getEmail();
        $password = $parameters->getPassword();

        $account = $this->accountService->getAccountWithEmail($email);

        if(! $account->isPasswordEquals($password)) {
            throw new InvalidPasswordException('Invalid password');
        }

        $this->eventEmitter->emit(self::EVENT_SIGN_IN, [$account]);

        return $this->auth($account);
    }

    public function signInOauth2(AbstractProvider $provider, string $code): AuthToken
    {
        $accessToken = $provider->getAccessToken('authorization_code', [
            'code' =>  $code,
        ]);

        $oAuth2Account = $this->oauth2Service->signIn(
            $this->oAuth2RequestFactory->createOAuth2Request($provider, $accessToken)
        );

        $this->eventEmitter->emit(self::EVENT_SIGN_IN_OAUTH2, [$oAuth2Account]);
        $this->eventEmitter->emit(self::EVENT_SIGN_IN, [$oAuth2Account->getAccount()]);

        return $this->auth($oAuth2Account->getAccount());
    }

    public function signOut()
    {
        if($this->authToken->isSignedIn()) {
            $this->eventEmitter->emit(self::EVENT_SIGN_OUT, [$this->authToken->getAccount()]);
            $this->authToken->signOut();
        }
    }

    public function acceptSignUpRequest(string $acceptToken): AuthToken
    {
        $signUpRequest = $this->repository->getSignUpRequestsWithAcceptToken($acceptToken);

        if($this->accountService->hasAccountWithEmail($signUpRequest->getEmail())) {
            throw new DuplicateAccountException(sprintf('Account with email `%s` already exists', $signUpRequest->getEmail()));
        }

        $account = $this->createAccountFromSignUpRequest($signUpRequest);

        $this->eventEmitter->emit(self::EVENT_SIGN_UP, [$account]);
        $this->eventEmitter->emit(self::EVENT_SIGN_IN, [$account]);

        return $this->auth($account);
    }

    private function createAccountFromSignUpRequest(SignUpRequest $request): Account
    {
        $account = $this->accountRegistrationService->registerUserAccount(new AccountRegisterParameters(
            $request->getEmail(),
            $request->getPassword(),
            new CreateProfileParameters(
                $request->getFirstName(),
                $request->getLastName()
            )
        ), [
            'hash_password' => false,
        ]);

        if($request->hasPhone()) {
            $this->accountService->setPhone($account, $request->getPhone());
        }

        return $account;
    }
}
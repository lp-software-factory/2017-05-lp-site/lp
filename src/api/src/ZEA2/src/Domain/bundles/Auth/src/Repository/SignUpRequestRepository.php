<?php
namespace ZEA2\Domain\Bundles\Auth\Repository;

use Doctrine\ORM\EntityRepository;
use ZEA2\Domain\Bundles\Auth\Entity\SignUpRequest;
use ZEA2\Domain\Bundles\Auth\Exceptions\SignUpRequestNotFoundException;
use ZEA2\Domain\Criteria\IdsCriteria;
use ZEA2\Domain\Criteria\SeekCriteria;

final class SignUpRequestRepository extends EntityRepository
{
    public function createSignUpRequest(SignUpRequest $signUpRequest): int
    {
        while($this->hasWithSID($signUpRequest->getSID())) {
            $signUpRequest->regenerateSID();
        }

        $this->getEntityManager()->persist($signUpRequest);
        $this->getEntityManager()->flush($signUpRequest);

        return $signUpRequest->getId();
    }

    public function saveSignUpRequest(SignUpRequest $signUpRequest)
    {
        $this->getEntityManager()->flush($signUpRequest);
    }

    public function deleteSignUpRequest(SignUpRequest $signUpRequest)
    {
        $this->getEntityManager()->remove($signUpRequest);
        $this->getEntityManager()->flush($signUpRequest);
    }

    public function listSignUpRequests(SeekCriteria $seek): array
    {
        $query = $this->createQueryBuilder('c');
        $query->setFirstResult($seek->getOffset());
        $query->setMaxResults($seek->getLimit());

        return $query->getQuery()->execute();
    }

    public function getById(int $id): SignUpRequest
    {
        $result = $this->find($id);

        if($result instanceof SignUpRequest) {
            return $result;
        }else{
            throw new SignUpRequestNotFoundException(sprintf('SignUpRequest `ID(%d)` not found', $id));
        }
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        $ids = $idsCriteria->getIds();
        $results = $this->findBy(['id' => $ids]);

        if(count($ids) !== count($results)) {
            throw new \Exception('SignUpRequests with IDs (%s) not found',
                array_diff($ids, array_map(function(SignUpRequest $signUpRequest) {
                    return $signUpRequest->getId();
                }, $results)));
        }

        return $results;
    }

    public function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

    public function getBySID(string $sid): SignUpRequest
    {
        $result = $this->findOneBy(['sid' => $sid]);

        if($result instanceof SignUpRequest) {
            return $result;
        }else{
            throw new SignUpRequestNotFoundException(sprintf('SignUpRequest `SID(%s)` not found', $sid));
        }
    }

    public function hasSignUpRequestsWithAcceptToken(string $acceptToken): bool
    {
        return $this->findOneBy(['acceptToken' => $acceptToken]) !== null;
    }

    public function getSignUpRequestsWithAcceptToken(string $acceptToken): SignUpRequest
    {
        $result = $this->findOneBy(['acceptToken' => $acceptToken]);

        if($result instanceof SignUpRequest) {
            return $result;
        }else{
            throw new SignUpRequestNotFoundException(sprintf('Invalid access token'));
        }
    }

    public function getSignUpRequestsByEmail(string $email): array
    {
        return $this->findBy(['email' => $email]);
    }
}
<?php
namespace ZEA2\Domain\Bundles\Auth;

use function DI\get;
use function DI\object;
use function DI\factory;

use ZEA2\Domain\Bundles\Auth\Config\SessionJWTServiceConfig;
use ZEA2\Domain\Bundles\Auth\Factory\Doctrine\SignUpRequestRepositoryFactory;
use ZEA2\Domain\Bundles\Auth\Repository\SignUpRequestRepository;
use ZEA2\Domain\Bundles\Auth\Service\JWTTokenService;

return [
    SessionJWTServiceConfig::class => object()->constructorParameter('config', get('config.zea2.main-site.account.session')),
    SignUpRequestRepository::class => factory(SignUpRequestRepositoryFactory::class),
    JWTTokenService::class => object()
        ->constructorParameter('env', get('config.environment'))
        ->constructorParameter('jwtSecret', get('config.auth.jwt.secret')),
];
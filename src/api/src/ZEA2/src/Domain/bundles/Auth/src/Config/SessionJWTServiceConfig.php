<?php
namespace ZEA2\Domain\Bundles\Auth\Config;

final class SessionJWTServiceConfig
{
    private $sessionKey;

    public function __construct(array $config)
    {
        $this->sessionKey = $config['key'];
    }

    public function getSessionKey()
    {
        return $this->sessionKey;
    }
}
<?php
namespace ZEA2\Domain\Bundles\Auth;

use ZEA2\Domain\Bundles\Auth\Frontend\AuthTokenFrontendScript;
use ZEA2\Domain\Bundles\Auth\Subscriptions\AuthTokenSubscription;
use ZEA2\Domain\Bundles\Auth\Subscriptions\ResetPasswordServiceSubscription;
use ZEA2\Platform\Bundles\Frontend\Markers\FrontendBundleInjectable\FrontendBundleInjectable;
use ZEA2\Platform\Bundles\Markers\ZEA2EventSubscriptionsBundle\ZEA2EventSubscriptionsBundle;
use ZEA2\Platform\Bundles\ZEA2Bundle;

final class AuthDomainBundle extends ZEA2Bundle implements FrontendBundleInjectable, ZEA2EventSubscriptionsBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getFrontendScripts(): array
    {
        return [
            AuthTokenFrontendScript::class,
        ];
    }

    public function getEventScripts(): array
    {
        return [
            AuthTokenSubscription::class,
            ResetPasswordServiceSubscription::class,
        ];
    }
}
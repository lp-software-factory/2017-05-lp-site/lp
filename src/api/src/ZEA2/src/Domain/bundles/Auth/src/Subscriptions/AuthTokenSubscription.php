<?php
namespace ZEA2\Domain\Bundles\Auth\Subscriptions;

use ZEA2\Domain\Bundles\Auth\Inject\AuthToken;
use ZEA2\Domain\Bundles\Auth\Service\SessionJWTService;
use ZEA2\Platform\Event\ZEA2EventEmitter;
use ZEA2\Platform\Event\ZEA2EventScript;

final class AuthTokenSubscription implements ZEA2EventScript
{
    /** @var AuthToken */
    private $authToken;

    /** @var SessionJWTService */
    private $sessionJWTService;

    public function __construct(AuthToken $authToken, SessionJWTService $sessionJWTService)
    {
        $this->authToken = $authToken;
        $this->sessionJWTService = $sessionJWTService;
    }

    public function __invoke(ZEA2EventEmitter $eventEmitter)
    {
        $this->authToken->getEvenEmitter()->on(AuthToken::EVENT_AUTH, function(string $jwt, AuthToken $authToken) {
            $this->sessionJWTService->saveJWT($jwt);
        });
    }
}
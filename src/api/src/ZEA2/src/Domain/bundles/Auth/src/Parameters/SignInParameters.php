<?php
namespace ZEA2\Domain\Bundles\Auth\Parameters;

final class SignInParameters
{
    /** @var string */
    private $email;

    /** @var string */
    private $password;

    public function __construct(string $email, string $password)
    {
        $this->email = $email;
        $this->password = $password;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }
}
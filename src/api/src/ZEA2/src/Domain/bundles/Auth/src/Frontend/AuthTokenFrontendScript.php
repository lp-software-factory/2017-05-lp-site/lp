<?php
namespace ZEA2\Domain\Bundles\Auth\Frontend;

use ZEA2\Domain\Bundles\Auth\Formatter\AuthTokenFormatter;
use ZEA2\Domain\Bundles\Auth\Inject\AuthToken;
use ZEA2\Platform\Bundles\Frontend\Markers\FrontendBundleInjectable\FrontendScript;

final class AuthTokenFrontendScript implements FrontendScript
{
    /** @var AuthToken */
    private $authToken;

    /** @var AuthTokenFormatter */
    private $authTokenFormatter;
    
    public function __construct(AuthToken $authToken, AuthTokenFormatter $authTokenFormatter)
    {
        $this->authToken = $authToken;
        $this->authTokenFormatter = $authTokenFormatter;
    }

    public function __invoke(): array
    {
        if($this->authToken->isSignedIn()) {
            return [
                'auth' => $this->authTokenFormatter->format($this->authToken)
            ];
        }else{
            return [];
        }
    }
}
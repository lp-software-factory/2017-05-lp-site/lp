<?php
namespace ZEA2\Domain\Bundles\Auth\Formatter;

use ZEA2\Domain\Bundles\Auth\Inject\AuthToken;

final class AuthTokenFormatter
{
    public function format(AuthToken $authToken): array
    {
        return $authToken->toJSON();
    }
}
<?php
namespace ZEA2\Domain\Bundles\Auth\Exceptions;

final class AuthTokenIsNotAvailableException extends \Exception {}
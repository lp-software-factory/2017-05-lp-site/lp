<?php
namespace ZEA2\Domain\Bundles\Auth\InitScripts;

use ZEA2\Domain\Bundles\Auth\Inject\AuthToken;
use ZEA2\Domain\Bundles\Auth\Service\SessionJWTService;
use ZEA2\Platform\Init\InitScript;

final class SessionJWTInitScript implements InitScript
{
    /** @var AuthToken */
    private $authToken;

    /** @var SessionJWTService */
    private $sessionJWTToken;

    public function __construct(AuthToken $authToken, SessionJWTService $sessionJWTToken)
    {
        $this->authToken = $authToken;
        $this->sessionJWTToken = $sessionJWTToken;
    }

    public function __invoke()
    {
        if($this->sessionJWTToken->isAvailable()) {
            $this->authToken->auth($this->sessionJWTToken->getJWT());
        }
    }
}
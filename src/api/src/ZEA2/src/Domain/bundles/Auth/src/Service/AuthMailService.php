<?php
namespace ZEA2\Domain\Bundles\Auth\Service;

use ZEA2\Domain\Bundles\Auth\Entity\SignUpRequest;
use ZEA2\Domain\Bundles\Mail\Service\MailService;

final class AuthMailService
{
    public const MAIL_TYPE_SIGN_UP = 'hi.domain.auth.sign-up';

    /** @var MailService */
    private $mailService;

    public function __construct(MailService $mailService)
    {
        $this->mailService = $mailService;
    }

    public function sendTokenVerificationEmail(SignUpRequest $request): \Swift_Message
    {
        $replaces = [
            'auth_proceed_token' => $request->getAcceptToken(),
            'auth_first_name' => $request->getFirstName(),
            'auth_email' => $request->getEmail(),
            'auth_request' => $request,
        ];

        $message = \Swift_Message::newInstance()
            ->setTo($request->getEmail())
            ->setContentType('text/html');

        $message->setBody($this->mailService->getTemplateLocalized('@mail/api/zea2/domain/auth/sign-up', $replaces));

        $this->mailService->sendNoReply($message, self::MAIL_TYPE_SIGN_UP);

        return $message;
    }
}
<?php
namespace ZEA2\Domain\Bundles\Auth\Subscriptions;

use ZEA2\Domain\Bundles\Account\Entity\Account;
use ZEA2\Domain\Bundles\Account\Service\ResetPasswordService;
use ZEA2\Domain\Bundles\Auth\Service\AuthService;
use ZEA2\Platform\Event\ZEA2EventEmitter;
use ZEA2\Platform\Event\ZEA2EventScript;

final class ResetPasswordServiceSubscription implements ZEA2EventScript
{
    /** @var AuthService */
    private $authService;

    /** @var ResetPasswordService */
    private $resetPasswordService;

    public function __construct(AuthService $authService, ResetPasswordService $resetPasswordService)
    {
        $this->authService = $authService;
        $this->resetPasswordService = $resetPasswordService;
    }

    public function __invoke(ZEA2EventEmitter $eventEmitter)
    {
        $this->resetPasswordService->getEventEmitter()->on(ResetPasswordService::EVENT_RESET_PASSWORD, function(Account $account) {
            $this->authService->auth($account);
        });
    }
}
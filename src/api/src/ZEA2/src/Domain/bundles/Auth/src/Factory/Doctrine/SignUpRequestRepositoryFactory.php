<?php
namespace ZEA2\Domain\Bundles\Auth\Factory\Doctrine;

use ZEA2\Domain\Bundles\Auth\Entity\SignUpRequest;
use ZEA2\Platform\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;

final class SignUpRequestRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return SignUpRequest::class;
    }
}
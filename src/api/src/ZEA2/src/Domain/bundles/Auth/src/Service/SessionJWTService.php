<?php
namespace ZEA2\Domain\Bundles\Auth\Service;

use ZEA2\Domain\Bundles\Auth\Config\SessionJWTServiceConfig;

final class SessionJWTService
{
    /** @var \ZEA2\Domain\Bundles\Auth\Config\SessionJWTServiceConfig */
    private $config;

    public function __construct(SessionJWTServiceConfig $config)
    {
        $this->config = $config;
    }

    public function saveJWT(string $jwt)
    {
        $_SESSION[$this->config->getSessionKey()] = $jwt;
    }

    public function getJWT(): string
    {
        if(! $this->isAvailable()) {
            throw new \Exception("Session JWT is not available");
        }

        return $_SESSION[$this->config->getSessionKey()];
    }

    public function isAvailable(): bool
    {
        return isset($_SESSION[$this->config->getSessionKey()])
            && is_string($_SESSION[$this->config->getSessionKey()])
            && strlen($_SESSION[$this->config->getSessionKey()]) > 0;
    }

    public function destroyJWT()
    {
        unset($_SESSION[$this->config->getSessionKey()]);
    }
}
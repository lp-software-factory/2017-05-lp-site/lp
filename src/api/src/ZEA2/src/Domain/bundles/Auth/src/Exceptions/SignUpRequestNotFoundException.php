<?php
namespace ZEA2\Domain\Bundles\Auth\Exceptions;

final class SignUpRequestNotFoundException extends \Exception {}
<?php
namespace ZEA2\Domain\Bundles\Version;

use ZEA2\Platform\Bundles\ZEA2Bundle;

final class VersionDomainBundle extends ZEA2Bundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}
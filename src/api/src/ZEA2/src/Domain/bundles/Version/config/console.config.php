<?php
namespace ZEA2\Domain\Bundles\Version;

use ZEA2\Domain\Bundles\Version\Console\Command\CurrentVersionCommand;

return [
    'config.zea2.platform.console' => [
        'commands' => [
            'Version' => [
                CurrentVersionCommand::class,
            ]
        ]
    ]
];
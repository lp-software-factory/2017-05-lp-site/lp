<?php
namespace ZEA2\Domain\Bundles\Version;

use function DI\get;
use function DI\object;

use ZEA2\Domain\Bundles\Version\Service\VersionService;

return [
    VersionService::class => object()
        ->constructorParameter('composer', get('config.zea2.platform.composer.json')),
];
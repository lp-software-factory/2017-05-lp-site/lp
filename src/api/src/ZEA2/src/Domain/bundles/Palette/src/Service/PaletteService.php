<?php
namespace ZEA2\Domain\Bundles\Palette\Service;

use ZEA2\Domain\Bundles\Palette\Entity\Palette;
use ZEA2\Domain\Bundles\Palette\Repository\ColorsRepository;
use ZEA2\Domain\Bundles\Palette\Repository\PaletteRepository;

class PaletteService
{
    /** @var \ZEA2\Domain\Bundles\Palette\Repository\ColorsRepository */
    private $colorRepository;

    /** @var \ZEA2\Domain\Bundles\Palette\Repository\PaletteRepository */
    private $paletteRepository;

    public function __construct(ColorsRepository $colorRepository, PaletteRepository $paletteRepository)
    {
        $this->colorRepository = $colorRepository;
        $this->paletteRepository = $paletteRepository;
    }

    public function getColors(): array
    {
        return $this->colorRepository->getColors();
    }

    public function getPalettes(): array
    {
        return $this->paletteRepository->getPalettes();
    }

    public function getRandomPalette(): Palette
    {
        $palettes = $this->paletteRepository->getPalettes();

        return $palettes[array_rand($palettes)];
    }
}
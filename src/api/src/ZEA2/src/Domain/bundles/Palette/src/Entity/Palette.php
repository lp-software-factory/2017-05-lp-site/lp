<?php
namespace ZEA2\Domain\Bundles\Palette\Entity;

use ZEA2\Domain\Markers\JSONSerializable;
use ZEA2\Domain\Markers\VersionEntity\VersionEntity;
use ZEA2\Domain\Markers\VersionEntity\VersionEntityTrait;

class Palette implements JSONSerializable, VersionEntity
{
    public const LATEST_VERSION = '1.0.0';

    use VersionEntityTrait;

    /** @var string */
    private $code;

    /** @var Color */
    private $background;

    /** @var Color */
    private $foreground;

    /** @var Color */
    private $border;

    public function __construct(string $code, Color $background, Color $foreground, Color $border)
    {
        $this->setCurrentVersion(self::LATEST_VERSION);

        $this->code = $code;
        $this->background = $background;
        $this->foreground = $foreground;
        $this->border = $border;
    }

    public static function createFromJSON(array $json)
    {
        return new self(
            $json['code'],
            new Color($json['background']['code'], $json['background']['hexCode']),
            new Color($json['foreground']['code'], $json['foreground']['hexCode']),
            new Color($json['border']['code'], $json['border']['hexCode'])
        );
    }

    public function toJSON(array $options = []): array
    {
        return [
            'code' => $this->code,
            'background' => $this->background->toJSON($options, $options),
            'foreground' => $this->foreground->toJSON($options, $options),
            'border' => $this->border->toJSON($options, $options),
        ];
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getBackground(): Color
    {
        return $this->background;
    }

    public function getForeground(): Color
    {
        return $this->foreground;
    }

    public function getBorder(): Color
    {
        return $this->border;
    }
}
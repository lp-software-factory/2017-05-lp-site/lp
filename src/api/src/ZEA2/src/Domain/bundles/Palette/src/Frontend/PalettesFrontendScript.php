<?php
namespace ZEA2\Domain\Bundles\Palette\Frontend;

use ZEA2\Domain\Bundles\Palette\Entity\Palette;
use ZEA2\Domain\Bundles\Palette\Service\PaletteService;
use ZEA2\Platform\Bundles\Frontend\Markers\FrontendBundleInjectable\FrontendScript;

abstract class PalettesFrontendScript implements FrontendScript
{
    /** @var PaletteService */
    private $paletteService;

    public function __construct(PaletteService $paletteService)
    {
        $this->paletteService = $paletteService;
    }

    public function __invoke(): array
    {
        return [
            'palettes' => array_map(function(Palette $palette) {
                return $palette->toJSON();
            }, $this->paletteService->getPalettes())
        ];
    }
}
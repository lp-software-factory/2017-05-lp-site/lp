<?php
namespace ZEA2\Domain\Bundles\Palette;

use ZEA2\Platform\Bundles\ZEA2Bundle;

final class PaletteDomainBundle extends ZEA2Bundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}
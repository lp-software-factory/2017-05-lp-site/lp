<?php
namespace ZEA2\Domain\Bundles\Palette;

use ZEA2\Domain\Bundles\Palette\Doctrine\Type\Palette\PaletteType;

return [
    'config.zea2.domain.doctrine.types' => [
        'palette' => PaletteType::class,
    ]
];
<?php
namespace ZEA2\Domain\Bundles\ACL\InTheSet\AdminAccount;

use ZEA2\Domain\Bundles\ACL\Entity\ACLResource;
use ZEA2\Domain\Bundles\ACL\Entity\ACLRoleResolver;

final class AdminAccountRoleResolver implements ACLRoleResolver
{
    public function isAllowed(ACLResource $resource)
    {
        return __DIR__;
    }
}
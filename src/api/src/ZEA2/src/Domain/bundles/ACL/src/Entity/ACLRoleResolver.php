<?php
namespace ZEA2\Domain\Bundles\ACL\Entity;

interface ACLRoleResolver
{
    public function isAllowed(ACLResource $resource);
}
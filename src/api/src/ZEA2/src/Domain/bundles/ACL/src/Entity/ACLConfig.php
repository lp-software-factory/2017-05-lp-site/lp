<?php
namespace ZEA2\Domain\Bundles\ACL\Entity;

interface ACLConfig
{
    public function getACLRoles(ACLResource $resource): array;
}
<?php
namespace ZEA2\Domain\Bundles\ACL\InTheSet\UserAccountRoleResolver;

use ZEA2\Domain\Bundles\ACL\Entity\ACLResource;
use ZEA2\Domain\Bundles\ACL\Entity\ACLRoleResolver;

final class UserAccountRoleResolver implements ACLRoleResolver
{
    public function isAllowed(ACLResource $resource)
    {
        return __DIR__;
    }
}
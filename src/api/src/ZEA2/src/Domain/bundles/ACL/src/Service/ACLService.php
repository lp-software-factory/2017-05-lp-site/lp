<?php
namespace ZEA2\Domain\Bundles\ACL\Service;

use Interop\Container\ContainerInterface;
use ZEA2\Domain\Bundles\ACL\Entity\ACLAwareEntity;
use ZEA2\Domain\Bundles\ACL\Entity\ACLConfig;
use ZEA2\Domain\Bundles\ACL\Entity\ACLRole;
use ZEA2\Domain\Bundles\ACL\Entity\ACLRoleResolver;
use ZEA2\Domain\Bundles\ACL\Exceptions\ACLAccessException;

final class ACLService
{
    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function access(ACLAwareEntity $entity, string $key): void
    {
        if(! $this->isAllowed($entity, $key)) {
            throw new ACLAccessException(sprintf('You have no access for key `%s`', $key));
        }
    }

    public function isAllowed(ACLAwareEntity $entity, string $key): bool
    {
        /** @var ACLConfig $config */
        $config = $this->container->get($entity->getACLConfigClassName());
        $resource = $entity->getACLKey($key);

        foreach($config->getACLRoles($resource) as $aclRole) {
            if($aclRole instanceof ACLRole) {
                $resolver = $this->getRoleResolverFor($aclRole);

                if($resolver->isAllowed($resource)) {
                    return true;
                }
            }else{
                throw new \Exception(sprintf('Invalid role, should be an instance of ', ACLRole::class));
            }
        }

        return false;
    }

    private function getRoleResolverFor(ACLRole $role): ACLRoleResolver
    {
        return $this->container->get($role->getResolverClassName());
    }
}
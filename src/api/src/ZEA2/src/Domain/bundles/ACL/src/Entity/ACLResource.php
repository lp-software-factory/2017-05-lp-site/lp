<?php
namespace ZEA2\Domain\Bundles\ACL\Entity;

interface ACLResource
{
    public function getKey(): string;
    public function getValue(): array;
}
<?php
namespace ZEA2\Domain\Bundles\ACL\Entity;

use ZEA2\Domain\Util\ArrayValueUsingDotsNotation;

trait ACLAwareEntityDoctrineTrait
{
    /**
     * @Column(type="json_array", name="acl")
     * @var array
     */
    private $acl = [];

    public function getACLFull(): array
    {
        return $this->acl;
    }

    public function getACLKey(string $key): array
    {
        return ArrayValueUsingDotsNotation::get($this->acl, $key);
    }

    public function setACLKey(string $key, array $value): void
    {
        ArrayValueUsingDotsNotation::set($this->acl, $key, $value);
    }
}
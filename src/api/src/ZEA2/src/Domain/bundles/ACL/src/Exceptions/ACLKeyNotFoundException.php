<?php
namespace ZEA2\Domain\Bundles\ACL\Exceptions;

final class ACLKeyNotFoundException extends ACLException {}
<?php
namespace ZEA2\Domain\Bundles\ACL\Entity;

interface ACLAwareEntity
{
    public function getACLConfigClassName(): string;
    public function getACLFull(): array;
    public function getACLKey(string $key): ACLResource;
    public function setACLKey(string $key, array $value): ACLResource;
}
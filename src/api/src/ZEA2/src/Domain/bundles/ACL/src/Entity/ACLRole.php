<?php
namespace ZEA2\Domain\Bundles\ACL\Entity;

interface ACLRole
{
    public function getCode(): string;
    public function getResolverClassName(): string;
}
<?php
namespace ZEA2\Domain\Bundles\ACL\InTheSet\AdminAccount;

use ZEA2\Domain\Bundles\ACL\Entity\ACLRole;

final class AdminAccountRole implements ACLRole
{
    public const ROLE_CODE_ACCOUNT_ADMIN = 'zea2.account.admin';

    public function getCode(): string
    {
        return self::ROLE_CODE_ACCOUNT_ADMIN;
    }

    public function getResolverClassName(): string
    {
        return AdminAccountRoleResolver::class;
    }
}
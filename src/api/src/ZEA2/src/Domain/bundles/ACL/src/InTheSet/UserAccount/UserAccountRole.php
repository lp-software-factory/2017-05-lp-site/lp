<?php
namespace ZEA2\Domain\Bundles\ACL\InTheSet\UserAccountRoleResolver;

use ZEA2\Domain\Bundles\ACL\Entity\ACLRole;

final class UserAccountRole implements ACLRole
{
    public const ROLE_CODE_ACCOUNT_USER = 'zea2.account.user';

    public function getCode(): string
    {
        return self::ROLE_CODE_ACCOUNT_USER;
    }

    public function getResolverClassName(): string
    {
        return UserAccountRoleResolver::class;
    }
}
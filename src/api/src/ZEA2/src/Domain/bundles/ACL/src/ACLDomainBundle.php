<?php
namespace ZEA2\Domain\Bundles\ACL;

use ZEA2\Platform\Bundles\ZEA2Bundle;

final class ACLDomainBundle extends ZEA2Bundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}
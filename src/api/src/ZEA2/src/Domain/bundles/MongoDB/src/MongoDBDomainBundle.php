<?php
namespace ZEA2\Domain\Bundles\MongoDB;

use ZEA2\Platform\Bundles\ZEA2Bundle;

final class MongoDBDomainBundle extends ZEA2Bundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}
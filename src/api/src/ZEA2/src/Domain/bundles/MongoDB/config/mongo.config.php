<?php
namespace ZEA2\Domain\Bundles\MongoDB;

return [
    'config.zea2.domain.mongodb.connection' => [
        'db_prefix' => 'zea2',
        'server' => 'mongodb://127.0.0.1:27017',
        'options' => [
            'connect' => true
        ],
        'driver_options' => []
    ],
];
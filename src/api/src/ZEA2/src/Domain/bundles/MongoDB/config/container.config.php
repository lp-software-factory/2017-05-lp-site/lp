<?php
namespace ZEA2\Domain\Bundles\Doctrine;

use function DI\factory;

use ZEA2\Domain\Bundles\MongoDB\Factory\MongoDBFactory;
use MongoDB\Database;

return [
    Database::class => factory(MongoDBFactory::class),
];
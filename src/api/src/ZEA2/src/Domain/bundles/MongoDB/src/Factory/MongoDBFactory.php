<?php
namespace ZEA2\Domain\Bundles\MongoDB\Factory;

use MongoDB\Database;
use MongoDB\Client;
use Interop\Container\ContainerInterface;
use ZEA2\Platform\Service\EnvironmentService;

final class MongoDBFactory
{
    public function __invoke(ContainerInterface $container, EnvironmentService $environmentService): Database
    {
        $config = $container->get('config.zea2.domain.mongodb.connection');
        $env = $environmentService->getCurrent();

        $mongoClient = new Client(
            $config['server'] ?? 'mongodb://127.0.0.1:27017',
            $config['options'] ?? ['connect' => true],
            $config['driver_options'] ?? []
        );

        return $mongoClient->selectDatabase(sprintf('%s_%s', $config['db_prefix'] ?? 'zea2', $env));
    }
}
<?php
namespace ZEA2\Domain\Bundles\Mail;

use ZEA2\Platform\Bundles\ZEA2Bundle;

final class MailDomainBundle extends ZEA2Bundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}
<?php
namespace ZEA2\Domain\Bundles\Mail;

use function DI\factory;
use function DI\get;

use ZEA2\Domain\Bundles\Mail\Factory\SwiftTransportFactory;
use ZEA2\Domain\Bundles\Mail\Spool\PHPUnitMemorySpool;

return [
    \Swift_MemorySpool::class => get(PHPUnitMemorySpool::class),
    \Swift_Events_EventDispatcher::class => get(\Swift_Events_SimpleEventDispatcher::class),
    \Swift_Transport::class => factory(SwiftTransportFactory::class),
];
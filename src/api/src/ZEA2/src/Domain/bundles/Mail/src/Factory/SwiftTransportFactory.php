<?php
namespace ZEA2\Domain\Bundles\Mail\Factory;

use Interop\Container\ContainerInterface;
use ZEA2\Platform\Constants\Environment;
use ZEA2\Platform\Service\EnvironmentService;

final class SwiftTransportFactory
{
    public function __invoke(ContainerInterface $container, EnvironmentService $environmentService)
    {
        if($environmentService->getCurrent() === Environment::TESTING) {
            return new \Swift_Transport_SpoolTransport(
                new \Swift_Events_SimpleEventDispatcher(),
                $container->get(\Swift_MemorySpool::class)
            );
        }else{
            return new \Swift_Transport_MailTransport(
                new \Swift_Transport_SimpleMailInvoker(),
                new \Swift_Events_SimpleEventDispatcher()
            );
        }
    }
}
<?php
namespace ZEA2\Domain\Bundles\Mail\Exceptions;

final class FailedToSendMessageException extends \Exception {}
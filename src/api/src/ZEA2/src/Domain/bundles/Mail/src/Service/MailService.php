<?php
namespace ZEA2\Domain\Bundles\Mail\Service;

use ZEA2\Domain\Bundles\Locale\Service\LocaleService;
use ZEA2\Domain\Bundles\Mail\Config\MailConfig;
use ZEA2\Domain\Bundles\Mail\Exceptions\FailedToSendMessageException;
use ZEA2\Domain\Bundles\Twig\Service\TwigService;

final class MailService
{
    /** @var bool */
    private $enabled = true;

    /** @var \Swift_Mailer */
    private $mailer;

    /** @var MailConfig */
    private $mailConfig;

    /** @var MailTemplateService */
    private $mailTemplateService;

    /** @var TwigService */
    private $twigService;

    /** @var LocaleService */
    private $localeService;

    public function __construct(
        \Swift_Mailer $mailer,
        MailTemplateService $mailTemplateService,
        MailConfig $noReply,
        TwigService $twigService,
        LocaleService $localeService
    ) {
        $this->mailer = $mailer;
        $this->mailTemplateService = $mailTemplateService;
        $this->mailConfig = $noReply;
        $this->twigService = $twigService;
        $this->localeService = $localeService;
    }

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function enable(): void
    {
        $this->enabled = true;
    }

    public function disable(): void
    {
        $this->enabled = false;
    }

    public function getTemplate(string $twigPath, array $context): string
    {
        return $this->twigService->render($twigPath, $context);
    }

    public function getTemplateLocalized(string $twigPath, array $context): string
    {
        $current = $this->localeService->getCurrentLocale();
        $twigPath = sprintf('%s/%s.%s/index.html.twig', $twigPath, $current->getLanguage(), $current->getRegion());

        return $this->twigService->render($twigPath, $context);
    }

    public function sendNoReply(\Swift_Message $message, string $mailType): void
    {
        $message->setFrom([$this->mailConfig->getFrom() => $this->mailConfig->getFromName()]);

        $this->send($message, $mailType);
    }

    public function send(\Swift_Message $message, string $mailType): void
    {
        if(! strlen($message->getSubject())) {
            preg_match('/\<title\>(.*)\<\/title\>/', $message->getBody(), $matches);

            if(isset($matches[1])) {
                $message->setSubject($matches[1]);
            }else{
                $message->setSubject('No Reply');
            }
        }

        if($this->isEnabled()) {
            if($this->mailer->send($message) === 0) {
                throw new FailedToSendMessageException(sprintf('Failed to send message `%s`',$message->getSubject()));
            }
        }
    }
}
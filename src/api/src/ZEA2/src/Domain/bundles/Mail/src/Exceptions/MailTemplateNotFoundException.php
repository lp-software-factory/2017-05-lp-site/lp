<?php
namespace ZEA2\Domain\Bundles\Mail\Exceptions;

final class MailTemplateNotFoundException extends \Exception {}
<?php
namespace ZEA2\Domain\Bundles\Colors\Frontend;

use ZEA2\Domain\Bundles\Colors\Entity\Palette;
use ZEA2\Domain\Bundles\Colors\Service\ColorsService;
use ZEA2\Platform\Bundles\Frontend\Markers\FrontendBundleInjectable\FrontendScript;

abstract class ConfigColorsFrontendScript implements FrontendScript
{
    /** @var ColorsService */
    private $colorService;

    public function __construct(ColorsService $colorService)
    {
        $this->colorService = $colorService;
    }

    public function __invoke(): array
    {
        return [
            'config' => [
                'palettes' => array_map(function (Palette $palette) {
                    return $palette->toJSON();
                }, $this->colorService->getPalettes())
            ]
        ];
    }
}
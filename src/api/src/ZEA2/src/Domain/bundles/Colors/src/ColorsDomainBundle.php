<?php
namespace ZEA2\Domain\Bundles\Colors;

use ZEA2\Platform\Bundles\ZEA2Bundle;

final class ColorsDomainBundle extends ZEA2Bundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}
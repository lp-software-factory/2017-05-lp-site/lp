<?php
namespace ZEA2\Domain\Bundles\Attachment\Service;

interface AttachmentTypeDetector
{
    public static function detect(string $tmpFile);
}
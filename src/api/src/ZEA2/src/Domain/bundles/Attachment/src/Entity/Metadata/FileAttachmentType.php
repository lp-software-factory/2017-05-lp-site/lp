<?php
namespace ZEA2\Domain\Bundles\Attachment\Entity\Metadata;

interface FileAttachmentType extends AttachmentType
{
    public function getMinFileSizeBytes();

    public function getMaxFileSizeBytes();
}
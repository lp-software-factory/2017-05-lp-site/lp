<?php
namespace ZEA2\Domain\Bundles\Attachment\Entity;

interface AttachmentOwner
{
    public function getAttachmentOwnerId(): string;
    public function getAttachmentOwnerCode(): string;
}
<?php
namespace ZEA2\Domain\Bundles\Attachment\Exception;

class AttachmentFactoryException extends \Exception
{
}
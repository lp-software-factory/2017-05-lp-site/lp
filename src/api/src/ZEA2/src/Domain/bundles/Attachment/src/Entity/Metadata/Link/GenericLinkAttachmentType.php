<?php
namespace ZEA2\Domain\Bundles\Attachment\Entity\Metadata\Link;

use ZEA2\Domain\Bundles\Attachment\Entity\Metadata\LinkAttachmentType;

class GenericLinkAttachmentType implements LinkAttachmentType
{
    public function getCode()
    {
        return 'link';
    }
}
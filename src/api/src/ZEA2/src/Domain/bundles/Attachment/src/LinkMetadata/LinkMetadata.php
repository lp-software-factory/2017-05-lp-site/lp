<?php
namespace ZEA2\Domain\Bundles\Attachment\LinkMetadata;

use ZEA2\Domain\Markers\JSONSerializable;

interface LinkMetadata extends JSONSerializable
{
    public function getTitle(): string;
    public function getDescription(): string;
    public function getVersion(): int;
    public function getURL(): string;
    public function getResourceType(): string;
    public function toJSON(array $options = []): array;
}
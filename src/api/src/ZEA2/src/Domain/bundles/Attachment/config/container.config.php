<?php
namespace ZEA2\Domain\Bundles\Attachment;

use function DI\object;
use function DI\factory;

use ZEA2\Domain\Bundles\Attachment\Config\AttachmentConfig;
use ZEA2\Domain\Bundles\Attachment\Factory\AttachmentConfigFactory;
use ZEA2\Domain\Bundles\Attachment\Factory\Doctrine\AttachmentRepositoryFactory;
use ZEA2\Domain\Bundles\Attachment\Repository\AttachmentRepository;
use ZEA2\Domain\Bundles\Attachment\Service\AttachmentService;

return [
    AttachmentConfig::class => factory(AttachmentConfigFactory::class),
    AttachmentRepository::class => factory(AttachmentRepositoryFactory::class),
    AttachmentService::class => object()
        ->constructorParameter('generatePreviews', true),
];
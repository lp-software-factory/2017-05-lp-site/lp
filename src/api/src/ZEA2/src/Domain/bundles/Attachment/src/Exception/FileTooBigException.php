<?php
namespace ZEA2\Domain\Bundles\Attachment\Exception;

class FileTooBigException extends AttachmentFactoryException
{
}
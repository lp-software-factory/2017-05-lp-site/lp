<?php
namespace ZEA2\Domain\Bundles\Attachment\Exception;

class URLRestrictedException extends InvalidURLException
{
}
<?php
namespace ZEA2\Domain\Bundles\Attachment\Entity\Metadata\File;

use ZEA2\Domain\Bundles\Attachment\Entity\Metadata\FileAttachmentType;

class GenericFileAttachmentType implements FileAttachmentType
{
    public function getCode()
    {
        return 'file';
    }

    public function getMinFileSizeBytes()
    {
        return 1;
    }

    public function getMaxFileSizeBytes()
    {
        return 1024*1024*128;
    }
}
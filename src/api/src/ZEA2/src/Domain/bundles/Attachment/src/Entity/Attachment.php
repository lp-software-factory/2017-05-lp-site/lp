<?php
namespace ZEA2\Domain\Bundles\Attachment\Entity;

use ZEA2\Domain\Markers\IdEntity\IdEntity;
use ZEA2\Domain\Markers\IdEntity\IdEntityTrait;
use ZEA2\Domain\Markers\JSONMetadataEntity\JSONMetadataEntity;
use ZEA2\Domain\Markers\JSONMetadataEntity\JSONMetadataEntityTrait;
use ZEA2\Domain\Markers\JSONSerializable;
use ZEA2\Domain\Markers\ModificationEntity\ModificationEntity;
use ZEA2\Domain\Markers\ModificationEntity\ModificationEntityTrait;
use ZEA2\Domain\Markers\SIDEntity\SIDEntity;
use ZEA2\Domain\Markers\SIDEntity\SIDEntityTrait;
use ZEA2\Domain\Markers\VersionEntity\VersionEntity;
use ZEA2\Domain\Markers\VersionEntity\VersionEntityTrait;

/**
 * @Entity(repositoryClass="ZEA2\Domain\Bundles\Attachment\Repository\AttachmentRepository")
 * @Table(name="attachment")
 */
class Attachment implements JSONSerializable, IdEntity, SIDEntity, ModificationEntity, JSONMetadataEntity, VersionEntity
{
    public const LATEST_VERSION = '1.0.0';

    use IdEntityTrait, SIDEntityTrait, JSONMetadataEntityTrait, ModificationEntityTrait, VersionEntityTrait;

    /**
     * @Column(type="string", name="title")
     * @var string
     */
    private $title;

    /**
     * @Column(type="string", name="description")
     * @var string
     */
    private $description;

    /**
     * @Column(type="datetime", name="date_attached_on")
     * @var \DateTime
     */
    private $dateAttachedOn;

    /**
     * @Column(type="boolean", name="is_attached")
     * @var bool
     */
    private $isAttached = false;

    /**
     * @Column(type="string", name="owner_id")
     * @var int
     */
    private $ownerId;

    /**
     * @Column(type="string", name="owner_code")
     * @var string
     */
    private $ownerCode;

    public function __construct(string $title, string $description)
    {
        $this->setCurrentVersion(self::LATEST_VERSION);

        $this->title = $title;
        $this->description = $description;

        $this->regenerateSID();
        $this->initModificationEntity();
    }

    public function toJSON(array $options = []): array
    {
        $result = [
            'id' => $this->isPersisted() ? $this->getId() : '#NEW_ATTACHMENT',
            'sid' => $this->getSID(),
            'date_created_at' => $this->getDateCreatedAt()->format(\DateTime::RFC2822),
            'last_updated_on' => $this->getLastUpdatedOn()->format(\DateTime::RFC2822),
            'title' => $this->getTitle(),
            'description' => $this->getDescription(),
            'link' => $this->getMetadata(),
            'is_attached' => $this->isAttached(),
            'metadata' => array_merge($this->getMetadata(), [
                'version' => $this->getMetadataVersion(),
            ]),
        ];

        if($this->isAttached()) {
            $result = array_merge($result, [
                'date_attached_on' => $this->getDateAttachedOn()->format(\DateTime::RFC2822),
                'owner' => [
                    'id' => $this->getOwnerId(),
                    'code' => $this->getOwnerCode(),
                ],
            ]);
        }

        return $result;
    }

    public function getVersion(): string
    {
        return "1.0.0";
    }

    public function getMetadataVersion(): string
    {
        return "1.0.0";
    }

    public function attach(AttachmentOwner $owner): self
    {
        $this->isAttached = true;
        $this->dateAttachedOn = new \DateTime();
        $this->ownerId = $owner->getAttachmentOwnerId();
        $this->ownerCode = $owner->getAttachmentOwnerCode();

        return $this;
    }


    public function getDateAttachedOn(): \DateTime
    {
        return $this->dateAttachedOn;
    }

    public function detach(): self
    {
        $this->isAttached = false;
        $this->dateAttachedOn = null;

        return $this;
    }

    public function isAttached()
    {
        return $this->isAttached;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getOwnerCode(): string
    {
        return $this->ownerCode;
    }

    public function getOwnerId(): int
    {
        return $this->ownerId;
    }
}
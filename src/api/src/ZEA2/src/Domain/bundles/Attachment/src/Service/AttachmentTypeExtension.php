<?php
namespace ZEA2\Domain\Bundles\Attachment\Service;

use ZEA2\Domain\Bundles\Attachment\Entity\Attachment;

interface AttachmentTypeExtension
{
    public function extend(Attachment $attachment): array;
}
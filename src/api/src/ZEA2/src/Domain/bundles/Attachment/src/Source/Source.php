<?php
namespace ZEA2\Domain\Bundles\Attachment\Source;

use ZEA2\Domain\Markers\JSONSerializable;

interface Source extends JSONSerializable
{
    public function getCode(): string;
}
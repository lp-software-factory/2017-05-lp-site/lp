<?php
namespace ZEA2\Domain\Bundles\Attachment\Formatter;

use ZEA2\Domain\Bundles\Attachment\Entity\Attachment;

final class AttachmentFormatter
{
    public function format(Attachment $attachment)
    {
        return $attachment->toJSON();
    }
}
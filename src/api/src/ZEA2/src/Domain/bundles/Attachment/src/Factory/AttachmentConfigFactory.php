<?php
namespace ZEA2\Domain\Bundles\Attachment\Factory;

use DI\Container;
use ZEA2\Domain\Bundles\Attachment\Config\AttachmentConfig;
use ZEA2\Domain\Service\StorageConfig;
use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;
use League\Flysystem\Memory\MemoryAdapter;
use ZEA2\Platform\Constants\Environment;
use ZEA2\Platform\Service\EnvironmentService;

final class AttachmentConfigFactory
{
    public function __invoke(Container $container, StorageConfig $storageConfig, EnvironmentService $environmentService)
    {
        $config = $container->get('config.zea2.domain.attachment');

        $dir = sprintf($config['dir'], $storageConfig->getDir());
        $www = sprintf($config['www'], $storageConfig->getWww());

        if($environmentService->getCurrent() === Environment::TESTING) {
            $adapter = new MemoryAdapter();
        }else{
            $adapter = new Local($dir);
        }

        return new AttachmentConfig($dir, $www, new Filesystem($adapter));
    }
}
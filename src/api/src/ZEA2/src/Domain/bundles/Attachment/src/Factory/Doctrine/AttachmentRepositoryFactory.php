<?php
namespace ZEA2\Domain\Bundles\Attachment\Factory\Doctrine;

use ZEA2\Domain\Bundles\Attachment\Entity\Attachment;
use ZEA2\Platform\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;

final class AttachmentRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return Attachment::class;
    }
}
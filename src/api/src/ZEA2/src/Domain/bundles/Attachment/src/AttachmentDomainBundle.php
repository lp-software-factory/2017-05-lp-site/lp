<?php
namespace ZEA2\Domain\Bundles\Attachment;

use ZEA2\Platform\Bundles\ZEA2Bundle;

class AttachmentDomainBundle extends ZEA2Bundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}
<?php
namespace ZEA2\Domain\Bundles\Attachment\Entity\Metadata;

interface AttachmentType
{
    public function getCode();
}
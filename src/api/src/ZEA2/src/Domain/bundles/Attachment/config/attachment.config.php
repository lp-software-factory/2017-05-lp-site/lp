<?php
namespace ZEA2\Domain\Bundles\Attachment;

return [
    'config.zea2.domain.attachment' => [
        'dir' => '%s/attachment',
        'www' => '%s/attachment',
    ]
];
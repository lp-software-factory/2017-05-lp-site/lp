<?php
namespace ZEA2\Domain\Bundles\Profile;

use ZEA2\Domain\Bundles\Profile\Console\Command\RegenerateProfileImageCommand;

return [
    'config.console' => [
        'commands' => [
            'Profile' => [
                RegenerateProfileImageCommand::class,
            ],
        ],
    ],
];
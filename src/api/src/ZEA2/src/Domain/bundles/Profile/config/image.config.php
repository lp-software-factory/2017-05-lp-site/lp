<?php
namespace ZEA2\Domain\Bundles\Profile;

return [
    'config.zea2.domain.profile.image.www' => 'profile/image',
    'config.zea2.domain.profile.image.dir' => 'profile/image',
];
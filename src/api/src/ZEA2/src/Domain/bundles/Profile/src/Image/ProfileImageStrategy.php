<?php
namespace ZEA2\Domain\Bundles\Profile\Image;

use ZEA2\Domain\Bundles\Avatar\Entity\ImageEntity;
use ZEA2\Domain\Bundles\Avatar\Strategy\SquareImageStrategy;
use ZEA2\Domain\Bundles\Profile\Entity\Profile;
use League\Flysystem\FilesystemInterface;

final class ProfileImageStrategy extends SquareImageStrategy
{
    /** @var Profile */
    private $profile;

    /** @var FilesystemInterface */
    private $fileSystem;

    /** @var string */
    private $wwwPath;

    public function __construct(Profile $profile, FilesystemInterface $fileSystem, string $wwwPath)
    {
        $this->profile = $profile;
        $this->fileSystem = $fileSystem;
        $this->wwwPath = $wwwPath;
    }

    public function getEntity(): ImageEntity
    {
        return $this->profile;
    }

    public function getEntityId(): string
    {
        return $this->profile->getId();
    }

    public function getLetter(): string
    {
        return mb_substr($this->profile->getFirstName(), 0, 1);
    }

    public function getFilesystem(): FilesystemInterface
    {
        return $this->fileSystem;
    }

    public function getPublicPath(): string
    {
        return $this->wwwPath;
    }
}
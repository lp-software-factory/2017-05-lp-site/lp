<?php
namespace ZEA2\Domain\Bundles\Profile\Parameters;

final class CreateProfileParameters
{
    /** @var string */
    private $firstName;

    /** @var string */
    private $lastName;

    /** @var ?string */
    private $middleName;

    public function __construct(string $firstName, string $lastName, ?string $middleName = null)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->middleName = $middleName;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getMiddleName(): ?string
    {
        return $this->middleName;
    }
}
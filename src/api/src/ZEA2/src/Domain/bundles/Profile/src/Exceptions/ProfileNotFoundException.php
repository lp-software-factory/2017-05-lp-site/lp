<?php
namespace ZEA2\Domain\Bundles\Profile\Exceptions;

final class ProfileNotFoundException extends \Exception {}
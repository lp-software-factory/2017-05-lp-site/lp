<?php
namespace ZEA2\Domain\Bundles\Profile\Service;

use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use ZEA2\Domain\Bundles\Account\Entity\Account;
use ZEA2\Domain\Bundles\Avatar\Image\ImageCollection;
use ZEA2\Domain\Bundles\Avatar\Parameters\UploadImageParameters;
use ZEA2\Domain\Bundles\Avatar\Service\AvatarService;
use ZEA2\Domain\Bundles\Profile\Entity\Profile;
use ZEA2\Domain\Bundles\Profile\Image\ProfileImageStrategyFactory;
use ZEA2\Domain\Bundles\Profile\Parameters\CreateProfileParameters;
use ZEA2\Domain\Bundles\Profile\Repository\ProfileRepository;
use ZEA2\Domain\Criteria\IdsCriteria;

final class ProfileService
{
    public const EVENT_PROFILE_CREATED = 'hi.domain.profile.created';
    public const EVENT_PROFILE_UPDATED = 'hi.domain.profile.updated';
    public const EVENT_PROFILE_EDITED = 'hi.domain.profile.edited';
    public const EVENT_PROFILE_DELETE = 'hi.domain.profile.delete';
    public const EVENT_PROFILE_DELETED = 'hi.domain.profile.deleted';

    /** @var EventEmitterInterface */
    private $eventEmitter;

    /** @var ProfileRepository */
    private $profileRepository;

    /** @var AvatarService */
    private $avatarService;

    /** @var ProfileImageStrategyFactory */
    private $profileImageStrategyFactory;

    public function __construct(
        ProfileRepository $profileRepository,
        AvatarService $avatarService,
        ProfileImageStrategyFactory $profileImageStrategyFactory
    ) {
        $this->eventEmitter = new EventEmitter();
        $this->profileRepository = $profileRepository;
        $this->avatarService = $avatarService;
        $this->profileImageStrategyFactory = $profileImageStrategyFactory;
    }

    public function getEventEmitter(): EventEmitterInterface
    {
        return $this->eventEmitter;
    }

    public function createProfileForAccount(Account $account, CreateProfileParameters $parameters): Profile
    {
        $profile = new Profile(
            $account,
            $parameters->getFirstName(),
            $parameters->getLastName(),
            $parameters->getMiddleName()
        );

        $this->profileRepository->createProfile($profile);

        $this->getEventEmitter()->emit(self::EVENT_PROFILE_CREATED, [$profile]);

        $this->avatarService->generateImage($this->profileImageStrategyFactory->forProfile($profile));
        $this->profileRepository->saveProfile($profile);

        $account->attachProfile($profile);

        return $profile;
    }

    public function regenerateProfileImage(int $profileId): ImageCollection
    {
        $profile = $this->profileRepository->getProfileById($profileId);
        $image = $this->avatarService->generateImage($this->profileImageStrategyFactory->forProfile($profile));

        $this->profileRepository->saveProfile($profile);

        $this->getEventEmitter()->emit(self::EVENT_PROFILE_UPDATED, [$profile]);

        return $image;
    }

    public function uploadProfileImage(int $profileId, UploadImageParameters $parameters): ImageCollection
    {
        $profile = $this->profileRepository->getProfileById($profileId);
        $image = $this->avatarService->uploadImage($this->profileImageStrategyFactory->forProfile($profile), $parameters);

        $this->profileRepository->saveProfile($profile);

        $this->getEventEmitter()->emit(self::EVENT_PROFILE_UPDATED, [$profile]);

        return $image;
    }

    public function getProfileById(int $profileId): Profile
    {
        return $this->profileRepository->getProfileById($profileId);
    }

    public function getProfilesByIds(IdsCriteria $idsCriteria): array
    {
        return $this->profileRepository->getProfilesByIds($idsCriteria);
    }
}
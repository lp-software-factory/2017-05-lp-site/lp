<?php
namespace ZEA2\Domain\Bundles\Profile\Factory;

use ZEA2\Domain\Bundles\Attachment\Config\AttachmentConfig;
use ZEA2\Domain\Bundles\Profile\Config\ProfileImageConfig;
use Interop\Container\ContainerInterface;
use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;
use League\Flysystem\Memory\MemoryAdapter;
use ZEA2\Platform\Constants\Environment;
use ZEA2\Platform\Service\EnvironmentService;

final class ProfileImageConfigFactory
{
    public function __invoke(ContainerInterface $container, AttachmentConfig $config, EnvironmentService $environmentService)
    {
        $www = sprintf('%s/%s', $config->getWww(), $container->get('config.zea2.domain.profile.image.www'));
        $dir = $dir = sprintf('%s/%s', $config->getDir(), $container->get('config.zea2.domain.profile.image.dir'));

        if($environmentService->getCurrent() === Environment::TESTING) {
            $adapter = new MemoryAdapter();
        }else{
            $adapter = new Local($dir);
        }

        return new ProfileImageConfig($www, $dir, new Filesystem($adapter));
    }
}
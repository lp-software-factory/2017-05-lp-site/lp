<?php
namespace ZEA2\Domain\Bundles\Profile\Entity;

use ZEA2\Domain\Bundles\Account\Entity\Account;
use ZEA2\Domain\Bundles\Avatar\Entity\ImageEntity;
use ZEA2\Domain\Bundles\Avatar\Entity\ImageEntityTrait;
use ZEA2\Domain\Markers\IdEntity\IdEntity;
use ZEA2\Domain\Markers\IdEntity\IdEntityTrait;
use ZEA2\Domain\Markers\JSONMetadataEntity\JSONMetadataEntity;
use ZEA2\Domain\Markers\JSONMetadataEntity\JSONMetadataEntityTrait;
use ZEA2\Domain\Markers\JSONSerializable;
use ZEA2\Domain\Markers\ModificationEntity\ModificationEntity;
use ZEA2\Domain\Markers\ModificationEntity\ModificationEntityTrait;
use ZEA2\Domain\Markers\SIDEntity\SIDEntity;
use ZEA2\Domain\Markers\SIDEntity\SIDEntityTrait;
use ZEA2\Domain\Markers\VersionEntity\VersionEntity;
use ZEA2\Domain\Markers\VersionEntity\VersionEntityTrait;
use Zend\Validator\ValidatorChain;

/**
 * @Entity(repositoryClass="ZEA2\Domain\Bundles\Profile\Repository\ProfileRepository")
 * @Table(name="profile")
 */
class Profile implements JSONSerializable, IdEntity, SIDEntity, ModificationEntity, JSONMetadataEntity, ImageEntity, VersionEntity
{
    public const LATEST_VERSION = '1.0.0';

    use IdEntityTrait, SIDEntityTrait, JSONMetadataEntityTrait, ModificationEntityTrait, ImageEntityTrait, VersionEntityTrait;

    /**
     * @ManyToOne(targetEntity="ZEA2\Domain\Bundles\Account\Entity\Account")
     * @JoinColumn(name="account_id", referencedColumnName="id")
     * @var Account
     */
    private $account;

    /**
     * @Column(type="string", name="first_name")
     * @var string
     */
    private $firstName = '';

    /**
     * @Column(type="string", name="last_name")
     * @var string
     */
    private $lastName = '';

    /**
     * @Column(type="string", name="middle_name")
     * @var string|null
     */
    private $middleName = '';

    public function __construct(Account $account, string $firstName, string $lastName, ?string $middleName = null)
    {
        $this->setCurrentVersion(self::LATEST_VERSION);

        $this->account = $account;
        $this->changeName($firstName, $lastName, $middleName);
        $this->regenerateSID();
        $this->initModificationEntity();
    }

    public function toJSON(array $options = []): array
    {
        $result = [
            'id' => $this->getIdNoFall(),
            'account_id' => $this->getAccount()->getId(),
            'sid' => $this->getSID(),
            'metadata' => array_merge($this->getMetadata(), [
                'version' => $this->getMetadataVersion(),
            ]),
            'date_created_at' => $this->getDateCreatedAt()->format(\DateTime::RFC2822),
            'last_updated_on' => $this->getLastUpdatedOn()->format(\DateTime::RFC2822),
            'first_name' => $this->getFirstName(),
            'last_name' => $this->getLastName(),
            'image' => $this->getImages()->toJSON($options, $options),
        ];

        if($this->hasMiddleName()) {
            $result['middle_name'] = $this->getMiddleName();
        }

        return $result;
    }

    public function getMetadataVersion(): string
    {
        return "1.0.0";
    }

    public function changeName(string $firstName, string $lastName, ?string $middleName = null): self
    {
        $validator = (new ValidatorChain())
            ->attach(new \Zend\Validator\NotEmpty())
            ->attach(new \Zend\Validator\StringLength(['min' => 3, 'max' => '127']));

        foreach(['first' => $firstName, 'last' => $lastName, 'middle' => $lastName] as $key => $value) {
            if(! $validator->isValid($value)) {
                throw new \Exception(sprintf('Invalid %sName: %s', $key, $value));
            }
        }

        $this->firstName = mb_convert_case($firstName, MB_CASE_TITLE);
        $this->lastName = mb_convert_case($lastName, MB_CASE_TITLE);
        $this->middleName = mb_convert_case($middleName, MB_CASE_TITLE);

        $this->markAsUpdated();

        return $this;
    }

    public function getAccount(): Account
    {
        return $this->account;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function hasMiddleName(): bool
    {
        return $this->middleName !== null;
    }

    public function getMiddleName(): string
    {
        return $this->middleName;
    }
}
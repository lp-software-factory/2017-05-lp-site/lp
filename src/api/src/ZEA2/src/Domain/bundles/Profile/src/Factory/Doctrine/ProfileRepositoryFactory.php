<?php
namespace ZEA2\Domain\Bundles\Profile\Factory\Doctrine;

use ZEA2\Platform\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;
use ZEA2\Domain\Bundles\Profile\Entity\Profile;

final class ProfileRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return Profile::class;
    }
}
<?php
namespace ZEA2\Domain\Bundles\Profile\Config;

use League\Flysystem\FilesystemInterface;

final class ProfileImageConfig
{
    /** @var string */
    private $www;

    /** @var string */
    private $dir;

    /** @var FilesystemInterface */
    private $flySystem;

    public function __construct(string $www, string $dir, FilesystemInterface $flySystem)
    {
        $this->www = $www;
        $this->dir = $dir;
        $this->flySystem = $flySystem;
    }

    public function getWww(): string
    {
        return $this->www;
    }

    public function getDir(): string
    {
        return $this->dir;
    }

    public function getFlySystem(): FilesystemInterface
    {
        return $this->flySystem;
    }
}
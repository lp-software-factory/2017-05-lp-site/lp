<?php
namespace ZEA2\Domain\Bundles\Profile;

use ZEA2\Platform\Bundles\ZEA2Bundle;

final class ProfileDomainBundle extends ZEA2Bundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}
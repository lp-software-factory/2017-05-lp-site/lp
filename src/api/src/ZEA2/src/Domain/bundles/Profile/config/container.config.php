<?php
namespace ZEA2\Domain\Bundles\Profile;

use function DI\factory;

use ZEA2\Domain\Bundles\Profile\Config\ProfileImageConfig;
use ZEA2\Domain\Bundles\Profile\Factory\Doctrine\ProfileRepositoryFactory;
use ZEA2\Domain\Bundles\Profile\Factory\ProfileImageConfigFactory;
use ZEA2\Domain\Bundles\Profile\Repository\ProfileRepository;

return [
    ProfileRepository::class => factory(ProfileRepositoryFactory::class),
    ProfileImageConfig::class => factory(ProfileImageConfigFactory::class),
];
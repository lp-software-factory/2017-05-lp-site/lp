<?php
namespace ZEA2\Domain\Bundles\Profile\Formatter;

use ZEA2\Domain\Bundles\Profile\Entity\Profile;

final class ProfileFormatter
{
    public function formatMany(array $profiles)
    {
        return array_map(function(Profile $profile) {
            return $this->formatOne($profile);
        }, $profiles);
    }

    public function formatOne(Profile $profile)
    {
        return [
            'entity' => $profile->toJSON(),
        ];
    }
}
<?php
namespace ZEA2\Domain\Util;

final class ArrayValueUsingDotsNotation
{
    public static function get(array &$arr, string $path)
    {
        $loc = &$arr;

        foreach (explode('.', $path) as $step) {
            $loc = &$loc[$step];
        }

        return $loc;
    }

    public static function set(array &$arr, string $path, $val)
    {
        $loc = &$arr;

        foreach (explode('.', $path) as $step) {
            $loc = &$loc[$step];
        }

        return $loc = $val;
    }
}
<?php
namespace ZEA2\Domain;

use ZEA2\Domain\Bundles\Account\AccountDomainBundle;
use ZEA2\Domain\Bundles\Attachment\AttachmentDomainBundle;
use ZEA2\Domain\Bundles\Auth\AuthDomainBundle;
use ZEA2\Domain\Bundles\Avatar\AvatarDomainBundle;
use ZEA2\Domain\Bundles\Colors\ColorsDomainBundle;
use ZEA2\Domain\Bundles\Locale\LocaleDomainBundle;
use ZEA2\Domain\Bundles\Mail\MailDomainBundle;
use ZEA2\Domain\Bundles\MongoDB\MongoDBDomainBundle;
use ZEA2\Domain\Bundles\OAuth2\OAuth2DomainBundle;
use ZEA2\Domain\Bundles\Profile\ProfileDomainBundle;
use ZEA2\Domain\Bundles\Register\RegisterDomainBundle;
use ZEA2\Domain\Bundles\Translation\TranslationDomainBundle;
use ZEA2\Domain\Bundles\Version\VersionDomainBundle;
use ZEA2\Domain\Bundles\Twig\TwigDomainBundle;
use ZEA2\Platform\Bundles\ZEA2Bundle;
use ZEA2\Platform\Init\InitScriptInjectableBundle;

final class ZEA2DomainBundle extends ZEA2Bundle implements InitScriptInjectableBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getBundles(): array
    {
        return [
            new AccountDomainBundle(),
            new AttachmentDomainBundle(),
            new AuthDomainBundle(),
            new AvatarDomainBundle(),
            new ColorsDomainBundle(),
            new LocaleDomainBundle(),
            new MailDomainBundle(),
            new MongoDBDomainBundle(),
            new OAuth2DomainBundle(),
            new ProfileDomainBundle(),
            new TwigDomainBundle(),
            new VersionDomainBundle(),
            new TranslationDomainBundle(),
            new RegisterDomainBundle(),
        ];
    }

    public function getInitScripts(): array
    {
        return [];
    }
}
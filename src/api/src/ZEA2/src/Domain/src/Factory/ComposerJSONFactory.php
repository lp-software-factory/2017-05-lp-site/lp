<?php
namespace ZEA2\Domain\Factory;

use Interop\Container\ContainerInterface;

final class ComposerJSONFactory
{
    public function __invoke(ContainerInterface $container): array
    {
        return json_decode(file_get_contents(__DIR__.'./../../../../../../composer.json'), true);
    }
}
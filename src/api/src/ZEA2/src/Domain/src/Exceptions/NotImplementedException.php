<?php
namespace ZEA2\Domain\Exceptions;

final class NotImplementedException extends \Exception  {}
<?php
namespace ZEA2\Domain\Exceptions;

final class DoNotImplementItException extends \Exception {}
<?php
namespace ZEA2\Domain\Service\CurrentIPService;

interface CurrentIPServiceInterface
{
    function getCurrentIP(): string;
}
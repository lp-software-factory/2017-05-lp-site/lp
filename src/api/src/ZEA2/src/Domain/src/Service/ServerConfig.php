<?php
namespace ZEA2\Domain\Service;

final class ServerConfig
{
    public const DEFAULT_PROTOCOL = 'http';
    public const DEFAULT_IP = '127.0.0.1';
    public const DEFAULT_HOST = 'localhost';
    public const DEFAULT_PORT = 8080;

    /** @var string */
    private $protocol;

    /** @var string */
    private $ip;

    /** @var string */
    private $host;

    /** @var int */
    private $port;

    public function __construct(array $config)
    {
        $this->protocol = (string) ($config['protocol'] ?? self::DEFAULT_PROTOCOL);
        $this->ip = (string) ($config['ip'] ?? self::DEFAULT_IP);
        $this->host = (string) ($config['host'] ?? self::DEFAULT_HOST);
        $this->port = (string) ($config['port'] ?? self::DEFAULT_PORT);
    }

    public function getProtocol(): string
    {
        return $this->protocol;
    }

    public function getIp(): string
    {
        return $this->ip;
    }

    public function getHost(): string
    {
        return $this->host;
    }

    public function getPort(): int
    {
        return $this->port;
    }
}
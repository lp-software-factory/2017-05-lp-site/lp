<?php
namespace ZEA2\Domain\Service\CurrentIPService;

class CurrentIPService implements CurrentIPServiceInterface
{
    public function getCurrentIP(): string
    {
        return $_SERVER['REMOTE_ADDR'];
    }
}
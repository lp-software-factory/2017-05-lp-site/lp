<?php
namespace ZEA2\Domain\Service;

final class StorageConfig
{
    public const DEFAULT_STORAGE_DIR = '/tmp/zea2-storage';
    public const DEFAULT_STORAGE_WWW = 'storage';

    /** @var string */
    private $dir;

    /** @var string */
    private $www;

    public function __construct(array $config)
    {
        $this->dir = $config['dir'] ?? self::DEFAULT_STORAGE_DIR;
        $this->www = $config['www'] ?? self::DEFAULT_STORAGE_WWW;
    }

    public function getDir(): string
    {
        if(! (is_dir($this->dir) && is_writable($this->dir))) {
            throw new \Exception(sprintf('Path `%s` is not dir or is not writable', $this->dir));
        }

        return $this->dir;
    }

    public function getWww(): string
    {
        return $this->www;
    }
}
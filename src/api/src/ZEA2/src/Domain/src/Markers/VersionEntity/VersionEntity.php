<?php
namespace ZEA2\Domain\Markers\VersionEntity;

interface VersionEntity
{
    public function getVersion(): string;
}
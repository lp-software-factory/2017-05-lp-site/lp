<?php
namespace ZEA2\Domain\Markers\SelectedEntity;

interface SelectedEntity
{
    public function select();
    public function blur();
    public function isSelected(): boolean;
}
<?php
namespace ZEA2\Domain\Markers\URLEntity;

interface URLEntity
{
    public function setUrl(string $url);
    public function getUrl(): string;
}
<?php
namespace ZEA2\Domain\Markers\SelectedEntity;

final class SelectedManager
{
    /** @var SelectedEntity[] */
    private $siblings;

    public function __construct(array $siblings)
    {
        $this->siblings = $siblings;
    }

    public function select(SelectedEntity $entity)
    {
        $selected = 0;

        array_map(function(SelectedEntity $sibbling) use ($entity, &$selected) {
            if($sibbling === $entity) {
                $sibbling->select();
                $selected++;
            }else{
                $sibbling->blur();
            }
        }, $this->siblings);

        if(! $selected) {
            throw new \Exception('No entity selected');
        }
    }
}
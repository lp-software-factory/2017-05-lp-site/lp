<?php
namespace ZEA2\Domain\Markers\SerialEntity;

interface SerialEntity
{
    public function getPosition(): int;
    public function setPosition(int $position);
    public function incrementPosition();
}
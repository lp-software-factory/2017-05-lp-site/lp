<?php
namespace ZEA2\Domain\Markers\URLEntity;

use ZEA2\Domain\Markers\ModificationEntity\ModificationEntity;

trait URLEntityTrait
{
    /**
     * @Column(name="url", type="string")
     * @var string
     */
    private $url;

    public function getUrl(): string
    {
        return $this->url;
    }

    public function setUrl(string $url)
    {
        if(preg_match('/[[^a-z0-9\_\-]]/', $url)) {
            throw new \Exception(sprintf('Invalid URL "%s"', $url));
        }

        if($this instanceof ModificationEntity) {
            /** @noinspection PhpUndefinedMethodInspection */
            $this->markAsUpdated();
        }

        $this->url = $url;
    }
}
<?php
namespace ZEA2\Domain\Markers\VersionEntity;

trait VersionEntityTrait
{
    /**
     * @Column(name="version", type="string")
     * @var string
     */
    private $version = '1.0.0';

    protected function setCurrentVersion(string $version)
    {
        $this->version = $version;
    }

    public function getVersion(): string
    {
        return $this->version;
    }
}
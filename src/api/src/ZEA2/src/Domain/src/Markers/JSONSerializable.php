<?php
namespace ZEA2\Domain\Markers;

interface JSONSerializable
{
    public function toJSON(array $options = []): array;
}
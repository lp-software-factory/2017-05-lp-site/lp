<?php
namespace ZEA2\Domain\Markers\SubscribeEntity;

interface SubscribeableEntity
{
    public function getSubscribers();
    public function increaseSubscribers();
    public function decreaseSubscribers();
}
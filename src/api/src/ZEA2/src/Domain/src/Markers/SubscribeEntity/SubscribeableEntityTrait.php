<?php
namespace ZEA2\Domain\Markers\SubscribeEntity;

trait SubscribeableEntityTrait
{
    /**
     * @Column(type="integer", name="subscribers")
     * @var int
     */
    protected $subscribers = 0;

    public function getSubscribers(): int
    {
        return $this->subscribers;
    }

    public function increaseSubscribers()
    {
        $this->subscribers++;

        return $this;
    }

    public function decreaseSubscribers()
    {
        $this->subscribers--;

        $this->subscribers = $this->subscribers < 0 ? 0 : $this->subscribers;

        return $this;
    }
}
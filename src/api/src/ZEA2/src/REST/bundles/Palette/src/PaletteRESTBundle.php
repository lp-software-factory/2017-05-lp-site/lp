<?php
namespace ZEA2\REST\Bundles\Palette;

use ZEA2\REST\Bundle\RESTBundle;

final class PaletteRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}
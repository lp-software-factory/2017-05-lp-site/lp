<?php
namespace ZEA2\REST\Bundles\Account\Middleware\Command;

use ZEA2\Domain\Bundles\Account\Exceptions\AccountNotFoundException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;

final class SendResetPasswordRequestAccountCommand extends AbstractAccountCommand
{
    public function __invoke(ServerRequestInterface $request, JSONResponseBuilder $responseBuilder): ResponseInterface
    {
        try {
            $this->resetPasswordService->sendResetPasswordRequest($request->getQueryParams()['email']);

            $responseBuilder
                ->setStatusSuccess();
        }catch(AccountNotFoundException $e) {
            $responseBuilder
                ->setStatusNotFound()
                ->setError($e);
        }

        return $responseBuilder->build();
    }
}
<?php
namespace ZEA2\REST\Bundles\Account\Request;

use ZEA2\Platform\Bundles\APIDocs\Schema\JSONSchema;
use ZEA2\Platform\Bundles\APIDocs\Schema\SchemaRequest;

final class ResetPasswordMiddlewareRequest extends SchemaRequest
{
    public function getParameters(): array
    {
        return $this->getData();
    }

    protected function getSchema(): JSONSchema
    {
        return SchemaRequest::getSchemaService()->getDefinition('ZEA2_AccountBundle_ResetPasswordRequest');
    }
}
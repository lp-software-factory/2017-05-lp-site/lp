<?php
namespace ZEA2\REST\Bundles\Account\Tests\Paths;

use ZEA2\REST\Bundles\Account\Tests\AccountMiddlewareTestCase;
use ZEA2\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;

final class SetLocaleMiddlewareTest extends AccountMiddlewareTestCase
{
    public function test200()
    {
        $this->requestAccountSetLocale('ru_RU')
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'locale' => [
                    'region' => 'ru_RU'
                ]
            ]);

        $this->requestAccountSetLocale('en_GB')
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'locale' => [
                    'region' => 'en_GB'
                ]
            ]);
    }

    public function test403()
    {
        $this->requestAccountSetLocale('ru_RU')
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {

    }
}
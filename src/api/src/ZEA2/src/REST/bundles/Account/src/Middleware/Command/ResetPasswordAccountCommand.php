<?php
namespace ZEA2\REST\Bundles\Account\Middleware\Command;

use ZEA2\Domain\Bundles\Auth\Exceptions\ResetPasswordRequestNotFoundException;
use ZEA2\REST\Bundles\Account\Request\ResetPasswordMiddlewareRequest;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class ResetPasswordAccountCommand extends AbstractAccountCommand
{
    public function __invoke(ServerRequestInterface $request, JSONResponseBuilder $responseBuilder): ResponseInterface
    {
        try {
            $parameters = (new ResetPasswordMiddlewareRequest($request))->getParameters();

            $account = $this->resetPasswordService->resetPassword($parameters['accept_token'], $parameters['new_password']);
            $authToken = $this->authService->auth($account);

            $responseBuilder
                ->setJSON([
                    'token' => $authToken->toJSON(),
                ])
                ->setStatusSuccess();
        }catch(ResetPasswordRequestNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}
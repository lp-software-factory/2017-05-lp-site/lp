<?php
namespace ZEA2\REST\Bundles\Account\Middleware\Command;

use ZEA2\Domain\Bundles\Account\Service\AccountService;
use ZEA2\Domain\Bundles\Account\Service\ResetPasswordService;
use ZEA2\Domain\Bundles\Auth\Inject\AuthToken;
use ZEA2\Domain\Bundles\Auth\Service\AuthService;
use ZEA2\REST\Bundles\Access\Service\AccessService;
use ZEA2\REST\Command\Command;

abstract class AbstractAccountCommand implements Command
{
    /** @var AuthService */
    protected $authService;

    /** @var AccessService */
    protected $accessService;

    /** @var AccountService */
    protected $accountService;

    /** @var AuthToken */
    protected $authToken;

    /** @var ResetPasswordService */
    protected $resetPasswordService;

    public final function __construct(
        AuthService $authService,
        AccessService $accessService,
        AccountService $accountService,
        AuthToken $authToken,
        ResetPasswordService $resetPasswordService
    ) {
        $this->authService = $authService;
        $this->accessService = $accessService;
        $this->accountService = $accountService;
        $this->authToken = $authToken;
        $this->resetPasswordService = $resetPasswordService;
    }

}
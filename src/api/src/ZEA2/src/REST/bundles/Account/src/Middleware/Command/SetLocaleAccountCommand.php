<?php
namespace ZEA2\REST\Bundles\Account\Middleware\Command;

use ZEA2\Domain\Bundles\Locale\Exceptions\LocaleNotFoundException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;

final class SetLocaleAccountCommand extends AbstractAccountCommand
{
    public function __invoke(ServerRequestInterface $request, JSONResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireProtectedAccess();

        try {
            $region = $request->getAttribute('region');
            $locale = $this->accountService->setLocale($this->authToken->getAccount(), $region);

            $responseBuilder
                ->setJSON([
                    'locale' => $locale->toJSON(),
                ])
                ->setStatusSuccess();
        }catch(LocaleNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}
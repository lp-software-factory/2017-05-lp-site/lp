<?php
namespace ZEA2\REST\Bundles\Account;

use ZEA2\REST\Bundles\Account\Middleware\AccountMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'put',
                'path' => '/account/{command:request-reset-password}[/]',
                'middleware' => AccountMiddleware::class
            ],
            [
                'method' => 'post',
                'path' => '/account/{command:reset-password}[/]',
                'middleware' => AccountMiddleware::class
            ],
            [
                'method' => 'post',
                'path' => '/account/{command:set-locale}/{region}[/]',
                'middleware' => AccountMiddleware::class
            ],
        ]
    ]
];
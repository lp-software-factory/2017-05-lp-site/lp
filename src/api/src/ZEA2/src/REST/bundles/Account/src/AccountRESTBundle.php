<?php
namespace ZEA2\REST\Bundles\Account;

use ZEA2\REST\Bundle\RESTBundle;

final class AccountRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}
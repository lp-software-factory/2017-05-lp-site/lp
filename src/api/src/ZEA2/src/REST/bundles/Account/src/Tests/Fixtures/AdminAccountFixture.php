<?php
namespace ZEA2\REST\Bundles\Account\Tests\Fixtures;

use Doctrine\ORM\EntityManager;
use ZEA2\Domain\Bundles\Account\Entity\Account;
use ZEA2\Domain\Bundles\Account\Parameters\AccountRegisterParameters;
use ZEA2\Domain\Bundles\Account\Service\AccountRegistrationService;
use ZEA2\Domain\Bundles\Auth\Service\JWTTokenService;
use ZEA2\Domain\Bundles\Profile\Entity\Profile;
use ZEA2\REST\Bundles\PHPUnit\Fixture;
use ZEA2\Domain\Bundles\Profile\Parameters\CreateProfileParameters;
use Zend\Expressive\Application;

final class AdminAccountFixture implements Fixture
{
    /** @var Account */
    private static $account;

    /** @var Profile */
    private static $profile;

    /** @var string */
    private static $jwt;

    public static $EMAIL = 'root@example.com';
    public static $PASS = '1234';

    public function up(Application $app, EntityManager $em)
    {
        $jwtTokenService = $app->getContainer()->get(JWTTokenService::class); /** @var JWTTokenService $jwtTokenService */
        $accountRegistrationService = $app->getContainer()->get(AccountRegistrationService::class); /** @var AccountRegistrationService $accountRegistrationService */

        self::$account = $accountRegistrationService->registerAdminAccount(new AccountRegisterParameters(
            self::$EMAIL, self::$PASS, new CreateProfileParameters('PHPUnit', 'Admin')
        ));
        self::$profile = self::$account->getCurrentProfile();
        self::$jwt = $jwtTokenService->generateJWTFor(self::$account, self::$profile);
    }

    public static function getAccount(): Account
    {
        return self::$account;
    }

    public static function getProfile(): Profile
    {
        return self::$profile;
    }

    public static function getJWT(): string
    {
        return self::$jwt;
    }
}
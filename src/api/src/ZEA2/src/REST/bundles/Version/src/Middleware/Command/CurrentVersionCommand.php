<?php
namespace ZEA2\REST\Bundles\Version\Middleware\Command;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;

final class CurrentVersionCommand extends Command
{
    public function __invoke(ServerRequestInterface $request, JSONResponseBuilder $responseBuilder): ResponseInterface
    {
        return $responseBuilder
            ->setJSON([
                'version' => $this->versionService->getCurrentVersion(),
            ])
            ->setStatusSuccess()
            ->build();
    }
}
<?php
namespace ZEA2\REST\Bundles\Version;

use ZEA2\REST\Bundle\RESTBundle;

final class VersionRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}
<?php
namespace ZEA2\REST\Bundles\Version;

use ZEA2\REST\Bundles\Version\Middleware\VersionMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'group' => 'default',
                'method' => 'get',
                'path' => '/version/{command:current}[/]',
                'middleware' => VersionMiddleware::class,
            ],
        ]
    ]
];
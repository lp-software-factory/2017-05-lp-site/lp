<?php
namespace ZEA2\REST\Bundles\Version\Middleware;

use ZEA2\REST\Bundles\Version\Middleware\Command\CurrentVersionCommand;
use ZEA2\REST\Command\Service\CommandService;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;
use Zend\Stratigility\MiddlewareInterface;

final class VersionMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('current', CurrentVersionCommand::class)
            ->resolve($request)
        ;

        return $resolver->__invoke($request, new JSONResponseBuilder($response));
    }
}
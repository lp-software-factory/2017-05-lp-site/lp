<?php
namespace ZEA2\REST\Bundles\Version\Tests;

use ZEA2\REST\Bundles\PHPUnit\TestCase\ZEA2MiddlewareTestCase;

final class VersionMiddlewareTest extends ZEA2MiddlewareTestCase
{
    protected function getFixtures(): array
    {
        return [];
    }

    public function testCurrentVersion200()
    {
        $this->requestVersionCurrent()
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'version' => $this->expectString(),
            ]);
    }
}
<?php
namespace ZEA2\REST\Bundles\Version\Middleware\Command;

use ZEA2\Domain\Bundles\Version\Service\VersionService;

abstract class Command implements \ZEA2\REST\Command\Command
{
    /** @var VersionService */
    protected $versionService;

    public function __construct(VersionService $versionService)
    {
        $this->versionService = $versionService;
    }
}
<?php
namespace ZEA2\REST\Bundles\Version\Tests\Definition;

use ZEA2\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;

trait VersionRESTDefinitionTrait
{
    abstract function request(string $method, string $uri, array $queryParams = null): RESTRequest;

    protected function requestVersionCurrent(): RESTRequest
    {
        return $this->request('GET', '/version/current');
    }
}
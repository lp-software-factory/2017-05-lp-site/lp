<?php
namespace ZEA2\REST\Bundles\Auth\Request;

use ZEA2\Domain\Bundles\Auth\Parameters\SignInParameters;
use ZEA2\Platform\Bundles\APIDocs\Schema\JSONSchema;
use ZEA2\Platform\Bundles\APIDocs\Schema\SchemaRequest;

final class SignInRequest extends SchemaRequest
{
    public function getParameters(): SignInParameters
    {
        $data = $this->getData();

        return new SignInParameters($data['email'], $data['password']);
    }

    protected function getSchema(): JSONSchema
    {
        return SchemaRequest::getSchemaService()->getDefinition('ZEA2_AuthBundle_Request_SignInRequest');
    }
}
<?php
namespace ZEA2\REST\Bundles\Auth\Tests\REST\Paths;

use ZEA2\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use ZEA2\REST\Bundles\Auth\Tests\AuthMiddlewareTestCase;

final class SignOutMiddlewareTest extends AuthMiddlewareTestCase
{
    public function test200()
    {
        $this->requestAuthSignIn(UserAccountFixture::$EMAIL, UserAccountFixture::$PASS)
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'token' => [
                    'jwt' => $this->expectString(),
                    'account' => UserAccountFixture::getAccount()->toJSON(),
                    'profile' => UserAccountFixture::getProfile()->toJSON(),
                ]
            ])
            ->fetch(function(array $json) {
                return $json['token']['jwt'];
            })
        ;

        $this->requestAuthSignOut()
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
            ]);
    }
}
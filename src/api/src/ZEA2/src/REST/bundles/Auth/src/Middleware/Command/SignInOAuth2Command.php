<?php
namespace ZEA2\REST\Bundles\Auth\Middleware\Command;

use ZEA2\Domain\Bundles\Account\Exceptions\AccountNotFoundException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;

final class SignInOAuth2Command extends Command
{
    public function __invoke(ServerRequestInterface $request, JSONResponseBuilder $responseBuilder): ResponseInterface
    {
        $league = $this->oauth2Service->getOAuth2LeagueProvider($request->getAttribute('provider'));

        if(isset($_GET['code'])) {
            $this->preventCSRFAttack();

            try {
                $this->authService->signInOauth2($league, $_GET['code']);

                header('Location: /browser');
                exit();
            }catch(AccountNotFoundException $e) {
                $responseBuilder
                    ->setStatusNotFound()
                    ->setError($e);
            }
        }else{
            $authorizationUrl = $league->getAuthorizationUrl();
            $_SESSION['oauth2state'] = $league->getState();
            header('Location: ' . $authorizationUrl);
            exit;
        }

        return $responseBuilder->build();
    }

    private function preventCSRFAttack()
    {
        $hasState = !empty($_GET['state']);
        $isSameClient = !empty($_GET['state']) && ($_GET['state'] === $_SESSION['oauth2state']);

        if (!($hasState && $isSameClient)) {
            throw new \Exception('Invalid state');
        }
    }
}
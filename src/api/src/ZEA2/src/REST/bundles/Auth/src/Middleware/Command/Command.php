<?php
namespace ZEA2\REST\Bundles\Auth\Middleware\Command;

use ZEA2\Domain\Bundles\Auth\Inject\AuthToken;
use ZEA2\Domain\Bundles\Auth\Service\AuthService;
use ZEA2\Domain\Bundles\OAuth2\Factory\LeagueProviderFactory;
use ZEA2\Domain\Bundles\OAuth2\Factory\OAuth2RequestFactory;
use ZEA2\Domain\Bundles\OAuth2\Service\OAuth2Service;
use ZEA2\REST\Bundles\Access\Service\AccessService;
use ZEA2\Domain\Bundles\Auth\Formatter\AuthTokenFormatter;

abstract class Command implements \ZEA2\REST\Command\Command
{
    /** @var AccessService */
    protected $accessService;

    /** @var AuthService */
    protected $authService;

    /** @var AuthToken */
    protected $authToken;

    /** @var \ZEA2\Domain\Bundles\Auth\Formatter\AuthTokenFormatter */
    protected $authTokenFormatter;

    /** @var LeagueProviderFactory */
    protected $leagueProviderFactory;

    /** @var OAuth2RequestFactory */
    protected $oauth2RepositoryFactory;

    /** @var OAuth2Service */
    protected $oauth2Service;

    public function __construct(
        AccessService $accessService,
        AuthService $authService,
        AuthToken $authToken,
        AuthTokenFormatter $authTokenFormatter,
        LeagueProviderFactory $leagueProviderFactory,
        OAuth2RequestFactory $oauth2RequestFactory,
        OAuth2Service $OAuth2Service
    ) {
        $this->accessService = $accessService;
        $this->authService = $authService;
        $this->authToken = $authToken;
        $this->authTokenFormatter = $authTokenFormatter;
        $this->leagueProviderFactory = $leagueProviderFactory;
        $this->oauth2RepositoryFactory = $oauth2RequestFactory;
        $this->oauth2Service = $OAuth2Service;
    }
}
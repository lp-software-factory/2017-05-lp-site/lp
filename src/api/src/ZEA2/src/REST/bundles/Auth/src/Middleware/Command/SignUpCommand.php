<?php
namespace ZEA2\REST\Bundles\Auth\Middleware\Command;

use ZEA2\Domain\Bundles\Account\Exceptions\DuplicateAccountException;
use ZEA2\REST\Bundles\Auth\Request\SignUpRequest;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;

final class SignUpCommand extends Command
{
    public function __invoke(ServerRequestInterface $request, JSONResponseBuilder $responseBuilder): ResponseInterface
    {
        try {
            $request = (new SignUpRequest($request))->getParameters();

            $this->authService->signUp($request);

            $responseBuilder
                ->setStatusSuccess();
        }catch(DuplicateAccountException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusConflict();
        }

        return $responseBuilder->build();
    }
}
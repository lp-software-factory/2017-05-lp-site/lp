<?php
namespace ZEA2\REST\Bundles\Auth\Tests\REST\Paths;

use ZEA2\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use ZEA2\REST\Bundles\Auth\Tests\AuthMiddlewareTestCase;

final class CurrentMiddlewareTest extends AuthMiddlewareTestCase
{
    public function test403()
    {
        $this->requestAuthCurrent()
            ->__invoke()
            ->expectAuthError();
    }

    public function test200()
    {
        $this->requestAuthCurrent()
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'token' => [
                    'jwt' => $this->expectString(),
                    'account' => UserAccountFixture::getAccount()->toJSON(),
                    'profile' => UserAccountFixture::getProfile()->toJSON(),
                ],
            ]);
    }
}
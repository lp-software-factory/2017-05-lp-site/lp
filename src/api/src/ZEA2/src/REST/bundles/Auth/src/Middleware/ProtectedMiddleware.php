<?php
namespace ZEA2\REST\Bundles\Auth\Middleware;

use ZEA2\Domain\Bundles\Account\Exceptions\AccountNotFoundException;
use ZEA2\Domain\Bundles\Auth\Exceptions\JWTTokenFailException;
use ZEA2\Domain\Bundles\Auth\Inject\AuthToken;
use ZEA2\Domain\Bundles\Profile\Exceptions\ProfileNotFoundException;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;
use Zend\Stratigility\MiddlewareInterface;

final class ProtectedMiddleware implements MiddlewareInterface
{
    /** @var AuthToken */
    private $authToken;

    public function __construct(AuthToken $authToken)
    {
        $this->authToken = $authToken;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        try {
            if ($request->hasHeader('Authorization') && strlen($request->getHeader('Authorization')[0])) {
                $this->authToken->auth($request->getHeader('Authorization')[0]);
            } else if(isset($_SESSION[AuthToken::SESSION_KEY_JWT]) && is_string($_SESSION[AuthToken::SESSION_KEY_JWT])) {
                $this->authToken->auth($_SESSION[AuthToken::SESSION_KEY_JWT]);
            }

            return $out($request, $response);
        }catch(AccountNotFoundException | ProfileNotFoundException $e) {
            unset($_SESSION[AuthToken::SESSION_KEY_JWT]);

            $responseBuilder = new JSONResponseBuilder($response);

            $responseBuilder
                ->setError($e)
                ->setStatusNotAllowed();

            return $responseBuilder->build();
        }catch(JWTTokenFailException $e) {
            unset($_SESSION[AuthToken::SESSION_KEY_JWT]);

            $responseBuilder = new JSONResponseBuilder($response);

            $responseBuilder
                ->setError($e)
                ->setStatusNotAllowed();

            return $responseBuilder->build();
        }
    }
}
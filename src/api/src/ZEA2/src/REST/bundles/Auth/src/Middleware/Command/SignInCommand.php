<?php
namespace ZEA2\REST\Bundles\Auth\Middleware\Command;

use ZEA2\Domain\Bundles\Account\Exceptions\AccountNotFoundException;
use ZEA2\Domain\Bundles\Auth\Exceptions\InvalidPasswordException;
use ZEA2\REST\Bundles\Auth\Request\SignInRequest;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;

final class SignInCommand extends Command
{
    public function __invoke(ServerRequestInterface $request, JSONResponseBuilder $responseBuilder): ResponseInterface
    {
        try {
            $request = (new SignInRequest($request))->getParameters();

            $authToken = $this->authService->signIn($request);

            $responseBuilder
                ->setJSON([
                    'token' => $authToken->toJSON()
                ])
                ->setStatusSuccess();
        }catch(InvalidPasswordException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotAllowed();
        }catch(AccountNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}
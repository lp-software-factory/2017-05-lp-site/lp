<?php
namespace ZEA2\REST\Bundles\Auth\Exceptions;

final class UnknownProviderTypeException extends \Exception {}
<?php
namespace ZEA2\REST\Bundles\Auth\Middleware\Command;

use ZEA2\Platform\Response\JSON\JSONResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class SignOutCommand extends Command
{
    public function __invoke(ServerRequestInterface $request, JSONResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->authService->signOut();

        return $responseBuilder
            ->setStatusSuccess()
            ->build();
    }
}
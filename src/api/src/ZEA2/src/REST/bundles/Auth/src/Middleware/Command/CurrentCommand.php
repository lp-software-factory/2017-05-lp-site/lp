<?php
namespace ZEA2\REST\Bundles\Auth\Middleware\Command;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;

final class CurrentCommand extends Command
{
    public function __invoke(ServerRequestInterface $request, JSONResponseBuilder $responseBuilder): ResponseInterface
    {
        if($this->authToken->isSignedIn()) {
            $responseBuilder
                ->setJSON([
                    'token' => $this->authToken->toJSON()
                ])
                ->setStatusSuccess();
        }else{
            $responseBuilder
                ->setError("You're not signed in.")
                ->setStatusNotAllowed();
        }

        return $responseBuilder->build();
    }
}
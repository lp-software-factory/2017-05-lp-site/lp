<?php
namespace ZEA2\REST\Bundles\Auth;

use ZEA2\REST\Bundle\RESTBundle;

final class AuthRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}
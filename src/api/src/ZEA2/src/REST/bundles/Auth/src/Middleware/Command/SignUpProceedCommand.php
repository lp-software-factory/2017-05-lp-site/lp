<?php
namespace ZEA2\REST\Bundles\Auth\Middleware\Command;

use ZEA2\Domain\Bundles\Account\Exceptions\AccountNotFoundException;
use ZEA2\Domain\Bundles\Account\Exceptions\DuplicateAccountException;
use ZEA2\Domain\Bundles\Auth\Exceptions\InvalidAcceptTokenException;
use ZEA2\Domain\Bundles\Auth\Exceptions\SignUpRequestNotFoundException;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class SignUpProceedCommand extends Command
{
    public function __invoke(ServerRequestInterface $request, JSONResponseBuilder $responseBuilder): ResponseInterface
    {
        try {
            $token = $request->getQueryParams()['token'];

            $authToken = $this->authService->acceptSignUpRequest($token);

            $responseBuilder
                ->setJSON([
                    'token' => $this->authTokenFormatter->format($authToken),
                ])
                ->setStatusSuccess();
        }catch(SignUpRequestNotFoundException | InvalidAcceptTokenException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatus(422);
        }catch(AccountNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusConflict();
        }catch(DuplicateAccountException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusConflict();
        }

        return $responseBuilder->build();
    }
}
<?php
namespace ZEA2\REST\Bundles\PHPUnit\InitScripts;

use ZEA2\REST\Bundles\PHPUnit\TestCase\MiddlewareTestCase;
use ZEA2\Platform\Init\InitScript;
use Zend\Expressive\Application;

final class PHPUnitInitScript implements InitScript
{
    /** @var Application */
    private $application;

    public function __construct(Application $application)
    {
        $this->application = $application;
    }

    public function __invoke()
    {
        MiddlewareTestCase::$app = $this->application;
    }
}
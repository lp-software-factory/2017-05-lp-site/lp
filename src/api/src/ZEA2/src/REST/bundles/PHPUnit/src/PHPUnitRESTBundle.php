<?php
namespace ZEA2\REST\Bundles\PHPUnit;

use ZEA2\REST\Bundle\RESTBundle;
use ZEA2\REST\Bundles\PHPUnit\InitScripts\PHPUnitInitScript;
use ZEA2\Platform\Init\InitScriptInjectableBundle;

class PHPUnitRESTBundle extends RESTBundle implements InitScriptInjectableBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getInitScripts(): array
    {
        return [
            PHPUnitInitScript::class,
        ];
    }
}
<?php
namespace ZEA2\REST\Bundles\PHPUnit\TestCase\Expectations\Traits;

use ZEA2\REST\Bundles\PHPUnit\TestCase\Expectations\ExpectArray;
use ZEA2\REST\Bundles\PHPUnit\TestCase\Expectations\ExpectDate;
use ZEA2\REST\Bundles\PHPUnit\TestCase\Expectations\ExpectId;
use ZEA2\REST\Bundles\PHPUnit\TestCase\Expectations\ExpectImageCollection;
use ZEA2\REST\Bundles\PHPUnit\TestCase\Expectations\ExpectString;
use ZEA2\REST\Bundles\PHPUnit\TestCase\Expectations\ExpectUndefined;

trait AllExpectationsTrait
{
    public function expectId():  ExpectId
    {
        return new ExpectId();
    }

    public function expectString(): ExpectString
    {
        return new ExpectString();
    }

    public function expectDate(): ExpectDate
    {
        return new ExpectDate();
    }

    public function expectUndefined(): ExpectUndefined
    {
        return new ExpectUndefined();
    }

    public function expectImageCollection(): ExpectImageCollection
    {
        return new ExpectImageCollection();
    }

    public function expectArray(): ExpectArray
    {
        return new ExpectArray();
    }
}
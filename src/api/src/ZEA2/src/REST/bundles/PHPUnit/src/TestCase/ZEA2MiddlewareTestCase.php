<?php
namespace ZEA2\REST\Bundles\PHPUnit\TestCase;

use Doctrine\ORM\EntityManager;
use ZEA2\Domain\Bundles\Auth\Inject\AuthToken;
use ZEA2\Domain\Bundles\Mail\Spool\PHPUnitMemorySpool;
use ZEA2\REST\Bundles\Account\Tests\Definition\AccountRESTDefinitionTrait;
use ZEA2\REST\Bundles\Attachment\Tests\Definition\AttachmentRESTDefinitionTrait;
use ZEA2\REST\Bundles\Auth\Tests\REST\Definition\AuthRESTDefinitionTrait;
use ZEA2\REST\Bundles\Locale\Tests\Definition\LocaleRESTDefinitionTrait;
use ZEA2\REST\Bundles\OAuth2Providers\Tests\Definition\OAuth2ProvidersRESTDefinitionTrait;
use ZEA2\REST\Bundles\PHPUnit\Fixture;
use ZEA2\REST\Bundles\Register\Bundles\RegisterRecord\Tests\Definition\RegisterRecordRESTDefinitionTrait;
use ZEA2\REST\Bundles\Register\Bundles\RegisterRecordGroup\Tests\Definition\RegisterRecordGroupRESTDefinitionTrait;
use ZEA2\REST\Bundles\Translation\Tests\Definition\TranslationRESTDefinitionTrait;
use ZEA2\REST\Bundles\Version\Tests\Definition\VersionRESTDefinitionTrait;
use ZEA2\Platform\Bundles\DoctrineORM\Service\DoctrineORMTransactionService;

abstract class ZEA2MiddlewareTestCase extends MiddlewareTestCase
{
    use AttachmentRESTDefinitionTrait, AccountRESTDefinitionTrait, LocaleRESTDefinitionTrait,
        OAuth2ProvidersRESTDefinitionTrait, VersionRESTDefinitionTrait,
        AuthRESTDefinitionTrait, TranslationRESTDefinitionTrait, RegisterRecordGroupRESTDefinitionTrait,
        RegisterRecordRESTDefinitionTrait;

    /**
     * Фикстуры
     * Возвращает массив фикстур, которые применяются к каждому юнит-тесту
     *
     * Массив должен быть пустым, либо содержать набор объектов классов,
     *  имплементирующие интерфейс ZEA2\Platform\Bundles\PHPUnit\Fixture
     *
     * Каждая фикстура содержит в себе статическую переменную, в которой хранятся объект/записи, созданные ей
     *
     * @return array
     */
    protected abstract function getFixtures(): array;

    /**
     * При выполнении каждого юнит-теста происходят следующие действия:
     *
     *  1. Стартует транзакция
     *  2. Применяются фикстуры
     *  3. Инжектится SchemaService в SchemaParams
     *
     * @throws \DI\NotFoundException
     */
    protected function setUp()
    {
        $authToken = $this->container()->get(AuthToken::class); /** @var AuthToken $authToken */
        $authToken->signOut();

        $transactionService = $this->container()->get(DoctrineORMTransactionService::class); /** @var DoctrineORMTransactionService $transactionService */
        $transactionService->beginTransaction();

        $spool = $this->container()->get(PHPUnitMemorySpool::class); /** @var PHPUnitMemorySpool $spool */
        $spool->clear();

        $app = $this->app();
        $em = $this->container()->get(EntityManager::class);

        array_map(function(Fixture $fixture) use ($app, $em) {
            $this->upFixture($fixture);
        }, $this->getFixtures());
    }

    /**
     * После завершения каждого юнит-теста происходит роллбак транзакции и удаляются MongoDB-записи
     *
     * @throws \DI\NotFoundException
     */
    protected function tearDown()
    {
        $transactionService = $this->container()->get(DoctrineORMTransactionService::class); /** @var DoctrineORMTransactionService $transactionService */
        $transactionService->rollback();

        $authToken = $this->container()->get(AuthToken::class); /** @var AuthToken $authToken */
        $authToken->signOut();
    }

    /**
     * Поднимает указанную фикстуру
     *
     * @param Fixture $fixture
     * @return ZEA2MiddlewareTestCase
     */
    protected function upFixture(Fixture $fixture): self
    {
        $app = $this->app();
        $em = $this->container()->get(EntityManager::class);
        $fixture->up($app, $em);

        return $this;
    }

    /**
     * Поднимает все фикстуры
     *
     * @param Fixture[] $fixtures
     * @return ZEA2MiddlewareTestCase
     */
    protected function upFixtures(array $fixtures): self
    {
        foreach($fixtures as $fixture) {
            $this->upFixture($fixture);
        }

        return $this;
    }
}
<?php
namespace ZEA2\REST\Bundles\PHPUnit\TestCase\Expectations;

class ExpectDate implements Expectation
{
    public function __toString(): string
    {
        return "{{DATE}}";
    }

    public function expect(ExpectationParams $params)
    {
        $case = $params->getCase();
        $key = $params->getKey();
        $actual = $params->getActual();

        $case->assertArrayHasKey($key, $actual);
        $case->assertTrue(is_string($actual[$key]));
    }
}
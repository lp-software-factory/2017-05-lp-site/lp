<?php
namespace ZEA2\REST\Bundles\Access\Exceptions\AccessDenied;

use ZEA2\Platform\StatusExceptions\StatusException403;

class AccessDeniedException extends \Exception implements StatusException403 {}
<?php
namespace ZEA2\REST\Bundles\Access\Service;

use ZEA2\Domain\Bundles\Account\Entity\Account;
use ZEA2\Domain\Bundles\Auth\Inject\AuthToken;
use ZEA2\REST\Bundles\Access\Exceptions\AccessDenied\RouteIsAdminProtectedException;
use ZEA2\REST\Bundles\Access\Exceptions\AccessDenied\RouteIsProtectedException;

final class AccessService
{
    /** @var AuthToken */
    private $authToken;

    public function __construct(AuthToken $authToken)
    {
        $this->authToken = $authToken;
    }

    public function isAdmin(): bool
    {
        return $this->authToken->isSignedIn() && $this->authToken->getAccount()->hasAccess(Account::APP_ACCESS_ADMIN);
    }

    public function isAuthenticated(): bool {
        return $this->authToken->isSignedIn();
    }

    public function requireProtectedAccess()
    {
        if(! $this->authToken->isSignedIn()) {
            throw new RouteIsProtectedException("You're not signed in.");
        }
    }

    public function requireAdminAccess()
    {
        $this->requireProtectedAccess();

        if(! $this->isAdmin()) {
            throw new RouteIsAdminProtectedException("You're not an administrator");
        }
    }

    public function requireOwner(int $profileId)
    {
        $this->requireProtectedAccess();

        if(! ($this->authToken->getAccount()->getCurrentProfile()->getId() === (int) $profileId)) {
            throw new RouteIsProtectedException(sprintf("You're not an owner of profile(%d)", $profileId));
        }
    }
}
<?php
namespace ZEA2\REST\Bundles\Access\Exceptions\AccessDenied;

final class RouteIsAdminProtectedException extends AccessDeniedException {}
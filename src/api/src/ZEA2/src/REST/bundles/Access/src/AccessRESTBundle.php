<?php
namespace ZEA2\REST\Bundles\Access;

use ZEA2\REST\Bundle\RESTBundle;

final class AccessRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}
<?php
namespace ZEA2\REST\Bundles\Register\Bundles\RegisterRecordGroup\Middleware\Command;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ZEA2\Domain\Bundles\Register\Exceptions\RegisterRecordGroupNotFoundException;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;

final class DeleteRegisterRecordGroupCommand extends AbstractRegisterRecordGroupCommand
{
    public function __invoke(ServerRequestInterface $request, JSONResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->access->requireAdminAccess();

        try {
            $this->service->delete($request->getAttribute('recordGroupId'));

            $responseBuilder
                ->setStatusSuccess();
        }catch(RegisterRecordGroupNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}
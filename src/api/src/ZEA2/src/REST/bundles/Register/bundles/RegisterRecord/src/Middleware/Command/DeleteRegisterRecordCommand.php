<?php
namespace ZEA2\REST\Bundles\Register\Bundles\RegisterRecord\Middleware\Command;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ZEA2\Domain\Bundles\Register\Exceptions\RegisterRecordNotFoundException;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;

final class DeleteRegisterRecordCommand extends AbstractRegisterRecordCommand
{
    public function __invoke(ServerRequestInterface $request, JSONResponseBuilder $responseBuilder): ResponseInterface
    {
        try {
            $this->access->requireAdminAccess();

            $this->service->delete($request->getAttribute('recordId'));

            $responseBuilder
                ->setStatusSuccess();
        }catch(RegisterRecordNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}
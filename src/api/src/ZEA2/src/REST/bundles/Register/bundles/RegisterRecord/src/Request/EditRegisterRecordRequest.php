<?php
namespace ZEA2\REST\Bundles\Register\Bundles\RegisterRecord\Request;

use ZEA2\Platform\Bundles\APIDocs\Schema\JSONSchema;
use ZEA2\Platform\Bundles\APIDocs\Schema\SchemaRequest;

final class EditRegisterRecordRequest extends SchemaRequest
{
    public function getParameters()
    {
        return $this->getData();
    }

    protected function getSchema(): JSONSchema
    {
        return self::getSchemaService()->getDefinition('ZEA2_RegisterBundle_RegisterRecordBundle_EditRegisterRecordRequest');
    }
}
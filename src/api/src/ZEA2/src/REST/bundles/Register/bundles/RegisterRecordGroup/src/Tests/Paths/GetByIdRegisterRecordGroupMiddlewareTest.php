<?php
namespace ZEA2\REST\Bundles\Register\Bundles\RegisterRecordGroup\Tests\Paths;

use ZEA2\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use ZEA2\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use ZEA2\REST\Bundles\Register\Bundles\RegisterRecordGroup\Tests\Fixture\RegisterRecordGroupFixture;
use ZEA2\REST\Bundles\Register\Bundles\RegisterRecordGroup\Tests\RegisterRecordGroupMiddlewareTestCase;

final class GetByIdRegisterRecordGroupMiddlewareTest extends RegisterRecordGroupMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new RegisterRecordGroupFixture());

        $group = RegisterRecordGroupFixture::$recordGroup1;

        $this->requestRegisterRecordGroupGet($group->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'register_record_group' => [
                    'id' => $group->getId(),
                ]
            ]);
    }

    public function test403()
    {
        $this->upFixture(new RegisterRecordGroupFixture());

        $group = RegisterRecordGroupFixture::$recordGroup1;

        $this->requestRegisterRecordGroupGet($group->getId())
            ->__invoke()
            ->expectAuthError();

        $this->requestRegisterRecordGroupGet($group->getId())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new RegisterRecordGroupFixture());

        $this->requestRegisterRecordGroupGet(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}
<?php
namespace ZEA2\REST\Bundles\Register\Bundles\RegisterRecordGroup\Middleware\Command;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ZEA2\Domain\Bundles\Register\Exceptions\DuplicateRecordGroupException;
use ZEA2\Domain\Bundles\Register\Exceptions\InvalidAppNameException;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;

final class CreateRegisterRecordGroupCommand extends AbstractRegisterRecordGroupCommand
{
    public function __invoke(ServerRequestInterface $request, JSONResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->access->requireAdminAccess();

        try {
            $recordGroup = $this->service->create(
                $this->parametersFactory->factoryCreateRegisterRecordGroupParameters($request)
            );

            $responseBuilder
                ->setStatusSuccess()
                ->setJSON([
                    'register_record_group' => $this->formatter->formatOne($recordGroup),
                ]);
        }catch(InvalidAppNameException $appNameException) {
            $responseBuilder
                ->setError($appNameException)
                ->setStatusConflict();
        }catch(DuplicateRecordGroupException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusConflict();
        }

        return $responseBuilder->build();
    }
}
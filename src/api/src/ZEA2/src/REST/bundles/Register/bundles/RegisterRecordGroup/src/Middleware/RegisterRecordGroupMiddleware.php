<?php
namespace ZEA2\REST\Bundles\Register\Bundles\RegisterRecordGroup\Middleware;

use ZEA2\Platform\Response\JSON\JSONResponseBuilder;
use ZEA2\REST\Bundles\Register\Bundles\RegisterRecordGroup\Middleware\Command\CreateRegisterRecordGroupCommand;
use ZEA2\REST\Bundles\Register\Bundles\RegisterRecordGroup\Middleware\Command\DeleteRegisterRecordGroupCommand;
use ZEA2\REST\Bundles\Register\Bundles\RegisterRecordGroup\Middleware\Command\EditRegisterRecordGroupCommand;
use ZEA2\REST\Bundles\Register\Bundles\RegisterRecordGroup\Middleware\Command\GetAllRegisterRecordGroupCommand;
use ZEA2\REST\Bundles\Register\Bundles\RegisterRecordGroup\Middleware\Command\GetByIdRegisterRecordGroupCommand;
use ZEA2\REST\Command\Service\CommandService;
use Zend\Stratigility\MiddlewareInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class RegisterRecordGroupMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new JSONResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('create', CreateRegisterRecordGroupCommand::class)
            ->attachDirect('edit', EditRegisterRecordGroupCommand::class)
            ->attachDirect('delete', DeleteRegisterRecordGroupCommand::class)
            ->attachDirect('all', GetAllRegisterRecordGroupCommand::class)
            ->attachDirect('get', GetByIdRegisterRecordGroupCommand::class)
            ->resolve($request);

        return $resolver->__invoke($request, $responseBuilder);
    }
}
<?php
namespace ZEA2\REST\Bundles\Register\Bundles\RegisterRecord\Tests\Paths;

use ZEA2\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use ZEA2\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use ZEA2\REST\Bundles\Register\Bundles\RegisterRecord\Tests\Fixture\RegisterRecordFixture;
use ZEA2\REST\Bundles\Register\Bundles\RegisterRecord\Tests\RegisterRecordMiddlewareTestCase;

final class GetAllRegisterRecordMiddlewareTest extends RegisterRecordMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new RegisterRecordFixture());

        $num = $this->requestRegisterRecordAll()
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->fetch(function(array $json) {
                return count($json['register_records']);
            })
        ;

        $this->assertEquals($num, count(RegisterRecordFixture::getOrderedList()));
    }

    public function test403()
    {
        $this->upFixture(new RegisterRecordFixture());

        $this->requestRegisterRecordAll()
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
        ;

        $this->requestRegisterRecordAll()
            ->__invoke()
            ->expectAuthError();
        ;
    }
}
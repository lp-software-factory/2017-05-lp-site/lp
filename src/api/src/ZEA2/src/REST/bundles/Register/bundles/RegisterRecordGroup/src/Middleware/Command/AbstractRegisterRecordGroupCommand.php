<?php
namespace ZEA2\REST\Bundles\Register\Bundles\RegisterRecordGroup\Middleware\Command;

use ZEA2\Domain\Bundles\Register\Formatter\RegisterRecordGroupFormatter;
use ZEA2\Domain\Bundles\Register\Service\RegisterRecordGroup\RegisterRecordGroupService;
use ZEA2\REST\Bundles\Access\Service\AccessService;
use ZEA2\REST\Bundles\Register\Bundles\RegisterRecordGroup\ParametersFactory\RegisterRecordGroupParametersFactory;
use ZEA2\REST\Command\Command;

abstract class AbstractRegisterRecordGroupCommand implements Command
{
    /** @var AccessService */
    protected $access;

    /** @var RegisterRecordGroupService */
    protected $service;

    /** @var RegisterRecordGroupParametersFactory */
    protected $parametersFactory;

    /** @var RegisterRecordGroupFormatter */
    protected $formatter;

    public function __construct(
        AccessService $access,
        RegisterRecordGroupService $service,
        RegisterRecordGroupParametersFactory $parametersFactory,
        RegisterRecordGroupFormatter $formatter
    ) {
        $this->access = $access;
        $this->service = $service;
        $this->parametersFactory = $parametersFactory;
        $this->formatter = $formatter;
    }
}
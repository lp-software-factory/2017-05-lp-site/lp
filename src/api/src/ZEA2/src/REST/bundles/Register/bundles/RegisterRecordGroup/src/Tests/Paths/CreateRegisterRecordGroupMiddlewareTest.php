<?php
namespace ZEA2\REST\Bundles\Register\Bundles\RegisterRecordGroup\Tests\Paths;

use ZEA2\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use ZEA2\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use ZEA2\REST\Bundles\Register\Bundles\RegisterRecordGroup\Tests\RegisterRecordGroupMiddlewareTestCase;

final class CreateRegisterRecordGroupMiddlewareTest extends RegisterRecordGroupMiddlewareTestCase
{
    private function getTestJSON(string $appName = 'demo'): array
    {
        return [
            'app_name' => $appName,
        ];
    }

    public function test200()
    {
        $this->requestRegisterRecordGroupCreate($json = $this->getTestJSON())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'register_record_group' => [
                    'id' => $this->expectId(),
                    'app_name' => $json['app_name'],
                ]
            ]);
    }

    public function test403()
    {
        $this->requestRegisterRecordGroupCreate($json = $this->getTestJSON())
            ->__invoke()
            ->expectAuthError();

        $this->requestRegisterRecordGroupCreate($json = $this->getTestJSON())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test409Duplicate()
    {
        $this->requestRegisterRecordGroupCreate($json = $this->getTestJSON())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200);

        $this->requestRegisterRecordGroupCreate($json = $this->getTestJSON())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(409);
    }

    public function test409BadNames()
    {
        $names = [
            '',
            'приложение',
        ];

        foreach($names as $name) {
            $this->requestRegisterRecordGroupCreate($json = $this->getTestJSON($name))
                ->auth(AdminAccountFixture::getJWT())
                ->__invoke()
                ->expectStatusCode(409)
            ;
        }
    }
}
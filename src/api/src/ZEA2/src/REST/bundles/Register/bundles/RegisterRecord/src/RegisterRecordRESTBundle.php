<?php
namespace ZEA2\REST\Bundles\Register\Bundles\RegisterRecord;

use ZEA2\REST\Bundle\RESTBundle;

final class RegisterRecordRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}
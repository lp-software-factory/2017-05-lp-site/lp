<?php
namespace ZEA2\REST\Bundles\Register\Bundles\RegisterRecord\Tests\Definition;

use ZEA2\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;

trait RegisterRecordRESTDefinitionTrait
{
    abstract function request(string $method, string $uri, array $queryParams = null): RESTRequest;

    protected function requestRegisterRecordCreate(array $json): RESTRequest
    {
        return $this->request('put', '/register/record/create')
            ->setParameters($json);
    }

    protected function requestRegisterRecordEdit(int $recordId, array $json): RESTRequest
    {
        return $this->request('post', sprintf('/register/record/%d/edit', $recordId))
            ->setParameters($json);
    }

    protected function requestRegisterRecordEditBulk(array $json): RESTRequest
    {
        return $this->request('post', sprintf('/register/record/edit-bulk'))
            ->setParameters($json);
    }

    protected function requestRegisterRecordDelete(int $recordId): RESTRequest
    {
        return $this->request('delete', sprintf('/register/record/%d/delete', $recordId));
    }

    protected function requestRegisterRecordGet(int $recordId): RESTRequest
    {
        return $this->request('get', sprintf('/register/record/%d/get', $recordId));
    }

    protected function requestRegisterRecordGetBulk(array $json): RESTRequest
    {
        return $this->request('post', sprintf('/register/record/get-bulk'))
            ->setParameters($json);
    }

    protected function requestRegisterRecordAll(): RESTRequest
    {
        return $this->request('get', '/register/record/all');
    }
}
<?php
namespace ZEA2\REST\Bundles\Register\Bundles\RegisterRecord\Tests\Paths;

use ZEA2\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use ZEA2\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use ZEA2\REST\Bundles\Register\Bundles\RegisterRecord\Tests\Fixture\RegisterRecordFixture;
use ZEA2\REST\Bundles\Register\Bundles\RegisterRecord\Tests\RegisterRecordMiddlewareTestCase;

final class EditBulkRegisterRecordMiddlewareTest extends RegisterRecordMiddlewareTestCase
{
    private function getTestJSON(): array
    {
        $record1 = RegisterRecordFixture::$record1;
        $record2 = RegisterRecordFixture::$record2;

        return [
            [
                'key' => $record1->getKey(),
                'value' => '*'.$record1->getValue(),
            ],
            [
                'key' => $record2->getKey(),
                'value' => '*'.$record2->getValue(),
            ],
        ];
    }

    public function test200()
    {
        $this->upFixture(new RegisterRecordFixture());

        $this->requestRegisterRecordEditBulk($json = $this->getTestJSON())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'register_records' => [
                    0 => [
                        'key' => $json[0]['key'],
                        'value' => $json[0]['value'],
                    ],
                    1 => [
                        'key' => $json[1]['key'],
                        'value' => $json[1]['value'],
                    ],
                ]
            ])
        ;
    }

    public function test403()
    {
        $this->upFixture(new RegisterRecordFixture());

        $this->requestRegisterRecordEditBulk($json = $this->getTestJSON())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
        ;

        $this->requestRegisterRecordEditBulk($json = $this->getTestJSON())
            ->__invoke()
            ->expectAuthError();
        ;
    }

    public function test404RecordNotFound()
    {
        $this->upFixture(new RegisterRecordFixture());

        $record1 = RegisterRecordFixture::$record1;
        $record2 = RegisterRecordFixture::$record2;

        $json = [
            [
                'key' => $record1->getKey(),
                'value' => '*'.$record1->getValue(),
            ],
            [
                'key' => 'there-is-no-key-with-this-name',
                'value' => '*'.$record2->getValue(),
            ],
        ];

        $this->requestRegisterRecordEditBulk($json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
        ;
    }
}
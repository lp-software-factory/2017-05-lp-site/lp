<?php
namespace ZEA2\REST\Bundles\Register\Bundles\RegisterRecord\Tests\Fixture;

use Doctrine\ORM\EntityManager;
use ZEA2\Domain\Bundles\Register\Entity\RegisterRecord;
use ZEA2\Domain\Bundles\Register\Parameters\RegisterRecord\CreateRegisterRecordParameters;
use ZEA2\Domain\Bundles\Register\Service\RegisterRecord\RegisterRecordService;
use ZEA2\REST\Bundles\PHPUnit\Fixture;
use ZEA2\REST\Bundles\Register\Bundles\RegisterRecordGroup\Tests\Fixture\RegisterRecordGroupFixture;
use Zend\Expressive\Application;

final class RegisterRecordFixture implements Fixture
{
    /** @var RegisterRecord */
    public static $record1;

    /** @var RegisterRecord */
    public static $record2;

    /** @var RegisterRecord */
    public static $record3;

    public function up(Application $app, EntityManager $em)
    {
        $service = $app->getContainer()->get(RegisterRecordService::class);

        self::$record1 = $service->create(new CreateRegisterRecordParameters(RegisterRecordGroupFixture::$recordGroup1, 'demo_1', 'foo', []));
        self::$record2 = $service->create(new CreateRegisterRecordParameters(RegisterRecordGroupFixture::$recordGroup2, 'demo_2', 'bar', []));
        self::$record3 = $service->create(new CreateRegisterRecordParameters(RegisterRecordGroupFixture::$recordGroup3, 'demo_3', 'baz', []));
    }

    public static function getOrderedList(): array
    {
        return [
            self::$record1,
            self::$record2,
            self::$record3,
        ];
    }
}
<?php
namespace ZEA2\REST\Bundles\Register\Bundles\RegisterRecord\Tests\Paths;

use ZEA2\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use ZEA2\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use ZEA2\REST\Bundles\Register\Bundles\RegisterRecord\Tests\RegisterRecordMiddlewareTestCase;
use ZEA2\REST\Bundles\Register\Bundles\RegisterRecordGroup\Tests\Fixture\RegisterRecordGroupFixture;

final class CreateRegisterRecordMiddlewareTest extends RegisterRecordMiddlewareTestCase
{
    private function getTestJSON(string $key = 'foo', string $value = 'woof'): array
    {
        return [
            'register_record_group_id' => RegisterRecordGroupFixture::$recordGroup1->getId(),
            'key' => $key,
            'value' => $value,
            'options' => [
                'foo' => 'bar',
            ]
        ];
    }

    public function test200()
    {
        $keys = [
            'foo',
            'foo.bar',
            'foo.bar.baz',
            '_foo',
            '__',
        ];

        foreach($keys as $key) {
            $json = $this->getTestJSON($key);

            $this->requestRegisterRecordCreate($json)
                ->auth(AdminAccountFixture::getJWT())
                ->__invoke()
                ->expectStatusCode(200)
                ->expectJSONContentType()
                ->expectJSONBody([
                    'register_record' => [
                        'id' => $this->expectId(),
                        'key' => $key,
                        'value' => 'woof',
                    ]
                ])
            ;
        }
    }

    public function test403()
    {
        $json = $this->getTestJSON();

        $this->requestRegisterRecordCreate($json)
            ->__invoke()
            ->expectAuthError();
        ;

        $this->requestRegisterRecordCreate($json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
        ;
    }

    public function test409DuplicateKey()
    {
        $keys = [
            'foo',
            'foo.bar',
            'foo.bar.baz',
            '_foo',
            '__',
        ];

        foreach($keys as $key) {
            $json = $this->getTestJSON($key);

            $this->requestRegisterRecordCreate($json)
                ->auth(AdminAccountFixture::getJWT())
                ->__invoke()
                ->expectStatusCode(200)
            ;

            $this->requestRegisterRecordCreate($json)
                ->auth(AdminAccountFixture::getJWT())
                ->__invoke()
                ->expectStatusCode(409)
            ;
        }
    }

    public function test409InvalidKeyName()
    {
        $keys = [
            'демо',
            'de^^mo',
            '',
        ];

        foreach($keys as $key) {
            $json = $this->getTestJSON($key);

            $this->requestRegisterRecordCreate($json)
                ->auth(AdminAccountFixture::getJWT())
                ->__invoke()
                ->expectStatusCode(409)
            ;
        }
    }

    public function test404RecordGroupNotFound()
    {
        $json = $this->getTestJSON();
        $json['register_record_group_id'] = self::NOT_FOUND_ID;

        $this->requestRegisterRecordCreate($json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
        ;
    }
}
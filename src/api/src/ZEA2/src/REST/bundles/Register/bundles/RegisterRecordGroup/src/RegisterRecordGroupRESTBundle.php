<?php
namespace ZEA2\REST\Bundles\Register\Bundles\RegisterRecordGroup;

use ZEA2\REST\Bundle\RESTBundle;

final class RegisterRecordGroupRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}
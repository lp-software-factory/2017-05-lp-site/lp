<?php
namespace ZEA2\REST\Bundles\Register\Bundles\RegisterRecordGroup\Middleware\Command;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ZEA2\Domain\Bundles\Register\Exceptions\RegisterRecordGroupNotFoundException;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;

final class GetByIdRegisterRecordGroupCommand extends AbstractRegisterRecordGroupCommand
{
    public function __invoke(ServerRequestInterface $request, JSONResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->access->requireAdminAccess();

        try {
            $recordGroup = $this->service->getById($request->getAttribute('recordGroupId'));

            $responseBuilder
                ->setStatusSuccess()
                ->setJSON([
                    'register_record_group' => $this->formatter->formatOne($recordGroup),
                ]);
        }catch(RegisterRecordGroupNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}
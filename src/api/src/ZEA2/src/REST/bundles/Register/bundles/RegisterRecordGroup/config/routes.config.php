<?php
namespace ZEA2\REST\Bundles\Register\Bundles\RegisterRecordGroup;

use ZEA2\REST\Bundles\Register\Bundles\RegisterRecordGroup\Middleware\RegisterRecordGroupMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'put',
                'path' => '/register/record/group/{command:create}[/]',
                'middleware' => RegisterRecordGroupMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/register/record/group/{recordGroupId}/{command:edit}[/]',
                'middleware' => RegisterRecordGroupMiddleware::class,
            ],
            [
                'method' => 'delete',
                'path' => '/register/record/group/{recordGroupId}/{command:delete}[/]',
                'middleware' => RegisterRecordGroupMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/register/record/group/{recordGroupId}/{command:get}[/]',
                'middleware' => RegisterRecordGroupMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/register/record/group/{command:all}[/]',
                'middleware' => RegisterRecordGroupMiddleware::class,
            ],
        ],
    ],
];
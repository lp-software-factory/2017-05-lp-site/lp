<?php
namespace ZEA2\REST\Bundles\Register\Bundles\RegisterRecordGroup\Tests\Paths;

use ZEA2\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use ZEA2\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use ZEA2\REST\Bundles\Register\Bundles\RegisterRecordGroup\Tests\Fixture\RegisterRecordGroupFixture;
use ZEA2\REST\Bundles\Register\Bundles\RegisterRecordGroup\Tests\RegisterRecordGroupMiddlewareTestCase;

final class DeleteRegisterRecordGroupMiddlewareTest extends RegisterRecordGroupMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new RegisterRecordGroupFixture());

        $group = RegisterRecordGroupFixture::$recordGroup1;

        $this->requestRegisterRecordGroupDelete($group->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
            ]);
    }

    public function test403()
    {
        $this->upFixture(new RegisterRecordGroupFixture());

        $group = RegisterRecordGroupFixture::$recordGroup1;

        $this->requestRegisterRecordGroupDelete($group->getId())
            ->__invoke()
            ->expectAuthError();

        $this->requestRegisterRecordGroupDelete($group->getId())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }
}
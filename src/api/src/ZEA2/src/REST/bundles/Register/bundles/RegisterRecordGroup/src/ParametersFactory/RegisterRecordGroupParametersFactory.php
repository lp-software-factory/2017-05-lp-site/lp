<?php
namespace ZEA2\REST\Bundles\Register\Bundles\RegisterRecordGroup\ParametersFactory;

use Psr\Http\Message\ServerRequestInterface;
use ZEA2\Domain\Bundles\Register\Parameters\RegisterRecordGroup\CreateRegisterRecordGroupParameters;
use ZEA2\Domain\Bundles\Register\Parameters\RegisterRecordGroup\EditRegisterRecordGroupParameters;
use ZEA2\REST\Bundles\Register\Bundles\RegisterRecordGroup\Request\CreateRegisterRecordGroupRequest;
use ZEA2\REST\Bundles\Register\Bundles\RegisterRecordGroup\Request\EditRegisterRecordGroupRequest;

final class RegisterRecordGroupParametersFactory
{
    public function factoryCreateRegisterRecordGroupParameters(ServerRequestInterface $request): CreateRegisterRecordGroupParameters
    {
        $json = (new CreateRegisterRecordGroupRequest($request))->getParameters();

        return new CreateRegisterRecordGroupParameters($json['app_name']);
    }

    public function factoryEditRegisterRecordGroupParameters(ServerRequestInterface $request): EditRegisterRecordGroupParameters
    {
        $json = (new EditRegisterRecordGroupRequest($request))->getParameters();

        return new EditRegisterRecordGroupParameters($json['app_name']);
    }
}
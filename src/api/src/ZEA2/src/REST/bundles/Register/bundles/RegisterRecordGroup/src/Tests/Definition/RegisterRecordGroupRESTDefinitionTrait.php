<?php
namespace ZEA2\REST\Bundles\Register\Bundles\RegisterRecordGroup\Tests\Definition;

use ZEA2\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;

trait RegisterRecordGroupRESTDefinitionTrait
{
    abstract function request(string $method, string $uri, array $queryParams = null): RESTRequest;

    protected function requestRegisterRecordGroupCreate(array $json): RESTRequest
    {
        return $this->request('put', '/register/record/group/create')
            ->setParameters($json);
    }

    protected function requestRegisterRecordGroupEdit(int $recordGroupId, array $json): RESTRequest
    {
        return $this->request('post', sprintf('/register/record/group/%d/edit', $recordGroupId))
            ->setParameters($json);
    }

    protected function requestRegisterRecordGroupDelete(int $recordGroupId): RESTRequest
    {
        return $this->request('delete', sprintf('/register/record/group/%d/delete', $recordGroupId));
    }

    protected function requestRegisterRecordGroupGet(int $recordGroupId): RESTRequest
    {
        return $this->request('get', sprintf('/register/record/group/%d/get', $recordGroupId));
    }

    protected function requestRegisterRecordGroupAll(): RESTRequest
    {
        return $this->request('get', '/register/record/group/all');
    }
}
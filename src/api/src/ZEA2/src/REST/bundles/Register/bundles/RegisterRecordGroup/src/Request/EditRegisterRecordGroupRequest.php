<?php
namespace ZEA2\REST\Bundles\Register\Bundles\RegisterRecordGroup\Request;

use ZEA2\Platform\Bundles\APIDocs\Schema\JSONSchema;
use ZEA2\Platform\Bundles\APIDocs\Schema\SchemaRequest;

final class EditRegisterRecordGroupRequest extends SchemaRequest
{
    public function getParameters()
    {
        return $this->getData();
    }

    protected function getSchema(): JSONSchema
    {
        return self::getSchemaService()->getDefinition('ZEA2_RegisterBundle_RegisterRecordGroupBundle_EditRegisterRecordGroupRequest');
    }
}
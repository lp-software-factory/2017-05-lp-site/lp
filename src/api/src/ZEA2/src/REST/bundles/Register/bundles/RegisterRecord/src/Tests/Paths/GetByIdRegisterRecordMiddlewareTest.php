<?php
namespace ZEA2\REST\Bundles\Register\Bundles\RegisterRecord\Tests\Paths;

use ZEA2\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use ZEA2\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use ZEA2\REST\Bundles\Register\Bundles\RegisterRecord\Tests\Fixture\RegisterRecordFixture;
use ZEA2\REST\Bundles\Register\Bundles\RegisterRecord\Tests\RegisterRecordMiddlewareTestCase;

final class GetByIdRegisterRecordMiddlewareTest extends RegisterRecordMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new RegisterRecordFixture());

        $record = RegisterRecordFixture::$record1;
        $recordId = $record->getId();

        $this->requestRegisterRecordGet($recordId)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200);
    }

    public function test403()
    {
        $this->upFixture(new RegisterRecordFixture());

        $record = RegisterRecordFixture::$record1;
        $recordId = $record->getId();

        $this->requestRegisterRecordGet($recordId)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
        ;

        $this->requestRegisterRecordGet($recordId)
            ->__invoke()
            ->expectAuthError();
        ;
    }

    public function test404RecordNotFound()
    {
        $this->upFixture(new RegisterRecordFixture());

        $this->requestRegisterRecordGet(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
        ;
    }
}
<?php
namespace ZEA2\REST\Bundles\Register\Bundles\RegisterRecord\Middleware\Command;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ZEA2\Domain\Bundles\Register\Exceptions\RegisterRecordNotFoundException;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;

final class EditBulkRegisterRecordCommand extends AbstractRegisterRecordCommand
{
    public function __invoke(ServerRequestInterface $request, JSONResponseBuilder $responseBuilder): ResponseInterface
    {
        try {
            $this->access->requireAdminAccess();

            $records = $this->service->editBulk(
                $this->parametersFactory->factoryEditBulkRegisterRecordGroup($request)
            );

            $responseBuilder
                ->setJSON([
                    'register_records' => $this->formatter->formatMany($records),
                ])
                ->setStatusSuccess();
        }catch(RegisterRecordNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}
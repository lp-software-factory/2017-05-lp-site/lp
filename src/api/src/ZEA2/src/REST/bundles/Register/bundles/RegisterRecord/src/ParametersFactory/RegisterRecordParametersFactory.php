<?php
namespace ZEA2\REST\Bundles\Register\Bundles\RegisterRecord\ParametersFactory;

use Psr\Http\Message\ServerRequestInterface;
use ZEA2\Domain\Bundles\Register\Parameters\RegisterRecord\CreateRegisterRecordParameters;
use ZEA2\Domain\Bundles\Register\Parameters\RegisterRecord\EditBulkRegisterRecordParameters;
use ZEA2\Domain\Bundles\Register\Parameters\RegisterRecord\EditRegisterRecordParameters;
use ZEA2\Domain\Bundles\Register\Parameters\RegisterRecord\GetBulkRegisterRecordParameters;
use ZEA2\Domain\Bundles\Register\Service\RegisterRecordGroup\RegisterRecordGroupService;
use ZEA2\Domain\Bundles\Register\Util\ImmutableRegisterKeyValuePair;
use ZEA2\REST\Bundles\Register\Bundles\RegisterRecord\Request\CreateRegisterRecordRequest;
use ZEA2\REST\Bundles\Register\Bundles\RegisterRecord\Request\EditBulkRegisterRecordRequest;
use ZEA2\REST\Bundles\Register\Bundles\RegisterRecord\Request\EditRegisterRecordRequest;
use ZEA2\REST\Bundles\Register\Bundles\RegisterRecord\Request\GetBulkRegisterRecordRequest;

final class RegisterRecordParametersFactory
{
    /** @var RegisterRecordGroupService */
    private $registerRecordGroupService;
    
    public function __construct(RegisterRecordGroupService $registerRecordGroupService)
    {
        $this->registerRecordGroupService = $registerRecordGroupService;
    }

    public function factoryCreateRegisterRecordGroup(ServerRequestInterface $request): CreateRegisterRecordParameters
    {
        $json = (new CreateRegisterRecordRequest($request))->getParameters();

        return new CreateRegisterRecordParameters(
            $this->registerRecordGroupService->getById($json['register_record_group_id']),
            $json['key'],
            $json['value'],
            $json['options'] ?? []
        );
    }

    public function factoryEditRegisterRecordGroup(ServerRequestInterface $request): EditRegisterRecordParameters
    {
        $json = (new EditRegisterRecordRequest($request))->getParameters();

        return new EditRegisterRecordParameters(
            $this->registerRecordGroupService->getById($json['register_record_group_id']),
            $json['key'],
            $json['value'],
            $json['options'] ?? []
        );
    }

    public function factoryEditBulkRegisterRecordGroup(ServerRequestInterface $request): EditBulkRegisterRecordParameters
    {
        $json = (new EditBulkRegisterRecordRequest($request))->getParameters();

        return new EditBulkRegisterRecordParameters(array_map(function(array $pair) {
            return new ImmutableRegisterKeyValuePair(
                $pair['key'],
                $pair['value']
            );
        }, $json));
    }

    public function factoryGetBulkRegisterRecordGroup(ServerRequestInterface $request): GetBulkRegisterRecordParameters
    {
        $json = (new GetBulkRegisterRecordRequest($request))->getParameters();

        return new GetBulkRegisterRecordParameters($json['keys']);
    }
}
<?php
namespace ZEA2\REST\Bundles\Register\Bundles\RegisterRecordGroup\Tests\Fixture;

use Doctrine\ORM\EntityManager;
use ZEA2\Domain\Bundles\Register\Entity\RegisterRecordGroup;
use ZEA2\Domain\Bundles\Register\Parameters\RegisterRecordGroup\CreateRegisterRecordGroupParameters;
use ZEA2\Domain\Bundles\Register\Service\RegisterRecordGroup\RegisterRecordGroupService;
use ZEA2\REST\Bundles\PHPUnit\Fixture;
use Zend\Expressive\Application;

final class RegisterRecordGroupFixture implements Fixture
{
    /** @var RegisterRecordGroup */
    public static $recordGroup1;

    /** @var RegisterRecordGroup */
    public static $recordGroup2;

    /** @var RegisterRecordGroup */
    public static $recordGroup3;

    public function up(Application $app, EntityManager $em)
    {
        $service = $app->getContainer()->get(RegisterRecordGroupService::class);

        self::$recordGroup1 = $service->create(new CreateRegisterRecordGroupParameters('demo_1'));
        self::$recordGroup2 = $service->create(new CreateRegisterRecordGroupParameters('demo_2'));
        self::$recordGroup3 = $service->create(new CreateRegisterRecordGroupParameters('demo_3'));
    }

    public static function getOrderedList(): array
    {
        return [
            self::$recordGroup1,
            self::$recordGroup2,
            self::$recordGroup3,
        ];
    }
}
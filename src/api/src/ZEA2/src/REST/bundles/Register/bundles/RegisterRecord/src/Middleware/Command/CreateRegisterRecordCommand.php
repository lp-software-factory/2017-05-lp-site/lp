<?php
namespace ZEA2\REST\Bundles\Register\Bundles\RegisterRecord\Middleware\Command;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ZEA2\Domain\Bundles\Register\Exceptions\DuplicateKeyException;
use ZEA2\Domain\Bundles\Register\Exceptions\InvalidKeyNameException;
use ZEA2\Domain\Bundles\Register\Exceptions\RegisterRecordGroupNotFoundException;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;

final class CreateRegisterRecordCommand extends AbstractRegisterRecordCommand
{
    public function __invoke(ServerRequestInterface $request, JSONResponseBuilder $responseBuilder): ResponseInterface
    {
        try {
            $this->access->requireAdminAccess();

            $entity = $this->service->create($this->parametersFactory->factoryCreateRegisterRecordGroup($request));

            $responseBuilder
                ->setJSON([
                    'register_record' => $this->formatter->formatOne($entity),
                ])
                ->setStatusSuccess();
        }catch(DuplicateKeyException|InvalidKeyNameException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusConflict();
        }catch(RegisterRecordGroupNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}
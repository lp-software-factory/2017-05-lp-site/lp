<?php
namespace ZEA2\REST\Bundles\Register\Bundles\RegisterRecord\Middleware\Command;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;

final class GetAllRegisterRecordCommand extends AbstractRegisterRecordCommand
{
    public function __invoke(ServerRequestInterface $request, JSONResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->access->requireAdminAccess();

        $responseBuilder
            ->setJSON([
                'register_records' => $this->formatter->formatMany(
                    $this->service->getAll()
                ),
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}
<?php
namespace ZEA2\REST\Bundles\Register\Bundles\RegisterRecord\Middleware\Command;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ZEA2\Domain\Bundles\Register\Exceptions\RegisterRecordNotFoundException;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;

final class GetByIdRegisterRecordCommand extends AbstractRegisterRecordCommand
{
    public function __invoke(ServerRequestInterface $request, JSONResponseBuilder $responseBuilder): ResponseInterface
    {
        try {
            $this->access->requireAdminAccess();

            $entity = $this->service->getById($request->getAttribute('recordId'));

            $responseBuilder
                ->setJSON([
                    'register_record' => $this->formatter->formatOne($entity),
                ])
                ->setStatusSuccess();
        }catch(RegisterRecordNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}
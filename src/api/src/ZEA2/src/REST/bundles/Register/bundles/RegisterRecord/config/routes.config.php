<?php
namespace ZEA2\REST\Bundles\Register\Bundles\RegisterRecord;

use ZEA2\REST\Bundles\Register\Bundles\RegisterRecord\Middleware\RegisterRecordMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'put',
                'path' => '/register/record/{command:create}[/]',
                'middleware' => RegisterRecordMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/register/record/{recordId}/{command:edit}[/]',
                'middleware' => RegisterRecordMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/register/record/{command:edit-bulk}[/]',
                'middleware' => RegisterRecordMiddleware::class,
            ],
            [
                'method' => 'delete',
                'path' => '/register/record/{recordId}/{command:delete}[/]',
                'middleware' => RegisterRecordMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/register/record/{recordId}/{command:get}[/]',
                'middleware' => RegisterRecordMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/register/record/{command:get-bulk}[/]',
                'middleware' => RegisterRecordMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/register/record/{command:all}[/]',
                'middleware' => RegisterRecordMiddleware::class,
            ],
        ],
    ],
];
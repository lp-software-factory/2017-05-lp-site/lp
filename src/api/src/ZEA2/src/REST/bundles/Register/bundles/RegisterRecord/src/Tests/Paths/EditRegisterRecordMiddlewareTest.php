<?php
namespace ZEA2\REST\Bundles\Register\Bundles\RegisterRecord\Tests\Paths;

use ZEA2\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use ZEA2\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use ZEA2\REST\Bundles\Register\Bundles\RegisterRecord\Tests\Fixture\RegisterRecordFixture;
use ZEA2\REST\Bundles\Register\Bundles\RegisterRecord\Tests\RegisterRecordMiddlewareTestCase;
use ZEA2\REST\Bundles\Register\Bundles\RegisterRecordGroup\Tests\Fixture\RegisterRecordGroupFixture;

final class EditRegisterRecordMiddlewareTest extends RegisterRecordMiddlewareTestCase
{
    private function getTestJSON(string $key = 'foo__', string $value = 'woof__'): array
    {
        return [
            'register_record_group_id' => RegisterRecordGroupFixture::$recordGroup1->getId(),
            'key' => $key,
            'value' => $value,
            'options' => [
                'foo' => 'bar',
            ]
        ];
    }

    public function test200()
    {
        $this->upFixture(new RegisterRecordFixture());

        $record = RegisterRecordFixture::$record1;

        $this->requestRegisterRecordEdit($recordId = $record->getId(), $json = $this->getTestJSON())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'register_record' => [
                    'id' => $recordId,
                    'key' => $json['key'],
                    'value' => $json['value'],
                ]
            ])
        ;
    }

    public function test403()
    {
        $this->upFixture(new RegisterRecordFixture());

        $record = RegisterRecordFixture::$record1;

        $this->requestRegisterRecordEdit($recordId = $record->getId(), $json = $this->getTestJSON())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
        ;

        $this->requestRegisterRecordEdit($recordId = $record->getId(), $json = $this->getTestJSON())
            ->__invoke()
            ->expectAuthError();
        ;
    }

    public function test409DuplicateKey()
    {
        $this->upFixture(new RegisterRecordFixture());

        $record = RegisterRecordFixture::$record1;
        $otherRecord = RegisterRecordFixture::$record2;

        $json = $json = $this->getTestJSON();
        $json['key'] = $otherRecord->getKey();

        $this->requestRegisterRecordEdit($recordId = $record->getId(), $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(409)
        ;
    }

    public function test409InvalidKeyName()
    {
        $this->upFixture(new RegisterRecordFixture());

        $keys = [
            'демо',
            'de^^mo',
            '',
        ];

        foreach($keys as $key) {
            $json = $this->getTestJSON($key);

            $record = RegisterRecordFixture::$record1;

            $this->requestRegisterRecordEdit($recordId = $record->getId(), $json)
                ->auth(AdminAccountFixture::getJWT())
                ->__invoke()
                ->expectStatusCode(409)
            ;
        }
    }

    public function test404RecordGroupNotFound()
    {
        $this->upFixture(new RegisterRecordFixture());

        $json = $this->getTestJSON();
        $json['register_record_group_id'] = self::NOT_FOUND_ID;

        $record = RegisterRecordFixture::$record1;

        $this->requestRegisterRecordEdit($recordId = $record->getId(), $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
        ;
    }

    public function test404RecordNotFound()
    {
        $this->upFixture(new RegisterRecordFixture());

        $this->requestRegisterRecordEdit(self::NOT_FOUND_ID, $json = $this->getTestJSON())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
        ;
    }
}
<?php
namespace ZEA2\REST\Bundles\Register\Bundles\RegisterRecord\Tests\Paths;

use ZEA2\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use ZEA2\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use ZEA2\REST\Bundles\Register\Bundles\RegisterRecord\Tests\Fixture\RegisterRecordFixture;
use ZEA2\REST\Bundles\Register\Bundles\RegisterRecord\Tests\RegisterRecordMiddlewareTestCase;

final class GetBulkRegisterRecordMiddlewareTest extends RegisterRecordMiddlewareTestCase
{
    private function getTestJSON(): array
    {
        return [
            'keys' => [
                'demo_1',
                'demo_2',
                'demo_3',
            ]
        ];
    }

    public function test200()
    {
        $this->upFixture(new RegisterRecordFixture());

        $this->requestRegisterRecordGetBulk($json = $this->getTestJSON())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expect(function(array $result) use ($json) {
                $keys = array_map(function(array $item) {
                    return $item['key'];
                }, $result['register_records']);

                $this->assertEquals($json['keys'], $keys);
            })
        ;
    }

    public function test404()
    {
        $this->upFixture(new RegisterRecordFixture());

        $this->requestRegisterRecordGetBulk(['keys' => ['you-should-not-found-this']])
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();

        $this->requestRegisterRecordGetBulk(['keys' => ['demo_1', 'you-should-not-found-this']])
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();

        $this->requestRegisterRecordGetBulk(['keys' => ['you-should-not-found-this', 'demo_2', 'demo_3']])
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test403()
    {
        $this->upFixture(new RegisterRecordFixture());

        $this->requestRegisterRecordGetBulk($this->getTestJSON())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
        ;

        $this->requestRegisterRecordGetBulk($this->getTestJSON())
            ->__invoke()
            ->expectAuthError();
        ;
    }
}
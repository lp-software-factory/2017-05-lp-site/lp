<?php
namespace ZEA2\REST\Bundles\Register\Bundles\RegisterRecordGroup\Middleware\Command;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;

final class GetAllRegisterRecordGroupCommand extends AbstractRegisterRecordGroupCommand
{
    public function __invoke(ServerRequestInterface $request, JSONResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->access->requireAdminAccess();

        $responseBuilder
            ->setStatusSuccess()
            ->setJSON([
                'register_record_groups' => $this->formatter->formatMany($this->service->getAll()),
            ]);

        return $responseBuilder->build();
    }
}
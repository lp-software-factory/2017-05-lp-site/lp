<?php
namespace ZEA2\REST\Bundles\Register\Bundles\RegisterRecordGroup\Tests;

use ZEA2\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use ZEA2\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use ZEA2\REST\Bundles\Locale\Tests\Fixture\LocaleFixture;
use ZEA2\REST\Bundles\PHPUnit\TestCase\ZEA2MiddlewareTestCase;

abstract class RegisterRecordGroupMiddlewareTestCase extends ZEA2MiddlewareTestCase
{
    protected function getFixtures(): array
    {
        return [
            new LocaleFixture(),
            new AdminAccountFixture(),
            new UserAccountFixture(),
        ];
    }
}
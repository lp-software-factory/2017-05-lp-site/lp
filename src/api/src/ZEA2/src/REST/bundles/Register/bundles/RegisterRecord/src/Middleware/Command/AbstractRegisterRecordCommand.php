<?php
namespace ZEA2\REST\Bundles\Register\Bundles\RegisterRecord\Middleware\Command;

use ZEA2\Domain\Bundles\Register\Formatter\RegisterRecordFormatter;
use ZEA2\Domain\Bundles\Register\Service\RegisterRecord\RegisterRecordService;
use ZEA2\REST\Bundles\Access\Service\AccessService;
use ZEA2\REST\Bundles\Register\Bundles\RegisterRecord\ParametersFactory\RegisterRecordParametersFactory;
use ZEA2\REST\Command\Command;

abstract class AbstractRegisterRecordCommand implements Command
{
    /** @var AccessService */
    protected $access;

    /** @var RegisterRecordService */
    protected $service;

    /** @var RegisterRecordFormatter */
    protected $formatter;

    /** @var RegisterRecordParametersFactory */
    protected $parametersFactory;

    public function __construct(
        AccessService $access,
        RegisterRecordService $service,
        RegisterRecordFormatter $formatter,
        RegisterRecordParametersFactory $parametersFactory
    ) {
        $this->access = $access;
        $this->service = $service;
        $this->formatter = $formatter;
        $this->parametersFactory = $parametersFactory;
    }
}
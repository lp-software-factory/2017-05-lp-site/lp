<?php
namespace ZEA2\REST\Bundles\Register;

use ZEA2\REST\Bundle\RESTBundle;
use ZEA2\REST\Bundles\Register\Bundles\RegisterRecord\RegisterRecordRESTBundle;
use ZEA2\REST\Bundles\Register\Bundles\RegisterRecordGroup\RegisterRecordGroupRESTBundle;

final class RegisterRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getBundles(): array
    {
        return [
            new RegisterRecordRESTBundle(),
            new RegisterRecordGroupRESTBundle(),
        ];
    }
}
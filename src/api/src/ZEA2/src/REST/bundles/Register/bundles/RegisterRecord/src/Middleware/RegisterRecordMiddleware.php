<?php
namespace ZEA2\REST\Bundles\Register\Bundles\RegisterRecord\Middleware;

use ZEA2\Platform\Response\JSON\JSONResponseBuilder;
use ZEA2\REST\Bundles\Register\Bundles\RegisterRecord\Middleware\Command\CreateRegisterRecordCommand;
use ZEA2\REST\Bundles\Register\Bundles\RegisterRecord\Middleware\Command\DeleteRegisterRecordCommand;
use ZEA2\REST\Bundles\Register\Bundles\RegisterRecord\Middleware\Command\EditBulkRegisterRecordCommand;
use ZEA2\REST\Bundles\Register\Bundles\RegisterRecord\Middleware\Command\EditRegisterRecordCommand;
use ZEA2\REST\Bundles\Register\Bundles\RegisterRecord\Middleware\Command\GetAllRegisterRecordCommand;
use ZEA2\REST\Bundles\Register\Bundles\RegisterRecord\Middleware\Command\GetBulkRegisterRecordCommand;
use ZEA2\REST\Bundles\Register\Bundles\RegisterRecord\Middleware\Command\GetByIdRegisterRecordCommand;
use ZEA2\REST\Command\Service\CommandService;
use Zend\Stratigility\MiddlewareInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class RegisterRecordMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new JSONResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('create', CreateRegisterRecordCommand::class)
            ->attachDirect('edit', EditRegisterRecordCommand::class)
            ->attachDirect('edit-bulk', EditBulkRegisterRecordCommand::class)
            ->attachDirect('delete', DeleteRegisterRecordCommand::class)
            ->attachDirect('all', GetAllRegisterRecordCommand::class)
            ->attachDirect('get', GetByIdRegisterRecordCommand::class)
            ->attachDirect('get-bulk', GetBulkRegisterRecordCommand::class)
            ->resolve($request);

        return $resolver->__invoke($request, $responseBuilder);
    }
}
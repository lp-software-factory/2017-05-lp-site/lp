<?php
namespace ZEA2\REST\Bundles\Register\Bundles\RegisterRecordGroup\Tests\Paths;

use ZEA2\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use ZEA2\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use ZEA2\REST\Bundles\Register\Bundles\RegisterRecordGroup\Tests\Fixture\RegisterRecordGroupFixture;
use ZEA2\REST\Bundles\Register\Bundles\RegisterRecordGroup\Tests\RegisterRecordGroupMiddlewareTestCase;

final class EditRegisterRecordGroupMiddlewareTest extends RegisterRecordGroupMiddlewareTestCase
{
    private function getTestJSON(string $appName = 'demo__'): array
    {
        return [
            'app_name' => $appName,
        ];
    }

    public function test200()
    {
        $this->upFixture(new RegisterRecordGroupFixture());

        $group = RegisterRecordGroupFixture::$recordGroup1;

        $this->requestRegisterRecordGroupEdit($group->getId(), $json = $this->getTestJSON())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'register_record_group' => [
                    'id' => $this->expectId(),
                    'app_name' => $json['app_name'],
                ]
            ]);
    }

    public function test403()
    {
        $this->upFixture(new RegisterRecordGroupFixture());

        $group = RegisterRecordGroupFixture::$recordGroup1;

        $this->requestRegisterRecordGroupEdit($group->getId(), $json = $this->getTestJSON())
            ->__invoke()
            ->expectAuthError();

        $this->requestRegisterRecordGroupCreate($json = $this->getTestJSON())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new RegisterRecordGroupFixture());

        $this->requestRegisterRecordGroupEdit(self::NOT_FOUND_ID, $json = $this->getTestJSON())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test409Duplicate()
    {
        $this->upFixture(new RegisterRecordGroupFixture());

        $this->requestRegisterRecordGroupEdit(RegisterRecordGroupFixture::$recordGroup1->getId(), $json = $this->getTestJSON())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200);

        $this->requestRegisterRecordGroupEdit(RegisterRecordGroupFixture::$recordGroup2->getId(), $json = $this->getTestJSON())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(409);
    }

    public function test409BadNames()
    {
        $this->upFixture(new RegisterRecordGroupFixture());

        $group = RegisterRecordGroupFixture::$recordGroup1;

        $names = [
            '',
            'приложение',
        ];

        foreach($names as $name) {
            $this->requestRegisterRecordGroupEdit($group->getId(), $json = $this->getTestJSON($name))
                ->auth(AdminAccountFixture::getJWT())
                ->__invoke()
                ->expectStatusCode(409)
            ;
        }
    }
}
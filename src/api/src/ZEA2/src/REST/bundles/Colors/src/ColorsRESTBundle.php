<?php
namespace ZEA2\REST\Bundles\Colors;

use ZEA2\REST\Bundle\RESTBundle;

final class ColorsRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}
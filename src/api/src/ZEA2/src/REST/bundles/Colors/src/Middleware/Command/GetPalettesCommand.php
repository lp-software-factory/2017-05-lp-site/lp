<?php
namespace ZEA2\REST\Bundles\Colors\Middleware\Command;

use ZEA2\REST\Command\Command;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;
use ZEA2\Domain\Bundles\Colors\Entity\Palette;
use ZEA2\Domain\Bundles\Colors\Service\ColorsService;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetPalettesCommand implements Command
{
    /** @var ColorsService */
    private $colorsService;

    public function __construct(ColorsService $colorsService)
    {
        $this->colorsService = $colorsService;
    }

    public function __invoke(ServerRequestInterface $request, JSONResponseBuilder $responseBuilder): ResponseInterface
    {
        return $responseBuilder
            ->setJSON([
                'palettes' => array_map(function (Palette $palette) {
                    return $palette->toJSON();
                }, $this->colorsService->getPalettes())
            ])
            ->setStatusSuccess()
            ->build();
    }
}
<?php
namespace ZEA2\REST\Bundles\Colors;

use ZEA2\REST\Bundles\Colors\Middleware\ColorsMiddleware;

$COLOR_ROUTES = [
    [
        'method' => 'get',
        'path' => '/colors/{command:get-colors}[/]',
        'middleware' => ColorsMiddleware::class,
        'group' => 'default',
    ],
    [
        'method' => 'get',
        'path' => '/colors/{command:get-palettes}[/]',
        'middleware' => ColorsMiddleware::class,
        'group' => 'default',
    ],
];

return [
    'router' => [
        'routes' => $COLOR_ROUTES,
    ]
];
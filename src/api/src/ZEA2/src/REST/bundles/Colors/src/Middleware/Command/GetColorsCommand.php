<?php
namespace ZEA2\REST\Bundles\Colors\Middleware\Command;

use ZEA2\REST\Command\Command;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;
use ZEA2\Domain\Bundles\Colors\Entity\Color;
use ZEA2\Domain\Bundles\Colors\Service\ColorsService;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetColorsCommand implements Command
{
    /** @var ColorsService */
    private $colorsService;

    public function __construct(ColorsService $colorsService)
    {
        $this->colorsService = $colorsService;
    }

    public function __invoke(ServerRequestInterface $request, JSONResponseBuilder $responseBuilder): ResponseInterface
    {
        return $responseBuilder
            ->setJSON([
                'colors' => array_map(function (Color $color) {
                    return $color->toJSON();
                }, $this->colorsService->getColors())
            ])
            ->setStatusSuccess()
            ->build();
    }
}
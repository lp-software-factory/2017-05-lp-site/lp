<?php
namespace ZEA2\REST\Bundles\Colors\Middleware;

use ZEA2\REST\Bundles\Colors\Middleware\Command\GetColorsCommand;
use ZEA2\REST\Bundles\Colors\Middleware\Command\GetPalettesCommand;
use ZEA2\REST\Command\Service\CommandService;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;
use Zend\Stratigility\MiddlewareInterface;

class ColorsMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new JSONResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('get-colors', GetColorsCommand::class)
            ->attachDirect('get-palettes', GetPalettesCommand::class)
            ->resolve($request);

        return $resolver->__invoke($request, $responseBuilder);
    }
}
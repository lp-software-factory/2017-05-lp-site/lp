<?php
namespace ZEA2\REST\Bundles\Locale\Request;

use ZEA2\Domain\Bundles\Locale\Parameters\CreateLocaleParameters;
use ZEA2\Platform\Bundles\APIDocs\Schema\JSONSchema;
use ZEA2\Platform\Bundles\APIDocs\Schema\SchemaRequest;

final class CreateLocaleRequest extends SchemaRequest
{
    public function getParameters(): CreateLocaleParameters
    {
        $data = $this->getData();

        return new CreateLocaleParameters(
            $data['language'],
            $data['region']
        );
    }

    protected function getSchema(): JSONSchema
    {
        return SchemaRequest::getSchemaService()->getDefinition('ZEA2_LocaleBundle_CreateLocaleRequest');
    }
}
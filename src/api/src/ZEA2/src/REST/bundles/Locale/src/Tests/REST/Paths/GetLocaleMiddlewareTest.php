<?php
namespace ZEA2\REST\Bundles\Locale\Tests\REST\Paths;

use ZEA2\REST\Bundles\Locale\Tests\Fixture\LocaleFixture;
use ZEA2\REST\Bundles\Locale\Tests\LocaleMiddlewareTestCase;

final class GetLocaleMiddlewareTest extends LocaleMiddlewareTestCase
{
    public function test404()
    {
        $this->requestLocaleGet(self::NOT_FOUND_ID)
            ->__invoke()
            ->expectNotFoundError();
        ;
    }

    public function test200()
    {
        $this->requestLocaleGet(LocaleFixture::getLocaleEN()->getId())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'locale' => LocaleFixture::getLocaleEN()->toJSON(),
            ])
        ;
    }
}
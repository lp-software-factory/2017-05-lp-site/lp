<?php
namespace ZEA2\REST\Bundles\Locale\Middleware\Command;

use ZEA2\Domain\Bundles\Locale\Exceptions\LocaleNotFoundException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;

final class DeleteLocaleCommand extends Command
{
    public function __invoke(ServerRequestInterface $request, JSONResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $this->localeService->deleteLocale($request->getAttribute('localeId'));

            $responseBuilder
                ->setStatusSuccess();
        }catch(LocaleNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}
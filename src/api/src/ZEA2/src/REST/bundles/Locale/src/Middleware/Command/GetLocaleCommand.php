<?php
namespace ZEA2\REST\Bundles\Locale\Middleware\Command;

use ZEA2\Domain\Bundles\Locale\Exceptions\LocaleNotFoundException;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetLocaleCommand extends Command
{
    public function __invoke(ServerRequestInterface $request, JSONResponseBuilder $responseBuilder): ResponseInterface
    {
        try {
            $locale = $this->localeService->getLocaleById($request->getAttribute('localeId'));

            $responseBuilder
                ->setJSON([
                    'locale' => $locale->toJSON(),
                ])
                ->setStatusSuccess();
        } catch(LocaleNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}
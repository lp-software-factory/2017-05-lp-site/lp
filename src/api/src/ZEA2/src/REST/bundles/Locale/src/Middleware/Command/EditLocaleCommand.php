<?php
namespace ZEA2\REST\Bundles\Locale\Middleware\Command;

use ZEA2\Domain\Bundles\Locale\Exceptions\DuplicateLocaleException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ZEA2\REST\Bundles\Locale\Request\EditLocaleRequest;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;

final class EditLocaleCommand extends Command
{
    public function __invoke(ServerRequestInterface $request, JSONResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $parameters = (new EditLocaleRequest($request))->getParameters();
            $locale = $this->localeService->editLocale($request->getAttribute('localeId'), $parameters);

            $responseBuilder
                ->setJSON([
                    'locale' => $locale->toJSON(),
                ])
                ->setStatusSuccess();
        }catch(DuplicateLocaleException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusConflict();
        }

        return $responseBuilder->build();
    }
}
<?php
namespace ZEA2\REST\Bundles\Locale;

use ZEA2\REST\Bundle\RESTBundle;

final class LocaleRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}
<?php
namespace ZEA2\REST\Bundles\Locale\Middleware\Command;

use ZEA2\Domain\Bundles\Locale\Exceptions\DuplicateLocaleException;
use ZEA2\REST\Bundles\Locale\Request\CreateLocaleRequest;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;

final class CreateLocaleCommand extends Command
{
    public function __invoke(ServerRequestInterface $request, JSONResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $parameters = (new CreateLocaleRequest($request))->getParameters();
            $locale = $this->localeService->createLocale($parameters);

            $responseBuilder
                ->setJSON([
                    'locale' => $locale->toJSON(),
                ])
                ->setStatusSuccess();
        }catch(DuplicateLocaleException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusConflict();
        }

        return $responseBuilder->build();
    }
}
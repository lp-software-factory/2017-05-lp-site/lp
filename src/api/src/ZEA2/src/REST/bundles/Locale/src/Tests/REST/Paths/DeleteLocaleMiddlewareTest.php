<?php
namespace ZEA2\REST\Bundles\Locale\Tests\REST\Paths;

use ZEA2\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use ZEA2\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use ZEA2\REST\Bundles\Locale\Tests\Fixture\LocaleFixture;
use ZEA2\REST\Bundles\Locale\Tests\LocaleMiddlewareTestCase;

final class DeleteLocaleMiddlewareTest extends LocaleMiddlewareTestCase
{
    public function test403()
    {
        $this->requestLocaleDelete(LocaleFixture::getLocaleEN()->getId())
            ->__invoke()
            ->expectAuthError();

        $this->requestLocaleDelete(LocaleFixture::getLocaleEN()->getId())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test200()
    {
        $localeId = LocaleFixture::getLocaleEN()->getId();

        $this->requestLocaleDelete($localeId)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true
            ]);

        $this->requestLocaleGet($localeId)
            ->__invoke()
            ->expectNotFoundError();
    }
}
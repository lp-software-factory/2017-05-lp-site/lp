<?php
namespace ZEA2\REST\Bundles\Locale\Tests\REST\Paths;

use ZEA2\REST\Bundles\Locale\Tests\Fixture\LocaleFixture;
use ZEA2\REST\Bundles\Locale\Tests\LocaleMiddlewareTestCase;

final class ListLocaleMiddlewareTest extends LocaleMiddlewareTestCase
{
    public function test200()
    {
        $this->requestLocaleList()
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'locales' => [
                    LocaleFixture::getLocaleEN()->toJSON(),
                    LocaleFixture::getLocaleRU()->toJSON(),
                ]
            ])
            ->expect(function(array $json) {
                return count($json['locales']) === 2;
            });
    }
}
<?php
namespace ZEA2\REST\Bundles\Locale\Tests\REST\Paths;

use ZEA2\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use ZEA2\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use ZEA2\REST\Bundles\Locale\Tests\Fixture\LocaleFixture;
use ZEA2\REST\Bundles\Locale\Tests\LocaleMiddlewareTestCase;

final class CreateLocaleMiddlewareTest extends LocaleMiddlewareTestCase
{
    public function test403()
    {
        $json = [
            'language' => 'jp',
            'region' => 'jp_JP',
        ];

        $this->requestLocaleCreate($json)
            ->__invoke()
            ->expectAuthError();

        $this->requestLocaleCreate($json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test409()
    {
        $this->requestLocaleCreate([
            'language' => LocaleFixture::getLocaleEN()->getLanguage(),
            'region' => LocaleFixture::getLocaleEN()->getRegion(),
        ])
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(409)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => false,
                'error' => $this->expectString(),
            ]);
    }

    public function test200()
    {
        $json = [
            'language' => 'jp',
            'region' => 'jp_JP',
        ];

        $this->requestLocaleCreate($json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'locale' => [
                    'id' => $this->expectId(),
                    'metadata' => [
                        'version' => $this->expectString(),
                    ],
                    'language' => $json['language'],
                    'region' => $json['region'],
                ],
            ]);
    }
}
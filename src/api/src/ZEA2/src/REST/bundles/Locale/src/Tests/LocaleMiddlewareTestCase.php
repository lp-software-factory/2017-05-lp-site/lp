<?php
namespace ZEA2\REST\Bundles\Locale\Tests;

use ZEA2\REST\Bundles\Locale\Tests\Fixture\LocaleFixture;
use ZEA2\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use ZEA2\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use ZEA2\REST\Bundles\PHPUnit\TestCase\ZEA2MiddlewareTestCase;

abstract class LocaleMiddlewareTestCase extends ZEA2MiddlewareTestCase
{
    protected function getFixtures(): array
    {
        return [
            new LocaleFixture(),
            new UserAccountFixture(),
            new AdminAccountFixture(),
        ];
    }
}
<?php
namespace ZEA2\REST\Bundles\Locale;

use ZEA2\REST\Bundles\Locale\Middleware\CurrentLocalePipe;
use ZEA2\REST\Bundles\Locale\Middleware\LocaleMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'pipe',
                'path' => '/',
                'group' => 'locale',
                'middleware' => CurrentLocalePipe::class,
            ],
            [
                'method' => 'put',
                'path' => '/locale/{command:create}[/]',
                'middleware' => LocaleMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/locale/{localeId}/{command:edit}[/]',
                'middleware' => LocaleMiddleware::class,
            ],
            [
                'method' => 'delete',
                'path' => '/locale/{localeId}/{command:delete}[/]',
                'middleware' => LocaleMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/locale/{localeId}/{command:get}[/]',
                'middleware' => LocaleMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/locale/{command:list}[/]',
                'middleware' => LocaleMiddleware::class,
            ],
        ],
    ],
];
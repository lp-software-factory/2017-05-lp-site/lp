<?php
namespace ZEA2\REST\Bundles\Locale\Request;

use ZEA2\Domain\Bundles\Locale\Parameters\EditLocaleParameters;
use ZEA2\Platform\Bundles\APIDocs\Schema\JSONSchema;
use ZEA2\Platform\Bundles\APIDocs\Schema\SchemaRequest;

final class EditLocaleRequest extends SchemaRequest
{
    public function getParameters(): EditLocaleParameters
    {
        $data = $this->getData();

        return new EditLocaleParameters(
            $data['language'],
            $data['region']
        );
    }

    protected function getSchema(): JSONSchema
    {
        return SchemaRequest::getSchemaService()->getDefinition('ZEA2_LocaleBundle_EditLocaleRequest');
    }
}
<?php
namespace ZEA2\REST\Bundles\Locale\Middleware\Command;

use ZEA2\Domain\Bundles\Locale\Entity\Locale;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class ListLocalesCommand extends Command
{
    public function __invoke(ServerRequestInterface $request, JSONResponseBuilder $responseBuilder): ResponseInterface
    {
        $responseBuilder
            ->setJSON([
                'locales' => array_map(function(Locale $locale) {
                    return $locale->toJSON();
                }, $this->localeService->getAllLocales())
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}
<?php
namespace ZEA2\REST\Bundles\Locale\Middleware;

use ZEA2\REST\Bundles\Locale\Middleware\Command\CreateLocaleCommand;
use ZEA2\REST\Bundles\Locale\Middleware\Command\DeleteLocaleCommand;
use ZEA2\REST\Bundles\Locale\Middleware\Command\EditLocaleCommand;
use ZEA2\REST\Bundles\Locale\Middleware\Command\GetLocaleCommand;
use ZEA2\REST\Bundles\Locale\Middleware\Command\ListLocalesCommand;
use ZEA2\REST\Command\Service\CommandService;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;
use Zend\Stratigility\MiddlewareInterface;

final class LocaleMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new JSONResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('create', CreateLocaleCommand::class)
            ->attachDirect('edit', EditLocaleCommand::class)
            ->attachDirect('delete', DeleteLocaleCommand::class)
            ->attachDirect('get', GetLocaleCommand::class)
            ->attachDirect('list', ListLocalesCommand::class)
            ->resolve($request);

        return $resolver->__invoke($request, $responseBuilder);
    }
}
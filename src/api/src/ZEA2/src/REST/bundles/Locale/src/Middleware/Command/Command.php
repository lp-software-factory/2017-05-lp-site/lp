<?php
namespace ZEA2\REST\Bundles\Locale\Middleware\Command;

use ZEA2\Domain\Bundles\Locale\Service\LocaleService;
use ZEA2\REST\Bundles\Access\Service\AccessService;

abstract class Command implements \ZEA2\REST\Command\Command
{
    /** @var AccessService */
    protected $accessService;

    /** @var LocaleService */
    protected $localeService;

    public function __construct(AccessService $accessService, LocaleService $localeService)
    {
        $this->accessService = $accessService;
        $this->localeService = $localeService;
    }
}
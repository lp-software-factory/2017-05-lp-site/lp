<?php
namespace ZEA2\REST\Bundles\Profile\Middleware;

use ZEA2\REST\Bundles\Profile\Middleware\Command\ProfileImageGenerateCommand;
use ZEA2\REST\Bundles\Profile\Middleware\Command\ProfileImageUploadCommand;
use ZEA2\REST\Command\Service\CommandService;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;
use Zend\Stratigility\MiddlewareInterface;

final class ProfileMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new JSONResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('image-upload', ProfileImageUploadCommand::class)
            ->attachDirect('image-regenerate', ProfileImageGenerateCommand::class)
            ->resolve($request);

        return $resolver->__invoke($request, $responseBuilder);
    }
}
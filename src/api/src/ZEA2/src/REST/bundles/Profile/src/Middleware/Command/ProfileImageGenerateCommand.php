<?php
namespace ZEA2\REST\Bundles\Profile\Middleware\Command;

use ZEA2\Domain\Bundles\Profile\Exceptions\ProfileNotFoundException;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class ProfileImageGenerateCommand extends AbstractProfileCommand
{
    public function __invoke(ServerRequestInterface $request, JSONResponseBuilder $responseBuilder): ResponseInterface
    {
        $profileId = $request->getAttribute('profileId');

        $this->accessService->requireOwner($profileId);

        try {
            $image = $this->profileService->regenerateProfileImage($profileId);

            $responseBuilder
                ->setJSON([
                    'image' => $image->toJSON(),
                ])
                ->setStatusSuccess();
        }catch (ProfileNotFoundException $e) {
            $responseBuilder
                ->setStatusNotFound()
                ->setError($e);
        }

        return $responseBuilder->build();
    }
}
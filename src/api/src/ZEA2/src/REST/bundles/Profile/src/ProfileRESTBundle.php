<?php
namespace ZEA2\REST\Bundles\Profile;

use ZEA2\REST\Bundle\RESTBundle;

final class ProfileRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}
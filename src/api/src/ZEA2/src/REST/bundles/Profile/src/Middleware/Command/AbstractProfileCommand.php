<?php
namespace ZEA2\REST\Bundles\Profile\Middleware\Command;

use ZEA2\Domain\Bundles\Profile\Service\ProfileService;
use ZEA2\REST\Bundles\Access\Service\AccessService;
use ZEA2\REST\Command\Command;

abstract class AbstractProfileCommand implements Command
{
    /** @var AccessService */
    protected $accessService;

    /** @var ProfileService */
    protected $profileService;

    public function __construct(AccessService $accessService, ProfileService $profileService)
    {
        $this->accessService = $accessService;
        $this->profileService = $profileService;
    }
}
<?php
namespace ZEA2\REST\Bundles\Profile;

use ZEA2\REST\Bundles\Frontend\Middleware\AbstractFrontendMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'get',
                'path' => '/frontend[/]',
                'middleware' => AbstractFrontendMiddleware::class,
            ]
        ],
    ],
];
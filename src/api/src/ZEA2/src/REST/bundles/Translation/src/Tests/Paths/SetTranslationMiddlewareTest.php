<?php
namespace ZEA2\REST\Bundles\Translation\Tests\Paths;

use ZEA2\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use ZEA2\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use ZEA2\REST\Bundles\Translation\Tests\Fixture\TranslationFixture;
use ZEA2\REST\Bundles\Translation\Tests\TranslationMiddlewareTestCase;

final class SetTranslationMiddlewareTest extends TranslationMiddlewareTestCase
{
    private function getTestJSON(): array
    {
        return [
            'value' => [
                ['region' => 'en_US', 'value' => 'Demo US'],
                ['region' => 'en_GB', 'value' => 'Demo GB'],
            ]
        ];
    }

    public function test200Update()
    {
        $key = 'hi.main.foo.x';

        $this->requestTranslationSet(TranslationFixture::PROJECT_ID, $key, $json = $this->getTestJSON())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'translation' => [
                    'id' => $this->expectId(),
                    'key' => $key,
                    'value' => $json['value'],
                ]
            ])
        ;
    }

    public function test200Insert()
    {
        $key = 'hi.main.foo.my.new.sub.key';

        $this->requestTranslationSet(TranslationFixture::PROJECT_ID, $key, $json = $this->getTestJSON())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'translation' => [
                    'id' => $this->expectId(),
                    'key' => $key,
                    'value' => $json['value'],
                ]
            ])
        ;
    }

    public function test403()
    {
        $key = 'hi.main.foo.my.new.sub.key';

        $this->requestTranslationSet(TranslationFixture::PROJECT_ID, $key, $json = $this->getTestJSON())
            ->__invoke()
            ->expectAuthError()
        ;

        $this->requestTranslationSet(TranslationFixture::PROJECT_ID, $key, $json = $this->getTestJSON())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError()
        ;
    }
}
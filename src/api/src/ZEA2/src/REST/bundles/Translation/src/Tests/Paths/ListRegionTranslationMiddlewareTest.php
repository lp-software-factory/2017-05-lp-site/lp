<?php
namespace ZEA2\REST\Bundles\Translation\Tests\Paths;

use ZEA2\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use ZEA2\REST\Bundles\Translation\Tests\Fixture\TranslationFixture;
use ZEA2\REST\Bundles\Translation\Tests\TranslationMiddlewareTestCase;

final class ListRegionTranslationMiddlewareTest extends TranslationMiddlewareTestCase
{
    public function test200()
    {
        $translations = $this->requestTranslationListRegion(TranslationFixture::PROJECT_ID, 'en_US')
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->fetch(function(array $json) {
                return $json['translations'];
            });

        $this->assertEquals(5, count($translations));
    }
}
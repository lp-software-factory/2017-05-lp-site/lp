<?php
namespace ZEA2\REST\Bundles\Translation\Middleware\Command;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ZEA2\Domain\Bundles\Translation\Exceptions\TranslationNotFoundException;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;

final class SetTranslationCommand extends AbstractTranslationCommand
{
    public function __invoke(ServerRequestInterface $request, JSONResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $entity = $this->service->upsert($this->parametersFactory->factorySetTranslationParameters($request));

            $responseBuilder
                ->setJSON([
                    'translation' => $this->formatter->formatOne($entity)
                ])
                ->setStatusSuccess();
        }catch(TranslationNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}
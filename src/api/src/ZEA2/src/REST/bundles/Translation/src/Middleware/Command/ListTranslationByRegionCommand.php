<?php
namespace ZEA2\REST\Bundles\Translation\Middleware\Command;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ZEA2\Domain\Bundles\Translation\Exceptions\TranslationNotFoundException;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;

final class ListTranslationByRegionCommand extends AbstractTranslationCommand
{
    public function __invoke(ServerRequestInterface $request, JSONResponseBuilder $responseBuilder): ResponseInterface
    {
        $projectId = $request->getAttribute('projectId');
        $region = $request->getAttribute('region');

        $this->accessService->requireAdminAccess();

        try {
            $responseBuilder
                ->setJSON([
                    'translations' => $this->formatter->formatManyRegion(
                        $this->service->listTranslationsByRegion($projectId, $region)
                    )
                ])
                ->setStatusSuccess();
        }catch(TranslationNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}
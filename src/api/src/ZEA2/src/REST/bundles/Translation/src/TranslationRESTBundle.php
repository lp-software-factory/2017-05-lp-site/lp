<?php
namespace ZEA2\REST\Bundles\Translation;

use ZEA2\REST\Bundle\RESTBundle;

final class TranslationRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
       return __DIR__;
    }
}
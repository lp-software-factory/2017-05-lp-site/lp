<?php
namespace ZEA2\REST\Bundles\Translation\Tests\Fixture;

use Doctrine\ORM\EntityManager;
use ZEA2\Domain\Bundles\Translation\Entity\Translation;
use ZEA2\Domain\Bundles\Translation\Parameters\SetTranslationParameters;
use ZEA2\Domain\Bundles\Translation\Repository\TranslationProjectRepository;
use ZEA2\Domain\Bundles\Translation\Service\TranslationService;
use ZEA2\REST\Bundles\PHPUnit\Fixture;
use ZEA2\Platform\Bundles\DoctrineORM\Type\LocalizedString\Value\ImmutableLocalizedString;
use Zend\Expressive\Application;

final class TranslationFixture implements Fixture
{
    public const PROJECT_ID = TranslationProjectRepository::PROJECT_ID_TEST;

    /** @var Translation */
    public static $translationEF1;

    /** @var Translation */
    public static $translationEF2;

    /** @var Translation */
    public static $translationEF3;

    /** @var TranslationService */
    private $translationService;

    public function up(Application $app, EntityManager $em)
    {
        $this->translationService = $app->getContainer()->get(TranslationService::class);

        $this->upTranslation($this->demoTranslation());
    }

    private function upTranslation(array $input, array $carry = []): void
    {
        foreach($input as $key => $value) {
            $next = array_values($carry);
            $next[] = $key;

            if(is_array($value)) {
                $this->upTranslation($value, $next);
            }else if(is_string($value)) {
                $this->translationService->upsert(new SetTranslationParameters(
                    self::PROJECT_ID,
                    join('.', $next),
                    new ImmutableLocalizedString([['region' => 'en_US', 'value' => $value]])
                ));
            }else{
                throw new \Exception('Fixture error');
            }
        }
    }

    private function demoTranslation(): array
    {
        return [
            'hi' => [
                'main' => [
                    'foo' => [
                        'x' => 'x-foo',
                        'y' => 'y-foo',
                    ],
                    'bar' => [
                        'q' => 'q-bar',
                        'p' => 'p-bar',
                    ]
                ]
            ]
        ];
    }
}
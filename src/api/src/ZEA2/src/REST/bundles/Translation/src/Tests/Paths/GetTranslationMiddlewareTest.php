<?php
namespace ZEA2\REST\Bundles\Translation\Tests\Paths;

use ZEA2\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use ZEA2\REST\Bundles\Translation\Tests\Fixture\TranslationFixture;
use ZEA2\REST\Bundles\Translation\Tests\TranslationMiddlewareTestCase;

final class GetTranslationMiddlewareTest extends TranslationMiddlewareTestCase
{
    public function test200()
    {
        $this->requestTranslationGet(TranslationFixture::PROJECT_ID, $key = 'hi.main.foo.x')
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'translation' => [
                    'id' => $this->expectId(),
                    'key' => $key,
                    'value' => [
                        ['region' => 'en_US', 'value' => 'x-foo'],
                    ]
                ]
            ]);
    }
}
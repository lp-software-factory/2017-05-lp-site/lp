<?php
namespace ZEA2\REST\Bundles\Translation\Middleware\Command;

use ZEA2\Domain\Bundles\Auth\Inject\AuthToken;
use ZEA2\Domain\Bundles\Translation\Formatter\TranslationFormatter;
use ZEA2\Domain\Bundles\Translation\Service\TranslationService;
use ZEA2\REST\Bundles\Access\Service\AccessService;
use ZEA2\REST\Bundles\Translation\ParametersFactory\TranslationParametersFactory;
use ZEA2\REST\Command\Command;

abstract class AbstractTranslationCommand implements Command
{
    /** @var AuthToken */
    protected $authToken;

    /** @var AccessService */
    protected $accessService;

    /** @var TranslationParametersFactory */
    protected $parametersFactory;

    /** @var TranslationFormatter */
    protected $formatter;

    /** @var TranslationService */
    protected $service;

    public function __construct(
        AuthToken $authToken,
        AccessService $accessService,
        TranslationParametersFactory $parametersFactory,
        TranslationFormatter $formatter,
        TranslationService $service
    ) {
        $this->authToken = $authToken;
        $this->accessService = $accessService;
        $this->parametersFactory = $parametersFactory;
        $this->formatter = $formatter;
        $this->service = $service;
    }
}
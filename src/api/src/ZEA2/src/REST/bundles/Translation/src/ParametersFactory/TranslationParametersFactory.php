<?php
namespace ZEA2\REST\Bundles\Translation\ParametersFactory;

use ZEA2\Domain\Bundles\Translation\Parameters\SetTranslationParameters;
use ZEA2\REST\Bundles\Translation\Request\SetTranslationRequest;
use Psr\Http\Message\ServerRequestInterface;
use ZEA2\Platform\Bundles\DoctrineORM\Type\LocalizedString\Value\ImmutableLocalizedString;

final class TranslationParametersFactory
{
    public function factorySetTranslationParameters(ServerRequestInterface $request): SetTranslationParameters
    {
        $key = $request->getQueryParams()['key'];
        $projectId = $request->getAttribute('projectId');

        $json = (new SetTranslationRequest($request))->getParameters();

        return new SetTranslationParameters($projectId, $key, new ImmutableLocalizedString($json['value']));
    }
}
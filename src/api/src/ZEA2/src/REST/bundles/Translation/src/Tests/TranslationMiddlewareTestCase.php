<?php
namespace ZEA2\REST\Bundles\Translation\Tests;

use ZEA2\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use ZEA2\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use ZEA2\REST\Bundles\Locale\Tests\Fixture\LocaleFixture;
use ZEA2\REST\Bundles\PHPUnit\TestCase\ZEA2MiddlewareTestCase;
use ZEA2\REST\Bundles\Translation\Tests\Fixture\TranslationFixture;

abstract class TranslationMiddlewareTestCase extends ZEA2MiddlewareTestCase
{
    protected function getFixtures(): array
    {
        return [
            new LocaleFixture(),
            new UserAccountFixture(),
            new AdminAccountFixture(),
            new TranslationFixture(),
        ];
    }

}
<?php
namespace ZEA2\REST\Bundles\Translation\Tests;

return [
    'hi' => [
        'main' => [
            'foo' => [
                'x' => 'x-foo-gr',
            ]
        ]
    ]
];
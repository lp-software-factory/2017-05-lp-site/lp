<?php
namespace ZEA2\REST\Bundles\Translation\Middleware;

use ZEA2\REST\Bundles\Translation\Middleware\Command\GetTranslationCommand;
use ZEA2\REST\Bundles\Translation\Middleware\Command\ListTranslationByRegionCommand;
use ZEA2\REST\Bundles\Translation\Middleware\Command\ListTranslationCommand;
use ZEA2\REST\Bundles\Translation\Middleware\Command\SetTranslationCommand;
use ZEA2\REST\Command\Service\CommandService;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;
use Zend\Stratigility\MiddlewareInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class TranslationMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new JSONResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('list', ListTranslationCommand::class)
            ->attachDirect('list-region', ListTranslationByRegionCommand::class)
            ->attachDirect('get', GetTranslationCommand::class)
            ->attachDirect('set', SetTranslationCommand::class)
            ->resolve($request);

        return $resolver->__invoke($request, $responseBuilder);
    }
}
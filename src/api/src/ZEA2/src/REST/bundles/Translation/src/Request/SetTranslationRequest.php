<?php
namespace ZEA2\REST\Bundles\Translation\Request;

use ZEA2\Platform\Bundles\APIDocs\Schema\JSONSchema;
use ZEA2\Platform\Bundles\APIDocs\Schema\SchemaRequest;

final class SetTranslationRequest extends SchemaRequest
{
    public function getParameters(): array
    {
        return $this->getData();
    }

    protected function getSchema(): JSONSchema
    {
        return self::getSchemaService()->getDefinition('ZEA2_TranslationBundle_SetTranslationRequest');
    }
}
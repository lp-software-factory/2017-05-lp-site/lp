<?php
namespace ZEA2\REST\Bundles\OAuth2Providers\Tests\Fixture;

use Aego\OAuth2\Client\Provider\Yandex;
use Doctrine\ORM\EntityManager;
use ZEA2\Domain\Bundles\OAuth2\Entity\OAuth2Provider;
use ZEA2\Domain\Bundles\OAuth2\Parameters\CreateOAuth2ProviderParameters;
use ZEA2\Domain\Bundles\OAuth2\Service\OAuth2ProviderService;
use ZEA2\REST\Bundles\PHPUnit\Fixture;
use League\OAuth2\Client\Provider\Facebook;
use League\OAuth2\Client\Provider\Google;
use Zend\Expressive\Application;

final class OAuth2ProviderFixture implements Fixture
{
    /** @var OAuth2Provider */
    public static $PROVIDER_GOOGLE;

    /** @var OAuth2Provider */
    public static $PROVIDER_YANDEX;

    /** @var OAuth2Provider */
    public static $PROVIDER_FACEBOOK;

    public function up(Application $app, EntityManager $em)
    {
        /** @var OAuth2ProviderService $service */
        $service = $app->getContainer()->get(OAuth2ProviderService::class);

        self::$PROVIDER_GOOGLE = $service->createOAuth2Provider(new CreateOAuth2ProviderParameters(
            'google', Google::class, [
                'clientId' => 'CLIENT_ID_GOOGLE',
                'clientSecret' => 'CLIENT_SECRET_GOOGLE',
                'redirectUri' => 'http://localhost/redirect/google',
            ]
        ));

        self::$PROVIDER_YANDEX = $service->createOAuth2Provider(new CreateOAuth2ProviderParameters(
            'yandex', Yandex::class, [
                'clientId' => 'CLIENT_ID_YANDEX',
                'clientSecret' => 'CLIENT_SECRET_YANDEX',
                'redirectUri' => 'http://localhost/redirect/yandex',
            ]
        ));

        self::$PROVIDER_FACEBOOK = $service->createOAuth2Provider(new CreateOAuth2ProviderParameters(
            'facebook', Facebook::class, [
                'clientId' => 'CLIENT_ID_FACEBOOK',
                'clientSecret' => 'CLIENT_SECRET_FACEBOOK',
                'redirectUri' => 'http://localhost/redirect/facebook',
            ]
        ));
    }

    public static function getOrderedList(): array
    {
        return [
            self::$PROVIDER_GOOGLE,
            self::$PROVIDER_YANDEX,
            self::$PROVIDER_FACEBOOK,
        ];
    }
}
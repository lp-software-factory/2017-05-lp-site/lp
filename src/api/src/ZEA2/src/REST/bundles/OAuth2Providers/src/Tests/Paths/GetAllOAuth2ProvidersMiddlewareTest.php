<?php
namespace ZEA2\REST\Bundles\OAuth2Providers\Tests\Paths;

use ZEA2\Domain\Bundles\OAuth2\Entity\OAuth2Provider;
use ZEA2\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use ZEA2\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use ZEA2\REST\Bundles\OAuth2Providers\Tests\Fixture\OAuth2ProviderFixture;
use ZEA2\REST\Bundles\OAuth2Providers\Tests\OAuth2ProvidersMiddlewareTest;

final class GetAllOAuth2ProvidersMiddlewareTest extends OAuth2ProvidersMiddlewareTest
{
    public function test403()
    {
        $this->upFixture(new OAuth2ProviderFixture());

        $this->requestOAuth2ProvidersAllProviders()
            ->__invoke()
            ->expectAuthError();

        $this->requestOAuth2ProvidersAllProviders()
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test200()
    {
        $this->upFixture(new OAuth2ProviderFixture());

        $this->requestOAuth2ProvidersAllProviders()
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'providers' => array_map(function(OAuth2Provider $provider) {
                    return $provider->toJSON();
                }, OAuth2ProviderFixture::getOrderedList())
            ]);
    }
}
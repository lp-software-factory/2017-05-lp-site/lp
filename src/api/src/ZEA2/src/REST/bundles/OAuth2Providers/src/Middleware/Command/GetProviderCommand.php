<?php
namespace ZEA2\REST\Bundles\OAuth2Providers\Middleware\Command;

use ZEA2\Domain\Bundles\OAuth2\Exceptions\OAuth2ProviderNotFoundException;
use ZEA2\Domain\Bundles\OAuth2\Exceptions\UnknownLeagueProviderException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;

final class GetProviderCommand extends AbstractOAuth2ProvidersCommand
{
    public function __invoke(ServerRequestInterface $request, JSONResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $provider = $this->oauth2ProviderService->getProviderByCode($request->getAttribute('code'));

            $responseBuilder
                ->setJSON([
                    'provider' => $provider->toJSON(),
                ])
                ->setStatusSuccess();
        }catch(OAuth2ProviderNotFoundException | UnknownLeagueProviderException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}
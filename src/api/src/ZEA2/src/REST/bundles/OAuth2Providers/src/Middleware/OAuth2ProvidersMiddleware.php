<?php
namespace ZEA2\REST\Bundles\OAuth2Providers\Middleware;

use ZEA2\REST\Bundles\OAuth2Providers\Middleware\Command\CreateOAuth2ProviderCommand;
use ZEA2\REST\Bundles\OAuth2Providers\Middleware\Command\DeleteOAuth2ProviderCommand;
use ZEA2\REST\Bundles\OAuth2Providers\Middleware\Command\GetAllHandlersCommand;
use ZEA2\REST\Bundles\OAuth2Providers\Middleware\Command\GetAllProvidersCommand;
use ZEA2\REST\Bundles\OAuth2Providers\Middleware\Command\GetProviderCommand;
use ZEA2\REST\Bundles\OAuth2Providers\Middleware\Command\SetConfigCommand;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;
use Zend\Stratigility\MiddlewareInterface;
use ZEA2\REST\Command\Service\CommandService;

final class OAuth2ProvidersMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new JSONResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('create', CreateOAuth2ProviderCommand::class)
            ->attachDirect('delete', DeleteOAuth2ProviderCommand::class)
            ->attachDirect('handlers', GetAllHandlersCommand::class)
            ->attachDirect('providers', GetAllProvidersCommand::class)
            ->attachDirect('get-config', GetProviderCommand::class)
            ->attachDirect('set-config', SetConfigCommand::class)
            ->resolve($request);

        return $resolver->__invoke($request, $responseBuilder);
    }
}
<?php
namespace ZEA2\REST\Bundles\OAuth2Providers;

use ZEA2\REST\Bundle\RESTBundle;

final class OAuth2ProvidersRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}
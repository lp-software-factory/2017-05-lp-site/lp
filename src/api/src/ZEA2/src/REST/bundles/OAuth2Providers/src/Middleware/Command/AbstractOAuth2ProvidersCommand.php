<?php
namespace ZEA2\REST\Bundles\OAuth2Providers\Middleware\Command;

use ZEA2\Domain\Bundles\OAuth2\Config\ListAvailableLeagueProviders;
use ZEA2\Domain\Bundles\OAuth2\Service\OAuth2ProviderService;
use ZEA2\REST\Bundles\Access\Service\AccessService;
use ZEA2\REST\Command\Command;

abstract class AbstractOAuth2ProvidersCommand implements Command
{
    /** @var AccessService */
    protected $accessService;

    /** @var ListAvailableLeagueProviders */
    protected $listAvailableAdapter;

    /** @var OAuth2ProviderService */
    protected $oauth2ProviderService;

    public function __construct(
        AccessService $accessService,
        ListAvailableLeagueProviders $listAvailableAdapter,
        OAuth2ProviderService $oauth2ProviderService
    ) {
        $this->accessService = $accessService;
        $this->listAvailableAdapter = $listAvailableAdapter;
        $this->oauth2ProviderService = $oauth2ProviderService;
    }
}
<?php
namespace ZEA2\REST\Bundles\OAuth2Providers;

use ZEA2\REST\Bundles\OAuth2Providers\Middleware\OAuth2ProvidersMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'put',
                'path' => '/oauth2-providers/{command:create}[/]',
                'middleware' => OAuth2ProvidersMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/oauth2-providers/get-all/{command:providers}[/]',
                'middleware' => OAuth2ProvidersMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/oauth2-providers/get-all/{command:handlers}[/]',
                'middleware' => OAuth2ProvidersMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/oauth2-providers/{command:get-config}/{code}[/]',
                'middleware' => OAuth2ProvidersMiddleware::class,
            ],
            [
                'method' => 'delete',
                'path' => '/oauth2-providers/{command:delete}/{code}[/]',
                'middleware' => OAuth2ProvidersMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/oauth2-providers/{command:set-config}/{code}[/]',
                'middleware' => OAuth2ProvidersMiddleware::class,
            ],

        ],
    ],
];
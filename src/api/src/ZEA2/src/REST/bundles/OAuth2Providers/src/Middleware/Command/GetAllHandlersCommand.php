<?php
namespace ZEA2\REST\Bundles\OAuth2Providers\Middleware\Command;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;

final class GetAllHandlersCommand extends AbstractOAuth2ProvidersCommand
{
    public function __invoke(ServerRequestInterface $request, JSONResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        $responseBuilder
            ->setJSON([
                'handlers' => $this->listAvailableAdapter->getAvailable(),
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}
<?php
namespace ZEA2\REST\Bundles\OAuth2Providers\Tests\Definition;

use ZEA2\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;

trait OAuth2ProvidersRESTDefinitionTrait
{
    abstract function request(string $method, string $uri, array $queryParams = null): RESTRequest;

    protected function requestOAuth2ProvidersCreate(array $json): RESTRequest
    {
        return $this->request('put', '/oauth2-providers/create')
            ->setParameters($json);
    }

    protected function requestOAuth2ProvidersDelete(string $code): RESTRequest
    {
        return $this->request('delete', sprintf('/oauth2-providers/delete/%s', $code));
    }

    protected function requestOAuth2ProvidersGetConfig(string $code): RESTRequest
    {
        return $this->request('get', sprintf('/oauth2-providers/get-config/%s', $code));
    }

    protected function requestOAuth2ProvidersSetConfig(string $code, array $json): RESTRequest
    {
        return $this->request('post', sprintf('/oauth2-providers/set-config/%s', $code))
            ->setParameters($json);
    }

    protected function requestOAuth2ProvidersAllHandlers(): RESTRequest
    {
        return $this->request('get', '/oauth2-providers/get-all/handlers');
    }

    protected function requestOAuth2ProvidersAllProviders(): RESTRequest
    {
        return $this->request('get', '/oauth2-providers/get-all/providers');
    }
}
<?php
namespace ZEA2\REST\Bundles\OAuth2Providers\Request;

use ZEA2\Domain\Bundles\OAuth2\Parameters\SetOAuth2ConfigParameters;
use ZEA2\Platform\Bundles\APIDocs\Schema\JSONSchema;
use ZEA2\Platform\Bundles\APIDocs\Schema\SchemaRequest;

final class SetOAth2ProviderConfigRequest extends SchemaRequest
{
    public function getParameters(): SetOAuth2ConfigParameters
    {
        return new SetOAuth2ConfigParameters($this->getData()['config']);
    }

    protected function getSchema(): JSONSchema
    {
        return SchemaRequest::getSchemaService()->getDefinition('ZEA2_OAuth2ProvidersBundle_SetConfigRequest');
    }
}
<?php
namespace ZEA2\REST\Bundles\OAuth2Providers\Middleware\Command;

use ZEA2\Domain\Bundles\OAuth2\Exceptions\DuplicateOAuth2ProviderException;
use ZEA2\REST\Bundles\OAuth2Providers\Request\CreateOAuth2ProviderRequest;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class CreateOAuth2ProviderCommand extends AbstractOAuth2ProvidersCommand
{
    public function __invoke(ServerRequestInterface $request, JSONResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $parameters = (new CreateOAuth2ProviderRequest($request))->getParameters();

            $oauth2Provider = $this->oauth2ProviderService->createOAuth2Provider($parameters);

            $responseBuilder
                ->setJSON([
                    'provider' => $oauth2Provider->toJSON(),
                ])
                ->setStatusSuccess();
        }catch(DuplicateOAuth2ProviderException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusConflict();
        }

        return $responseBuilder->build();
    }
}
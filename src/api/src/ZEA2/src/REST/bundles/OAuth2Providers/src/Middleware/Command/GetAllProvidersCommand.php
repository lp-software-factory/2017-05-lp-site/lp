<?php
namespace ZEA2\REST\Bundles\OAuth2Providers\Middleware\Command;

use ZEA2\Domain\Bundles\OAuth2\Entity\OAuth2Provider;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;

final class GetAllProvidersCommand extends AbstractOAuth2ProvidersCommand
{
    public function __invoke(ServerRequestInterface $request, JSONResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        $responseBuilder
            ->setJSON([
                'providers' => array_map(function(OAuth2Provider $provider) {
                    return $provider->toJSON();
                }, $this->oauth2ProviderService->getAllProviders()),
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}
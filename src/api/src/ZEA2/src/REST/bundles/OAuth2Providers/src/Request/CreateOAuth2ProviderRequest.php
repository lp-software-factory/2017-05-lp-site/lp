<?php
namespace ZEA2\REST\Bundles\OAuth2Providers\Request;

use ZEA2\Domain\Bundles\OAuth2\Parameters\CreateOAuth2ProviderParameters;
use ZEA2\Platform\Bundles\APIDocs\Schema\JSONSchema;
use ZEA2\Platform\Bundles\APIDocs\Schema\SchemaRequest;

final class CreateOAuth2ProviderRequest extends SchemaRequest
{
    public function getParameters(): CreateOAuth2ProviderParameters
    {
        $data = $this->getData();

        return new CreateOAuth2ProviderParameters(
            $data['code'],
            $data['handler'],
            $data['config']
        );
    }

    protected function getSchema(): JSONSchema
    {
        return SchemaRequest::getSchemaService()->getDefinition('ZEA2_OAuth2ProvidersBundle_CreateOAuth2ProviderRequest');
    }
}
<?php
namespace ZEA2\REST\Bundles\OAuth2Providers\Middleware\Command;

use ZEA2\Domain\Bundles\OAuth2\Exceptions\OAuth2ProviderNotFoundException;
use ZEA2\Domain\Bundles\OAuth2\Exceptions\UnknownLeagueProviderException;
use ZEA2\REST\Bundles\OAuth2Providers\Request\SetOAth2ProviderConfigRequest;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class SetConfigCommand extends AbstractOAuth2ProvidersCommand
{
    public function __invoke(ServerRequestInterface $request, JSONResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $code = $request->getAttribute('code');
            $config = (new SetOAth2ProviderConfigRequest($request))->getParameters();

            $provider =  $this->oauth2ProviderService->setConfigFor($code, $config);

            $responseBuilder
                ->setJSON([
                    'provider' => $provider->toJSON()]
                )
                ->setStatusSuccess();
        }catch(OAuth2ProviderNotFoundException | UnknownLeagueProviderException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}
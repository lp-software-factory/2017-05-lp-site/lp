<?php
namespace ZEA2\REST\Bundles\OAuth2Providers\Tests\Paths;

use ZEA2\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use ZEA2\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use ZEA2\REST\Bundles\OAuth2Providers\Tests\OAuth2ProvidersMiddlewareTest;
use League\OAuth2\Client\Provider\Google;

final class CreateOAuth2ProviderMiddlewareTest extends OAuth2ProvidersMiddlewareTest
{
    public function test403()
    {
        $json = [
            'code' => 'google',
            'handler' => Google::class,
            'config' => [
                'clientId' => 'CLIENT_ID',
                'clientSecret' => 'CLIENT_SECRET',
                'redirectUri' => 'http://localhost/redirect/google'
            ]
        ];

        $this->requestOAuth2ProvidersCreate($json)
            ->__invoke()
            ->expectAuthError();

        $this->requestOAuth2ProvidersCreate($json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test200()
    {
        $json = [
            'code' => 'google',
            'handler' => Google::class,
            'config' => [
                'clientId' => 'CLIENT_ID',
                'clientSecret' => 'CLIENT_SECRET',
                'redirectUri' => 'http://localhost/redirect/google',
                'foo' => 'bar'
            ]
        ];

        $this->requestOAuth2ProvidersCreate($json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'provider' => [
                    'id' => $this->expectId(),
                    'code' => $json['code'],
                    'handler' => $json['handler'],
                    'config' => $json['config']
                ]
            ])
        ;
    }
}
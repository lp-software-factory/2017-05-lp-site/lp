<?php
namespace ZEA2\REST\Bundles\OAuth2Providers\Tests\Paths;

use ZEA2\Domain\Util\GenerateRandomString;
use ZEA2\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use ZEA2\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use ZEA2\REST\Bundles\OAuth2Providers\Tests\Fixture\OAuth2ProviderFixture;
use ZEA2\REST\Bundles\OAuth2Providers\Tests\OAuth2ProvidersMiddlewareTest;

final class DeleteOAuth2ProvidersMiddlewareTest extends OAuth2ProvidersMiddlewareTest
{
    public function test403()
    {
        $this->upFixture(new OAuth2ProviderFixture());

        $providerCode = OAuth2ProviderFixture::$PROVIDER_GOOGLE->getCode();

        $this->requestOAuth2ProvidersDelete($providerCode)
            ->__invoke()
            ->expectAuthError();

        $this->requestOAuth2ProvidersDelete($providerCode)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->requestOAuth2ProvidersDelete(GenerateRandomString::generate(8))
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test200()
    {
        $this->upFixture(new OAuth2ProviderFixture());

        $providerCode = OAuth2ProviderFixture::$PROVIDER_GOOGLE->getCode();

        $this->requestOAuth2ProvidersGetConfig($providerCode)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200);

        $this->requestOAuth2ProvidersDelete($providerCode)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true
            ]);

        $this->requestOAuth2ProvidersGetConfig($providerCode)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }

}
<?php
namespace ZEA2\REST\Bundles\Attachment\Tests\Definition;

use ZEA2\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;
use Zend\Diactoros\UploadedFile;

trait AttachmentRESTDefinitionTrait
{
    abstract function request(string $method, string $uri, array $queryParams = null): RESTRequest;

    protected function requestAttachmentUpload(UploadedFile $localFile): RESTRequest
    {
        return $this
            ->request('POST', '/attachment/upload')
            ->setUploadedFiles([
                'file' => $localFile,
            ]);
    }
}
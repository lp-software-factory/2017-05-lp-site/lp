<?php
namespace ZEA2\REST\Bundles\Attachment\Middleware;

use ZEA2\REST\Bundles\Attachment\Middleware\Command\LinkCommand;
use ZEA2\REST\Bundles\Attachment\Middleware\Command\UploadCommand;
use ZEA2\REST\Command\Service\CommandService;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;
use Zend\Stratigility\MiddlewareInterface;

class AttachmentMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new JSONResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('upload', UploadCommand::class)
            ->attachDirect('link', LinkCommand::class)
            ->resolve($request);

        return $resolver->__invoke($request, $responseBuilder);
    }
}
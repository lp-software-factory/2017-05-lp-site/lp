<?php
namespace ZEA2\REST\Bundles\Attachment\Tests;

use ZEA2\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use ZEA2\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use ZEA2\REST\Bundles\Locale\Tests\Fixture\LocaleFixture;
use ZEA2\REST\Bundles\PHPUnit\TestCase\ZEA2MiddlewareTestCase;
use Zend\Diactoros\UploadedFile;

/**
 * @backupGlobals disabled
 */
class AttachmentMiddlewareTest extends ZEA2MiddlewareTestCase
{
    protected function getFixtures(): array
    {
        return [
            new LocaleFixture(),
            new UserAccountFixture(),
            new AdminAccountFixture(),
        ];
    }

    public function testUpload403()
    {
        $localFileName = __DIR__ . '/Resources/grid-example.png';
        $uploadFile = new UploadedFile($localFileName, filesize($localFileName), 0);

        $this->requestAttachmentUpload($uploadFile)
            ->__invoke()
            ->expectAuthError();
    }

    public function testUpload200()
    {
        $localFileName = __DIR__ . '/Resources/grid-example.png';
        $uploadFile = new UploadedFile($localFileName, filesize($localFileName), 0);

        return $this->requestAttachmentUpload($uploadFile)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
            ])
            ->expectJSONBody([
                'entity' => [
                    'id' => $this->expectId(),
                    'sid' => $this->expectString(),
                    'date_created_at' => $this->expectString(),
                    'link' => [
                        'url' => $this->expectString(),
                        'resource' => $this->expectString(),
                        'source' => [
                            'source' => 'local',
                            'public_path' => $this->expectString(),
                            'storage_path' => $this->expectString(),
                        ],
                    ],
                    'is_attached' => false,
                ],
            ]);
    }
}
<?php
namespace ZEA2\REST\Bundles\Attachment\Exceptions;

final class FileNotUploadedException extends \Exception {}
<?php
namespace ZEA2\REST\Bundles\Attachment\Middleware\Command;

use ZEA2\Domain\Bundles\Attachment\Exception\InvalidURLException;
use ZEA2\Domain\Bundles\Attachment\Exception\NotFoundException;
use ZEA2\Domain\Bundles\Attachment\Source\ExternalSource;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;

final class LinkCommand extends Command
{
    public function __invoke(ServerRequestInterface $request, JSONResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireProtectedAccess();

        try {
            $url = $request->getQueryParams()['url'] ?? '';
            $source = new ExternalSource($url);

            if(strpos($url, 'http') === false) {
                $url = 'http://' . $url;
            }

            $result = $this->fetchResourceService->fetchResource($url);
            $entity = $this->attachmentService->linkAttachment($url, '', '', $result, $source);

            $responseBuilder
                ->setStatusSuccess()
                ->setJSON([
                    'entity' => $entity->toJSON(),
                ]);
        } catch(InvalidURLException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusBadRequest();
        } catch(NotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}
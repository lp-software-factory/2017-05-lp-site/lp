<?php
namespace ZEA2\REST\Bundles\Attachment\Middleware\Command;

use ZEA2\Domain\Bundles\Attachment\Exception\AttachmentFactoryException;
use ZEA2\Domain\Bundles\Attachment\Exception\FileTooBigException;
use ZEA2\Domain\Bundles\Attachment\Exception\FileTooSmallException;
use ZEA2\Domain\Util\GenerateRandomString;
use ZEA2\REST\Bundles\Attachment\Exceptions\FileNotUploadedException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;
use Zend\Diactoros\UploadedFile;

class UploadCommand extends Command
{
    const AUTO_GENERATE_FILE_NAME_LENGTH = 8;

    public function __invoke(ServerRequestInterface $request, JSONResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireProtectedAccess();

        try {
            /** @var UploadedFile $file */
            $file = $request->getUploadedFiles()["file"];
            $filename = $file->getClientFilename();

            if(! strlen($filename)) {
                $filename = GenerateRandomString::generate(self::AUTO_GENERATE_FILE_NAME_LENGTH);
            }

            if($file->getError() !== 0) {
                throw new FileNotUploadedException('Failed to upload file');
            }

            $entity = $this->attachmentService->uploadAttachment(
                $file->getStream()->getMetadata('uri'),
                $filename
            );

            $responseBuilder
                ->setStatusSuccess()
                ->setJSON([
                    'entity' => $entity->toJSON(),
                ]);
        } catch(FileTooBigException  $e) {
            $responseBuilder
                ->setStatus(409)
                ->setError($e);
        } catch(FileTooSmallException $e) {
            $responseBuilder
                ->setStatus(409)
                ->setError($e);
        } catch(AttachmentFactoryException $e) {
            $responseBuilder
                ->setStatusBadRequest()
                ->setError($e);
        }

        return $responseBuilder->build();
    }
}
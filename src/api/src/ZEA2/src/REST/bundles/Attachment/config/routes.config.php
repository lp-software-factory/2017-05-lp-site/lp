<?php
namespace ZEA2\REST\Bundles\Attachment;

use ZEA2\REST\Bundles\Attachment\Middleware\AttachmentMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'post',
                'path' => '/attachment/{command:upload}[/]',
                'middleware' => AttachmentMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/attachment/{command:link}[/]',
                'middleware' => AttachmentMiddleware::class,
            ],
        ],
    ],
];
<?php
namespace ZEA2\REST\Bundles\Attachment\Middleware\Command;

use ZEA2\Domain\Bundles\Attachment\Service\AttachmentService;
use ZEA2\Domain\Bundles\Attachment\Service\FetchResourceService;
use ZEA2\REST\Bundles\Access\Service\AccessService;

abstract class Command implements \ZEA2\REST\Command\Command
{
    /** @var AccessService */
    protected $accessService;

    /** @var AttachmentService */
    protected $attachmentService;

    /** @var FetchResourceService */
    protected $fetchResourceService;

    public function __construct(
        AccessService $accessService,
        AttachmentService $attachmentService,
        FetchResourceService $fetchResourceService
    ) {
        $this->accessService = $accessService;
        $this->attachmentService = $attachmentService;
        $this->fetchResourceService = $fetchResourceService;
    }
}
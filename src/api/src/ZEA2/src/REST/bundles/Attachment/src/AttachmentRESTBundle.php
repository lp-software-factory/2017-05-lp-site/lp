<?php
namespace ZEA2\REST\Bundles\Attachment;

use ZEA2\REST\Bundle\RESTBundle;

final class AttachmentRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}
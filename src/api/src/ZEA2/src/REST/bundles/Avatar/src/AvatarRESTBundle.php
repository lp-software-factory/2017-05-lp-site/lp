<?php
namespace ZEA2\REST\Bundles\Avatar;

use ZEA2\REST\Bundle\RESTBundle;

final class AvatarRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}
<?php
namespace ZEA2\REST\Bundle;

use ZEA2\Platform\Bundles\APIDocs\Bundle\ZEA2APIDocsBundle;
use ZEA2\Platform\Bundles\ZEA2Bundle;

abstract class RESTBundle extends ZEA2Bundle implements ZEA2APIDocsBundle
{
    public function hasAPIDocs(): bool
    {
        return is_dir(sprintf('%s/../api-docs', $this->getDir()));
    }

    public function getAPIDocsDir(): string
    {
        return sprintf('%s/../api-docs', $this->getDir());
    }
}
<?php
namespace ZEA2\REST;

use ZEA2\REST\Bundle\RESTBundle;
use ZEA2\REST\Bundles\Access\AccessRESTBundle;
use ZEA2\REST\Bundles\Account\AccountRESTBundle;
use ZEA2\REST\Bundles\Attachment\AttachmentRESTBundle;
use ZEA2\REST\Bundles\Auth\AuthRESTBundle;
use ZEA2\REST\Bundles\Avatar\AvatarRESTBundle;
use ZEA2\REST\Bundles\Colors\ColorsRESTBundle;
use ZEA2\REST\Bundles\Locale\LocaleRESTBundle;
use ZEA2\REST\Bundles\OAuth2Providers\OAuth2ProvidersRESTBundle;
use ZEA2\REST\Bundles\Palette\PaletteRESTBundle;
use ZEA2\REST\Bundles\PHPUnit\PHPUnitRESTBundle;
use ZEA2\REST\Bundles\Profile\ProfileRESTBundle;
use ZEA2\REST\Bundles\Register\RegisterRESTBundle;
use ZEA2\REST\Bundles\Translation\TranslationRESTBundle;
use ZEA2\REST\Bundles\Version\VersionRESTBundle;
use ZEA2\Platform\Init\InitScriptInjectableBundle;
use ZEA2\Platform\Init\InitScripts\ErrorHandlerPipeInitScript;
use ZEA2\Platform\Init\InitScripts\EventsInitScript;
use ZEA2\Platform\Init\InitScripts\InjectSchemaServiceInitScript;
use ZEA2\Platform\Init\InitScripts\NotFoundPipeInitScript;
use ZEA2\Platform\Init\InitScripts\PipeMiddlewaresInitScript;
use ZEA2\Platform\Init\InitScripts\RoutesInitScript;

final class ZEA2RESTBundle extends RESTBundle implements InitScriptInjectableBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getBundles(): array
    {
        $bundles = [
            new AccessRESTBundle(),
            new AccountRESTBundle(),
            new AttachmentRESTBundle(),
            new AuthRESTBundle(),
            new AvatarRESTBundle(),
            new ColorsRESTBundle(),
            new LocaleRESTBundle(),
            new OAuth2ProvidersRESTBundle(),
            new PHPUnitRESTBundle(),
            new ProfileRESTBundle(),
            new PaletteRESTBundle(),
            new VersionRESTBundle(),
            new TranslationRESTBundle(),
            new RegisterRESTBundle(),
        ];

        foreach($bundles as $bundle) {
            if(! ($bundle instanceof RESTBundle)) {
                throw new \Exception(sprintf('Invalid bundle "%s", should be extended from `%s`', get_class($bundle), RESTBundle::class));
            }
        }

        return $bundles;
    }

    public function getInitScripts(): array
    {
        return [
            ErrorHandlerPipeInitScript::class,
            RoutesInitScript::class,
            InjectSchemaServiceInitScript::class,
            PipeMiddlewaresInitScript::class,
            NotFoundPipeInitScript::class,
            EventsInitScript::class,
        ];
    }
}
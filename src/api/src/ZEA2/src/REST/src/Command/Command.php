<?php
namespace ZEA2\REST\Command;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;

interface Command
{
    public function __invoke(ServerRequestInterface $request, JSONResponseBuilder $responseBuilder): ResponseInterface;
}
<?php
namespace ZEA2\REST\Exceptions;

final class FileNotUploadedException extends \Exception {}
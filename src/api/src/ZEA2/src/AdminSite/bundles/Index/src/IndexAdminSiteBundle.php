<?php
namespace ZEA2\AdminSite\Bundles\Index;

use ZEA2\Platform\Bundles\ZEA2Bundle;

final class IndexAdminSiteBundle extends ZEA2Bundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}
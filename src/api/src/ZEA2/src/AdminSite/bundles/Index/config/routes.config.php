<?php
namespace ZEA2\AdminSite\Bundles\Index;

use ZEA2\AdminSite\Bundles\Index\Middleware\IndexMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'path' => '/',
                'method' => 'pipe',
                'group' => 'spa',
                'middleware' => IndexMiddleware::class,
            ],
        ]
    ]
];
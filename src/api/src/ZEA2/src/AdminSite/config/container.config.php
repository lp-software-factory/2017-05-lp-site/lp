<?php
namespace ZEA2\AppSite;

use function DI\get;
use function DI\object;

use ZEA2\Domain\Bundles\Mail\Config\MailConfig;
use ZEA2\Platform\Bundles\APIDocs\Generator\Impl\BundlesAPIDocsGenerator;
use ZEA2\Platform\Bundles\APIDocs\Service\APIDocsService;

return [
    APIDocsService::class => object()->constructorParameter('generator', get(BundlesAPIDocsGenerator::class)),
    MailConfig::class => object()->constructorParameter('config', get('config.zea2-admin-site.rest.mail')),
];
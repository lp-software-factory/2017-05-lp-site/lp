<?php
namespace ZEA2\Domain\Bundles\Mail;

return [
    'config.zea2-admin-site.rest.mail' => [
        'dir' => __DIR__.'/../../../../frontend/src/mail',
        'host' => 'example.com',
        'no-reply' => [
            'from' => 'no-reply@example.com',
            'name' => 'ZEA2 No Reply',
        ]
    ]
];
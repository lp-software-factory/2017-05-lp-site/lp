<?php
namespace ZEA2\AdminSite;

use ZEA2\Domain\Bundles\Auth\InitScripts\SessionJWTInitScript;
use ZEA2\Platform\Bundles\ZEA2Bundle;
use ZEA2\Platform\Init\InitScriptInjectableBundle;
use ZEA2\Platform\Init\InitScripts\ErrorHandlerPipeInitScript;
use ZEA2\Platform\Init\InitScripts\EventsInitScript;
use ZEA2\Platform\Init\InitScripts\InjectSchemaServiceInitScript;
use ZEA2\Platform\Init\InitScripts\NotFoundPipeInitScript;
use ZEA2\Platform\Init\InitScripts\PipeMiddlewaresInitScript;
use ZEA2\Platform\Init\InitScripts\RoutesInitScript;
use ZEA2\AdminSite\Bundles\Index\IndexAdminSiteBundle;

final class ZEA2AdminSiteBundle extends ZEA2Bundle implements InitScriptInjectableBundle
{
    public function getBundles(): array
    {
        return [
            new IndexAdminSiteBundle(),
        ];
    }

    public function getDir(): string
    {
        return __DIR__;
    }

    public function getInitScripts(): array
    {
        return [
            ErrorHandlerPipeInitScript::class,
            SessionJWTInitScript::class,
            RoutesInitScript::class,
            InjectSchemaServiceInitScript::class,
            PipeMiddlewaresInitScript::class,
            NotFoundPipeInitScript::class,
            EventsInitScript::class,
        ];
    }
}
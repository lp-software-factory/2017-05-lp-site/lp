<?php
namespace Lp;

use Lp\AppSite\LpAppSiteBundle;
use Lp\Domain\LpDomainBundle;
use ZEA2\Domain\ZEA2DomainBundle;
use ZEA2\Platform\ZEA2PlatformBundle;

$appBuilder = require __DIR__.'/../../../../../ZEA2/src/Platform/resources/bootstrap/bootstrap.php';

return $appBuilder([
    'bundles' => [
        new ZEA2PlatformBundle(),
        new ZEA2DomainBundle(),
        new LpDomainBundle(),
        new LpAppSiteBundle(),
    ]
]);
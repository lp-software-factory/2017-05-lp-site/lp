<?php
namespace Lp\AppSite\Bundles\Index\Middleware;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use ZEA2\Domain\Bundles\Twig\Service\TwigHelper;
use Zend\Stratigility\MiddlewareInterface;

final class IndexMiddleware implements MiddlewareInterface
{
    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $uri = $request->getUri()->getPath();

        if(in_array($uri, ['/frontend/app', '/frontend/app/'])) {
            return $out($request, $response);
        }else{
            return TwigHelper::getInstance()->htmlResponse($response, '@app/views/index/index/index.html.twig');
        }
    }
}
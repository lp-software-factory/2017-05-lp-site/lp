<?php
namespace Lp\AppSite\Bundles\Index;

use Lp\AppSite\Bundles\Index\Middleware\IndexMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'path' => '/',
                'method' => 'pipe',
                'group' => 'spa',
                'middleware' => IndexMiddleware::class,
            ],
        ]
    ]
];
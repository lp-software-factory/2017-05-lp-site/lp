<?php
namespace Lp\AppSite\Bundles\Index;

use ZEA2\Platform\Bundles\ZEA2Bundle;

final class IndexAppSiteBundle extends ZEA2Bundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}
<?php
namespace Lp\AppSite\Bundles\Frontend\FrontendScripts;

use ZEA2\Domain\Bundles\Locale\Service\SessionLocaleService;
use ZEA2\Domain\Bundles\Translation\Formatter\TranslationFormatter;
use ZEA2\Domain\Bundles\Translation\Repository\TranslationProjectRepository;
use ZEA2\Domain\Bundles\Translation\Service\TranslationService;
use ZEA2\Platform\Bundles\Frontend\Markers\FrontendBundleInjectable\FrontendScript;
use ZEA2\Platform\Bundles\Frontend\Service\FrontendService;

final class TranslationFrontendScript implements FrontendScript
{
    /** @var FrontendService */
    private $frontendService;

    /** @var TranslationService */
    private $translationService;

    /** @var TranslationFormatter */
    private $translationFormatter;

    /** @var SessionLocaleService */
    private $sessionLocaleService;

    public function __construct(
        FrontendService $frontendService,
        TranslationService $translationService,
        TranslationFormatter $translationFormatter,
        SessionLocaleService $sessionLocaleService
    ) {
        $this->frontendService = $frontendService;
        $this->translationService = $translationService;
        $this->translationFormatter = $translationFormatter;
        $this->sessionLocaleService = $sessionLocaleService;
    }

    public function __invoke(): array
    {
        $region = $this->sessionLocaleService->getCurrentRegion();
        $resources = $this->translationService->listTranslationsByRegion(TranslationProjectRepository::PROJECT_ID_ZEA2_FRONTEND, $region);

        return [
            'translations' => [
                'region' => $region,
                'fallback' => 'en_US',
                'resources' => $this->translationFormatter->formatManyRegion($resources)
            ]
        ];
    }
}
<?php
namespace Lp\AppSite\Bundles\Frontend\Middleware;

use Lp\REST\Bundles\Frontend\Middleware\AbstractFrontendMiddleware;

final class AppFrontendMiddleware extends AbstractFrontendMiddleware {}
<?php
namespace Lp\AppSite\Bundles\Frontend\FrontendScripts;

use Lp\Domain\Config\LpCompanyContactsConfig;
use Lp\Domain\Config\LpCompanyLegalConfig;
use ZEA2\Platform\Bundles\Frontend\Markers\FrontendBundleInjectable\FrontendScript;

final class ContactsFrontendScript implements FrontendScript
{
    /** @var LpCompanyLegalConfig */
    private $legal;

    /** @var LpCompanyContactsConfig */
    private $contacts;

    public function __construct(
        LpCompanyLegalConfig $legal,
        LpCompanyContactsConfig $contacts
    ) {
        $this->legal = $legal;
        $this->contacts = $contacts;
    }

    public function __invoke(): array
    {
        return [
            'lp' => [
                'contacts' => $this->contacts->toJSON(),
                'legal' => $this->legal->toJSON(),
            ]
        ];
    }
}
<?php
namespace Lp\AppSite\Bundles\Frontend;

use Lp\AppSite\Bundles\Frontend\FrontendScripts\ContactsFrontendScript;
use Lp\AppSite\Bundles\Frontend\FrontendScripts\TranslationFrontendScript;
use ZEA2\Platform\Bundles\Frontend\Markers\FrontendBundleInjectable\FrontendBundleInjectable;
use ZEA2\Platform\Bundles\ZEA2Bundle;

final class FrontendAppSiteBundle extends ZEA2Bundle implements FrontendBundleInjectable
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getFrontendScripts(): array
    {
        return [
            TranslationFrontendScript::class,
            ContactsFrontendScript::class,
        ];
    }
}
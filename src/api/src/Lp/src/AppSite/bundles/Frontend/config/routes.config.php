<?php
namespace Lp\AppSite\Bundles\Frontend;

use Lp\AppSite\Bundles\Frontend\Middleware\AppFrontendMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'get',
                'path' => '/frontend/app[/]',
                'middleware' => AppFrontendMiddleware::class,
                'group' => 'default',
            ],
        ]
    ]
];
<?php
namespace Lp\AppSite;

use Lp\AppSite\Bundles\Index\IndexAppSiteBundle;
use Lp\AppSite\Bundles\Frontend\FrontendAppSiteBundle;
use ZEA2\Platform\Bundles\ZEA2Bundle;
use ZEA2\Platform\Init\InitScriptInjectableBundle;
use ZEA2\Platform\Init\InitScripts\ErrorHandlerPipeInitScript;
use ZEA2\Platform\Init\InitScripts\EventsInitScript;
use ZEA2\Platform\Init\InitScripts\InjectSchemaServiceInitScript;
use ZEA2\Platform\Init\InitScripts\NotFoundPipeInitScript;
use ZEA2\Platform\Init\InitScripts\PipeMiddlewaresInitScript;
use ZEA2\Platform\Init\InitScripts\RoutesInitScript;

final class LpAppSiteBundle extends ZEA2Bundle implements InitScriptInjectableBundle
{
    public function getBundles(): array
    {
        return [
            new IndexAppSiteBundle(),
            new FrontendAppSiteBundle(),
        ];
    }

    public function getDir(): string
    {
        return __DIR__;
    }

    public function getInitScripts(): array
    {
        return [
            ErrorHandlerPipeInitScript::class,
            RoutesInitScript::class,
            InjectSchemaServiceInitScript::class,
            PipeMiddlewaresInitScript::class,
            NotFoundPipeInitScript::class,
            EventsInitScript::class,
        ];
    }
}
<?php
namespace Lp\Domain\Bundles\Mail;

return [
    'config.lp.rest.mail' => [
        'dir' => __DIR__.'/../../../../frontend/src/mail',
        'host' => 'example.com',
        'no-reply' => [
            'from' => 'no-reply@example.com',
            'name' => 'Lp No Reply',
        ]
    ]
];
<?php
namespace Lp\Domain;

use Lp\Domain\Bundles\Feedback\FeedbackDomainBundle;
use ZEA2\Platform\Bundles\ZEA2Bundle;
use ZEA2\Platform\Init\InitScriptInjectableBundle;

final class LpDomainBundle extends ZEA2Bundle implements InitScriptInjectableBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getBundles(): array
    {
        return [
            new FeedbackDomainBundle(),
        ];
    }

    public function getInitScripts(): array
    {
        return [];
    }
}
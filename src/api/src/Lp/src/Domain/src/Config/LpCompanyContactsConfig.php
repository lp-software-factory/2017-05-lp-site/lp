<?php
namespace Lp\Domain\Config;

use ZEA2\Domain\Bundles\Register\Service\Register\RegisterService;
use ZEA2\Domain\Markers\JSONSerializable;

final class LpCompanyContactsConfig implements JSONSerializable
{
    /** @var string */
    private $emailMain;

    /** @var string[] */
    private $emailAdditional = [];

    /** @var string */
    private $phoneMain;

    /** @var string[] */
    private $phoneAdditional;

    /** @var string */
    private $skype;

    /** @var string */
    private $map;

    public function __construct(RegisterService $registerService)
    {
        $registerService->with([
            'lp.common.contacts.email.main' => function(string $value) {
                $this->emailMain = $value;
            },
            'lp.common.contacts.email.additional' => function(string $value) {
                $this->emailAdditional = json_decode($value, true);
            },
            'lp.common.contacts.phone.main' => function(string $value) {
                $this->phoneMain = $value;
            },
            'lp.common.contacts.phone.additional' => function(string $value) {
                $this->phoneAdditional = json_decode($value, true);
            },
            'lp.common.contacts.skype' => function(string $value) {
                $this->skype = $value;
            },
            'lp.common.contacts.map' => function(string $value) {
                $this->map = $value;
            },
        ]);
    }

    public function toJSON(array $options = []): array
    {
        return [
            'email'=> [
                'main' => $this->emailMain,
                'additional' => $this->emailAdditional,
            ],
            'phone' => [
                'main' => $this->phoneMain,
                'additional' => $this->phoneAdditional,
            ],
            'skype' => $this->skype,
            'map' => $this->map,
        ];
    }

    public function getEmailMain(): string
    {
        return $this->emailMain;
    }

    public function getEmailAdditional(): array
    {
        return $this->emailAdditional;
    }

    public function getPhoneMain(): string
    {
        return $this->phoneMain;
    }

    public function getPhoneAdditional(): array
    {
        return $this->phoneAdditional;
    }

    public function getSkype(): string
    {
        return $this->skype;
    }

    public function getMap(): string
    {
        return $this->map;
    }
}
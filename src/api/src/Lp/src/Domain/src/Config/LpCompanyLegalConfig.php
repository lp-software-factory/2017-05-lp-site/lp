<?php
namespace Lp\Domain\Config;

use ZEA2\Domain\Bundles\Register\Service\Register\RegisterService;
use ZEA2\Domain\Markers\JSONSerializable;
use ZEA2\Platform\Bundles\DoctrineORM\Type\LocalizedString\Value\ImmutableLocalizedString;

final class LpCompanyLegalConfig implements JSONSerializable
{
    /** @var ImmutableLocalizedString */
    private $name;

    /** @var string */
    private $inn;

    /** @var string */
    private $kpp;

    /** @var ImmutableLocalizedString */
    private $legalAddress;

    /** @var ImmutableLocalizedString */
    private $realAddress;

    /** @var string */
    private $bik;

    /** @var ImmutableLocalizedString */
    private $bank;

    /** @var string */
    private $account;

    /** @var string */
    private $accountBank;

    public function __construct(RegisterService $register)
    {
        $register->with([
            'lp.common.legal.name' => function(string $value) {
                $this->name = new ImmutableLocalizedString(json_decode($value, true));
            },
            'lp.common.legal.inn' => function(string $value) {
                $this->inn = $value;
            },
            'lp.common.legal.kpp' => function(string $value) {
                $this->kpp = $value;
            },
            'lp.common.legal.legal_address' => function(string $value) {
                $this->legalAddress = new ImmutableLocalizedString(json_decode($value, true));;
            },
            'lp.common.legal.real_address' => function(string $value) {
                $this->realAddress = new ImmutableLocalizedString(json_decode($value, true));;
            },
            'lp.common.legal.bik' => function(string $value) {
                $this->bik = $value;
            },
            'lp.common.legal.bank' => function(string $value) {
                $this->bank = new ImmutableLocalizedString(json_decode($value, true));;
            },
            'lp.common.legal.account' => function(string $value) {
                $this->account = $value;
            },
            'lp.common.legal.account_bank' => function(string $value) {
                $this->accountBank = $value;
            },
        ]);
    }

    public function toJSON(array $options = []): array
    {
        return [
            'name' => $this->name->toJSON(),
            'inn' => $this->inn,
            'kpp' => $this->kpp,
            'legal_address' => $this->legalAddress->toJSON(),
            'real_address' => $this->realAddress->toJSON(),
            'bik' => $this->bik,
            'bank' => $this->bank->toJSON(),
            'account' => $this->account,
            'account_bank' => $this->accountBank,
        ];
    }

    public function getName(): ImmutableLocalizedString
    {
        return $this->name;
    }

    public function getInn(): string
    {
        return $this->inn;
    }

    public function getKpp(): string
    {
        return $this->kpp;
    }

    public function getLegalAddress(): ImmutableLocalizedString
    {
        return $this->legalAddress;
    }

    public function getRealAddress(): ImmutableLocalizedString
    {
        return $this->realAddress;
    }

    public function getBik(): string
    {
        return $this->bik;
    }

    public function getBank(): ImmutableLocalizedString
    {
        return $this->bank;
    }

    public function getAccount(): string
    {
        return $this->account;
    }

    public function getAccountBank(): string
    {
        return $this->accountBank;
    }
}
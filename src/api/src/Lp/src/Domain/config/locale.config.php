<?php
namespace Lp\Platform\Bundles\Locale;

return [
    'config.zea2.domain.locale' => [
        'fallback' => [
            ['language' => 'ru', 'region' => 'ru_RU'],
            ['language' => 'en', 'region' => 'en_US'],
            ['language' => 'en', 'region' => 'en_GB'],
        ],
        'default' => ['language' => 'ru', 'region' => 'ru_RU'],
        'session' => [
            'key' => 'config.zea2.platform.locale.session.key',
            'default' => 'ru_RU',
        ]
    ],
];
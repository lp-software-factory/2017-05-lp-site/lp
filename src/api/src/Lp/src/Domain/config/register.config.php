<?php
namespace Lp\Domain;

use ZEA2\Platform\Bundles\DoctrineORM\Type\LocalizedString\Value\ImmutableLocalizedString;

return [
    'zea2.stage.register.bootstrap' => [
        "lp.common" => [
            // Legal Info
            'legal.name' => json_encode((new ImmutableLocalizedString([
                [
                    'region' => 'ru_RU',
                    'value' => 'ООО "ЛП софтвейр"',
                ],
            ]))->toJSON()),
            'legal.inn' => '000000000000',
            'legal.kpp' => '000000000000',
            'legal.legal_address' => json_encode((new ImmutableLocalizedString([
                [
                    'region' => 'ru_RU',
                    'value' => 'Ивановская область, Иваново, Посадский переулок 4, офис 115',
                ],
            ]))->toJSON()),
            'legal.real_address' => json_encode((new ImmutableLocalizedString([
                [
                    'region' => 'ru_RU',
                    'value' => 'Ивановская область, Иваново, Посадский переулок 4, офис 115',
                ],
            ]))->toJSON()),
            'legal.bik' => '',
            'legal.bank' => json_encode((new ImmutableLocalizedString([
                [
                    'region' => 'ru_RU',
                    'value' => 'АО "Тинькофф Банк"',
                ],
            ]))->toJSON()),
            'legal.account' => '',
            'legal.account_bank' => '',

            // Contacts
            'contacts.email.main' => 'info@lpsoftwarefactory.com',
            'contacts.email.additional' => json_encode([]),
            'contacts.phone.main' => '+7 (495) 849-3220',
            'contacts.phone.additional' => json_encode([
                '+7 (958) 183-05-20',
                '+7 (958) 183-05-21'
            ]),
            'contacts.skype' => 'example.demo',
            'contacts.map' => 'https://www.google.com/maps/embed/v1/place?q=place_id:ChIJN--wODYUTUERPIseCilF4HQ&key=AIzaSyBSrwhKbHVbV1FPPBW02r9eqgSzqEHxqIY',
        ],
    ],
];
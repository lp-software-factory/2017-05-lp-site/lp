<?php
namespace Lp\Domain;

use ZEA2\Platform\Init\InitScripts\RoutesInitScript;

return [
    RoutesInitScript::class => \DI\object()->constructorParameter('groups', [
        'locale',
        'auth',
        'pipes',
        'default',
        'final',
        'spa',
    ])
];
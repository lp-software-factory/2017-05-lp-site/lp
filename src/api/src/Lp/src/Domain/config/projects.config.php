<?php
namespace Lp\Domain;

use ZEA2\Domain\Bundles\Translation\Repository\TranslationProjectRepository;

return [
    'config.zea2.domain.translation.projects' => [
        [
            'id' => TranslationProjectRepository::PROJECT_ID_TEST,
            'title' => 'Translation Test',
            'position' => -1,
            'sources' => [
                [
                    'type' => 'json',
                    'dirs' => [
                        __DIR__.'/../../../../ZEA2/src/REST/bundles/Translation/src/Tests/Resources/json',
                    ],
                ],
                [
                    'type' => 'php',
                    'dirs' => [
                        __DIR__.'/../../../../ZEA2/src/REST/bundles/Translation/src/Tests/Resources/php',
                    ],
                ],
            ],
        ],
        [
            'id' => TranslationProjectRepository::PROJECT_ID_ZEA2_FRONTEND,
            'title' => 'ZEA2 Frontend App',
            'position' => 1,
            'sources' => [
                [
                    'type' => 'json',
                    'dirs' => [
                        __DIR__.'/../../../../../../app/src/frontend/lp-app/src/translations'
                    ],
                ],
            ],
        ],
        [
            'id' => TranslationProjectRepository::PROJECT_ID_ZEA2_ADMIN,
            'title' => 'ZEA2 Admin App',
            'position' => 1,
            'sources' => [
                [
                    'type' => 'json',
                    'dirs' => [
                        __DIR__.'/../../../../../../admin/src/frontend/zea2-admin-app/src/module/zea2-common/translations',
                        __DIR__.'/../../../../../../admin/src/frontend/zea2-admin-app/src/module/zea2/translations',
                        __DIR__.'/../../../../../../admin/src/frontend/zea2-admin-app/src/module/lp/translations',
                    ],
                ],

            ],
        ],
    ]
];
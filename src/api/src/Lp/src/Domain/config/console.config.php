<?php
namespace Lp\Domain;

return [
    'config.zea2.platform.console' => [
        'title' => 'ZEA2 Console',
        'version' => '1.0.0',
        'commands' => [],
    ],
];
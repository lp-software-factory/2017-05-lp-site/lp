<?php
namespace Lp\Domain;

return [
    'config.zea2.domain.mail' => [
        'dir' => __DIR__.'/../../../../frontend/src/mail',
        'host' => 'example.com',
        'no-reply' => [
            'from' => 'no-reply@example.com',
            'name' => 'ZEA2 No Reply',
        ]
    ]
];
<?php
namespace Lp\Domain;

return [
    'config.zea2.domain.attachment' => [
        'dir' => '/var/vm-project/storage/attachment',
        'www' => '/storage/attachment',
    ],
];
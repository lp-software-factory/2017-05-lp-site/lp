<?php
namespace Lp\Domain\Bundles\Twig;

use Lp\Domain\Config\LpCompanyContactsConfig;
use Lp\Domain\Config\LpCompanyLegalConfig;
use ZEA2\Domain\Bundles\Auth\Inject\AuthToken;
use ZEA2\Domain\Bundles\Auth\Service\SessionJWTService;
use ZEA2\Domain\Bundles\Locale\Service\SessionLocaleService;
use ZEA2\Platform\Bundles\Frontend\Service\FrontendService;

return [
    'config.zea2.domain.twig' => [
        'exports' => [
            'AuthToken' => AuthToken::class,
            'SessionLocaleService' => SessionLocaleService::class,
            'SessionJWTService' => SessionJWTService::class,
            'FrontendService' => FrontendService::class,
            'LpCompanyLegalConfig' => LpCompanyLegalConfig::class,
            'LpCompanyContactsConfig' => LpCompanyContactsConfig::class,
        ],
        'paths' => [
            'admin' => __DIR__.'/../../../../../../admin/src/twig',
            'app' => __DIR__.'/../../../../../../app/src/twig',
            'mail' => __DIR__.'/../../../../../../mail',
        ],
        'options' => [
            'charset' => 'utf-8',
            'debug' => false,
            'auto_reload' => true,
            'strict_variables' => true,
            'autoescape' => true,
            'optimizations' => -1,
        ],
    ]
];
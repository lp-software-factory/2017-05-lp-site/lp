<?php
namespace Lp\Domain;

use function DI\get;

return [
    'config.storage.dir' => '/var/vm-project/storage',
    'config.storage.www' => '/storage',
    'config.storage' => [
        'dir' => get('config.storage.dir'),
        'www' => get('config.storage.www'),
    ],
];
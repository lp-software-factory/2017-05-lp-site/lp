<?php
namespace Lp\Platform\Bundles\MongoDB;

return [
    'config.zea2.platform.mongodb.connection' => [
        'db_prefix' => 'lp',
        'server' => 'mongodb://127.0.0.1:27017',
        'options' => [
            'connect' => true
        ],
        'driver_options' => []
    ],
];
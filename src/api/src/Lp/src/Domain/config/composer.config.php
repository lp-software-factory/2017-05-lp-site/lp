<?php
namespace Lp\Domain;

use function DI\factory;

use ZEA2\Domain\Factory\ComposerJSONFactory;

return [
    'config.zea2.platform.composer.json' => factory(ComposerJSONFactory::class),
];
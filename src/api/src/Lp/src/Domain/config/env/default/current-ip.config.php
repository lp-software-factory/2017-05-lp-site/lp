<?php
namespace Lp\Domain;

use ZEA2\Domain\Service\CurrentIPService\CurrentIPService;
use ZEA2\Domain\Service\CurrentIPService\CurrentIPServiceInterface;
use function DI\get;

return [
    CurrentIPServiceInterface::class => get(CurrentIPService::class),
];
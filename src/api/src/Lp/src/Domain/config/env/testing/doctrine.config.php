<?php
namespace Lp\Domain;

return [
    'config.zea2.platform.doctrine2orm' => [
        'is_dev_mode' => true,
        'connection_options'=> [
            'user' => 'zea2_pgsql',
            'password' => '1234',
            'host' => '127.0.0.1',
            'dbname' => 'zea2_testing',
            'driver'   => 'pdo_pgsql',
            'charset'  => 'utf8',
        ],
    ]
];
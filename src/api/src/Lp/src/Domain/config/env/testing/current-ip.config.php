<?php
namespace Lp\Domain;

use ZEA2\Domain\Service\CurrentIPService\CurrentIPServiceInterface;
use ZEA2\Domain\Service\CurrentIPService\MockCurrentIPService;
use function DI\get;

return [
    CurrentIPServiceInterface::class => get(MockCurrentIPService::class),
];
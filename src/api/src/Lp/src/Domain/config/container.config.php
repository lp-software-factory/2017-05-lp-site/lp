<?php
namespace Lp\Domain;

use function DI\get;
use function DI\object;

use ZEA2\Domain\Service\ServerConfig;
use ZEA2\Domain\Service\StorageConfig;

return [
    'config.zea2.domain.mail.dir' => __DIR__.'/../../../../../mail/api',
    ServerConfig::class => object()->constructorParameter('config', get('config.server')),
    StorageConfig::class => object()->constructorParameter('config', get('config.storage')),
];
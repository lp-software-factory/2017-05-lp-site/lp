<?php
namespace Lp\Domain\Bundles\Feedback\Repository;

use Doctrine\ORM\EntityRepository;
use Lp\Domain\Bundles\Feedback\Entity\Feedback;
use Lp\Domain\Bundles\Feedback\Exceptions\FeedbackNotFoundException;
use ZEA2\Domain\Criteria\IdsCriteria;
use ZEA2\Domain\Criteria\SeekCriteria;

final class FeedbackRepository extends EntityRepository
{
    public function createFeedback(Feedback $feedback): int
    {
        while($this->hasWithSID($feedback->getSID())) {
            $feedback->regenerateSID();
        }

        $this->getEntityManager()->persist($feedback);
        $this->getEntityManager()->flush($feedback);

        return $feedback->getId();
    }

    public function saveFeedback(Feedback $feedback)
    {
        $this->getEntityManager()->flush($feedback);
    }

    public function saveFeedbacks(array $feedbacks)
    {
        $this->getEntityManager()->flush($feedbacks);
    }

    public function deleteFeedback(Feedback $feedback)
    {
        $this->getEntityManager()->remove($feedback);
        $this->getEntityManager()->flush($feedback);
    }

    public function getById(int $id): Feedback
    {
        $result = $this->find($id);

        if($result instanceof Feedback) {
            return $result;
        }else{
            throw new FeedbackNotFoundException(sprintf('Feedback `ID(%d)` not found', $id));
        }
    }

    public function getAll(): array
    {
        return $this->findAll();
    }

    public function getBy(SeekCriteria $seek): array
    {
        return $this->findBy([], ['id' => 'desc'], $seek->getLimit(), $seek->getOffset());
    }

    public function getAllOfFeedbackCategory(int $feedbackCategoryId): array
    {
        return $this->findBy([
            'feedbackCategory' => $feedbackCategoryId,
        ]);
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        $ids = $idsCriteria->getIds();
        $results = $this->findBy(['id' => $ids]);

        if(count($ids) !== count($results)) {
            throw new FeedbackNotFoundException(sprintf(
                'Feedbacks with IDs (%s) not found',
                join(', ', array_diff($ids, array_map(function(Feedback $feedback) {
                    return $feedback->getId();
                }, $results)))
            ));
        }

        return $results;
    }

    public function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

    public function hasFeedbackWithId(string $id): bool
    {
        return $this->findOneBy(['id' => $id]) !== null;
    }

    public function getBySID(string $sid): Feedback
    {
        $result = $this->findBy(['sid' => $sid]);

        if($result instanceof Feedback) {
            return $result;
        }else{
            throw new FeedbackNotFoundException(sprintf('Feedback `SID(%s)` not found', $sid));
        }
    }
}
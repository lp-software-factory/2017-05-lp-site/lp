<?php
namespace Lp\Domain\Bundles\Feedback;

use Lp\Domain\Bundles\Feedback\Factory\Doctrine\FeedbackRepositoryDoctrineFactory;
use Lp\Domain\Bundles\Feedback\Repository\FeedbackRepository;

return [
    FeedbackRepository::class => \DI\factory(FeedbackRepositoryDoctrineFactory::class),
];
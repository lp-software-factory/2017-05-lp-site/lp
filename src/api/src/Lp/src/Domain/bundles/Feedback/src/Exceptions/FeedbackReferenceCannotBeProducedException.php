<?php
namespace Lp\Domain\Bundles\Feedback\Exceptions;

final class FeedbackReferenceCannotBeProducedException extends \Exception {}
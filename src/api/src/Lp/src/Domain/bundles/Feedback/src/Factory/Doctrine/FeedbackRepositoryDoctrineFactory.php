<?php
namespace Lp\Domain\Bundles\Feedback\Factory\Doctrine;

use Lp\Domain\Bundles\Feedback\Entity\Feedback;
use ZEA2\Platform\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;

final class FeedbackRepositoryDoctrineFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return Feedback::class;
    }
}
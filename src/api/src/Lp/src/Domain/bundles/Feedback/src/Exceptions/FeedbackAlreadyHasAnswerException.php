<?php
namespace Lp\Domain\Bundles\Feedback\Exceptions;

final class FeedbackAlreadyHasAnswerException extends \Exception {}
<?php
namespace ZEA2\Stage;

return [
    'zea2.stage.register.bootstrap' => [
        "lp.api" => [
            'feedback.email' => 'demo@example.com',
            'feedback.phone' => '+7 919 1044417',
            'feedback.name' => 'Lp',
        ],
    ],
];
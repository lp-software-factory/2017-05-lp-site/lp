<?php
namespace Lp\Domain\Bundles\Feedback\Parameters;

final class AnswerFeedbackParameters
{
    /** @var string */
    private $message;

    /** @var array[] */
    private $attachmentIds;

    public function __construct($message, array $attachmentIds)
    {
        $this->message = $message;
        $this->attachmentIds = $attachmentIds;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function getAttachmentIds(): array
    {
        return $this->attachmentIds;
    }
}
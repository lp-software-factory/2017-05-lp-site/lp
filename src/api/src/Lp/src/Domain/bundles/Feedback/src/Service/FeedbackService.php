<?php
namespace Lp\Domain\Bundles\Feedback\Service;

use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use Lp\Domain\Bundles\Feedback\Entity\Feedback;
use Lp\Domain\Bundles\Feedback\Exceptions\FeedbackAlreadyHasAnswerException;
use Lp\Domain\Bundles\Feedback\Parameters\AnswerFeedbackParameters;
use Lp\Domain\Bundles\Feedback\Parameters\CreateFeedbackParameters;
use Lp\Domain\Bundles\Feedback\Parameters\ListFeedbackParameters;
use Lp\Domain\Bundles\Feedback\Repository\FeedbackRepository;

final class FeedbackService
{
    public const EVENT_CREATED = 'lp.domain.feedback.created';
    public const EVENT_DELETE = 'lp.domain.feedback.delete';
    public const EVENT_DELETED = 'lp.domain.feedback.deleted';
    public const EVENT_READ = 'lp.domain.feedback.read';
    public const EVENT_ANSWER = 'lp.domain.feedback.answer';

    /** @var EventEmitterInterface */
    private $eventEmitter;

    /** @var FeedbackRepository */
    private $feedbackRepository;

    public function __construct(FeedbackRepository $feedbackRepository)
    {
        $this->eventEmitter = new EventEmitter();
        $this->feedbackRepository = $feedbackRepository;
    }

    public function getEventEmitter(): EventEmitterInterface
    {
        return $this->eventEmitter;
    }

    public function create(CreateFeedbackParameters $parameters): Feedback
    {
        $feedback = new Feedback(
            $parameters->getReference(),
            $parameters->getFromName(),
            $parameters->getFromEmail(),
            $parameters->getFromPhone(),
            $parameters->getMessage(),
            $parameters->getAttachmentIds()
        );

        $this->feedbackRepository->createFeedback($feedback);
        $this->eventEmitter->emit(self::EVENT_CREATED, [$feedback]);

        return $feedback;
    }

    public function markAsRead(int $feedbackId): Feedback
    {
        $feedback = $this->getById($feedbackId);
        $feedback->markAsRead();

        $this->feedbackRepository->saveFeedback($feedback);
        $this->eventEmitter->emit(self::EVENT_READ, [$feedback]);

        return $feedback;
    }

    public function answer(int $feedbackId, AnswerFeedbackParameters $parameters): Feedback
    {
        $feedback = $this->getById($feedbackId);

        if($feedback->hasAnswer()) {
            throw new FeedbackAlreadyHasAnswerException(sprintf('Feedback with ID `%d` already has an answer', $feedback->getId()));
        }

        $feedback->setAnswer(
            $parameters->getMessage(),
            $parameters->getAttachmentIds()
        );

        $this->feedbackRepository->saveFeedback($feedback);
        $this->eventEmitter->emit(self::EVENT_ANSWER, [$feedback]);

        return $feedback;
    }

    public function delete(int $feedbackId): void
    {
        $feedback = $this->getById($feedbackId);

        $this->eventEmitter->emit(self::EVENT_DELETE, [$feedback, $feedbackId]);
        $this->feedbackRepository->deleteFeedback($feedback);
        $this->eventEmitter->emit(self::EVENT_DELETED, [$feedback, $feedbackId]);
    }

    public function getById(int $feedbackId): Feedback
    {
        return $this->feedbackRepository->getById($feedbackId);
    }

    public function getAll(): array
    {
        return $this->feedbackRepository->getAll();
    }

    public function getBy(ListFeedbackParameters $parameters)
    {
        return $this->feedbackRepository->getBy($parameters->getSeek());
    }
}
<?php
namespace Lp\Domain\Bundles\Feedback\Config;

use ZEA2\Domain\Bundles\Register\Service\RegisterRecord\RegisterRecordService;

final class FeedbackMailServiceConfig
{
    /** @var string */
    private $fromName;

    /** @var string */
    private $fromEmail;

    public function __construct(RegisterRecordService $recordService)
    {
        $this->fromName = $recordService->getById('lp.api.feedback.name');
        $this->fromEmail = $recordService->getById('lp.api.feedback.email');
    }

    public function getFromName(): string
    {
        return $this->fromName;
    }

    public function getFromEmail(): string
    {
        return $this->fromEmail;
    }
}
<?php
namespace Lp\Domain\Bundles\Feedback\Parameters;

final class CreateFeedbackParameters
{
    /** @var string */
    private $reference;

    /** @var string */
    private $fromName;

    /** @var string */
    private $fromEmail;

    /** @var string */
    private $fromPhone;

    /** @var string */
    private $message;

    /** @var int[] */
    private $attachmentIds;

    public function __construct(
        string $reference,
        string $fromName,
        string $fromEmail,
        string $fromPhone,
        string $message,
        array $attachmentIds
    ) {
        $this->reference = $reference;
        $this->fromName = $fromName;
        $this->fromEmail = $fromEmail;
        $this->fromPhone = $fromPhone;
        $this->message = $message;
        $this->attachmentIds = $attachmentIds;
    }

    public function getReference(): string
    {
        return $this->reference;
    }

    public function getFromName(): string
    {
        return $this->fromName;
    }

    public function getFromEmail(): string
    {
        return $this->fromEmail;
    }

    public function getFromPhone(): string
    {
        return $this->fromPhone;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function getAttachmentIds(): array
    {
        return $this->attachmentIds;
    }
}
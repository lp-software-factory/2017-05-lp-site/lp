<?php
namespace Lp\Domain\Bundles\Feedback\Entity;

use ZEA2\Domain\Markers\IdEntity\IdEntity;
use ZEA2\Domain\Markers\IdEntity\IdEntityTrait;
use ZEA2\Domain\Markers\JSONMetadataEntity\JSONMetadataEntity;
use ZEA2\Domain\Markers\JSONMetadataEntity\JSONMetadataEntityTrait;
use ZEA2\Domain\Markers\JSONSerializable;
use ZEA2\Domain\Markers\ModificationEntity\ModificationEntity;
use ZEA2\Domain\Markers\ModificationEntity\ModificationEntityTrait;
use ZEA2\Domain\Markers\SIDEntity\SIDEntity;
use ZEA2\Domain\Markers\SIDEntity\SIDEntityTrait;
use ZEA2\Domain\Markers\VersionEntity\VersionEntity;
use ZEA2\Domain\Markers\VersionEntity\VersionEntityTrait;

/**
 * @Entity(repositoryClass="Lp\Domain\Bundles\Feedback\Repository\FeedbackRepository")
 * @Table(name="feedback")
 */
class Feedback implements JSONSerializable, IdEntity, SIDEntity, JSONMetadataEntity, ModificationEntity, VersionEntity
{
    public const FEEDBACK_REFERENCE_DEFAULT = 'global';

    public const LATEST_VERSION = "1.0.0";

    public const STATUS_MAIL_SENT = 0;
    public const STATUS_MAIL_WAITING = 1;
    public const STATUS_MAIL_FAIL = 2;

    use IdEntityTrait, SIDEntityTrait, JSONMetadataEntityTrait, ModificationEntityTrait, VersionEntityTrait;

    /**
     * @Column(name="reference", type="string")
     * @var string
     */
    private $reference = self::FEEDBACK_REFERENCE_DEFAULT;

    /**
     * @Column(name="is_read", type="boolean")
     * @var bool
     */
    private $isRead = false;

    /**
     * @Column(name="date_read", type="datetime")
     * @var \DateTime
     */
    private $dateRead;

    /**
     * @Column(name="from_mail_status", type="integer")
     * @var integer
     */
    private $fromMailStatus = self::STATUS_MAIL_WAITING;

    /**
     * @Column(name="answer_mail_status", type="integer")
     * @var integer
     */
    private $answerMailStatus = self::STATUS_MAIL_WAITING;

    /**
     * @Column(name="from_name", type="string")
     * @var string
     */
    private $fromName;

    /**
     * @Column(name="from_email", type="string")
     * @var string
     */
    private $fromEmail;

    /**
     * @Column(name="from_phone", type="string")
     * @var string
     */
    private $fromPhone;

    /**
     * @Column(name="message", type="string")
     * @var string
     */
    private $message;

    /**
     * @Column(name="message_attachment_ids", type="json_array")
     * @var string
     */
    private $messageAttachmentIds = [];

    /**
     * @Column(name="answer", type="string")
     * @var string
     */
    private $answer;

    /**
     * @Column(name="answer_date", type="datetime")
     * @var \DateTime
     */
    private $answerDate;

    /**
     * @Column(name="answer_attachment_ids", type="json_array")
     * @var int[]
     */
    private $answerAttachmentIds = [];

    public function __construct(
        string $reference,
        string $fromName,
        string $fromEmail,
        string $fromPhone,
        string $message,
        array $messageAttachmentIds
    ) {
        $this->setCurrentVersion(self::LATEST_VERSION);

        $this->reference = $reference;
        $this->fromName = $fromName;
        $this->fromEmail = $fromEmail;
        $this->fromPhone = $fromPhone;
        $this->message = $message;
        $this->messageAttachmentIds = $messageAttachmentIds;

        $this->regenerateSID();
        $this->initModificationEntity();
    }

    public function getReference(): string
    {
        return $this->reference;
    }

    public function setAnswer(string $message, array $attachmentIds): self
    {
        $this->answer = $message;
        $this->answerAttachmentIds = $attachmentIds;
        $this->answerDate = new \DateTime();

        return $this;
    }

    public function hasAnswer(): bool
    {
        return $this->answer !== null;
    }

    public function destroyAnswer(): void
    {
        if($this->hasAnswer()) {
            $this->answer = null;
            $this->answerAttachmentIds = [];
            $this->answerDate = null;
        }
    }

    public function getMetadataVersion(): string
    {
        return "1.0.0";
    }

    public function toJSON(array $options = []): array
    {
        $json = [
            "id" => $this->getIdNoFall(),
            "sid" => $this->getSID(),
            "date_created_at" => $this->getDateCreatedAt()->format(\DateTime::RFC2822),
            "last_updated_on" => $this->getLastUpdatedOn()->format(\DateTime::RFC2822),
            "version" => $this->getVersion(),
            "metadata" => array_merge($this->getMetadata(), [
                "version" =>  $this->getMetadataVersion(),
            ]),
            "reference" => $this->getReference(),
            "is_read" => $this->isRead(),
            "from" => [
                "email" => $this->getFromEmail(),
                "phone" => $this->getFromPhone(),
                "name" => $this->getFromName(),
                'mail_status' => $this->getFromMailStatus(),
            ],
            "message" => $this->getMessage(),
            "message_attachment_ids" => $this->getMessageAttachmentIds(),
        ];

        if($this->isRead()) {
            $json["date_read"] = $this->getDateRead()->format(\DateTime::RFC2822);
        }

        if($this->hasAnswer()) {
            $json["answer"] = [
                "message" => $this->getAnswer(),
                "message_attachment_ids" => $this->getAnswerAttachmentIds(),
                "date" => $this->getAnswerDate()->format(\DateTime::RFC2822),
                "mail_status" => $this->getAnswerMailStatus(),
            ];
        }

        return $json;
    }

    public function markAsRead(): self
    {
        $this->isRead = true;
        $this->dateRead = new \DateTime();
        $this->markAsUpdated();

        return $this;
    }

    public function getFromMailStatus(): int
    {
        return $this->fromMailStatus;
    }

    public function setFromMailStatus(int $fromMailStatus): self
    {
        $this->fromMailStatus = $fromMailStatus;
        $this->markAsUpdated();

        return $this;
    }

    public function getAnswerMailStatus(): int
    {
        return $this->answerMailStatus;
    }

    public function setAnswerMailStatus(int $answerMailStatus): self
    {
        $this->answerMailStatus = $answerMailStatus;
        $this->markAsUpdated();

        return $this;
    }

    public function isRead(): bool
    {
        return $this->isRead;
    }

    public function getDateRead(): \DateTime
    {
        return $this->dateRead;
    }

    public function getFromName(): string
    {
        return $this->fromName;
    }

    public function getFromEmail(): string
    {
        return $this->fromEmail;
    }

    public function getFromPhone(): string
    {
        return $this->fromPhone;
    }

    public function getMessageAttachmentIds(): array
    {
        return $this->messageAttachmentIds;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function getAnswer(): string
    {
        return $this->answer;
    }

    public function getAnswerAttachmentIds(): array
    {
        return $this->answerAttachmentIds;
    }

    public function getAnswerDate(): \DateTime
    {
        return $this->answerDate;
    }
}
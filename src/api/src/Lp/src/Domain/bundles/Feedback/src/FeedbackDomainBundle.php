<?php
namespace Lp\Domain\Bundles\Feedback;

use Lp\Domain\Bundles\Feedback\Subscriptions\FeedbackMailServiceSubscription;
use ZEA2\Platform\Bundles\Markers\ZEA2EventSubscriptionsBundle\ZEA2EventSubscriptionsBundle;
use ZEA2\Platform\Bundles\ZEA2Bundle;

final class FeedbackDomainBundle extends ZEA2Bundle implements ZEA2EventSubscriptionsBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getEventScripts(): array
    {
        return [
            FeedbackMailServiceSubscription::class,
        ];
    }
}
<?php
namespace Lp\Domain\Bundles\Feedback\Parameters;

use ZEA2\Domain\Criteria\SeekCriteria;

final class ListFeedbackParameters
{
    /** @var SeekCriteria */
    private $seek;

    public function __construct(SeekCriteria $seek)
    {
        $this->seek = $seek;
    }

    public function getSeek(): SeekCriteria
    {
        return $this->seek;
    }
}
<?php
namespace Lp\Domain\Bundles\Feedback\ReferenceFactory;

use Lp\Domain\Bundles\Feedback\Entity\Feedback;

interface FeedbackReferenceFactoryStrategy
{
    public function isMatched(Feedback $feedback): bool;
    public function getReferenceJSON(Feedback $feedback): array;
}
<?php
namespace Lp\Domain\Bundles\Feedback\Subscriptions;

use Lp\Domain\Bundles\Feedback\Entity\Feedback;
use Lp\Domain\Bundles\Feedback\Service\FeedbackMailService;
use Lp\Domain\Bundles\Feedback\Service\FeedbackService;
use ZEA2\Platform\Event\ZEA2EventEmitter;
use ZEA2\Platform\Event\ZEA2EventScript;

final class FeedbackMailServiceSubscription implements ZEA2EventScript
{
    /** @var FeedbackService */
    private $feedbackService;

    /** @var FeedbackMailService */
    private $feedbackMailService;

    public function __construct(
        FeedbackService $feedbackService,
        FeedbackMailService $feedbackMailService
    ) {
        $this->feedbackService = $feedbackService;
        $this->feedbackMailService = $feedbackMailService;
    }

    public function __invoke(ZEA2EventEmitter $eventEmitter)
    {
        $this->feedbackService->getEventEmitter()->on(FeedbackService::EVENT_CREATED, function(Feedback $feedback) {
            $this->feedbackMailService->sendMessageMail($feedback);
        });

        $this->feedbackService->getEventEmitter()->on(FeedbackService::EVENT_ANSWER, function(Feedback $feedback) {
            $this->feedbackMailService->sendAnswerMail($feedback);
        });
    }
}
<?php
namespace Lp\Domain\Bundles\Feedback\Service;

use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use Lp\Domain\Bundles\Feedback\Entity\Feedback;
use ZEA2\Domain\Bundles\Mail\Service\MailService;

final class FeedbackMailService
{
    public const EVENT_SEND_MAIL_TURN_ON = 'lp.domain.feedback.mail.turn-on';
    public const EVENT_SEND_MAIL_TURN_OFF = 'lp.domain.feedback.mail.turn-off';

    /** @var bool */
    private $sendMailEnabled = true;

    /** @var EventEmitterInterface */
    private $eventEmitter;

    /** @var MailService */
    private $mailService;

    /** @var FeedbackService */
    private $feedbackService;

    public function __construct(MailService $mailService, FeedbackService $feedbackService)
    {
        $this->eventEmitter = new EventEmitter();
        $this->mailService = $mailService;
        $this->feedbackService = $feedbackService;
    }

    public function getEventEmitter(): EventEmitterInterface
    {
        return $this->eventEmitter;
    }

    public function turnOnSendingMails(): void
    {
        $this->sendMailEnabled = true;
        $this->eventEmitter->emit(self::EVENT_SEND_MAIL_TURN_ON);
    }

    public function turnOffSendingMails(): void
    {
        $this->sendMailEnabled = true;
        $this->eventEmitter->emit(self::EVENT_SEND_MAIL_TURN_OFF);
    }

    public function isSendingMailTurnedOn(): bool
    {
        return $this->sendMailEnabled;
    }

    public function sendMessageMail(Feedback $feedback): void
    {

    }

    public function sendAnswerMail(Feedback $feedback): void
    {

    }
}
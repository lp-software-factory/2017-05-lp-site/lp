<?php
namespace Lp\Domain\Bundles\Feedback\ReferenceFactory\Impl;

use Lp\Domain\Bundles\Feedback\Entity\Feedback;
use Lp\Domain\Bundles\Feedback\ReferenceFactory\FeedbackReferenceFactoryStrategy;

final class GlobalFeedbackReferenceFactoryStrategy implements FeedbackReferenceFactoryStrategy
{
    public const FEEDBACK_TYPE = 'global';

    public function isMatched(Feedback $feedback): bool
    {
        return $feedback === self::FEEDBACK_TYPE;
    }

    public function getReferenceJSON(Feedback $feedback): array
    {
        return [
            'type' => self::FEEDBACK_TYPE,
        ];
    }
}
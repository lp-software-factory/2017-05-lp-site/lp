<?php
namespace Lp\Domain\Bundles\Feedback\Exceptions;

final class FeedbackNotFoundException extends \Exception {}
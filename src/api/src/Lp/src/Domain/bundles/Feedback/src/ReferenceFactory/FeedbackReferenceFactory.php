<?php
namespace Lp\Domain\Bundles\Feedback\ReferenceFactory;

use Interop\Container\ContainerInterface;
use Lp\Domain\Bundles\Feedback\Entity\Feedback;
use Lp\Domain\Bundles\Feedback\Exceptions\FeedbackReferenceCannotBeProducedException;
use Lp\Domain\Bundles\Feedback\ReferenceFactory\Impl\GlobalFeedbackReferenceFactoryStrategy;

final class FeedbackReferenceFactory
{
    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getReferenceJSON(Feedback $feedback): array
    {
        /** @var FeedbackReferenceFactoryStrategy $strategy */
        foreach($this->getStrategies() as $strategy) {
            if($strategy->isMatched($feedback)) {
                return $strategy->getReferenceJSON($feedback);
            }
        }

        throw new FeedbackReferenceCannotBeProducedException('No reference available');
    }

    private function getStrategies(): array
    {
        return array_map(function(string $className) {
            $object = $this->container->get($className);

            if(! ($object instanceof FeedbackReferenceFactoryStrategy)) {
                throw new \Exception('Not a ReferenceFactoryStrategy');
            }

            return $object;
        }, [
            GlobalFeedbackReferenceFactoryStrategy::class,
        ]);
    }
}
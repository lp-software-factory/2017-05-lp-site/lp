<?php
namespace Lp\Domain\Bundles\Feedback\Formatter;

use Lp\Domain\Bundles\Feedback\Entity\Feedback;
use Lp\Domain\Bundles\Feedback\Exceptions\FeedbackReferenceCannotBeProducedException;
use Lp\Domain\Bundles\Feedback\ReferenceFactory\FeedbackReferenceFactory;
use ZEA2\Domain\Bundles\Attachment\Service\AttachmentService;

final class FeedbackFormatter
{
    /** @var AttachmentService */
    private $attachmentService;

    /** @var FeedbackReferenceFactory */
    private $feedbackReferenceFactory;

    public function __construct(
        AttachmentService $attachmentService,
        FeedbackReferenceFactory $feedbackReferenceFactory
    ) {
        $this->attachmentService = $attachmentService;
        $this->feedbackReferenceFactory = $feedbackReferenceFactory;
    }

    public function formatMany(array $entities): array
    {
        return array_map(function(Feedback $feedback) {
            return $this->formatOne($feedback);
        }, $entities);
    }

    public function formatOne(Feedback $feedback): array
    {
        $entityJSON = $feedback->toJSON();

        $messageAttachmentsJSON = array_map(function(int $id) {
            return $this->attachmentService->getById($id)->toJSON();
        }, $feedback->getMessageAttachmentIds());

        $answerAttachmentsJSON = $feedback->hasAnswer() ? array_map(function(int $id) {
            return $this->attachmentService->getById($id)->toJSON();
        }, $feedback->getAnswerAttachmentIds()) : [];

        $reference = [];

        try {
            $reference = $this->feedbackReferenceFactory->getReferenceJSON($feedback);
        }catch(FeedbackReferenceCannotBeProducedException $e) {}

        return [
            'entity' => $entityJSON,
            'message_attachments' => $messageAttachmentsJSON,
            'answer_attachments' => $answerAttachmentsJSON,
            'reference' => $reference,
        ];
    }
}
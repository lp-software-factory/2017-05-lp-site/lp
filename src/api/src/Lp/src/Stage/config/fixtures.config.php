<?php
namespace ZEA2\Stage;

use Lp\Stage\Fixtures\AdminStageFixture\AdminStageFixture;
use Lp\Stage\Fixtures\DemoFeedbackFixture\DemoFeedbackFixture;
use Lp\Stage\Fixtures\LocalesStageFixture\LocalesStageFixture;
use Lp\Stage\Fixtures\LpRegisterFixture\LpRegisterFixture;

return [
    'config.zea2.stage.fixtures' => [
        LocalesStageFixture::class,
        LpRegisterFixture::class,
        AdminStageFixture::class,
        DemoFeedbackFixture::class,
    ]
];
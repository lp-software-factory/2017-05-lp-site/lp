<?php
namespace Lp\Stage;

use ZEA2\Platform\Bundles\ZEA2Bundle;

final class LpStageBundle extends ZEA2Bundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}
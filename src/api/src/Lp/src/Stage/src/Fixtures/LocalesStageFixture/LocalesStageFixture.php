<?php
namespace Lp\Stage\Fixtures\LocalesStageFixture;

use ZEA2\Domain\Bundles\Locale\Parameters\CreateLocaleParameters;
use ZEA2\Domain\Bundles\Locale\Service\LocaleService;
use ZEA2\Stage\Fixtures\StageFixture;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class LocalesStageFixture implements StageFixture
{
    /** @var LocaleService */
    private $localeService;

    public function __construct(LocaleService $localeService)
    {
        $this->localeService = $localeService;
    }

    public function up(OutputInterface $output, InputInterface $input): void
    {
        foreach ($this->getLocales() as $request) {
            $output->writeln(sprintf('  - %s', $request['region']));

            $this->localeService->createLocale(new CreateLocaleParameters(
                $request['language'],
                $request['region']
            ));
        }
    }

    private function getLocales(): array
    {
        return json_decode(file_get_contents(__DIR__.'/resources/locales.json'), true);
    }
}
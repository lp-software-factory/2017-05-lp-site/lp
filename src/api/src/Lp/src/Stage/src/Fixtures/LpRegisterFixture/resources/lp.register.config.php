<?php
namespace Lp\Stage\Fixtures\LpRegisterFixture;

use ZEA2\Domain\Bundles\Register\Parameters\RegisterRecord\AbstractRegisterRecordParameters;

return [
    "lp.common" => [
        'admin.email' => [
            'value' => 'root@example.com',
            'options' => [
                AbstractRegisterRecordParameters::OPTION_EXPORTS_TO_FRONTEND => true,
            ]
        ],
    ],
    "lp.admin" => [],
    "lp.app" => [],
    "lp.api" => [],
];
<?php
namespace Lp\Stage\Fixtures\AdminStageFixture;

use ZEA2\Domain\Bundles\Account\Service\AccountService;
use ZEA2\Domain\Bundles\Profile\Entity\Profile;
use ZEA2\Domain\Bundles\Profile\Parameters\CreateProfileParameters;
use ZEA2\Domain\Bundles\Profile\Service\ProfileService;
use ZEA2\Stage\Fixtures\StageFixture;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class AdminStageFixture implements StageFixture
{
    /** @var Profile[] */
    public static $designers = [];

    /** @var AccountService */
    private $accountService;

    /** @var ProfileService */
    private $profileService;

    public function __construct(
        AccountService $accountService,
        ProfileService $profileService
    ) {
        $this->accountService = $accountService;
        $this->profileService = $profileService;
    }

    public function up(OutputInterface $output, InputInterface $input): void
    {
        foreach($this->getAccounts() as $request) {
            $output->writeln(sprintf('  - %s', $request['email']));

            $account = $this->accountService->createAccount($request['email'], $request['password'], array_diff($request['roles'], ['designer']));
            $profile = $this->profileService->createProfileForAccount($account, new CreateProfileParameters(
                $request['first_name'],
                $request['last_name'],
                $request['middle_name']
            ));
        }
    }

    private function getAccounts(): array
    {
        return json_decode(file_get_contents(__DIR__.'/resources/accounts.json'), true);
    }
}
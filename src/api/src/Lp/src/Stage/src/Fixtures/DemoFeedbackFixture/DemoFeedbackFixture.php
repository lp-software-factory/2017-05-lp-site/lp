<?php
namespace Lp\Stage\Fixtures\DemoFeedbackFixture;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Lp\Domain\Bundles\Feedback\Parameters\CreateFeedbackParameters;
use Lp\Domain\Bundles\Feedback\Service\FeedbackMailService;
use Lp\Domain\Bundles\Feedback\Service\FeedbackService;
use ZEA2\Domain\Bundles\Attachment\Entity\Attachment;
use ZEA2\Domain\Bundles\Attachment\Service\AttachmentService;
use ZEA2\Stage\Fixtures\StageFixture;
use Zend\Diactoros\UploadedFile;

final class DemoFeedbackFixture implements StageFixture
{
    /** @var AttachmentService */
    private $attachmentService;

    /** @var FeedbackService */
    private $feedbackService;

    /** @var FeedbackMailService */
    private $feedbackMailService;

    public function __construct(
        AttachmentService $attachmentService,
        FeedbackService $feedbackService,
        FeedbackMailService $feedbackMailService
    ) {
        $this->attachmentService = $attachmentService;
        $this->feedbackService = $feedbackService;
        $this->feedbackMailService = $feedbackMailService;
    }

    public function up(OutputInterface $output, InputInterface $input): void
    {
        $output->writeln('[*] Demo Feedback');

        $this->feedbackMailService->turnOffSendingMails();

        foreach($this->fetchJSON() as $json) {
            $output->writeln('   + F');

            $attachmentIds = array_map(function(string $file) {
                return $this->uploadFile(__DIR__.'/resources/attachments/'.$file)->getId();
            }, $json['from']['attachments']);

            $parameters = new CreateFeedbackParameters(
                $json['reference'],
                $json['from']['name'],
                $json['from']['email'],
                $json['from']['phone'],
                $json['from']['message'],
                $attachmentIds
            );

            $this->feedbackService->create($parameters);
        }
    }

    private function fetchJSON(): array
    {
        return json_decode(file_get_contents(__DIR__.'/resources/feedback.json'), true);
    }


    private function uploadFile(string $path): Attachment
    {
        $uploadFile = new UploadedFile($path, filesize($path), 0);

        return $this->attachmentService->uploadAttachment(
            $uploadFile->getStream()->getMetadata('uri'), basename($path)
        );
    }
}
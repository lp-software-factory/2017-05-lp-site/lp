<?php
namespace Lp\Stage\Fixtures\LpRegisterFixture;

use ZEA2\Domain\Bundles\Register\StageKit\AbstractZEA2RegisterStageFixture;

final class LpRegisterFixture extends AbstractZEA2RegisterStageFixture
{
    protected function getJSONConfig(): array
    {
        return require __DIR__.'/resources/lp.register.config.php';
    }
}
<?php
namespace Lp\REST\TestCase;

use Lp\REST\Bundles\Feedback\Tests\Definition\FeedbackRESTDefinitionTrait;
use ZEA2\REST\Bundles\PHPUnit\TestCase\ZEA2MiddlewareTestCase;

abstract class LpMiddlewareTestCase extends ZEA2MiddlewareTestCase
{
    use FeedbackRESTDefinitionTrait;
}
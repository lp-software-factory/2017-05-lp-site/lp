<?php
namespace Lp\REST;

use Lp\REST\Bundles\Feedback\FeedbackRESTBundle;
use Lp\REST\Bundles\Frontend\FrontendRESTBundle;
use ZEA2\REST\Bundle\RESTBundle;

final class LpRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getBundles(): array
    {
        $bundles = [
            new FrontendRESTBundle(),
            new FeedbackRESTBundle(),
        ];

        foreach($bundles as $bundle) {
            if(! ($bundle instanceof RESTBundle)) {
                throw new \Exception(sprintf('Invalid bundle "%s", should be extended from `%s`', get_class($bundle), RESTBundle::class));
            }
        }

        return $bundles;
    }
}
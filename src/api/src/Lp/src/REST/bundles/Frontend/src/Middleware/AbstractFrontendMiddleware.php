<?php
namespace Lp\REST\Bundles\Frontend\Middleware;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use ZEA2\Platform\Bundles\Frontend\Service\FrontendService;
use Zend\Stratigility\MiddlewareInterface;

abstract class AbstractFrontendMiddleware implements MiddlewareInterface
{
    /** @var FrontendService */
    private $frontendService;

    public function __construct(FrontendService $frontendService)
    {
        $this->frontendService = $frontendService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $response->getBody()->write(json_encode([
                'success' => true
            ] + $this->frontendService->fetch(), JSON_UNESCAPED_SLASHES));

        return $response
            ->withStatus(200)
            ->withHeader('Content-Type', 'application/json')
        ;
    }
}
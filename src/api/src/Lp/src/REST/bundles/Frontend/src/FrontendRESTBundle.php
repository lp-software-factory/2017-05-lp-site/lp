<?php
namespace Lp\REST\Bundles\Frontend;

use ZEA2\REST\Bundle\RESTBundle;

final class FrontendRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}
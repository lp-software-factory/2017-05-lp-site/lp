<?php
namespace Lp\REST\Bundles\Feedback\Tests\Paths;

use Lp\REST\Bundles\Feedback\Tests\FeedbackMiddlewareTestCase;
use Lp\REST\Bundles\Feedback\Tests\Fixture\FeedbackFixture;
use ZEA2\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use ZEA2\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;

final class ListFeedbackMiddlewareTest extends FeedbackMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new FeedbackFixture());

        $ids = $this->requestFeedbackList(['seek' => ['offset' => 0, 'limit' => 2]])
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->fetch(function(array $json) {
                return array_map(function(array $entity) {
                    return $entity['entity']['id'];
                }, $json['entities']);
            });

        $this->assertEquals($ids, [
            FeedbackFixture::$feedback3->getId(),
            FeedbackFixture::$feedback2->getId()
        ]);
    }

    public function test403()
    {
        $this->upFixture(new FeedbackFixture());

        $this->requestFeedbackList(['seek' => ['offset' => 0, 'limit' => 2]])
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();

        $this->requestFeedbackList(['seek' => ['offset' => 0, 'limit' => 2]])
            ->__invoke()
            ->expectAuthError();
    }
}
<?php
namespace Lp\REST\Bundles\Feedback\Tests\Definition;

use ZEA2\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;

trait FeedbackRESTDefinitionTrait
{
    abstract function request(string $method, string $uri, array $queryParams = null): RESTRequest;

    protected function requestFeedbackCreate(array $json): RESTRequest
    {
        return $this->request('put', '/feedback/create')
            ->setParameters($json);
    }

    protected function requestFeedbackAnswer(int $feedbackId, array $json): RESTRequest
    {
        return $this->request('put', sprintf('/feedback/%d/answer', $feedbackId))
            ->setParameters($json);
    }

    protected function requestFeedbackDelete(int $feedbackId): RESTRequest
    {
        return $this->request('delete', sprintf('/feedback/%d/delete', $feedbackId));
    }

    protected function requestFeedbackGetById(int $feedbackId): RESTRequest
    {
        return $this->request('get', sprintf('/feedback/%d/get', $feedbackId));
    }

    protected function requestFeedbackMarkAsRead(int $feedbackId): RESTRequest
    {
        return $this->request('post', sprintf('/feedback/%d/mark-as-read', $feedbackId));
    }

    protected function requestFeedbackList(array $json): RESTRequest
    {
        return $this->request('post', sprintf('/feedback/list'))
            ->setParameters($json);
    }
}
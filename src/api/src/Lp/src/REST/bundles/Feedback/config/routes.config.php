<?php
namespace Lp\REST\Bundles\Feedback;

use Lp\REST\Bundles\Feedback\Middleware\FeedbackMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'put',
                'path' => '/feedback/{command:create}[/]',
                'middleware' => FeedbackMiddleware::class,
                'group' => 'default',
            ],
            [
                'method' => 'put',
                'path' => '/feedback/{feedbackId}/{command:answer}[/]',
                'middleware' => FeedbackMiddleware::class,
                'group' => 'default',
            ],
            [
                'method' => 'post',
                'path' => '/feedback/{feedbackId}/{command:mark-as-read}[/]',
                'middleware' => FeedbackMiddleware::class,
                'group' => 'default',
            ],
            [
                'method' => 'delete',
                'path' => '/feedback/{feedbackId}/{command:delete}[/]',
                'middleware' => FeedbackMiddleware::class,
                'group' => 'default',
            ],
            [
                'method' => 'get',
                'path' => '/feedback/{feedbackId}/{command:get}[/]',
                'middleware' => FeedbackMiddleware::class,
                'group' => 'default',
            ],
            [
                'method' => 'post',
                'path' => '/feedback/{command:list}[/]',
                'middleware' => FeedbackMiddleware::class,
                'group' => 'default',
            ],
        ]
    ]
];
<?php
namespace Lp\REST\Bundles\Feedback\Middleware\Command;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Lp\Domain\Bundles\Feedback\Exceptions\FeedbackNotFoundException;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;

final class DeleteFeedbackCommand extends AbstractFeedbackCommand
{
    public function __invoke(ServerRequestInterface $request, JSONResponseBuilder $responseBuilder): ResponseInterface
    {
        try {
            $this->access->requireAdminAccess();

            $this->service->delete($request->getAttribute('feedbackId'));

            $responseBuilder
                ->setStatusSuccess();
        }catch(FeedbackNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}
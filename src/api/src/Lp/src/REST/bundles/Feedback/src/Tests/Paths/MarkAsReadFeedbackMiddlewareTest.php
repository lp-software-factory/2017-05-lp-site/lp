<?php
namespace Lp\REST\Bundles\Feedback\Tests\Paths;

use Lp\REST\Bundles\Feedback\Tests\FeedbackMiddlewareTestCase;
use Lp\REST\Bundles\Feedback\Tests\Fixture\FeedbackFixture;
use ZEA2\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use ZEA2\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;

final class MarkAsReadFeedbackMiddlewareTest extends FeedbackMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new FeedbackFixture());

        $this->requestFeedbackMarkAsRead(FeedbackFixture::$feedback1->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'feedback' => [
                    'entity' => [
                        'id' => FeedbackFixture::$feedback1->getId(),
                        'is_read' => true,
                    ]
                ]
            ]);
    }

    public function test403()
    {
        $this->upFixture(new FeedbackFixture());

        $this->requestFeedbackMarkAsRead(FeedbackFixture::$feedback1->getId())
            ->__invoke()
            ->expectAuthError();

        $this->requestFeedbackMarkAsRead(FeedbackFixture::$feedback1->getId())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new FeedbackFixture());

        $this->requestFeedbackMarkAsRead(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}
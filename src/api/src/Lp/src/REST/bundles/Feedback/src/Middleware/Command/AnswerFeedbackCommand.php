<?php
namespace Lp\REST\Bundles\Feedback\Middleware\Command;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Lp\Domain\Bundles\Feedback\Exceptions\FeedbackAlreadyHasAnswerException;
use Lp\Domain\Bundles\Feedback\Exceptions\FeedbackNotFoundException;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;

final class AnswerFeedbackCommand extends AbstractFeedbackCommand
{
    public function __invoke(ServerRequestInterface $request, JSONResponseBuilder $responseBuilder): ResponseInterface
    {
        try {
            $this->access->requireAdminAccess();

            $parameters = $this->parametersFactory->factoryAnswerFeedbackParameters($request);
            $feedback = $this->service->answer($request->getAttribute('feedbackId'), $parameters);

            $responseBuilder
                ->setJSON([
                    'feedback' => $this->formatter->formatOne($feedback),
                ])
                ->setStatusSuccess();
        }catch(FeedbackAlreadyHasAnswerException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusConflict();
        }catch(FeedbackNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}
<?php
namespace Lp\REST\Bundles\Feedback\Tests\Paths;

use Lp\REST\Bundles\Feedback\Tests\FeedbackMiddlewareTestCase;
use Lp\REST\Bundles\Feedback\Tests\Fixture\FeedbackFixture;
use ZEA2\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use ZEA2\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;

final class GetByIdFeedbackMiddlewareTest extends FeedbackMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new FeedbackFixture());

        $feedbackId = FeedbackFixture::$feedback1->getId();

        $this->requestFeedbackGetById($feedbackId)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'feedback' => [
                    'entity' => [
                        'id' => $feedbackId,
                    ]
                ]
            ])
        ;
    }

    public function test403()
    {
        $this->upFixture(new FeedbackFixture());

        $feedbackId = FeedbackFixture::$feedback1->getId();

        $this->requestFeedbackGetById($feedbackId)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();

        $this->requestFeedbackGetById($feedbackId)
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new FeedbackFixture());

        $this->requestFeedbackGetById(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}
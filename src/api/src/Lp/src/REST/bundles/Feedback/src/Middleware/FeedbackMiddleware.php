<?php
namespace Lp\REST\Bundles\Feedback\Middleware;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Lp\REST\Bundles\Feedback\Middleware\Command\AnswerFeedbackCommand;
use Lp\REST\Bundles\Feedback\Middleware\Command\CreateFeedbackCommand;
use Lp\REST\Bundles\Feedback\Middleware\Command\DeleteFeedbackCommand;
use Lp\REST\Bundles\Feedback\Middleware\Command\GetByIdFeedbackCommand;
use Lp\REST\Bundles\Feedback\Middleware\Command\ListFeedbackCommand;
use Lp\REST\Bundles\Feedback\Middleware\Command\MarkAsReadFeedbackCommand;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;
use ZEA2\REST\Command\Service\CommandService;
use Zend\Stratigility\MiddlewareInterface;

final class FeedbackMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new JSONResponseBuilder($response);

        $resolved = $this->commandService->createResolverBuilder()
            ->attachDirect('create', CreateFeedbackCommand::class)
            ->attachDirect('answer', AnswerFeedbackCommand::class)
            ->attachDirect('delete', DeleteFeedbackCommand::class)
            ->attachDirect('get', GetByIdFeedbackCommand::class)
            ->attachDirect('list', ListFeedbackCommand::class)
            ->attachDirect('mark-as-read', MarkAsReadFeedbackCommand::class)
            ->resolve($request);

        return $resolved->__invoke($request, $responseBuilder);
    }
}
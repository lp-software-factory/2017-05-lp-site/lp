<?php
namespace Lp\REST\Bundles\Feedback\Tests\Paths;

use Lp\Domain\Bundles\Feedback\Entity\Feedback;
use Lp\REST\Bundles\Feedback\Tests\FeedbackMiddlewareTestCase;
use ZEA2\REST\Bundles\Attachment\Tests\Fixture\AttachmentFixture;

final class CreateFeedbackMiddlewareTest extends FeedbackMiddlewareTestCase
{
    private function getTestJSON(): array
    {
        return [
            'reference' => Feedback::FEEDBACK_REFERENCE_DEFAULT,
            'from' => [
                'name' => 'Sample',
                'email' => 'sample@example.com',
                'phone' => '8 800 000 00',
            ],
            'message' => 'My Demo Feedback',
            'message_attachment_ids' => [
                AttachmentFixture::$attachmentImage->getId(),
            ]
        ];
    }

    public function test200()
    {
        $this->requestFeedbackCreate($json = $this->getTestJSON())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'feedback' => [
                    'entity' => [
                        'id' => $this->expectId(),
                        'from' => [
                            'name' => $json['from']['name'],
                            'email' => $json['from']['email'],
                            'phone' => $json['from']['phone']
                        ],
                        'message' => $json['message'],
                        'message_attachment_ids' => $json['message_attachment_ids'],
                    ]
                ]
            ]);
    }
}
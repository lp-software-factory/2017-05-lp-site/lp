<?php
namespace Lp\REST\Bundles\Feedback\Tests\Fixture;

use Doctrine\ORM\EntityManager;
use Lp\Domain\Bundles\Feedback\Entity\Feedback;
use Lp\Domain\Bundles\Feedback\Parameters\CreateFeedbackParameters;
use Lp\Domain\Bundles\Feedback\Service\FeedbackService;
use ZEA2\REST\Bundles\Attachment\Tests\Fixture\AttachmentFixture;
use ZEA2\REST\Bundles\PHPUnit\Fixture;
use Zend\Expressive\Application;

final class FeedbackFixture implements Fixture
{
    /** @var Feedback */
    public static $feedback1;

    /** @var Feedback */
    public static $feedback2;

    /** @var Feedback */
    public static $feedback3;

    /** @var FeedbackService */
    private $feedbackService;

    public function up(Application $app, EntityManager $em)
    {
        $this->feedbackService = $app->getContainer()->get(FeedbackService::class);

        self::$feedback1 = $this->feedbackService->create(new CreateFeedbackParameters(
            Feedback::FEEDBACK_REFERENCE_DEFAULT,
            'Annie',
            'annie@example.com',
            '+7 800 000 0000',
            'My Sample Feedback',
            []
        ));

        self::$feedback2 = $this->feedbackService->create(new CreateFeedbackParameters(
            Feedback::FEEDBACK_REFERENCE_DEFAULT,
            'Irelia',
            'irelia@example.com',
            '+7 800 000 0000',
            'My Sample Feedback',
            [
                AttachmentFixture::$attachmentImage->getId(),
            ]
        ));

        self::$feedback3 = $this->feedbackService->create(new CreateFeedbackParameters(
            Feedback::FEEDBACK_REFERENCE_DEFAULT,
            'Evelynn',
            'evelynn@example.com',
            '+7 800 000 0000',
            'My Sample Feedback',
            [
                AttachmentFixture::$attachmentImage2->getId(),
                AttachmentFixture::$attachmentImage3->getId(),
            ]
        ));
    }

    public static function getOrderedList(): array
    {
        return [
            self::$feedback1,
            self::$feedback2,
            self::$feedback3,
        ];
    }
}
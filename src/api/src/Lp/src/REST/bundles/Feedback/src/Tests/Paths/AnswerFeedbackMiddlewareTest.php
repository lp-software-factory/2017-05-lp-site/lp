<?php
namespace Lp\REST\Bundles\Feedback\Tests\Paths;

use Lp\REST\Bundles\Feedback\Tests\FeedbackMiddlewareTestCase;
use Lp\REST\Bundles\Feedback\Tests\Fixture\FeedbackFixture;
use ZEA2\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use ZEA2\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use ZEA2\REST\Bundles\Attachment\Tests\Fixture\AttachmentFixture;

final class AnswerFeedbackMiddlewareTest extends FeedbackMiddlewareTestCase
{
    private function getTestJSON(): array
    {
        return [
            'message' => 'My Demo Answer',
            'message_attachment_ids' => [
                AttachmentFixture::$attachmentImage->getId(),
            ]
        ];
    }

    public function test200()
    {
        $this->upFixture(new FeedbackFixture());

        $this->requestFeedbackAnswer(FeedbackFixture::$feedback1->getId(), $json = $this->getTestJSON())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'feedback' => [
                    'entity' => [
                        'answer' => [
                            'message' => $json['message'],
                            'message_attachment_ids' => $json['message_attachment_ids'],
                            'date' => $this->expectString(),
                        ]
                    ]
                ]
            ]);
    }

    public function test403()
    {
        $this->upFixture(new FeedbackFixture());

        $this->requestFeedbackAnswer(FeedbackFixture::$feedback1->getId(), $json = $this->getTestJSON())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();

        $this->requestFeedbackAnswer(FeedbackFixture::$feedback1->getId(), $json = $this->getTestJSON())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404FeedbackNotFound()
    {
        $this->upFixture(new FeedbackFixture());

        $this->requestFeedbackAnswer(self::NOT_FOUND_ID, $json = $this->getTestJSON())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test409HasAnswer()
    {
        $this->upFixture(new FeedbackFixture());

        $this->upFixture(new FeedbackFixture());

        $this->requestFeedbackAnswer(FeedbackFixture::$feedback1->getId(), $json = $this->getTestJSON())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200);

        $this->requestFeedbackAnswer(FeedbackFixture::$feedback1->getId(), $json = $this->getTestJSON())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(409);
    }
}
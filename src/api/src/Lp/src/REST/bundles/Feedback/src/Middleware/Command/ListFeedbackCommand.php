<?php
namespace Lp\REST\Bundles\Feedback\Middleware\Command;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;

final class ListFeedbackCommand extends AbstractFeedbackCommand
{
    public function __invoke(ServerRequestInterface $request, JSONResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->access->requireAdminAccess();

        $entities = $this->service->getBy($this->parametersFactory->factoryListFeedbackParameters($request));

        $responseBuilder
            ->setJSON([
                'entities' => $this->formatter->formatMany($entities),
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}
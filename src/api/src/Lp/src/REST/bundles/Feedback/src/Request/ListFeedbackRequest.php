<?php
namespace Lp\REST\Bundles\Feedback\Request;

use ZEA2\Platform\Bundles\APIDocs\Schema\JSONSchema;
use ZEA2\Platform\Bundles\APIDocs\Schema\SchemaRequest;

final class ListFeedbackRequest extends SchemaRequest
{
    public function getParameters()
    {
        return $this->getData();
    }

    protected function getSchema(): JSONSchema
    {
        return self::getSchemaService()->getDefinition('LpFeedbackBundle_Request_ListFeedbackRequest');
    }
}
<?php
namespace Lp\REST\Bundles\Feedback\Middleware\Command;

use Lp\Domain\Bundles\Feedback\Formatter\FeedbackFormatter;
use Lp\Domain\Bundles\Feedback\Service\FeedbackService;
use Lp\REST\Bundles\Feedback\ParametersFactory\FeedbackParametersFactory;
use ZEA2\REST\Bundles\Access\Service\AccessService;
use ZEA2\REST\Command\Command;

abstract class AbstractFeedbackCommand implements Command
{
    /** @var AccessService */
    protected $access;

    /** @var FeedbackService */
    protected $service;

    /** @var FeedbackParametersFactory */
    protected $parametersFactory;

    /** @var FeedbackFormatter */
    protected $formatter;

    public function __construct(
        AccessService $access,
        FeedbackService $service,
        FeedbackParametersFactory $parametersFactory,
        FeedbackFormatter $formatter
    ) {
        $this->access = $access;
        $this->service = $service;
        $this->parametersFactory = $parametersFactory;
        $this->formatter = $formatter;
    }
}
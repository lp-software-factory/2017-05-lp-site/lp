<?php
namespace Lp\REST\Bundles\Feedback\Tests;

use Lp\REST\TestCase\LpMiddlewareTestCase;
use ZEA2\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use ZEA2\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use ZEA2\REST\Bundles\Attachment\Tests\Fixture\AttachmentFixture;
use ZEA2\REST\Bundles\Locale\Tests\Fixture\LocaleFixture;

abstract class FeedbackMiddlewareTestCase extends LpMiddlewareTestCase
{
    protected function getFixtures(): array
    {
        return [
            new LocaleFixture(),
            new UserAccountFixture(),
            new AdminAccountFixture(),
            new AttachmentFixture(),
        ];
    }
}
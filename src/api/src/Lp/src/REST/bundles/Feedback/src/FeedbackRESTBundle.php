<?php
namespace Lp\REST\Bundles\Feedback;

use ZEA2\REST\Bundle\RESTBundle;

final class FeedbackRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}
<?php
namespace Lp\REST\Bundles\Feedback\Middleware\Command;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ZEA2\Platform\Response\JSON\JSONResponseBuilder;

final class CreateFeedbackCommand extends AbstractFeedbackCommand
{
    public function __invoke(ServerRequestInterface $request, JSONResponseBuilder $responseBuilder): ResponseInterface
    {
        $parameters = $this->parametersFactory->factoryCreateFeedbackParameters($request);
        $feedback = $this->service->create($parameters);

        $responseBuilder
            ->setJSON([
                'feedback' => $this->formatter->formatOne($feedback),
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}
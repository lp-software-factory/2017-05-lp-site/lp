<?php
namespace Lp\REST\Bundles\Feedback\Tests\Paths;

use Lp\REST\Bundles\Feedback\Tests\FeedbackMiddlewareTestCase;
use Lp\REST\Bundles\Feedback\Tests\Fixture\FeedbackFixture;
use ZEA2\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use ZEA2\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;

final class DeleteFeedbackMiddlewareTest extends FeedbackMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new FeedbackFixture());

        $feedbackId = FeedbackFixture::$feedback1->getId();

        $this->requestFeedbackGetById($feedbackId)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200);

        $this->requestFeedbackDelete($feedbackId)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
            ]);

        $this->requestFeedbackGetById($feedbackId)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test403()
    {
        $this->upFixture(new FeedbackFixture());

        $this->requestFeedbackDelete(FeedbackFixture::$feedback1->getId())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();

        $this->requestFeedbackDelete(FeedbackFixture::$feedback1->getId())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new FeedbackFixture());

        $this->requestFeedbackDelete(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}
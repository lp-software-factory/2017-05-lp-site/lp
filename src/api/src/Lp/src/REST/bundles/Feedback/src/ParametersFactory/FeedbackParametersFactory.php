<?php
namespace Lp\REST\Bundles\Feedback\ParametersFactory;

use Psr\Http\Message\ServerRequestInterface;
use Lp\Domain\Bundles\Feedback\Parameters\AnswerFeedbackParameters;
use Lp\Domain\Bundles\Feedback\Parameters\CreateFeedbackParameters;
use Lp\Domain\Bundles\Feedback\Parameters\ListFeedbackParameters;
use Lp\REST\Bundles\Feedback\Request\AnswerFeedbackRequest;
use Lp\REST\Bundles\Feedback\Request\CreateFeedbackRequest;
use Lp\REST\Bundles\Feedback\Request\ListFeedbackRequest;
use ZEA2\Domain\Criteria\SeekCriteria;

final class FeedbackParametersFactory
{
    public function factoryCreateFeedbackParameters(ServerRequestInterface $request): CreateFeedbackParameters
    {
        $json = (new CreateFeedbackRequest($request))->getParameters();

        return new CreateFeedbackParameters(
            $json['reference'],
            $json['from']['name'],
            $json['from']['email'],
            $json['from']['phone'],
            $json['message'],
            $json['message_attachment_ids']
        );
    }

    public function factoryAnswerFeedbackParameters(ServerRequestInterface $request): AnswerFeedbackParameters
    {
        $json = (new AnswerFeedbackRequest($request))->getParameters();

        return new AnswerFeedbackParameters(
            $json['message'],
            $json['message_attachment_ids']
        );
    }

    public function factoryListFeedbackParameters(ServerRequestInterface $request): ListFeedbackParameters
    {
        $json = (new ListFeedbackRequest($request))->getParameters();

        return new ListFeedbackParameters(
            new SeekCriteria(1000, $json['seek']['offset'], $json['seek']['limit'])
        );
    }
}
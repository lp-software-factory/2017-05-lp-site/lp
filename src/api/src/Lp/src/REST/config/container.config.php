<?php
namespace ZEA2\REST;

use function DI\object;
use function DI\get;

use ZEA2\Platform\Bundles\APIDocs\Generator\Impl\BundlesAPIDocsGenerator;
use ZEA2\Platform\Bundles\APIDocs\Service\APIDocsService;
use ZEA2\Domain\Bundles\Mail\Config\MailConfig;

return [
    APIDocsService::class => object()->constructorParameter('generator', get(BundlesAPIDocsGenerator::class)),
    MailConfig::class => object()->constructorParameter('config', get('config.zea2.rest.mail')),
];
<?php
namespace Lp\AdminSite;

use Lp\AdminSite\Bundles\Frontend\FrontendAdminSiteBundle;
use ZEA2\Platform\Bundles\ZEA2Bundle;

final class LpAdminSiteBundle extends ZEA2Bundle
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getBundles(): array
    {
        return [
            new FrontendAdminSiteBundle(),
        ];
    }
}
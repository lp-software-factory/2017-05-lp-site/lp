<?php
namespace Lp\AdminSite\Bundles\Frontend;

use Lp\AdminSite\Bundles\Frontend\FrontendScripts\LpAdminRegisterFrontendScript;
use Lp\AdminSite\Bundles\Frontend\FrontendScripts\TranslationFrontendScript;
use ZEA2\Platform\Bundles\Frontend\Markers\FrontendBundleInjectable\FrontendBundleInjectable;
use ZEA2\Platform\Bundles\ZEA2Bundle;

final class FrontendAdminSiteBundle extends ZEA2Bundle implements FrontendBundleInjectable
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getFrontendScripts(): array
    {
        return [
            TranslationFrontendScript::class,
            LpAdminRegisterFrontendScript::class,
        ];
    }
}
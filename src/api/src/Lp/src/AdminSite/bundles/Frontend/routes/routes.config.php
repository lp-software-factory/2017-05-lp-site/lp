<?php
namespace Lp\AdminSite\Bundles\Frontend;

use Lp\AdminSite\Bundles\Frontend\Middleware\AdminFrontendMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'get',
                'path' => '/frontend/admin[/]',
                'middleware' => AdminFrontendMiddleware::class,
                'group' => 'default',
            ],
        ]
    ]
];
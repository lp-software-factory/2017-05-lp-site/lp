<?php
namespace Lp\AdminSite\Bundles\Frontend\Middleware;

use Lp\REST\Bundles\Frontend\Middleware\AbstractFrontendMiddleware;

final class AdminFrontendMiddleware extends AbstractFrontendMiddleware {}
<?php
namespace Lp\AdminSite\Bundles\Frontend\FrontendScripts;

use ZEA2\Domain\Bundles\Register\FrontendKit\AbstractRegisterFrontendScript;

final class LpAdminRegisterFrontendScript extends AbstractRegisterFrontendScript
{
    protected function getRegisterGroups(): array
    {
        return [
            'lp.common',
            'lp.admin',
            'lp.api',
        ];
    }
}
<?php
namespace ZEA2;

use Symfony\Component\Console\Application;

$application = require_once __DIR__.'./../bootstrap/bootstrap-console.php'; /** @var \Zend\Expressive\Application $application */

$consoleApp = $application->getContainer()->get(Application::class);
$consoleApp->run();
<?php

use Phinx\Migration\AbstractMigration;

class FeedbackReferenceMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('feedback')
            ->addColumn('reference', 'string', [
                'null' => false,
                'default' => 'global',
            ])
            ->save();
    }
}

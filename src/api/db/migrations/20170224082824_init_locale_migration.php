<?php

use Phinx\Migration\AbstractMigration;

class InitLocaleMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('locale')
            ->addColumn('version','string', [
                'null' => false,
                'limit' => 32,
                'default' => '1.0.0',
            ])
            ->addColumn('title', 'json', [
                'default' => '{}',
                'null' => false,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('language', 'string', [
                'limit' => 2
            ])
            ->addColumn('region', 'string', [
                'limit' => 5
            ])
            ->addIndex(['language', 'region'], ['unique' => true])
            ->create();
    }
}

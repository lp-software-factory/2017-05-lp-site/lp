<?php

use Phinx\Migration\AbstractMigration;

class InitTranslationMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('translation')
            ->addColumn('sid', 'string', [
                'null' => false,
                'limit' => 12,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('date_created_at', 'datetime', [
                'null' => false,
            ])
            ->addColumn('last_updated_on', 'datetime', [
                'null' => false,
            ])
            ->addColumn('version', 'string', [
                'limit' => 32,
                'null' => false,
                'default' => '1.0.0',
            ])
            ->addColumn('position', 'integer', [
                'null' => false,
                'default' => 1,
            ])
            ->addColumn('project_id', 'integer', [
                'null' => false,
            ])
            ->addColumn('key', 'string', [
                'null' => true,
            ])
            ->addColumn('title', 'json', [
                'null' => false,
                'default' => '[]',
            ])
            ->addIndex(['key', 'project_id'], ['unique' => true])
            ->create();
    }
}

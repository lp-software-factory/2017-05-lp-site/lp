<?php

use Phinx\Migration\AbstractMigration;

class InitFeedbackMigrations extends AbstractMigration
{
    public function change()
    {
        $this->table('feedback')
            ->addColumn('sid', 'string', [
                'null' => false,
                'limit' => 12,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('date_created_at', 'datetime', [
                'null' => false,
            ])
            ->addColumn('last_updated_on', 'datetime', [
                'null' => false,
            ])
            ->addColumn('version', 'string', [
                'limit' => 32,
                'null' => false,
                'default' => '1.0.0',
            ])
            ->addColumn('is_read', 'boolean', [
                'null' => false,
                'default' => false,
            ])
            ->addColumn('date_read', 'datetime', [
                'null' => true,
            ])
            ->addColumn('from_mail_status', 'integer', [
                'null' => false,
                'default' => 1,
            ])
            ->addColumn('answer_mail_status', 'integer', [
                'null' => false,
                'default' => 1,
            ])
            ->addColumn('from_name', 'string', [
                'null' => false,
            ])
            ->addColumn('from_email', 'string', [
                'null' => false,
            ])
            ->addColumn('from_phone', 'string', [
                'null' => false,
            ])
            ->addColumn('message', 'text', [
                'null' => false,
            ])
            ->addColumn('message_attachment_ids', 'json', [
                'null' => false,
                'default' => '[]',
            ])
            ->addColumn('answer', 'text', [
                'null' => true,
            ])
            ->addColumn('answer_date', 'datetime', [
                'null' => true,
            ])
            ->addColumn('answer_attachment_ids', 'json', [
                'null' => true,
                'default' => '[]',
            ])
            ->create();
    }
}

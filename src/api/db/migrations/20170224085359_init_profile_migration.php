<?php

use Phinx\Migration\AbstractMigration;

class InitProfileMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('profile')
            ->addColumn('sid', 'string', [
                'null' => false,
                'limit' => 12,
            ])
            ->addColumn('version','string', [
                'null' => false,
                'limit' => 32,
                'default' => '1.0.0',
            ])
            ->addColumn('date_created_at', 'datetime', [
                'null' => false,
            ])
            ->addColumn('last_updated_on', 'datetime', [
                'null' => false,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('account_id', 'integer', [
                'null' => false,
            ])
            ->addColumn('image', 'json', [
                'null' => false,
                'default' => json_encode([
                    'uid' => '#NONE',
                    'is_auto_generated' => true,
                    'variants' => []
                ])
            ])
            ->addColumn('first_name', 'string', [
                'limit' => 32,
            ])
            ->addColumn('last_name', 'string', [
                'limit' => 32,
            ])
            ->addColumn('middle_name', 'string', [
                'limit' => 32,
                'null' => true
            ])
            ->addForeignKey('account_id', 'account', 'id', [
                'update' => 'cascade',
                'delete' => 'cascade',
            ])
            ->addIndex('sid', ['unique' => true])
        ->create();

        $this->table('reset_password_request')
            ->addColumn('sid', 'string', [
                'null' => false,
                'limit' => 12,
            ])
            ->addColumn('version','string', [
                'null' => false,
                'limit' => 32,
                'default' => '1.0.0',
            ])
            ->addColumn('date_created_at', 'datetime', [
                'null' => false,
            ])
            ->addColumn('last_updated_on', 'datetime', [
                'null' => false,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('accept_token', 'string', [
                'limit' => 32,
            ])
            ->addColumn('account_id', 'integer', [
                'null' => false
            ])
            ->addForeignKey('account_id', 'account', 'id', [
                'update' => 'cascade',
                'delete' => 'cascade',
            ])
            ->addIndex('sid', ['unique' => true])
            ->create();

        $this->table('oauth2_provider')
            ->addColumn('sid', 'string', [
                'null' => false,
                'limit' => 12,
            ])
            ->addColumn('version','string', [
                'null' => false,
                'limit' => 32,
                'default' => '1.0.0',
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('date_created_at', 'datetime', [
                'null' => false,
            ])
            ->addColumn('last_updated_on', 'datetime', [
                'null' => false,
            ])
            ->addColumn('code', 'string', [
                'null' => false,
            ])
            ->addColumn('handler', 'string', [
                'null' => false,
            ])
            ->addColumn('config', 'json', [
                'null' => false,
            ])
            ->addIndex('sid', ['unique' => true])
            ->addIndex('code', ['unique' => true])
            ->addIndex('handler', ['unique' => true])
            ->create();

        $this->table('oauth2_account')
            ->addColumn('version','string', [
                'null' => false,
                'limit' => 32,
                'default' => '1.0.0',
            ])
            ->addColumn('sid', 'string', [
                'null' => false,
                'limit' => 12,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('date_created_at', 'datetime', [
                'null' => false,
            ])
            ->addColumn('last_updated_on', 'datetime', [
                'null' => false,
            ])
            ->addColumn('account_id', 'integer', [
                'null' => false
            ])
            ->addColumn('provider_id', 'integer', [
                'null' => false
            ])
            ->addColumn('refresh_token', 'string', [
                'null' => false
            ])
            ->addColumn('resource_owner_id', 'string', [
                'null' => false
            ])
            ->addIndex('sid', ['unique' => true])
            ->addForeignKey('account_id', 'account', 'id', [
                'update' => 'cascade',
                'delete' => 'cascade',
            ])
            ->addForeignKey('provider_id', 'oauth2_provider', 'id', [
                'update' => 'cascade',
                'delete' => 'restrict'
            ])
            ->create();
    }
}

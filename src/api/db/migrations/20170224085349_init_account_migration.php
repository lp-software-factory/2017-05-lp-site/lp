<?php

use Phinx\Migration\AbstractMigration;

class InitAccountMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('account')
            ->addColumn('sid', 'string', [
                'null' => false,
                'limit' => 12,
            ])
            ->addColumn('version','string', [
                'null' => false,
                'limit' => 32,
                'default' => '1.0.0',
            ])
            ->addColumn('date_created_at', 'datetime', [
                'null' => false,
            ])
            ->addColumn('last_updated_on', 'datetime', [
                'null' => false,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('app_access', 'json', [
                'default' => '{}',
            ])
            ->addColumn('email', 'string', [
                'limit' => 256,
            ])
            ->addColumn('phone', 'string', [
                'limit' => 32,
                'null' => true,
            ])
            ->addColumn('password', 'string', [
                'limit' => 127,
            ])
            ->addColumn('locale_id', 'integer', [
                'null' => false
            ])
            ->addForeignKey('locale_id', 'locale', 'id', [
                'update' => 'cascade',
                'delete' => 'restrict',
            ])
            ->addIndex('sid', ['unique' => true])
            ->addIndex('email', ['unique' => true])
            ->create();

        $this->table('sign_up_request')
            ->addColumn('version','string', [
                'null' => false,
                'limit' => 32,
                'default' => '1.0.0',
            ])
            ->addColumn('sid', 'string', [
                'null' => false,
                'limit' => 12,
            ])
            ->addColumn('date_created_at', 'datetime', [
                'null' => false,
            ])
            ->addColumn('last_updated_on', 'datetime', [
                'null' => false,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('accept_token', 'string', [
                'limit' => 32,
            ])
            ->addColumn('email', 'string', [
                'limit' => 127,
            ])
            ->addColumn('password', 'string', [
                'limit' => 256,
            ])
            ->addColumn('first_name', 'string', [
                'limit' => 32,
            ])
            ->addColumn('last_name', 'string', [
                'limit' => 32,
            ])
            ->addColumn('is_company_profile', 'boolean')
            ->addColumn('company_name', 'string', [
                'limit' => 256,
                'null' => true,
            ])
            ->addColumn('phone', 'string', [
                'limit' => 32,
                'null' => true,
            ])
            ->addIndex('sid', ['unique' => true])
            ->create();
    }
}

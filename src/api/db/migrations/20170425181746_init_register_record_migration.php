<?php

use Phinx\Migration\AbstractMigration;

class InitRegisterRecordMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('register_record_group')
            ->addColumn('sid', 'string', [
                'null' => false,
                'limit' => 12,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('date_created_at', 'datetime', [
                'null' => false,
            ])
            ->addColumn('last_updated_on', 'datetime', [
                'null' => false,
            ])
            ->addColumn('version', 'string', [
                'limit' => 32,
                'null' => false,
                'default' => '1.0.0',
            ])
            ->addColumn('app_name', 'string', [
                'null' => false,
                'limit' => 256,
            ])
            ->create();

        $this->table('register_record')
            ->addColumn('sid', 'string', [
                'null' => false,
                'limit' => 12,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('date_created_at', 'datetime', [
                'null' => false,
            ])
            ->addColumn('last_updated_on', 'datetime', [
                'null' => false,
            ])
            ->addColumn('version', 'string', [
                'limit' => 32,
                'null' => false,
                'default' => '1.0.0',
            ])
            ->addColumn('register_record_group_id', 'integer', [
                'null' => false,
            ])
            ->addColumn('key', 'string', [
                'limit' => 256,
                'null' => false,
            ])
            ->addColumn('value', 'text', [
                'null' => false,
            ])
            ->addColumn('exports_to_frontend', 'boolean', [
                'null' => false,
                'default' => false,
            ])
            ->addIndex('key', [
                'unique' => true,
            ])
            ->addForeignKey('register_record_group_id', 'register_record_group', 'id', [
                'update' => 'cascade',
                'delete' => 'cascade',
            ])
            ->create();
    }
}

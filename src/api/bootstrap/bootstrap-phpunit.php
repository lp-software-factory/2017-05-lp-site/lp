<?php
namespace ZEA2;

use Lp\Domain\LpDomainBundle;
use Lp\REST\LpRESTBundle;
use ZEA2\Domain\ZEA2DomainBundle;
use ZEA2\Platform\Constants\Environment;
use ZEA2\Platform\ZEA2PlatformBundle;
use ZEA2\REST\ZEA2RESTBundle;

$appBuilder = require __DIR__.'/../src/ZEA2/src/Platform/resources/bootstrap/bootstrap.php';

return $appBuilder([
    'environment' => Environment::TESTING,
    'use_cache' => false,
    'bundles' => [
        new ZEA2PlatformBundle(),
        new ZEA2DomainBundle(),
        new ZEA2RESTBundle(),
        new LpDomainBundle(),
        new LpRESTBundle(),
    ],
]);
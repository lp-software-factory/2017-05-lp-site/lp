<?php
namespace ZEA2;

use Lp\AppSite\LpAppSiteBundle;
use Lp\Domain\LpDomainBundle;
use Lp\REST\LpRESTBundle;
use ZEA2\Domain\ZEA2DomainBundle;
use ZEA2\Platform\ZEA2PlatformBundle;

$appBuilder = require __DIR__.'/../src/ZEA2/src/Platform/resources/bootstrap/bootstrap.php';

return $appBuilder([
    'bundles' => [
        new ZEA2PlatformBundle(),
        new ZEA2DomainBundle(),
        new LpDomainBundle(),
        new LpRESTBundle(),
        new LpAppSiteBundle(),
    ],
]);
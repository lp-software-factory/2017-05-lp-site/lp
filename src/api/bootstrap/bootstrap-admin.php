<?php
namespace ZEA2;

use Lp\AdminSite\LpAdminSiteBundle;
use Lp\Domain\LpDomainBundle;
use Lp\REST\LpRESTBundle;
use ZEA2\AdminSite\ZEA2AdminSiteBundle;
use ZEA2\Domain\ZEA2DomainBundle;
use ZEA2\Platform\ZEA2PlatformBundle;
use ZEA2\REST\ZEA2RESTBundle;

$appBuilder = require __DIR__.'/../src/ZEA2/src/Platform/resources/bootstrap/bootstrap.php';


return $appBuilder([
    'bundles' => [
        new ZEA2PlatformBundle(),
        new ZEA2DomainBundle(),
        new ZEA2AdminSiteBundle(),
        new LpDomainBundle(),
        new LpRESTBundle(),
        new LpAdminSiteBundle(),
    ],
]);
<?php
namespace ZEA2;

use Lp\Domain\LpDomainBundle;
use Lp\REST\LpRESTBundle;
use Lp\Stage\LpStageBundle;
use ZEA2\Domain\ZEA2DomainBundle;
use ZEA2\Platform\ZEA2PlatformBundle;
use ZEA2\REST\ZEA2RESTBundle;
use ZEA2\Stage\ZEA2StageBundle;

$appBuilder = require __DIR__.'/../src/ZEA2/src/Platform/resources/bootstrap/bootstrap.php';

return $appBuilder([
    'bundles' => [
        new ZEA2PlatformBundle(),
        new ZEA2DomainBundle(),
        new ZEA2RESTBundle(),
        new ZEA2StageBundle(),
        new LpDomainBundle(),
        new LpRESTBundle(),
        new LpStageBundle(),
    ],
]);
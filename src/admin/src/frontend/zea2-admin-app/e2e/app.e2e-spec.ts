import { ZEA2AdminPage } from './app.po';

describe('zea2-admin-app', () => {
  let page: ZEA2AdminPage;

  beforeEach(() => {
    page = new ZEA2AdminPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});

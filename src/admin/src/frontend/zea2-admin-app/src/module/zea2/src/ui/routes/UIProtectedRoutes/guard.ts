import {CanActivate, Router} from '@angular/router';

import {JWTService} from '../../../../../zea2-common/src/_module/auth/service/JWTService';
import {Injectable} from '@angular/core';

@Injectable()
export class UIProtectedRoutesGuard implements CanActivate
{
    constructor(
        private auth: JWTService,
        private router: Router,
    ) {}

    canActivate(): boolean {
        if (! (this.auth.hasJWT() && this.auth.isAdmin())) {
            this.auth.clearJWT();
            this.router.navigate(['/admin/public']);

            return false;
        } else {
            return true;
        }
    }
}

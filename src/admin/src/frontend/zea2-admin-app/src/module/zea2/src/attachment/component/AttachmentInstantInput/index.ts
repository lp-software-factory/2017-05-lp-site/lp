import {Component, Input, Output, EventEmitter} from "@angular/core";

import {LoadingStatus, LoadingManager} from "../../../../../zea2-common/src/_module/common/classes/LoadingStatus";
import {basename} from "../../../../../zea2-common/src/_module/common/functions/basename";
import {AttachmentEntity} from "../../../../../../definitions/src/zea2/src/definitions/attachment/entity/AttachmentEntity";
import {AttachmentRESTService} from "../../../../../../definitions/src/zea2/src/services/attachment/AttachmentRESTService";

@Component({
    selector: 'zea2-attachment-instant-input',
    templateUrl: './template.html',
    styleUrls: [
      './style.shadow.scss'
    ]
})
export class AttachmentInstantInput
{
    @Output() valueChange = new EventEmitter<any>();
    @Output('on-upload') onUploadEvent: EventEmitter<LoadingStatus> = new EventEmitter<LoadingStatus>();
    @Output('on-upload-fail') onUploadFailEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('on-upload-complete') onUploadCompleteEvent: EventEmitter<any> = new EventEmitter<any>();
    @Output('on-delete-value') onDeleteValueEvent: EventEmitter<any> = new EventEmitter<any>();
    @Output('on-delete-attachment') onDeleteAttachmentEvent: EventEmitter<any> = new EventEmitter<any>();

    @Input('allow_multiple') allow_multiple: boolean = false;
    @Input('readonly') readonly: boolean = false;

    @Input()
    get value() {
        return this._value;
    }

    set value(val) {
        this._value = val;
        this.valueChange.emit(val);
    }

    private _revert: AttachmentEntity<any>;
    private _value: any[] = [];
    public status: LoadingManager = new LoadingManager();

        constructor(
        private rest: AttachmentRESTService,
    ) {}

    onFileChange($event) {
        if($event.target.files.length) {
            this.upload($event.target.files);
        }
    }

    getFileName(file: AttachmentEntity<any>): string {
        return basename(file.link.url)
    }

    getDateUploaded(file: AttachmentEntity<any>): string {
        return file.date_attached_on;
    }

    downloadFile(file: AttachmentEntity<any>) {
        return file.link.url;
    }

    upload(files: Blob[]) {
        let loading = this.status.addLoading();

        this.onUploadEvent.emit(loading);

        if(this.allow_multiple) {
            let files_ids: number[] = [];

            if(this.value) {
                this.destroy();
            }

            for(let i = 0; i < files.length; i++) {
                this.rest.upload(files[i]).last().subscribe(
                    response => {
                        this.value.push(response['entity']);
                        files_ids.push(response['entity']);
                        if(i === files.length - 1){
                            this.onUploadCompleteEvent.emit(files_ids);
                        }
                    },
                    error => {
                        this.onUploadFailEvent.emit();

                        loading.is = false;
                    }
                )
            }
        } else {
            this.rest.upload(files[0]).last().subscribe(
                response => {
                    if(this.value) {
                        this.destroy();
                    }
                    this.value.push(response['entity']);
                    this.onUploadCompleteEvent.emit(response['entity']);
                },
                error => {
                    this.onUploadFailEvent.emit();

                    loading.is = false;
                }
            )
        }
    }

    destroy() {
        if(this.value) {
            this.value = [];
            this.onDeleteValueEvent.emit();
        }
    }

    revert(file) {
        if(this.value) {
            if(this.allow_multiple && this.value.length > 0) {
                this.value = this.value.filter(entity => {
                    return file.id !== entity.id;
                });
                this.onDeleteAttachmentEvent.emit(file);
            } else {
                this.destroy();
            }
        }
    }
}

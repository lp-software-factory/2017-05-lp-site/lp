import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';

import {ZEA2Frontend} from '../../../../../definitions/src/zea2/src/definitions/frontend/entity/ZEA2Frontend';
import {FrontendService} from "../../../../zea2-common/src/_module/frontend/FrontendService";

@Injectable()
export class FrontendServiceResolver<T extends ZEA2Frontend> implements Resolve<any>
{
    public static GLOBAL_EXPORTS = 'app-frontend';

    constructor(
        private frontend: FrontendService<T>,
    ) {}

    //noinspection TypeScriptUnresolvedVariable
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any>|Promise<any>|any {
        return Observable.create(observer => {
            if (this.tryWindow()) {
                observer.next();
                observer.complete();
            } else {
                this.frontend.fetch().last().subscribe(response => {
                    this.frontend.emit(response);

                    observer.next();
                    observer.complete();
                });
            }
        });
    }

    private tryWindow(): boolean {
        if (window && window[FrontendServiceResolver.GLOBAL_EXPORTS]) {
            this.frontend.emit(window[FrontendServiceResolver.GLOBAL_EXPORTS]);

            return true;
        } else {
            return false;
        }
    }
}

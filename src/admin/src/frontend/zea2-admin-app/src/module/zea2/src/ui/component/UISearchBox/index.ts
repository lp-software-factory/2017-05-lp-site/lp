import {Component} from '@angular/core';
import {UIFilterService} from "../../service/UIFilterService";
import {TranslateService} from '@ngx-translate/core';
import {Subscription} from "rxjs/Subscription";

@Component({
    selector: 'zea2-ui-search-box',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class UISearchBox
{
    private resetFilterSubscription: Subscription;
    public filterString = '';

    constructor(
        public uiFilter: UIFilterService,
        private translate: TranslateService,
    ) {}

    ngOnInit() {
        this.resetFilterSubscription = this.uiFilter.resetSubject.subscribe(next => {
            this.filterString = '';
        })
    }

    ngOnDestroy() {
        this.resetFilterSubscription.unsubscribe();
    }
}

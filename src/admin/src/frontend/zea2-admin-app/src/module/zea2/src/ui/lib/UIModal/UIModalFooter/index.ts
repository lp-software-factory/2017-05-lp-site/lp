import {Component} from '@angular/core';

import {ModalControl} from '../../../../../../zea2-common/src/_module/common/classes/ModalControl';

@Component({
    selector: 'zea2-ui-modal-footer',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class UIModalFooter
{
    constructor(
        public controls: ModalControl<any>,
    ) {}
}
import {Component, Injectable} from '@angular/core';

import {ModalControl} from '../../../../../zea2-common/src/_module/common/classes/ModalControl';

@Injectable()
export class LocaleModalControlCreate<T> extends ModalControl<T> {}

@Component({
    selector: 'zea2-locale-modal-create',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ],
    providers: [
        {
            provide: ModalControl,
            useExisting: LocaleModalControlCreate,
        },
    ]
})
export class LocaleModalCreate
{}
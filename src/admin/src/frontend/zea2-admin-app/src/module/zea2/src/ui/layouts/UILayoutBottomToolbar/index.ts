import {Component} from '@angular/core';

/**
 * Usage:
 *  zea2-ui-layout-bottom-toolbar
 *      div(ui-top)
 *          // ...
 *      div(ui-bottom)
 *          // ...
 */
@Component({
    selector: 'zea2-ui-layout-bottom-toolbar',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class UILayoutBottomToolbar
{
}
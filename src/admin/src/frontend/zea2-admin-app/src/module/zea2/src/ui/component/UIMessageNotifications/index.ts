import {Component} from '@angular/core';

import {UIMessageService, UIMessage, UI_MESSAGE_LEVEL} from '../../service/UIMessageService';

@Component({
	selector: 'zea2-ui-message-notifications',
	templateUrl: './template.html',
	styleUrls: [
		'./style.shadow.scss',
	]
})
export class UIMessageNotifications
{
	constructor(private service: UIMessageService) {
	}

	getMessages(): UIMessage[] {
		return this.service.list().slice(0, 5);
	}

	remove(message: UIMessage) {
		this.service.remove(message.id);
	}

	getNgClass(message: UIMessage) {
		let css = {};

		css[this.getCSSClassName(message)] = true;

		return css;
	}

	getCSSClassName(message: UIMessage): string {
		switch (message.level) {
			default:
				return '';
			case UI_MESSAGE_LEVEL.Primary:
				return 'is-primary';
			case UI_MESSAGE_LEVEL.Danger:
				return 'is-danger';
			case UI_MESSAGE_LEVEL.Info:
				return 'is-info';
			case UI_MESSAGE_LEVEL.Success:
				return 'is-success';
			case UI_MESSAGE_LEVEL.Warning:
				return 'is-warning';
		}
	}
}
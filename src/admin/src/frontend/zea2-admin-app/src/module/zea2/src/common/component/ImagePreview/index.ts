import {Component, Input, Output, ViewChild, EventEmitter, ElementRef, OnInit, OnDestroy} from "@angular/core";

@Component({
    selector: 'zea2-image-preview',
    templateUrl: './template.html',
    styleUrls: [
      './style.shadow.scss'
    ]
})
export class ImagePreview implements OnInit, OnDestroy
{
    static PADDING = 15;

    @Input('image') image: string;
    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();
    @ViewChild('screen') screen: ElementRef;

    private interval: any;
    private imgSizes: { width: number, height: number };

    ngOnInit(): void {
        this.interval = setInterval(() => {
            this.updateContainer();
        }, 1000);
    }

    ngOnDestroy(): void {
        clearInterval(this.interval);
    }

    updateContainer() {
        let containerSizes: { width: number, height: number } = {
            width: document.body.clientWidth,
            height: document.body.clientHeight,
        };
    }

    close() {
        this.closeEvent.emit();
    }
}

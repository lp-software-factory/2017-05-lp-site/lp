import {Component, Input, Output, EventEmitter} from "@angular/core";

import {basename} from "../../../../../zea2-common/src/_module/common/functions/basename";
import {ModalControl} from "../../../../../zea2-common/src/_module/common/classes/ModalControl";
import {UI_MESSAGE_LEVEL, UIMessageService} from "../../../ui/service/UIMessageService";
import {TranslateService} from "@ngx-translate/core";
import {AttachmentEntity} from "../../../../../../definitions/src/zea2/src/definitions/attachment/entity/AttachmentEntity";

@Component({
    selector: 'zea2-file-input',
    templateUrl: './template.html',
    styleUrls: [
      './style.shadow.scss'
    ]
})
export class FileInput
{
    private _value: File;
    private enabled: boolean = true;
    private lastFileName: string = '';
    private lastObjectUrl: string;

    public modal: ModalControl<any> = new ModalControl();

    @Input('preview') preview: any;
    @Input('readonly') readonly: boolean = false;
    @Input('allowed') allowed: string[] = ['image/jpeg', 'image/png', 'image/gif', 'image/svg+xml', 'application/pdf', '.xls', '.doc'];
    @Output('change-file') changeEvent: EventEmitter<File> = new EventEmitter<File>();
    @Output('on-delete-attachment') onDeleteAttachmentEvent: EventEmitter<any> = new EventEmitter<any>();
    @Output() valueChange = new EventEmitter<File>();

    @Input()
    get value(): File {
        return this._value;
    }

    set value(val: File) {
        this._value = val;
        this.valueChange.emit(val);
        this.changeEvent.emit(val);
    }

    constructor(
        private messages: UIMessageService,
        private translate: TranslateService,
    ) {}

    onFileChange($event) {
        if($event.target.files.length) {
            let file: File = $event.target.files[0];

            if(!~this.allowed.indexOf(file.type)) {
                this.messages.push(UI_MESSAGE_LEVEL.Danger, "common.image-file-input.not-an-correct-file", true);

                this.enabled = false;

                setTimeout(() => {
                    this.enabled = true;
                }, 500);
            }

            this.value = file;
            this.lastFileName = file.name;
            this.lastObjectUrl = undefined;
        }
    }

    downloadFile(file: AttachmentEntity<any>) {
        return file.link.url;
    }

    getFileName(file: AttachmentEntity<any>): string {
        return basename(file.link.url);
    }

    getDateUploaded(file: AttachmentEntity<any>): string {
        return file.date_attached_on;
    }

    destroy() {
        if(this.preview) {
            this.onDeleteAttachmentEvent.emit();
        }
    }
}

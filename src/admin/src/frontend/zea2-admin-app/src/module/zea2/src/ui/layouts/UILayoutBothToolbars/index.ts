import {Component} from '@angular/core';

@Component({
    selector: 'zea2-ui-layout-both-toolbars',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class UILayoutBothToolbars
{}
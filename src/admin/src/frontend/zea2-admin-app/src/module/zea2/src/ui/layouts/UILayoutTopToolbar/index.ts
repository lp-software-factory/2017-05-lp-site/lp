import {Component} from '@angular/core';

@Component({
    selector: 'zea2-ui-layout-top-toolbar',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class UILayoutTopToolbar
{}
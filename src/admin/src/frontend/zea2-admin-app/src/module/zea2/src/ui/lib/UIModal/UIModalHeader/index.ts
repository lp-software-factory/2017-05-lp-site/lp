import {Component, Input} from '@angular/core';

import {ModalControl} from '../../../../../../zea2-common/src/_module/common/classes/ModalControl';
import {UIHotkeyService} from "../../../service/UIHotkeyService";
import {UICRUDHotkeyEventType} from "../../UIHotkey/options";

@Component({
    selector: 'zea2-ui-modal-header',
    host: {'(window:keyup)': 'uiHotkey.hotkey($event)'},
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ],
})
export class UIModalHeader
{
    @Input('with-close-button') withCloseButton: boolean = true;

    constructor(
        public controls: ModalControl<any>,
        private uiHotkey: UIHotkeyService
    ) {}

    ngOnInit() {
        if(this.withCloseButton){
            this.uiHotkey.addHotkeys([{
                eventType: UICRUDHotkeyEventType.keyup,
                keyCode: 27,
                func: () => {
                    this.controls.close()
                }
            }])
        }
    }

    ngOnDestroy() {
        this.uiHotkey.resetHotkeys();
    }
}
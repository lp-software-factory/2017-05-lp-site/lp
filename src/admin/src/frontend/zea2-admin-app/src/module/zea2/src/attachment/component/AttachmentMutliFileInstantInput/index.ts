import {Component, Input, Output, EventEmitter} from "@angular/core";

import {AttachmentEntity} from "../../../../../../definitions/src/zea2/src/definitions/attachment/entity/AttachmentEntity";
import {AttachmentRESTService} from "../../../../../../definitions/src/zea2/src/services/attachment/AttachmentRESTService";
import {ImageAttachmentMetadata} from "../../../../../../definitions/src/zea2/src/definitions/attachment/entity/metadata/ImageAttachmentMetadata";

@Component({
	selector: 'zea2-attachment-multi-file-instant-input',
	templateUrl: './template.html',
	styleUrls: [
		'./style.shadow.scss'
	]
})
export class AttachmentMultiFileInstantInput
{
	@Output() valueChange = new EventEmitter<AttachmentEntity<any>[]>();
	@Output('on-upload-fail') onUploadFailEvent: EventEmitter<void> = new EventEmitter<void>();
	@Output('on-upload-complete') onUploadCompleteEvent: EventEmitter<AttachmentEntity<any>> = new EventEmitter<AttachmentEntity<any>>();
	@Output('on-delete') onDeleteEvent: EventEmitter<AttachmentEntity<any>> = new EventEmitter<AttachmentEntity<any>>();

	@Input()
	get value() {
		return this._value;
	}

	set value(val) {
		this._value = val;
		this.valueChange.emit(val);
	}

	private _value: AttachmentEntity<ImageAttachmentMetadata>[];
	private tempAttachment: AttachmentEntity<ImageAttachmentMetadata>;

	constructor(
		private rest: AttachmentRESTService
	) {}

	ngOnInit() {
	    this.addNewAttachment();
    }

	addNewAttachment() {
		this.value.push(this.tempAttachment);
	}

	attachAttachment($event, old_attachment) {
		if(old_attachment !== undefined) {
			this.value = this.value.map(entity => {
				if(old_attachment.id === entity.id) {
					entity = JSON.parse(JSON.stringify($event));
				}
				return entity;
			})
		} else {
			this.value.splice(this.value.length - 1, 1, $event);
		}

		this.onUploadCompleteEvent.emit($event);
	}

	deleteAttachment($event) {
		this.value = this.value.filter(entity => {
			return $event.id !== entity.id;
		});

		this.onDeleteEvent.emit($event);
	}
}

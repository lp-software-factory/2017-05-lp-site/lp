import {Component, Injectable} from '@angular/core';

import {ModalControl} from '../../../../../zea2-common/src/_module/common/classes/ModalControl';
import {LocaleEntity} from '../../../../../../definitions/src/zea2/src/definitions/locale/definitions/LocaleEntity';

@Injectable()
export class LocaleModalControlEdit<T> extends ModalControl<T> {}

@Component({
    selector: 'zea2-locale-modal-edit',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ],
    providers: [
        {
            provide: ModalControl,
            useExisting: LocaleModalControlEdit,
        },
    ]
})
export class LocaleModalEdit
{
    constructor(
        public controls: LocaleModalControlEdit<LocaleEntity>,
    ) {}
}
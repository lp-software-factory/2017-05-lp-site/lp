import {Component, Input} from '@angular/core';

import {LocaleModalControlRemove} from '../../modal/LocaleModalRemove/index';
import {LocaleEntity} from '../../../../../../definitions/src/zea2/src/definitions/locale/definitions/LocaleEntity';
import {LocaleRESTService} from '../../../../../../definitions/src/zea2/src/services/locale/LocaleRESTService';
import {LocaleRepositoryService} from '../../service/LocaleRepositoryService';
import {BehaviorSubject, Subscription} from 'rxjs';
import {Observable} from "rxjs/Observable";

@Component({
    selector: 'zea2-locale-form-remove',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class LocaleFormRemove
{
    private current: number[] = [];
    private lastSubscription: Subscription;

    @Input('original') original: BehaviorSubject<LocaleEntity>[];

    constructor(
        private rest: LocaleRESTService,
        private service: LocaleRepositoryService,
        private controls: LocaleModalControlRemove<LocaleEntity>,
    ) {}

    ngOnChanges(): void {
        if (this.original) {
            if (this.lastSubscription) {
                this.lastSubscription.unsubscribe();
            }

            this.original.map(orig => {
                orig.subscribe(n => {
                    this.current.push(n.id);
                });
            });
        }
    }

    ngOnDestroy(): void {
        if (this.lastSubscription) {
            this.lastSubscription.unsubscribe();
        }
    }

    submit(): void {
        let loading = this.controls.status.addLoading();

        let observableRequest = [];

        this.current.forEach(sub => {
            observableRequest.push(this.rest.delete(sub));
            observableRequest[observableRequest.length - 1].subscribe(this.service.repository.remove(sub));
        });


        Observable.forkJoin(observableRequest).subscribe(
            response => {
                loading.is = false;
                this.current = [];
                this.close();
            },
            error => {
                loading.is = false;
            }
        );
    }

    close(): void {
        this.controls.close();
    }
}
import {Component, Input, Output, EventEmitter} from "@angular/core";

import {basename} from "../../../../../zea2-common/src/_module/common/functions/basename";
import {ModalControl} from "../../../../../zea2-common/src/_module/common/classes/ModalControl";
import {UI_MESSAGE_LEVEL, UIMessageService} from "../../../ui/service/UIMessageService";
import {TranslateService} from "@ngx-translate/core";

@Component({
    selector: 'zea2-image-file-input',
    templateUrl: './template.html',
    styleUrls: [
      './style.shadow.scss'
    ]
})
export class ImageFileInput
{
    private _value: File;
    public enabled: boolean = true;
    private lastFileName: string = '';
    private lastObjectUrl: string;

    public modal: ModalControl<any> = new ModalControl();

    @Input('preview') preview: string;
    @Input('readonly') readonly: boolean = false;
    @Input('allowed') allowed: string[] = ['image/jpeg', 'image/png', 'image/gif', 'image/svg+xml'];
    @Output('change-file') changeEvent: EventEmitter<File> = new EventEmitter<File>();
    @Output() valueChange = new EventEmitter<File>();

    @Input()
    get value(): File {
        return this._value;
    }

    set value(val: File) {
        this._value = val;
        this.valueChange.emit(val);
        this.changeEvent.emit(val);
    }

    constructor(
        private messages: UIMessageService,
        private translate: TranslateService,
    ) {}

    onFileChange($event) {
        if($event.target.files.length) {
            let file: File = $event.target.files[0];

            if(!~this.allowed.indexOf(file.type)) {
                this.messages.push(UI_MESSAGE_LEVEL.Danger, "common.image-file-input.not-an-image", true);

                this.enabled = false;

                setTimeout(() => {
                    this.enabled = true;
                }, 500);
            }

            this.value = file;
            this.lastFileName = file.name;
            this.lastObjectUrl = undefined;
        }
    }

    getTitle(): string {
        if(this.value instanceof Blob) {
            return basename(this.lastFileName);
        }else if(this.preview){
            return basename(this.preview);
        }else{
            //return translate string
        }
    }

    hasPreview(): boolean {
        return !!this.preview || !!this.value;
    }

    getPreview(): string {
        if(this.value instanceof Blob) {
            if(!this.lastObjectUrl) {
                this.lastObjectUrl = URL.createObjectURL(this.value);
            }

            return this.lastObjectUrl;
        }else{
            return this.preview;
        }
    }
}

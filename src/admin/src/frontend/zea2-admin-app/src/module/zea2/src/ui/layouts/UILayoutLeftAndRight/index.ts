import {Component} from '@angular/core';

@Component({
    selector: 'zea2-ui-layout-left-and-right',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class UILayoutLeftAndRight
{}
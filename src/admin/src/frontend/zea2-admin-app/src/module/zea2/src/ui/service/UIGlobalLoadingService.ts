import {Injectable} from '@angular/core';

import {LoadingManager} from '../../../../zea2-common/src/_module/common/classes/LoadingStatus';

@Injectable()
export class UIGlobalLoadingService extends LoadingManager {}
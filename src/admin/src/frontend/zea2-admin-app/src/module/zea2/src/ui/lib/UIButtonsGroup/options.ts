export interface UICRUDButton
{
    id: string;
    title: {
        value: string,
        translate: boolean,
    };
    icon?: string|undefined;
    level: UICRUDButtonVariant;
    isActive: () => boolean;
    click: () => void;
}

export enum UICRUDButtonVariant
{
    Default = <any>'default',
    Primary = <any>'primary',
    White = <any>'white',
    Light = <any>'light',
    Dark = <any>'dark',
    Black = <any>'black',
    Link = <any>'link',
    Info = <any>'info',
    Success = <any>'success',
    Warning = <any>'warning',
    Danger = <any>'danger',
}
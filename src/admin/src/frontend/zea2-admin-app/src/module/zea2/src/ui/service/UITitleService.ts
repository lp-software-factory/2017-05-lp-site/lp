import {Injectable} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Injectable()
export class UITitleService
{
    public static DEFAULT_PREFIX = '[ZEA2]';

    private prefix: string = UITitleService.DEFAULT_PREFIX;
    private segments: TitleSegment[] = [];
    private current: string = '';

    constructor(
        private translate: TranslateService,
    ) {}

    public usePrefix(prefix: string, options: { translate?: boolean }): void {
        this.prefix = prefix;

        if (options.translate) {
            this.translate.get(prefix).subscribe(n => this.prefix = n);
        }
    }

    public getSegments(): TitleSegment[] {
    	return this.segments;
    }

    public setTitle(segments: TitleSegment[]): void {
	    let title = [];
	    this.segments = JSON.parse(JSON.stringify(segments));

	    title = segments.map(segment => {
		    if (segment.translate) {
			    this.translate.get(segment.title).subscribe(n => segment.title = n);
		    }
		    return segment.title;
	    });

	    this.current = title.join().replace(/,/g, ' / ');
    }

    public getTitle(): string {
        if (this.prefix && this.prefix.length) {
            return this.prefix + ' ' + this.current;
        } else {
            return this.current;
        }
    }

    public getTitleWithoutPrefix(): string {
        return this.current;
    }
}

export interface TitleSegment {
	title: string, translate?: boolean
}
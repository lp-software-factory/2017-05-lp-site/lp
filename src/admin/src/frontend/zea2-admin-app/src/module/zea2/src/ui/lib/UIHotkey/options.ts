export interface UICRUDHotkey {
    eventType: UICRUDHotkeyEventType;
    keyCode: number;
    func: any;
}

export enum UICRUDHotkeyEventType {
    keydown = <any>'keydown',
    keyup = <any>'keyup'
}
import {Component, Input, Output, EventEmitter, Injectable} from "@angular/core";

import {LoadingStatus, LoadingManager} from "../../../../../zea2-common/src/_module/common/classes/LoadingStatus";
import {basename} from "../../../../../zea2-common/src/_module/common/functions/basename";
import {AttachmentEntity} from "../../../../../../definitions/src/zea2/src/definitions/attachment/entity/AttachmentEntity";
import {AttachmentRESTService} from "../../../../../../definitions/src/zea2/src/services/attachment/AttachmentRESTService";
import {ImageAttachmentMetadata} from "../../../../../../definitions/src/zea2/src/definitions/attachment/entity/metadata/ImageAttachmentMetadata";

@Component({
    selector: 'zea2-attachment-image-instant-input',
    templateUrl: './template.html',
    styleUrls: [
      './style.shadow.scss'
    ]
})
export class AttachmentImageInstantInput
{
    @Output() valueChange = new EventEmitter<AttachmentEntity<any>>();
    @Output('on-upload') onUploadEvent: EventEmitter<LoadingStatus> = new EventEmitter<LoadingStatus>();
    @Output('on-upload-fail') onUploadFailEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('on-upload-complete') onUploadCompleteEvent: EventEmitter<AttachmentEntity<any>> = new EventEmitter<AttachmentEntity<any>>();
    @Output('on-delete') onDeleteEvent: EventEmitter<AttachmentEntity<any>> = new EventEmitter<AttachmentEntity<any>>();

    @Input()
    get value() {
        return this._value;
    }

    set value(val) {
        this._value = val;
        this.valueChange.emit(val);
    }

    private _revert: AttachmentEntity<ImageAttachmentMetadata>;
    private _value: AttachmentEntity<ImageAttachmentMetadata>;
    public status: LoadingManager = new LoadingManager();

    constructor(
        private rest: AttachmentRESTService
    ) {}

    isUploaded(): boolean {
        return this.value !== undefined;
    }

    onFileChange($event: File) {
	    this.upload($event);
    }

    getPreview(): string {
        if(this.value) {
            return this.value.link.url;
        }else{
            return undefined;
        }
    }

    getFileName(): string {
        if(this.isUploaded()) {
            return basename(this.value.link.url);
        }else{
            return '<NOT UPLOADED>';
        }
    }

    getDateUploaded(): string {
        if(this.isUploaded()) {
            return this.value.date_created_on;
        }else{
            return '<NOT UPLOADED>';
        }
    }

    upload(file: File) {
        let loading = this.status.addLoading();

        this.onUploadEvent.emit(loading);

        this.rest.upload(file).subscribe(
            response => {
                if(this.value) {
                    this.destroy();
                }

                if (response.hasOwnProperty('entity')) {
                    this.value = response['entity'];
                    this.onUploadCompleteEvent.emit(response['entity']);
                }

                loading.is = false;
            },
            error => {
                this.onUploadFailEvent.emit();

                loading.is = false;
            }
        )
    }

    destroy() {
        if(this.value) {
            this.onDeleteEvent.emit(this.value);
            this._revert = this.value;
        }

        this.value = undefined;
    }

    revert() {
        if(this.revert) {
            this.value = this._revert;
            this.revert = undefined;
        }
    }
}

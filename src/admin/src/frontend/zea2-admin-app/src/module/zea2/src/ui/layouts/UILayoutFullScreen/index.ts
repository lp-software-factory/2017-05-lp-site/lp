import {Component} from '@angular/core';

@Component({
    selector: 'zea2-ui-layout-full-screen',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class UILayoutFullScreen
{}
import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {JWTService} from '../../../../../zea2-common/src/_module/auth/service/JWTService';

@Component({
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class UIProtectedRoutes
{
    constructor(
        private auth: JWTService,
        private router: Router,
    ) {
    }
}

import {Component} from '@angular/core';

@Component({
	selector: 'zea2-common-progress-lock',
	templateUrl: './template.html',
	styleUrls: [
		'./style.shadow.scss',
	]
})
export class ProgressLock
{
}
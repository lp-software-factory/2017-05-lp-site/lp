import {Component} from '@angular/core';

import {LocaleRepositoryService} from '../../service/LocaleRepositoryService';
import {LocaleRESTService} from '../../../../../../definitions/src/zea2/src/services/locale/LocaleRESTService';
import {CreateLocaleRequest} from '../../../../../../definitions/src/zea2/src/definitions/locale/paths/create';
import {ModalControl} from '../../../../../zea2-common/src/_module/common/classes/ModalControl';
import {UIMessageService, UI_MESSAGE_LEVEL} from '../../../ui/service/UIMessageService';

export interface LocaleFormCreateModel
{
    region: string;
    language: string;
}

@Component({
    selector: 'zea2-locale-form-create',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class LocaleFormCreate
{
    public model: LocaleFormCreateModel = {
        region: '',
        language: '',
    };

    constructor(
        private rest: LocaleRESTService,
        private service: LocaleRepositoryService,
        private controls: ModalControl<any>,
        private messages: UIMessageService,
    ) {}

    createRequest(): CreateLocaleRequest {
        return {
            region: this.model.region,
            language: this.model.language,
        };
    }

    submit(): void {
        let loading = this.controls.status.addLoading();

        this.rest.create(this.createRequest()).subscribe(
            next => {
                this.service.repository.set(next.locale);
            },
            error => {
                this.messages.push(UI_MESSAGE_LEVEL.Warning, error);

                loading.is = false;
            },
            () => {
                loading.is = false;

                this.controls.close();
            }
        );
    }

    close() {
        this.controls.close();
    }
}

import {Component} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class UINotFoundRoute
{
    constructor(
        private activatedRoute: ActivatedRoute
    ) {
        console.log('404 NOT FOUND', activatedRoute.snapshot.url, activatedRoute.snapshot.params);
        console.log(activatedRoute.snapshot);

    }
}
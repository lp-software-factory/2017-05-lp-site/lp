import {Injectable} from '@angular/core';
import {Subject, Observable} from "rxjs";

@Injectable()
export class UIFilterService
{
  constructor() {}

  public filterSubject: Subject<string> = new Subject<string>();
  public resetSubject: Subject<string> = new Subject<string>();

  public publish(filterString) {
    this.filterSubject.next(filterString);
  }
}

import {Routes} from '@angular/router';

export abstract class AbstractUIMainMenuDefinition {
    abstract getDefinition(): UIMainMenuDefinition;
}

export interface UIMainMenuDefinition
{
    groups: UIMainMenuGroupDefinition[];
}

export interface UIMainMenuGroupDefinition
{
    title: {
        value: string,
        translate: true,
    };
    items: UIMainMenuItemDefinition[];
}

export interface UIMainMenuItemDefinition
{
    icon: string;
    title: {
        value: string,
        translate: true,
    };
    route: {
        url: Routes,
        exactly: boolean,
    };
}

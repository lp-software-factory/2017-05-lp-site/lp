import {Component} from '@angular/core';
import {Router} from '@angular/router';

import {LoadingManager} from '../../../../../zea2-common/src/_module/common/classes/LoadingStatus';
import {withStatusCode} from '../../../../../zea2-common/src/_module/common/service/RESTAdapter';
import {JWTService} from '../../../../../zea2-common/src/_module/auth/service/JWTService';
import {AuthRESTService} from '../../../../../../definitions/src/zea2/src/services/auth/AuthRESTService';

@Component({
    selector: 'zea2-ui-sign-in-form',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class SignInForm
{
    public lastError: string;
    public lastErrorType: ERROR_TYPES = ERROR_TYPES.UNKNOWN;

    public locked = false;
    public status: LoadingManager = new LoadingManager();

    public model: SignInFormModel = {
        email: '',
        password: ''
    };

    constructor(
        private rest: AuthRESTService,
        private auth: JWTService,
        private router: Router,
    ) {}

    isValid(): boolean {
        return this.model.email.length > 0
            && this.model.password.length > 0;
    }

    isLoading(): boolean {
        return this.status.isLoading();
    }

    areInputsLocked(): boolean {
        return this.isLoading() || this.locked;
    }

    submit(): void {
        if (! this.isValid()) return;

        let loading = this.status.addLoading();

        this.rest.signIn({ email: this.model.email, password: this.model.password }).subscribe(
            next => {
                loading.is = false;

                this.lastError = undefined;

                if (!~next.token.account.app_access.indexOf('admin')) {
                    this.lastError = 'Account is not admin';
                    this.lastErrorType = ERROR_TYPES.NOT_ADMIN;
                } else {
                    this.locked = true;

                    this.auth.setJWT(next.token);
                    this.router.navigate(['/admin/protected']);
                }
            },
            error => {
                loading.is = false;

                this.lastError = '' + error;

                withStatusCode(404, error, () => this.lastErrorType = ERROR_TYPES.ACCOUNT_NOT_FOUND);
                withStatusCode(403, error, () => this.lastErrorType = ERROR_TYPES.PASSWORD_NOT_MATCH);
            },
        );
    }
}

interface SignInFormModel
{
    email: string;
    password: string;
}

enum ERROR_TYPES {
    UNKNOWN = <any>'unknown',
    ACCOUNT_NOT_FOUND = <any>'account-not-found',
    PASSWORD_NOT_MATCH = <any>'password-not-match',
    NOT_ADMIN = <any>'not-admin'
}

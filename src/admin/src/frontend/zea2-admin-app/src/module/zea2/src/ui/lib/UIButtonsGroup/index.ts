import {Component, Input} from '@angular/core';

import {UICRUDButton} from './options';

@Component({
    selector: 'zea2-ui-buttons-group',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class UIButtonsGroup
{
    @Input('config') buttons: UICRUDButton[] = [];

    private helpers: { [id: string]: UIButtonsGroupHelper } = {};

    ngOnChanges(): void {
        if (Array.isArray(this.buttons)) {
            for (let button of this.buttons) {
                this.helpers[button.id] = new UIButtonsGroupHelper(button);
            }
        }
    }

    withButton(b: UICRUDButton): UIButtonsGroupHelper {
        if (this.helpers.hasOwnProperty(b.id)) {
            return this.helpers[b.id];
        } else {
            throw new Error(`No button with ID ${b.id}`);
        }
    }
}

class UIButtonsGroupHelper
{
    constructor(private button: UICRUDButton) {}

    shouldTitleBeTranslated(): boolean {
        return this.button.title.translate;
    }

    getTitle(): string {
        return this.button.title.value;
    }

    hasFAIcon(): boolean {
        return this.button.icon !== undefined;
    }

    getFAIconCSSClasses(): string[] {
        return ['fa', 'fa-' + this.button.icon];
    }

    getButtonCSSClasses(): string[] {
        return ['button', 'is-' + <any>this.button.level];
    }

    isDisabled(): boolean {
        return ! this.button.isActive();
    }
    click(): void {
        this.button.click();
    }
}

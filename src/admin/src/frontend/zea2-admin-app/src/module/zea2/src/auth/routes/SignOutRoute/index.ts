import {Component} from '@angular/core';
import {JWTService} from '../../../../../zea2-common/src/_module/auth/service/JWTService';

import {Router} from '@angular/router';
import {AuthRESTService} from '../../../../../../definitions/src/zea2/src/services/auth/AuthRESTService';

@Component({
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class SignOutRoute
{
    constructor(
        private auth: JWTService,
        private rest: AuthRESTService,
        private router: Router,
    ) {
        this.rest.signOut().subscribe(
            next => {
                this.proceed();
            },
            error => {
                this.proceed();
            }
        );
    }

    private proceed(): void {
        this.auth.clearJWT();
        this.router.navigate(['/admin/public']);
    }
}
import {Component} from '@angular/core';

import {UIGlobalLoadingService} from '../../service/UIGlobalLoadingService';

@Component({
    selector: 'zea2-ui-global-loading-indicator',
    templateUrl: './template.html',

    styleUrls: [
        './style.shadow.scss',
    ]
})
export class UIGlobalLoadingIndicator
{
    constructor(
        private status: UIGlobalLoadingService,
    ) {}

    isLoading(): boolean {
        return this.status.isLoading();
    }
}
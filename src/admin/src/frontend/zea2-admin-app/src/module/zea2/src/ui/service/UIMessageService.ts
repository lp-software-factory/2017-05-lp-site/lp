import {Injectable} from '@angular/core';

export enum UI_MESSAGE_LEVEL
{
	Message = <any>'message',
	Primary = <any>'primary',
	Info = <any>'info',
	Success = <any>'success',
	Warning = <any>'warning',
	Danger = <any>'danger',
}

@Injectable()
export class UIMessageService
{
	private static NEXT_ID = 0;
	private static DEFAULT_TIMEOUT = 5 /* sec */ * 1000 /* ms */;

	private messages: UIMessage[] = [];

	public push(level: UI_MESSAGE_LEVEL, message: string, translate: boolean = false) {
		let id = ++UIMessageService.NEXT_ID;

		this.messages.push({
			id: id,
			level: level,
			translate: translate,
			content: message,
		});

		setTimeout(() => {
			this.remove(id);
		}, UIMessageService.DEFAULT_TIMEOUT);
	}

	public remove(id: number) {
		this.messages = this.messages.filter(message => {
			return message.id !== id;
		});
	}

	public list(): UIMessage[] {
		return this.messages;
	}
}

export interface UIMessage
{
	id: number;
	level: UI_MESSAGE_LEVEL;
	translate: boolean;
	content: string;
}
import {Component, Input} from '@angular/core';
import {UITitleService} from '../../service/UITitleService';
import {UIFilterService} from "../../service/UIFilterService";
import {TranslateService} from '@ngx-translate/core';

@Component({
    selector: 'zea2-ui-title',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class UITitle
{
    constructor(
        public uiTitle: UITitleService,
        private uiFilter: UIFilterService,
        private translate: TranslateService,
    ) {}
}

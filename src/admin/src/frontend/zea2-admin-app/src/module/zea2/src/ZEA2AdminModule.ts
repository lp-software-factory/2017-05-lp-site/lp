import {NgModule} from '@angular/core';
import {RESTAdapter} from '../../zea2-common/src/_module/common/service/RESTAdapter';

import {appRoutes} from '../../../app/app.routing';
import {appTranslateConfig} from '../../../app/app.translate';

import {BrowserModule} from '@angular/platform-browser';
import {CommonModule} from '@angular/common';
import {HttpModule} from '@angular/http';
import {FormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';
import {RouterModule} from '@angular/router';

import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {ZEA2CommonModule} from "../../zea2-common/src/ZEA2CommonModule";

import {JWTService} from '../../zea2-common/src/_module/auth/service/JWTService';
import {UIProtectedRoutesGuard} from './ui/routes/UIProtectedRoutes/guard';
import {UITitleService} from './ui/service/UITitleService';
import {UIGlobalLoadingService} from './ui/service/UIGlobalLoadingService';
import {UIMessageService} from './ui/service/UIMessageService';

import {AuthRoute} from './auth/routes/AuthRoute/index';
import {SignInRoute} from './auth/routes/SignInRoute/index';
import {SignOutRoute} from './auth/routes/SignOutRoute/index';
import {SignInForm} from './auth/component/SignInForm/index';
import {DashboardRoute} from './dashboard/routes/DashboardRoute/index';
import {ProgressLock} from './common/component/ProgressLock/index';
import {UIAdminRoute} from './ui/routes/UIAdminRoute/index';
import {UINotFoundRoute} from './ui/routes/UINotFoundRoute/index';
import {UIProtectedRoutes} from './ui/routes/UIProtectedRoutes/index';
import {UIPublicRoutes} from './ui/routes/UIPublicRoutes/index';
import {UIMainMenu} from './ui/component/UIMainMenu/index';
import {LocaleRoute} from './locale/routes/LocaleRoute/index';
import {LocaleManagerRoute} from './locale/routes/LocaleManagerRoute/index';
import {LocaleCRUD} from './locale/component/LocaleCRUD/index';
import {UITitle} from './ui/component/UITitle/index';
import {UILayoutBothToolbars} from './ui/layouts/UILayoutBothToolbars/index';
import {UILayoutBottomToolbar} from './ui/layouts/UILayoutBottomToolbar/index';
import {UILayoutCenteredLeftAndRight} from './ui/layouts/UILayoutCenteredLeftAndRight/index';
import {UILayoutFullScreen} from './ui/layouts/UILayoutFullScreen/index';
import {UILayoutLeftAndRight} from './ui/layouts/UILayoutLeftAndRight/index';
import {UILayoutTopToolbar} from './ui/layouts/UILayoutTopToolbar/index';
import {UIButtonsGroup} from './ui/lib/UIButtonsGroup/index';
import {UIMessageNotifications} from './ui/component/UIMessageNotifications/index';
import {UIGlobalLoadingIndicator} from './ui/component/UIGlobalLoadingIndicator/index';
import {UIModal} from './ui/lib/UIModal/UIModal/index';
import {UIModalHeader} from './ui/lib/UIModal/UIModalHeader/index';
import {UIModalContent} from './ui/lib/UIModal/UIModalContent/index';
import {UIModalFooter} from './ui/lib/UIModal/UIModalFooter/index';
import {LocaleModalCreate} from './locale/modal/LocaleModalCreate/index';
import {LocaleModalEdit} from './locale/modal/LocaleModalEdit/index';
import {LocaleModalRemove} from './locale/modal/LocaleModalRemove/index';
import {LocaleFormCreate} from './locale/form/LocaleFormCreate/index';
import {LocaleFormEdit} from './locale/form/LocaleFormEdit/index';
import {LocaleFormRemove} from './locale/form/LocaleFormRemove/index';
import {UIFilterService} from "./ui/service/UIFilterService";
import {UISearchBox} from "./ui/component/UISearchBox/index";
import {ImageFileInput} from "./common/component/ImageFileInput/index";
import {ImagePreview} from "./common/component/ImagePreview/index";
import {ImageCropper, ImageCropperService} from "./common/component/ImageCropper/index";
import {AttachmentInstantInput} from "./attachment/component/AttachmentInstantInput/index";
import {AttachmentImageInstantInput} from "./attachment/component/AttachmentImageInstantInput/index";
import {UIHotkeyService} from "./ui/service/UIHotkeyService";
import {AttachmentMultiImageInstantInput} from "./attachment/component/AttachmentMultiImageIstantInput/index";
import {ZEA2AdminFrontendService} from "../../lp/src/frontend/service/ZEA2AdminFrontendService";
import {FileInput} from "./common/component/FileInput/index";
import {AttachmentFileInstantInput} from "./attachment/component/AttachmentFileInstantInput/index";
import {AttachmentMultiFileInstantInput} from "./attachment/component/AttachmentMutliFileInstantInput/index";

@NgModule({
    imports: [
        BrowserModule,
        CommonModule,
        HttpModule,
        FormsModule,

        ZEA2CommonModule,

        RouterModule.forChild(appRoutes),
        TranslateModule.forChild(appTranslateConfig),

        NgxDatatableModule,
    ],
    declarations: [
        // auth,
        AuthRoute,
        SignInRoute,
        SignOutRoute,
        SignInForm,

        // dashboard,
        DashboardRoute,

        //attachment,
        AttachmentInstantInput,
        AttachmentFileInstantInput,
        AttachmentImageInstantInput,
        AttachmentMultiFileInstantInput,
	    AttachmentMultiImageInstantInput,

        // common,
        ProgressLock,
        FileInput,
        ImageFileInput,
        ImagePreview,
        ImageCropper,


        // ui,
        UIAdminRoute,
        UINotFoundRoute,
        UIProtectedRoutes,
        UIPublicRoutes,
        UIMainMenu,
        UISearchBox,
        UITitle,
        UIMessageNotifications,
        UIGlobalLoadingIndicator,

        // ui lib,
        UIButtonsGroup,

        // ui layouts,
        UILayoutBothToolbars,
        UILayoutBottomToolbar,
        UILayoutCenteredLeftAndRight,
        UILayoutFullScreen,
        UILayoutLeftAndRight,
        UILayoutTopToolbar,

        // ui modals,
        UIModal,
        UIModalHeader,
        UIModalContent,
        UIModalFooter,

        // locale
        LocaleRoute,
        LocaleManagerRoute,
        LocaleCRUD,
        LocaleModalCreate,
        LocaleModalEdit,
        LocaleModalRemove,
        LocaleFormCreate,
        LocaleFormEdit,
        LocaleFormRemove,
    ],
    providers: [
        // frontend
        ZEA2AdminFrontendService,

        // auth,
        JWTService,
        UIProtectedRoutesGuard,

        // ui,
        UITitleService,
        UIFilterService,
        UIGlobalLoadingService,
        UIHotkeyService,
        UIMessageService,

        //common,
        ImageCropperService
    ],
    exports: [
        // auth,
        AuthRoute,
        SignInRoute,
        SignOutRoute,

        // dashboard,
        DashboardRoute,

        //attachment,
        AttachmentInstantInput,
        AttachmentFileInstantInput,
        AttachmentImageInstantInput,
        AttachmentMultiFileInstantInput,
	    AttachmentMultiImageInstantInput,

        // common,
        ProgressLock,
        FileInput,
        ImageFileInput,
        ImagePreview,
        ImageCropper,

        // ui,
        UIAdminRoute,
        UINotFoundRoute,
        UIProtectedRoutes,
        UIPublicRoutes,
        UIMainMenu,
        UITitle,
        UIMessageNotifications,
        UIGlobalLoadingIndicator,

        // ui lib,
        UIButtonsGroup,

        // ui layouts,
        UILayoutBothToolbars,
        UILayoutBottomToolbar,
        UILayoutCenteredLeftAndRight,
        UILayoutFullScreen,
        UILayoutLeftAndRight,
        UILayoutTopToolbar,

        // ui modals,
        UIModal,
        UIModalHeader,
        UIModalContent,
        UIModalFooter,

        // locale
        LocaleRoute,
        LocaleManagerRoute,
    ]
})
export class ZEA2AdminModule
{}

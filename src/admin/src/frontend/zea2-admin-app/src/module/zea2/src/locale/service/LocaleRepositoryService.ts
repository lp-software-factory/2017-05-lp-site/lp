import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';

import {GenericEntityRepository} from '../../../../zea2-common/src/_module/common/classes/GenericEntityRepository';
import {LocaleEntity} from '../../../../../definitions/src/zea2/src/definitions/locale/definitions/LocaleEntity';
import {LocaleRESTService} from '../../../../../definitions/src/zea2/src/services/locale/LocaleRESTService';
import {ListLocalesResponse200} from '../../../../../definitions/src/zea2/src/definitions/locale/paths/list';

@Injectable()
export class LocaleRepositoryService
{
    public repository: GenericEntityRepository<LocaleEntity> = new GenericEntityRepository<LocaleEntity>();

    constructor(
        private rest: LocaleRESTService,
    ) {}

    fetch(): Observable<ListLocalesResponse200> {
        let observable = this.rest.list();

        observable.subscribe(next => {
            this.repository.setMany(next.locales);
        });

        return observable;
    }
}
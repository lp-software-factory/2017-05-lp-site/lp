import {Injectable} from "@angular/core";
import {UICRUDHotkey, UICRUDHotkeyEventType} from "../lib/UIHotkey/options";

@Injectable()
export class UIHotkeyService {

    private hotkeys: UICRUDHotkey[] = [];

    addHotkeys(hotkeys) {
       this.hotkeys = this.hotkeys.concat(hotkeys);
    }

    hotkey($event) {
        this.hotkeys.map(hotkey => {
            if (hotkey.keyCode === $event.keyCode && hotkey.eventType === $event.type) {
                $event.preventDefault();
                hotkey.func();
            }
        })
    }

    resetHotkeys() {
        this.hotkeys = [];
    }
}
import {BehaviorSubject} from 'rxjs';
import {Component} from '@angular/core';
import {TableColumn} from '@swimlane/ngx-datatable';
import {TranslateService} from '@ngx-translate/core';

import {LocaleRepositoryService} from '../../service/LocaleRepositoryService';
import {UIGlobalLoadingService} from '../../../ui/service/UIGlobalLoadingService';
import {UICRUDButton, UICRUDButtonVariant} from '../../../ui/lib/UIButtonsGroup/options';
import {LocaleEntity} from '../../../../../../definitions/src/zea2/src/definitions/locale/definitions/LocaleEntity';
import {LocaleModalControlCreate} from '../../modal/LocaleModalCreate/index';
import {LocaleModalControlEdit} from '../../modal/LocaleModalEdit/index';
import {LocaleModalControlRemove} from '../../modal/LocaleModalRemove/index';
import {Subscription} from "rxjs/Subscription";
import {UIFilterService} from "../../../ui/service/UIFilterService";

@Component({
    selector: 'zea2-locale-crud',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ],
    providers: [
        LocaleRepositoryService,
        LocaleModalControlCreate,
        LocaleModalControlEdit,
        LocaleModalControlRemove,
    ]
})
export class LocaleCRUD
{
    private filterSubscription: Subscription;

    public selected: LocaleEntity[] = [];

    public rows: LocaleEntity[] = [];
    private tempRows: LocaleEntity[] = [];
    public columns: TableColumn[];

    public crudButtons: UICRUDButton[] = [
        {
            id: 'add',
            title: {
                value: 'zea2-admin.m.locale.crud.buttons.create',
                translate: true,
            },
            icon: 'plus',
            level: UICRUDButtonVariant.Success,
            isActive: () => {
                return true;
            },
            click: () => {
                this.create();
            }
        },
        {
            id: 'edit',
            title: {
                value: 'zea2-admin.m.locale.crud.buttons.edit',
                translate: true,
            },
            icon: 'pencil-square-o',
            level: UICRUDButtonVariant.Primary,
            isActive: () => {
                return this.selected.length === 1;
            },
            click: () => {
                this.edit(this.getSelected()[0]);
            }
        },
        {
            id: 'delete',
            title: {
                value: 'zea2-admin.m.locale.crud.buttons.delete',
                translate: true,
            },
            icon: 'trash',
            level: UICRUDButtonVariant.Danger,
            isActive: () => {
                return this.selected.length > 0;
            },
            click: () => {
                if (this.selected.length) {
                    this.remove(this.getSelected());
                }
            }
        },
    ];

    constructor(
        private service: LocaleRepositoryService,
        private translate: TranslateService,
        private uiGlobalLoading: UIGlobalLoadingService,
        private uiFilterService: UIFilterService,

        private mCreate: LocaleModalControlCreate<void>,
        private mEdit: LocaleModalControlEdit<BehaviorSubject<LocaleEntity>>,
        private mRemove: LocaleModalControlRemove<BehaviorSubject<LocaleEntity>[]>,
    ) {}

    ngOnInit() {
        this.filterSubscription = this.uiFilterService.filterSubject.subscribe(next => {
            this.rows = this.tempRows.filter(row => {
            return row.region.toLowerCase().indexOf(next.toLowerCase()) !== -1 ||
                   row.language.toLowerCase().indexOf(next.toLowerCase()) !== -1 || !next;
            });
        });


        this.columns = [
            {
                name: 'zea2-admin.m.locale.crud.columns.id',
                prop: 'id',
                width: 50,
                resizeable: true,
                sortable: true,
                draggable: true,
            },
            {
                name: 'zea2-admin.m.locale.crud.columns.language',
                prop: 'language',
                width: 250,
                resizeable: true,
                sortable: true,
                draggable: true,
            },
            {
                name: 'zea2-admin.m.locale.crud.columns.region',
                prop: 'region',
                canAutoResize: true,
                resizeable: true,
                sortable: true,
                draggable: true,
            },
        ];

        this.columns = [
            {
                name: 'zea2-admin.m.locale.crud.columns.id',
                prop: 'id',
                width: 50,
                resizeable: true,
                sortable: true,
                draggable: true,
            },
            {
                name: 'zea2-admin.m.locale.crud.columns.language',
                prop: 'language',
                width: 250,
                resizeable: true,
                sortable: true,
                draggable: true,
            },
            {
                name: 'zea2-admin.m.locale.crud.columns.region',
                prop: 'region',
                canAutoResize: true,
                resizeable: true,
                sortable: true,
                draggable: true,
            },
        ];

        let loading = this.uiGlobalLoading.addLoading();

        for (let column of this.columns) {
            this.translate.get(column.name).subscribe(n => column.name = n);
        }

        this.service.fetch().subscribe(
            undefined,
            error => {
                loading.is = false;
            },
            () => {
                loading.is = false;
            }
        );

        this.service.repository.changes.subscribe(next => {
            this.rows = this.service.repository.getSnapshotAsArray();
            this.tempRows = this.service.repository.getSnapshotAsArray();
        });
    }

    ngOnDestroy() {
        this.filterSubscription.unsubscribe();
        this.uiFilterService.resetSubject.next();
    }

    onActivate($event: MouseEvent) {
        if ($event.type === 'dblclick') {
            this.edit(this.getSelected()[0]);
        }
    }

    getSelected(): BehaviorSubject<LocaleEntity>[] {
        if(this.selected.length > 0){
            let selectedArray:  BehaviorSubject<LocaleEntity>[] = [];
            this.selected.forEach(next => {
                selectedArray.push(this.service.repository.get(next.id))
            });
            return selectedArray;
        } else {
            return undefined;
        }
    }

    create(): void {
        this.mCreate.open(undefined);
    }

    edit(entity: BehaviorSubject<LocaleEntity>): void {
        this.mEdit.open(entity);
    }

    remove(entity: BehaviorSubject<LocaleEntity>[]): void {
        this.mRemove.open(entity);
    }
}


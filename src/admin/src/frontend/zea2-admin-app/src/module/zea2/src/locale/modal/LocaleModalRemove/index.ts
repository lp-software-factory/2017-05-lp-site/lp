import {Component, Injectable} from '@angular/core';

import {ModalControl} from '../../../../../zea2-common/src/_module/common/classes/ModalControl';
import {LocaleEntity} from '../../../../../../definitions/src/zea2/src/definitions/locale/definitions/LocaleEntity';

@Injectable()
export class LocaleModalControlRemove<T> extends ModalControl<T> {}

@Component({
    selector: 'zea2-locale-modal-remove',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ],
    providers: [
        {
            provide: ModalControl,
            useExisting: LocaleModalControlRemove,
        },
    ]
})
export class LocaleModalRemove
{
    constructor(
        public controls: LocaleModalControlRemove<LocaleEntity>,
    ) {}
}
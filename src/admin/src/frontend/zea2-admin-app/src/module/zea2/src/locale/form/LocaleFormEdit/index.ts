import {Component, Input} from '@angular/core';

import {LocaleRepositoryService} from '../../service/LocaleRepositoryService';
import {LocaleRESTService} from '../../../../../../definitions/src/zea2/src/services/locale/LocaleRESTService';
import {ModalControl} from '../../../../../zea2-common/src/_module/common/classes/ModalControl';
import {UIMessageService, UI_MESSAGE_LEVEL} from '../../../ui/service/UIMessageService';
import {EditLocaleRequest} from '../../../../../../definitions/src/zea2/src/definitions/locale/paths/locale';
import {LocaleEntity} from '../../../../../../definitions/src/zea2/src/definitions/locale/definitions/LocaleEntity';
import {BehaviorSubject, Subscription} from 'rxjs';

export interface LocaleFormEditModel
{
    id: number;
    region: string;
    language: string;
}

@Component({
    selector: 'zea2-locale-form-edit',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class LocaleFormEdit
{
    @Input('original') original: BehaviorSubject<LocaleEntity>|undefined;

    private lastSubscription: Subscription;

    public model: LocaleFormEditModel = {
        id: -1,
        region: '',
        language: '',
    };

    constructor(
        private rest: LocaleRESTService,
        private service: LocaleRepositoryService,
        private controls: ModalControl<any>,
        private messages: UIMessageService,
    ) {}

    ngOnChanges(): void {
        if (this.original !== undefined) {
            if (this.lastSubscription) {
                this.lastSubscription.unsubscribe();
            }

            this.original.subscribe(orig => {
                this.model.id = orig.id;
                this.model.region = orig.region;
                this.model.language = orig.language;
            });
        }
    }

    ngOnDestroy(): void {
        if (this.lastSubscription) {
            this.lastSubscription.unsubscribe();
        }
    }

    createRequest(): EditLocaleRequest {
        return {
            region: this.model.region,
            language: this.model.language,
        };
    }

    submit(): void {
        let loading = this.controls.status.addLoading();

        this.rest.edit(this.model.id, this.createRequest()).subscribe(
            next => {
                this.service.repository.set(next.locale);
            },
            error => {
                this.messages.push(UI_MESSAGE_LEVEL.Warning, error);

                loading.is = false;
            },
            () => {
                loading.is = false;

                this.controls.close();
            }
        );
    }

    close() {
        this.controls.close();
    }
}

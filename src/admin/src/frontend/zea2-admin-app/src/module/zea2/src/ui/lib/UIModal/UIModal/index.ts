import {Component} from '@angular/core';

import {ModalControl} from '../../../../../../zea2-common/src/_module/common/classes/ModalControl';

@Component({
    selector: 'zea2-ui-modal',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ],
})
export class UIModal
{
    constructor(
        public controls: ModalControl<any>
    ) {}
}
import {Component} from '@angular/core';

import {AbstractUIMainMenuDefinition, UIMainMenuDefinition} from './definitions';
import {TranslateService} from '@ngx-translate/core';

@Component({
    selector: 'zea2-ui-main-menu',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class UIMainMenu
{
    public definition: UIMainMenuDefinition;

    constructor(
        private provide: AbstractUIMainMenuDefinition,
        private translate: TranslateService,
    ) {
        this.definition = JSON.parse(JSON.stringify(provide.getDefinition()));

        for (let group of this.definition.groups) {
            if (group.title.translate) {
                this.translate.get(group.title.value).subscribe(n => group.title.value = n);
            }

            for (let item of group.items) {
                if (item.title.translate) {
                    this.translate.get(item.title.value).subscribe(n => item.title.value = n);
                }
            }
        }
    }
}
import {Component} from '@angular/core';

import {UITitleService} from '../../../ui/service/UITitleService';

@Component({
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ],
})
export class DashboardRoute
{
    constructor(
        private uiTitle: UITitleService,
    ) {
        uiTitle.setTitle([{title: 'zea2-admin.m.dashboard.title', translate: true}]);
    }
}
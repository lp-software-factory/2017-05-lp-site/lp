import {BehaviorSubject} from 'rxjs';

import {IdEntity} from '../../../../../../definitions/src/zea2/src/definitions/_common/markers/IdEntity';

export class GenericEntityRepository<T extends IdEntity>
{
    public changes: BehaviorSubject<void> = new BehaviorSubject<void>(undefined);

    private entities: { [id: number]: BehaviorSubject<T> } = {};
    private snapshot: { [id: number]: T } = {};

    set(entity: T): void {
        if (this.entities.hasOwnProperty(entity.id.toString())) {
            this.entities[entity.id.toString()].next(entity);
        } else {
            this.entities[entity.id.toString()] = new BehaviorSubject(entity);
            this.entities[entity.id.toString()].subscribe(entity => {
                this.snapshot[entity.id.toString()] = entity;
            });
        }

        this.changes.next(undefined);
    }

    setMany(entities: T[]): void {
        for (let entity of entities) {
            this.set(entity);
        }
    }

    size(): number {
        let result = 0;

        for (let i in this.entities) {
            if (this.entities.hasOwnProperty(i) && this.entities[i] !== undefined) {
                result++;
            }
        }

        return result;
    }

    get(id: number): BehaviorSubject<T> {
        if (this.entities.hasOwnProperty(id.toString())) {
            return this.entities[id.toString()];
        } else {
            throw new Error(`No entity found with ID ${id}`);
        }
    }

    clear(): void {
        for (let i in this.entities) {
            if (this.entities.hasOwnProperty(i) && this.entities[i] !== undefined) {
                this.entities[i].complete();

                delete this.entities[i];
            }
        }


        for (let i in this.snapshot) {
            if (this.snapshot.hasOwnProperty(i) && this.snapshot[i] !== undefined) {
                delete this.snapshot[i];
            }
        }
    }

    getAll(): { [id: number]: BehaviorSubject<T> } {
        return this.entities;
    }

    getSnapshot(): { [id: number]: T } {
        return this.snapshot;
    }

    getSnapshotAsArray(): T[] {
        let result: T[] = [];

        for (let i in this.snapshot) {
            if (this.snapshot.hasOwnProperty(i) && this.snapshot[i] !== undefined) {
                result.push(this.snapshot[i]);
            }
        }

        return result;
    }

    hasEntity(entity: T): boolean {
        return this.getSnapshotAsArray().filter(e => e.id === entity.id).length > 0;
    }

    hasEntityWithId(id: number): boolean {
        return this.entities.hasOwnProperty(id.toString());
    }

    remove(id: number): void {
        if (this.entities.hasOwnProperty(id.toString())) {
            this.entities[id.toString()].complete();

            delete this.entities[id.toString()];
            delete this.snapshot[id.toString()];

            this.changes.next(undefined);
        }
    }
}

import {UIMainMenuDefinition} from '../../../zea2/src/ui/component/UIMainMenu/definitions';

export function appUIMenuFactory() {
    return {
        getDefinition: () => {
            return appUIMainMenu;
        }
    };
}

export const appUIMainMenu: UIMainMenuDefinition = {
    groups: [
        {
            title: {
                value: 'zea2-admin.m.ui.main-menu.items.general.title',
                translate: true,
            },
            items: [
                {
                    icon: 'fa-ellipsis-h',
                    title: {
                        value: 'zea2-admin.m.ui.main-menu.items.general.dashboard',
                        translate: true,
                    },
                    route: {
                        url: ['/admin/protected/dashboard'],
                        exactly: false,
                    }
                },
                {
                    icon: 'fa-ellipsis-h',
                    title: {
                        value: 'zea2-admin.m.ui.main-menu.items.general.locale',
                        translate: true,
                    },
                    route: {
                        url: ['/admin/protected/locale/manager'],
                        exactly: false,
                    }
                },
            ]
        },
        {
            title: {
                value: 'zea2-admin.m.ui.main-menu.items.account.title',
                translate: true,
            },
            items: [
                {
                    icon: 'fa-ellipsis-h',
                    title: {
                        value: 'zea2-admin.m.ui.main-menu.items.account.sign-out',
                        translate: true,
                    },
                    route: {
                        url: ['/admin/public/auth/sign-out'],
                        exactly: false,
                    }
                },
            ]
        },
        {
            title: {
                value: 'lp-admin.m.ui.main-menu.company.title',
                translate: true,
            },
            items: [
                {
                    icon: 'fa-ellipsis-h',
                    title: {
                        value: 'lp-admin.m.ui.main-menu.company.contacts',
                        translate: true,
                    },
                    route: {
                        url: ['/admin/protected/contacts'],
                        exactly: false,
                    }
                },
            ]
        }
    ],
};

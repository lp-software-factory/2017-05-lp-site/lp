import {Injectable} from "@angular/core";
import {Observable} from "rxjs";

import {LpInfo} from "../../../../../definitions/src/lp/src/definitions/contacts/entity/LpContacts";
import {RegisterRecordRESTService} from "../../../../../definitions/src/zea2/src/services/register/record/RegisterRecordRESTService";
import {RegisterRecordEntity} from "../../../../../definitions/src/zea2/src/definitions/register/record/entity/RegisterRecord";

@Injectable()
export class ContactsService
{
    constructor(
        private rest: RegisterRecordRESTService,
    ) {}

    getContacts(): Observable<LpInfo> {
        return Observable.create(observer => {
            this.rest.getBulk({
                keys: [
                    'legal.name',
                    'legal.inn',
                    'legal.kpp',
                    'legal.legal_address',
                    'legal.real_address',
                    'legal.bik',
                    'legal.bank',
                    'legal.account',
                    'legal.account_bank',
                    'contacts.email.main',
                    'contacts.email.additional',
                    'contacts.phone.main',
                    'contacts.phone.additional',
                    'contacts.skype',
                    'contacts.map',
                ]
            }).subscribe(
                next => {
                    let map: { [key: string]: RegisterRecordEntity } = {};

                    for(let record of next.register_records) {
                        map[record.key] = record;
                    }

                    let result: LpInfo = {
                        contacts: {
                            email: {
                                main: map['contacts.email.main'].value,
                                additional: JSON.parse(map['contacts.email.additional'].value),
                            },
                            phone: {
                                main: map['contacts.phone.main'].value,
                                additional: JSON.parse(map['contacts.phone.additional'].value),
                            },
                            skype: map['contacts.skype'].value,
                            map: map['contacts.map'].value,
                        },
                        legal: {
                            name: JSON.parse(map['legal.name'].value),
                            inn: map['legal.inn'].value,
                            kpp: map['legal.kpp'].value,
                            real_address: JSON.parse(map['legal.real_address'].value),
                            legal_address: JSON.parse(map['legal.legal_address'].value),
                            account: map['legal.account'].value,
                            bank: JSON.parse(map['legal.bank'].value),
                            bik: map['legal.bik'].value,
                            account_bank: map['legal.account_bank'].value,
                        }
                    };

                    observer.next(result);
                    observer.complete();
                },
                error => {
                    observer.error(error);
                    observer.complete();
                }
            );
        });
    }
}
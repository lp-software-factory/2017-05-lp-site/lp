import {Component} from "@angular/core";

@Component({
    selector: 'lp-contacts-form',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class ContactsForm
{}
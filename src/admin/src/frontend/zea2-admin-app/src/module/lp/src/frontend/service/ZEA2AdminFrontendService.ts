import {Observable} from 'rxjs';

import {ZEA2AdminFrontend} from '../../../../../definitions/src/lp/src/definitions/frontend/entity/ZEA2AdminFrontend';
import {FrontendService} from "../../../../zea2-common/src/_module/frontend/FrontendService";

export class ZEA2AdminFrontendService<T extends ZEA2AdminFrontend> extends FrontendService<T>
{
    constructor(
        // private rest: ZEA2AdminFrontendRESTService,
    ) { super(); }

    fetch(): Observable<T> {
        return null;
    }
}

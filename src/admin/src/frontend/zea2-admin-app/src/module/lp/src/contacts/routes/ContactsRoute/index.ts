import {Component} from "@angular/core";
import {UITitleService} from "../../../../../zea2/src/ui/service/UITitleService";

@Component({
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ]
})
export class ContactsRoute
{
    constructor(
        private uiTitle: UITitleService,
    ) {}

    ngOnInit(): void {
        this.uiTitle.setTitle([{title: 'lp-admin.m.contacts.title', translate: true}]);
    }
}

import {NgModule} from '@angular/core';

import {ZEA2CommonModule} from "../../zea2-common/src/ZEA2CommonModule";
import {ZEA2AdminModule} from '../../zea2/src/ZEA2AdminModule';

import {RESTAdapter} from '../../zea2-common/src/_module/common/service/RESTAdapter';

import {RouterModule} from '@angular/router';
import {FormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
import {CommonModule} from "@angular/common";
import {BrowserModule} from "@angular/platform-browser";
import {TranslateModule} from '@ngx-translate/core';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';

import {appRoutes} from '../../../app/app.routing';
import {appTranslateConfig} from '../../../app/app.translate';
import {appUIMenuFactory} from './_module/app.ui.main.menu';

import {
  factoryZEA2AdminFrontendRESTService, factoryFeedbackRESTService
} from './_module/app.rest.service';

import {FrontendRESTService} from '../../../definitions/src/lp/src/services/frontend/FrontendRESTService';

import {ZEA2AdminFrontendService} from './frontend/service/ZEA2AdminFrontendService';
import {AbstractUIMainMenuDefinition} from '../../zea2/src/ui/component/UIMainMenu/definitions';
import {FrontendService} from "../../zea2-common/src/_module/frontend/FrontendService";
import {ContactsRoute} from "./contacts/routes/ContactsRoute/index";
import {ContactsForm} from "./contacts/component/ContactsForm/index";
import {ContactsService} from "./contacts/service/ContactsService";

@NgModule({
    imports: [
        ZEA2AdminModule,
        BrowserModule,
        CommonModule,
        HttpModule,
        FormsModule,

        ZEA2CommonModule,

        RouterModule.forChild(appRoutes),
        TranslateModule.forChild(appTranslateConfig),

        NgxDatatableModule,
    ],
    declarations: [
        // contacts
        ContactsRoute,
        ContactsForm,
    ],
    providers: [
        // REST Services
        {
            provide: FrontendRESTService,
            useFactory: factoryZEA2AdminFrontendRESTService,
            deps: [RESTAdapter],
        },
        {
            provide: FrontendService,
            useClass: ZEA2AdminFrontendService,
        },
        // Main Menu
        {
            provide: AbstractUIMainMenuDefinition,
            useFactory: appUIMenuFactory,
        },

        // contacts
        ContactsService,
    ]
})
export class LpAdminModule
{
}

import {RESTAdapter} from '../../../zea2-common/src/_module/common/service/RESTAdapter';

import {FrontendRESTService} from "../../../../definitions/src/lp/src/services/frontend/FrontendRESTService";
import {FeedbackRESTService} from "../../../../definitions/src/lp/src/services/feedback/FeedbackRESTService";

export function factoryZEA2AdminFrontendRESTService(adapter: RESTAdapter) {
	return new FrontendRESTService(adapter);
}

export function factoryFeedbackRESTService(adapter: RESTAdapter) {
	return new FeedbackRESTService(adapter);
}

import {Routes, RouterModule} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';

import {UIAdminRoute} from '../module/zea2/src/ui/routes/UIAdminRoute/index';
import {UIPublicRoutes} from '../module/zea2/src/ui/routes/UIPublicRoutes/index';
import {AuthRoute} from '../module/zea2/src/auth/routes/AuthRoute/index';
import {SignInRoute} from '../module/zea2/src/auth/routes/SignInRoute/index';
import {SignOutRoute} from '../module/zea2/src/auth/routes/SignOutRoute/index';
import {UIProtectedRoutes} from '../module/zea2/src/ui/routes/UIProtectedRoutes/index';
import {UIProtectedRoutesGuard} from '../module/zea2/src/ui/routes/UIProtectedRoutes/guard';
import {DashboardRoute} from '../module/zea2/src/dashboard/routes/DashboardRoute/index';
import {LocaleRoute} from '../module/zea2/src/locale/routes/LocaleRoute/index';
import {LocaleManagerRoute} from '../module/zea2/src/locale/routes/LocaleManagerRoute/index';
import {UINotFoundRoute} from '../module/zea2/src/ui/routes/UINotFoundRoute/index';
import {ContactsRoute} from "../module/lp/src/contacts/routes/ContactsRoute/index";

export const appRoutes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: 'admin'
    },
    {
        path: 'admin',
        pathMatch: 'prefix',
        component: UIAdminRoute,
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'protected'
            },
            {
                path: 'public',
                pathMatch: 'prefix',
                component: UIPublicRoutes,
                children: [
                    {
                        path: '',
                        pathMatch: 'full',
                        redirectTo: 'auth',
                    },
                    {
                        path: 'auth',
                        pathMatch: 'prefix',
                        component: AuthRoute,
                        children: [
                            {
                                path: '',
                                pathMatch: 'full',
                                redirectTo: 'sign-in'
                            },
                            {
                                path: 'sign-in',
                                pathMatch: 'full',
                                component: SignInRoute,
                            },
                            {
                                path: 'sign-out',
                                pathMatch: 'full',
                                component: SignOutRoute,
                            }
                        ]
                    }
                ]
            },
            {
                path: 'protected',
                pathMatch: 'prefix',
                component: UIProtectedRoutes,
                canActivate: [
                    UIProtectedRoutesGuard,
                ],
                children: [
                    {
                        path: '',
                        pathMatch: 'full',
                        redirectTo: 'dashboard',
                    },
                    {
                        path: 'dashboard',
                        pathMatch: 'full',
                        component: DashboardRoute,
                    },
                    {
                        path: 'locale',
                        pathMatch: 'prefix',
                        component: LocaleRoute,
                        children: [
                            {
                                path: '',
                                pathMatch: 'full',
                                redirectTo: 'manager',
                            },
                            {
                                path: 'manager',
                                pathMatch: 'full',
                                component: LocaleManagerRoute,
                            }
                        ]
                    },
                    {
                        path: 'contacts',
                        pathMatch: 'full',
                        component: ContactsRoute,
                    }
                ]
            },
        ]
    },
    {
        path: '**',
        component: UINotFoundRoute,
    }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);

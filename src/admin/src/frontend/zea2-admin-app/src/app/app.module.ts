import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {BrowserModule, DomSanitizer} from '@angular/platform-browser';
import {HttpModule} from '@angular/http';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';

import {AppComponent} from './app.component';

import {appRoutes} from './app.routing';
import {appTranslateConfig} from './app.translate';

import {ZEA2AdminModule} from '../module/zea2/src/ZEA2AdminModule';
import {LpAdminModule} from '../module/lp/src/LpAdminModule';

@NgModule({
    declarations: [
        AppComponent,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    imports: [
        BrowserModule,
        CommonModule,
        HttpModule,
        FormsModule,

        LpAdminModule,
        ZEA2AdminModule,

        RouterModule.forRoot(appRoutes),
        TranslateModule.forRoot(appTranslateConfig),
    ],
    bootstrap: [AppComponent],
})
export class AppModule
{

}

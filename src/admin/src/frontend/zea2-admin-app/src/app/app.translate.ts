import {Observable} from 'rxjs';

import {TranslateModuleConfig, TranslateLoader} from '@ngx-translate/core';
import {ZEA2AdminFrontendService} from "../module/lp/src/frontend/service/ZEA2AdminFrontendService";
import {FrontendService} from "../module/zea2-common/src/_module/frontend/FrontendService";

export const appTranslateConfig: TranslateModuleConfig = {
    loader: {
        provide: TranslateLoader,
        useFactory: factoryTranslateFrontendLoader,
        deps: [
            FrontendService,
        ]
    },
};

export function factoryTranslateFrontendLoader(frontend: ZEA2AdminFrontendService<any>) {
    return new TranslateFrontendLoader(frontend);
}

export class TranslateFrontendLoader implements TranslateLoader
{
    constructor(
        private frontend: FrontendService<any>,
    ) {}

    getTranslation(lang: string): Observable<any> {
        return Observable.create(observer => {
            this.frontend.replay.subscribe(next => {
                if (next.translations) {
                    observer.next(next.translations.resources);
                }
            });
        });
    }
}
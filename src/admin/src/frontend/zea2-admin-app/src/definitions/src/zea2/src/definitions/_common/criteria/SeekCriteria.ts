export interface SeekCriteria
{
	offset: number;
	limit: number;
}
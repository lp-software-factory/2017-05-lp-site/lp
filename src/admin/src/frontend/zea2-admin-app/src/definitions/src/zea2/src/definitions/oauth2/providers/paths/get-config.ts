import {Success200} from '../../../_common/response/Success200';
import {OAuth2ProviderEntity} from '../entity/OAuth2ProviderEntity';

export const urlGetOAuth2ProviderConfig = '/backend/api/oauth2-providers/get-config/{code}';

export interface GetOAuth2ProviderConfigResponse200 extends Success200
{
	provider: OAuth2ProviderEntity;
}
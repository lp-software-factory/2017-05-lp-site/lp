import {Success200} from "../../../_common/response/Success200";
import {RegisterRecordEntity} from "../entity/RegisterRecord";

export const urlRegisterRecordEditBulk = '/backend/api/register/record/edit-bulk';

export interface RegisterRecordEditBulkRequest
{
    key: string,
    value: string,
}

export interface RegisterRecordEditBulkResponse200 extends Success200
{
    register_records: RegisterRecordEntity[];
}
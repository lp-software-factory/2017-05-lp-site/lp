import {Success200} from '../../../_common/response/Success200';
import {AttachmentEntity, AttachmentMetadata} from '../AttachmentEntity';

export const urlAttachmentUpload = '/backend/api/attachment/upload/';

export interface UploadAttachmentResponse200 extends Success200
{
	entity: AttachmentEntity<AttachmentMetadata>;
}
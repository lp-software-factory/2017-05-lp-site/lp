import {Success200} from '../../_common/response/Success200';
import {LocaleEntity} from '../definitions/LocaleEntity';

export const urlEditLocale = '/backend/api/locale/{localeId}/edit';

export interface EditLocaleRequest
{
	language: string;
	region: string;
}

export interface EditLocaleResponse200 extends Success200
{
	locale: LocaleEntity;
}
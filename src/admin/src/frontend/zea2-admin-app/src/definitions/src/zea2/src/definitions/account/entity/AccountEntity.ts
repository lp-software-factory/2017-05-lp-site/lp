import {IdEntity} from '../../_common/markers/IdEntity';

export interface AccountEntity extends IdEntity
{
	sid: string;
	date_created_at: string;
	last_updated_on: string;
	app_access: string[];
	email: string;
	phone: string;
}
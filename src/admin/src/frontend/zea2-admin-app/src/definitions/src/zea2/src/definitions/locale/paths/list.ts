import {Success200} from '../../_common/response/Success200';
import {LocaleEntity} from '../definitions/LocaleEntity';

export const urlListLocales = '/backend/api/locale/list';

export interface ListLocalesResponse200 extends Success200
{
	locales: LocaleEntity[];
}
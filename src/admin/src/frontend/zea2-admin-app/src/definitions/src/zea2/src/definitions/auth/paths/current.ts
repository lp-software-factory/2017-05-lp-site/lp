import {Success200} from '../../_common/response/Success200';
import {JWTAuthToken} from '../entity/JWTAuthToken';

export const urlAuthCurrent = '/backend/api/auth/current';

export interface CurrentResponse200 extends Success200
{
	token: JWTAuthToken;
}
export function template(s, values, opening = '{', closing = '}') {
	let open = opening.replace(/[-[\]()*\s]/g, '\\$&').replace(/\$/g, '\\$');
	let close = closing.replace(/[-[\]()*\s]/g, '\\$&').replace(/\$/g, '\\$');
	let r = new RegExp(open + '(.+?)' + close, 'g');

	let matches = s.match(r) || [];

	matches.forEach(function(match) {
		let key = match.substring(opening.length, match.length - closing.length).trim(); // chop {{ and }}
		let value = typeof values[key] === 'undefined' ? '' : values[key];
		s = s.replace(match, value);
	});

	return s;
}

import {FrontendRESTService} from './frontend/FrontendRESTService';
import {FeedbackRESTService} from "./feedback/FeedbackRESTService";

// Lp services list
// Can be useful for angular.js

export const LP_LIST_SERVICES = [
    FrontendRESTService,
    FeedbackRESTService,
];
import {Success200} from '../../_common/response/Success200';
import {LocaleEntity} from '../definitions/LocaleEntity';

export const urlCreateLocale = '/backend/api/locale/create';

export interface CreateLocaleRequest
{
	language: string;
	region: string;
}

export interface CreateLocaleResponse200 extends Success200
{
	locale: LocaleEntity;
}
import {Success200} from "../../../../../zea2/src/definitions/_common/response/Success200";
import {Feedback} from "../entity/Feedback";

export const urlFeedbackGetById = '/backend/api/feedback/{feedbackId}/get';

export interface FeedbackGetByIdResponse200 extends Success200
{
    feedback: Feedback<any>,
}
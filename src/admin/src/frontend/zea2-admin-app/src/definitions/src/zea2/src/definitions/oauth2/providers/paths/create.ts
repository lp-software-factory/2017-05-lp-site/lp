import {Success200} from '../../../_common/response/Success200';
import {OAuth2ProviderConfigEntity} from '../entity/OAuth2ProviderConfigEntity';
import {OAuth2ProviderEntity} from '../entity/OAuth2ProviderEntity';

export const urlCreateOAuth2Provider = '/backend/api/oauth2-providers/create/';

export interface CreateOAuth2ProviderRequest
{
	code: string;
	handler: string;
	config: OAuth2ProviderConfigEntity;
}

export interface CreateOAuth2ProviderResponse200 extends Success200
{
	provider: OAuth2ProviderEntity;
}
import {IdEntity} from '../../_common/markers/IdEntity';

export interface Locale
{
	entity: LocaleEntity;
}

export interface LocaleEntity extends IdEntity
{
	metadata: {
		version: string;
	};
	language: string;
	region: string;
}
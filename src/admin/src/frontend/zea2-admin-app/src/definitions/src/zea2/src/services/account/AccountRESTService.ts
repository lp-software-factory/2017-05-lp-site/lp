import {Observable} from 'rxjs';

import {RequestResetPasswordResponse200, urlAccountRequestResetPassword, RequestResetPasswordRequest} from '../../definitions/account/paths/request-reset-password';
import {RESTAdapterInterface} from '../../essentials/RESTAdapterInterface';
import {SetLocaleResponse200, urlSetLocale} from '../../definitions/account/paths/set-locale';
import {ResetPasswordRequest, urlAccountResetPassword} from '../../definitions/account/paths/reset-password';
import {template} from '../../functions/template';

export interface AccountRESTAdapterInterface
{
	requestResetPassword(request: RequestResetPasswordRequest): Observable<RequestResetPasswordResponse200>;
	resetPassword(request: ResetPasswordRequest): Observable<RequestResetPasswordResponse200>;
	setLocale(locale: string): Observable<SetLocaleResponse200>;
}

export class AccountRESTService implements AccountRESTAdapterInterface
{
	constructor(private rest: RESTAdapterInterface) {}

	requestResetPassword(request: RequestResetPasswordRequest): Observable<RequestResetPasswordResponse200> {
		let urlParams = {
			email: request.email
		};

		return this.rest.put(urlAccountRequestResetPassword, {}, {
			search: urlParams
		});
	}

	resetPassword(request: ResetPasswordRequest): Observable<RequestResetPasswordResponse200> {
		return this.rest.post(urlAccountResetPassword, request);
	}

	setLocale(region: string): Observable<SetLocaleResponse200> {
		return this.rest.post(template(urlSetLocale, {
			region: region,
		}));
	}
}
export interface SortCriteria
{
	field: string;
	direction: SortCriteriaDirection;
}

export enum SortCriteriaDirection {
	Asc = <any>'asc',
	Desc = <any>'desc',
}
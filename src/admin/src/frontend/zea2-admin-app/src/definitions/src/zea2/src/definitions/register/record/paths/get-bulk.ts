import {RegisterRecordEntity} from "../entity/RegisterRecord";
import {Success200} from "../../../_common/response/Success200";

export const urlRegisterRecordGetBulk = '/backend/api/register/record/get-bulk';

export interface RegisterRecordGetBulkRequest
{
    keys: string[],
}

export interface RegisterRecordGetBulkResponse200 extends Success200
{
    register_records: RegisterRecordEntity[],
}
import {Success200} from '../../../_common/response/Success200';

export const urlGetAllHandlers = '/backend/api/oauth2-providers/get-all/handlers';

export interface GetAllOAuth2HandlersResponse200 extends Success200
{
	handlers: string[];
}
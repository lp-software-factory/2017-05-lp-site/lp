export interface LocalizedString
{
	region: string;
	value: string;
}
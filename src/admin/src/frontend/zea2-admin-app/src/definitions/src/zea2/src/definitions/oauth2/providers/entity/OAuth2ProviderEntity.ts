import {OAuth2ProviderConfigEntity} from './OAuth2ProviderConfigEntity';
import {IdEntity} from '../../../_common/markers/IdEntity';

export interface OAuth2ProviderEntity extends IdEntity
{
	sid: string;
	date_created_at: string;
	last_updated_on: string;
	metadata: {
		version: string
	};
	code: string;
	handler: string;
	config: OAuth2ProviderConfigEntity;
}

import {Success200} from "../../../_common/response/Success200";
import {RegisterRecordEntity} from "../entity/RegisterRecord";

export const urlRegisterRecordGetById = '/backend/api/register/record/{recordId}/get';

export interface RegisterRecordGetByIdResponse200 extends Success200
{
    register_record: RegisterRecordEntity;
}
export interface ImageEntity
{
	id: string;
	public_path: string;
	storage_path?: string;
}
import {Success200} from '../../_common/response/Success200';
import {LocaleEntity} from '../definitions/LocaleEntity';

export const urlGetLocale = '/backend/api/locale/{id}/get';

export interface GetLocaleResponse200 extends Success200
{
	locale: LocaleEntity;
}
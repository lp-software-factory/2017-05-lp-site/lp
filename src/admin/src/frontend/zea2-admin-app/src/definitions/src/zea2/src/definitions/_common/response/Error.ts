export interface Error
{
	success: boolean;
	error: string;
}
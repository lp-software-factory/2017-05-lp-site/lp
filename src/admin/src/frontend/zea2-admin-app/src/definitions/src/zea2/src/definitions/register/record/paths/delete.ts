import {Success200} from "../../../_common/response/Success200";

export const urlRegisterRecordDelete = '/backend/api/register/record/{recordId}/delete';

export interface RegisterRecordDeleteResponse200 extends Success200
{
}
import {Success200} from '../../../_common/response/Success200';
import {OAuth2ProviderConfigEntity} from '../entity/OAuth2ProviderConfigEntity';
import {OAuth2ProviderEntity} from '../entity/OAuth2ProviderEntity';

export const urlSetOAuth2Provider = '/backend/api/oauth2-providers/set-config/{code}';

export interface SetOAuth2ProviderConfigRequest
{
	config: OAuth2ProviderConfigEntity;
}

export interface SetOAuth2ProviderConfigResponse200 extends Success200
{
	provider: OAuth2ProviderEntity;
}
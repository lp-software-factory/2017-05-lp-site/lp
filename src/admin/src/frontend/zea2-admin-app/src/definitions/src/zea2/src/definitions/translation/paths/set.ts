import {Success200} from '../../_common/response/Success200';
import {TranslationEntity} from '../entity/Translation';
import {LocalizedString} from '../../locale/definitions/LocalizedString';

export const urlTranslationSet = '/backend/api/translation/{projectId}/set';

export interface TranslationSetRequest
{
    value: LocalizedString[];
}

export interface TranslationSetResponse200 extends Success200
{
    translation: TranslationEntity;
}
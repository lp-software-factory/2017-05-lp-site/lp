import {Success200} from '../../_common/response/Success200';
import {JWTAuthToken} from '../../auth/entity/JWTAuthToken';

export const urlAccountResetPassword = '/backend/api/account/reset-password';

export interface ResetPasswordResponse200 extends Success200
{
	token: JWTAuthToken;
}

export interface ResetPasswordRequest
{
	accept_token: string;
	new_password: string;
}
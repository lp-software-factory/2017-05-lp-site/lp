import {IdEntity} from "../../../../../zea2/src/definitions/_common/markers/IdEntity";
import {SIDEntity} from "../../../../../zea2/src/definitions/_common/markers/SIDEntity";
import {ModificationEntity} from "../../../../../zea2/src/definitions/_common/markers/ModificationEntity";
import {VersionEntity} from "../../../../../zea2/src/definitions/_common/markers/VersionEntity";
import {AttachmentEntity} from "../../../../../zea2/src/definitions/attachment/entity/AttachmentEntity";

export enum FeedbackMessageMailStatusENUM
{
    Sent = 0,
    Waiting = 1,
    Fail = 2,
}

export enum FeedbackAnswerMailStatusENUM
{
    Sent = 0,
    Waiting = 1,
    Fail = 2,
}

export interface FeedbackEntity extends IdEntity, SIDEntity, ModificationEntity, VersionEntity
{
    reference: string,
    is_read: boolean;
    date_read?: string;
    from: {
        name: string,
        email: string,
        phone: string,
        mail_status: FeedbackMessageMailStatusENUM,
    },
    message: string,
    message_attachment_ids: number[],
    answer?: {
        message: string,
        message_attachment_ids: number[],
        date: string,
        mail_status: FeedbackAnswerMailStatusENUM,
    }
}

export enum FeedbackReferenceTypeENUM
{
    Global = <any>"global",
}

export interface Feedback<T> extends IdEntity
{
    entity: FeedbackEntity,
    message_attachments: AttachmentEntity<any>[],
    answer_attachments: AttachmentEntity<any>[],
    reference: {
        type: FeedbackReferenceTypeENUM,
        entity: T,
    },
}
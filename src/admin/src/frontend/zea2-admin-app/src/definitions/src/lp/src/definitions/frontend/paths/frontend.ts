import {ZEA2AdminFrontend} from '../entity/ZEA2AdminFrontend';
import {LpAppFrontend} from '../entity/LpAppFrontend';

export const urlFrontend = '/backend/api/frontend/';

export interface GetAdminFrontendResponse200 extends ZEA2AdminFrontend
{}

export interface GetAppFrontendResponse200 extends LpAppFrontend
{}
import {IdEntity} from "../../../_common/markers/IdEntity";
import {SIDEntity} from "../../../_common/markers/SIDEntity";
import {VersionEntity} from "../../../_common/markers/VersionEntity";
import {ModificationEntity} from "../../../_common/markers/ModificationEntity";

export interface RegisterRecordEntity extends IdEntity, SIDEntity, VersionEntity, ModificationEntity
{
    register_record_group_id: number,
    key: string,
    value: string,
    options: {
        exports_to_frontend: boolean,
    }
}
import {Success200} from '../../../_common/response/Success200';
import {OAuth2ProviderEntity} from '../entity/OAuth2ProviderEntity';

export const urlGetAllOAuth2Providers = '/backend/api/oauth2-providers/get-all/providers';

export interface GetAllOAuth2ProvidersResponse200 extends Success200
{
	providers: Array<OAuth2ProviderEntity>;
}
import {Observable} from 'rxjs/Observable';

import {RESTAdapterInterface} from '../../essentials/RESTAdapterInterface';
import {UploadAttachmentResponse200, urlAttachmentUpload} from '../../definitions/attachment/entity/paths/upload';
import {LinkAttachmentResponse200, urlAttachmentLink} from '../../definitions/attachment/entity/paths/link';
import {UploadObservableFactory, UploadObservableProgress} from '../../functions/upload';

export interface AttachmentRESTAdapterInterface
{
	link(url: string): Observable<LinkAttachmentResponse200>;
	upload(file: Blob): Observable<UploadAttachmentResponse200|UploadObservableProgress>;
}

export class AttachmentRESTService implements AttachmentRESTAdapterInterface
{
	constructor(private rest: RESTAdapterInterface) {}

	link(url: string): Observable<LinkAttachmentResponse200> {

		return this.rest.put(urlAttachmentLink, {}, {
			search: {
				url: url,
			},
		});
	}

	upload(file: Blob): Observable<UploadAttachmentResponse200|UploadObservableProgress> {
		return (new UploadObservableFactory<UploadAttachmentResponse200>()).create({
			method: 'post',
			url: urlAttachmentUpload,
			file: file
		});
	}
}

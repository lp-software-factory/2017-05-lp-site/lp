export interface ModificationEntity
{
	date_created_at: string;
	last_updated_on: string;
}
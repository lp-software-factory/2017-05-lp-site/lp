export interface TranslationByRegion
{
    key: string;
    value: string;
}
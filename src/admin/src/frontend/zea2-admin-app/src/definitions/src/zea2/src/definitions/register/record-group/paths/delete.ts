import {Success200} from "../../../_common/response/Success200";
import {RegisterRecordGroupEntity} from "../entity/RegisterRecordGroup";

export const urlRegisterRecordGroupDelete = '/backend/api/register/group/{recordGroupId}/delete';

export interface RegisterRecordGroupDeleteResponse200 extends Success200
{
}
import {Observable} from 'rxjs';

import {urlFrontend} from '../../definitions/frontend/paths/frontend';
import {RESTAdapterInterface} from '../../../../zea2/src/essentials/RESTAdapterInterface';
import {LpAppFrontend} from '../../definitions/frontend/entity/LpAppFrontend';
import {ZEA2AdminFrontend} from '../../definitions/frontend/entity/ZEA2AdminFrontend';

export interface FrontendRESTAdapterInterface
{
	getAppFrontend(): Observable<LpAppFrontend>;
	getAdminFrontend(): Observable<ZEA2AdminFrontend>;
}

export class FrontendRESTService implements FrontendRESTAdapterInterface
{
	constructor(private rest: RESTAdapterInterface) {
	}

	getAppFrontend(): Observable<LpAppFrontend> {
		return this.rest.get(urlFrontend);
	}

	getAdminFrontend(): Observable<ZEA2AdminFrontend> {
		return this.rest.get(urlFrontend);
	}
}

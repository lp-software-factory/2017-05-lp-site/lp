import {Success200} from '../../_common/response/Success200';

export const urlAuthSignOut = '/backend/api/auth/sign-out';

export interface SignOutResponse200 extends Success200 {}
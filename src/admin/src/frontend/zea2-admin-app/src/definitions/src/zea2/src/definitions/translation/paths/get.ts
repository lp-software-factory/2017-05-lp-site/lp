import {Success200} from '../../_common/response/Success200';
import {TranslationEntity} from '../entity/Translation';

export const urlTranslationGet = '/backend/api/translation/{projectId}/get';

export interface TranslationGetResponse200 extends Success200
{
    translation: TranslationEntity;
}
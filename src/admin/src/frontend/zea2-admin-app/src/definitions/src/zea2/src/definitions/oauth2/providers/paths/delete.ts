import {Success200} from '../../../_common/response/Success200';

export const urlDeleteOAuth2Provider = '/backend/api/oauth2-providers/delete/{code}';

export interface DeleteOAuth2ProviderResponse200 extends Success200
{
}
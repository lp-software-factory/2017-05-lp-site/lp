import {Success200} from "../../../_common/response/Success200";
import {RegisterRecordEntity} from "../entity/RegisterRecord";

export const urlRegisterRecordEdit = '/backend/api/register/record/{recordId}/edit';

export interface RegisterRecordEditRequest
{
    register_record_group_id: number,
    key: string,
    value: string,
    options: {
        exports_to_frontend: boolean,
    }
}

export interface RegisterRecordEditResponse200 extends Success200
{
    register_record: RegisterRecordEntity;
}
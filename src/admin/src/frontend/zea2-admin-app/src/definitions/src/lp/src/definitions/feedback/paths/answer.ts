import {Success200} from "../../../../../zea2/src/definitions/_common/response/Success200";
import {Feedback} from "../entity/Feedback";

export const urlFeedbackAnswer = '/backend/api/feedback/{feedbackId}/answer';

export interface FeedbackAnswerRequest
{
    message: string,
    message_attachment_ids: number[],
}

export interface FeedbackAnswerResponse200 extends Success200
{
    feedback: Feedback<any>,
}
04-LAYOUTS
==========

Для расположения блоков на странице используйте компоненты:

- `UILayoutBothToolbars`
- `UILayoutBottomToolbar`
- `UILayoutCenteredLeftAndRight`
- `UILayoutFullScreen`
- `UILayoutLeftAndRight`
- `UILayoutTopToolbar`
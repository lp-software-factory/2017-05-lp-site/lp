03-TITLE.MD
===========

При заходе на тот или иной роут необходимо устанавливать заголовок страницы.

```
import {Component} from "@angular/core";

import {UITitleService} from "../../../ui/service/UITitleService";

@Component({
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ],
})
export class LocaleManagerRoute
{
    constructor(
        private uiTitle: UITitleService,
    ) {
        uiTitle.setTitle('zea2-admin.m.locale.manager.title', { translate: true });
    }
}
```

Основные правила
----------------

-  Запрешено устанавливать заголовок в компонентах. Только в роутах.
01-LOADING.MD
=============

Для индикации (глобальной) загрузки используйте `UGlobalLoadingService`:

```
import {Component} from "@angular/core";

import {LocaleCRUDService} from "../../service/LocaleCRUDService";
import {UIGlobalLoadingService} from "../../../ui/service/UIGlobalLoadingService";

@Component({
    selector: 'zea2-locale-crud',
    templateUrl: './template.html',
    styleUrls: [
        './style.shadow.scss',
    ],
    providers: [
        LocaleCRUDService,
    ]
})
export class LocaleCRUD
{
    constructor(
        private service: LocaleCRUDService,
        private uiGlobalLoading: UIGlobalLoadingService,
    ) {
        let loading = this.uiGlobalLoading.addLoading();
        
        this.service.fetch().subscribe(
            undefined,
            error => {
                loading.is = false;
            },
            () => {
                loading.is = false;
            }
        )
    }
}
```
#!/usr/bin/env bash
export LC_ALL="en_US.UTF-8"

sudo su - postgres -c "dropdb zea2"
sudo su - postgres -c "dropdb zea2_testing"
sudo su - postgres -c "createdb zea2"
sudo su - postgres -c "createdb zea2_testing"
sudo su - postgres -c $'psql -c "GRANT ALL PRIVILEGES ON DATABASE zea2 to zea2_pgsql"'
sudo su - postgres -c $'psql -c "GRANT ALL PRIVILEGES ON DATABASE zea2 to zea2_pgsql"'

cd /opt/vm-project/src/api
./vendor/bin/phinx migrate
./vendor/bin/phinx migrate -e zea2_testing

sudo chmod -R 777 /tmp
sudo rm -rf /var/vm-project/storage/storage/entity/attachment/*
sudo chmod -R 777 /var/vm-project/storage
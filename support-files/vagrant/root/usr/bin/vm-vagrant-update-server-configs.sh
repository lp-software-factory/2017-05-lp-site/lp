#!/usr/bin/env bash
cp -R /support-files/vagrant/root/* /

sudo sed -i 's/\r$//' /usr/bin/vm-*

chmod a+x /usr/bin/vm-*

sudo service php7.1-fpm restart
sudo service mongod restart
sudo service postgresql restart
sudo service nginx restart
#!/usr/bin/env bash

# server software
/usr/bin/vm-vagrant-update-server-configs.sh

# composer.phar
cd /opt/vm-project/src/api;
composer.phar update --ignore-platform-reqs -vvv

# database & stage
cd /opt/vm-project/src/api
./vendor/bin/phinx migrate
./vendor/bin/phinx migrate -e zea2_testing

# storage
sudo chown -R www-data:www-data /var/vm-project/storage

# reset
sudo service php7.1-fpm restart
sudo service nginx restart
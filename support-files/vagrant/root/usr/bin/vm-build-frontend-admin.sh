#!/usr/bin/env bash
cd /opt/vm-project/src/admin/src/frontend/zea2-admin-app
pug src/
ng build --aot --prod --output-hashing none

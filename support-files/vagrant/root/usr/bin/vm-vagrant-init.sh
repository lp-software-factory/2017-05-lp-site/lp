#!/usr/bin/env bash

# apt-get update
sudo apt-get update

# ppa
sudo apt-get install -y language-pack-en-base
sudo LC_ALL=en_US.UTF-8 add-apt-repository -y ppa:ondrej/php

# apt-get update
sudo apt-get update

# apt-get install
sudo apt-get install -y --allow-unauthenticated ffmpeg curl php7.1 php7.1-fpm php7.1-pgsql php7.1-zip php7.1-curl \
    php7.1-xml php7.1-gd php7.1-bcmath php7.1-mbstring php7.1-dom git npm nginx nginx-extras \
    php-pear php7.1-dev pkg-config libssl-dev libsslcommon2-dev php7.1-intl mongodb postgresql \
    sendmail htop memcached

# Install composer.phar
cd /tmp
sudo curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/bin/
sudo chmod a+x /usr/bin/composer.phar
sudo ln -s /usr/bin/composer.phar /usr/bin/composer

# composer.phar
cd /opt/vm-project/src/api;
sudo composer.phar dump-autoload -vvv
sudo composer.phar install --ignore-platform-reqs -vvv
sudo composer.phar update --ignore-platform-reqs -vvv

# npm update
npm install -g n
n stable

# error log
sudo touch /var/log/php-errors.log
sudo chown -R www-data:www-data /var/log/php-errors.log

# phpunit
cd /tmp
wget https://phar.phpunit.de/phpunit-5.7.phar
chmod +x phpunit-5.7.phar
sudo mv phpunit-5.7.phar /usr/local/bin/phpunit

# postgresql
sudo adduser --disabled-password --gecos "" zea2_pgsql
sudo su - postgres -c "createdb zea2"
sudo su - postgres -c "createdb zea2_testing"
sudo su - postgres -c $'psql -c "CREATE USER zea2_pgsql WITH PASSWORD \'1234\'"'
sudo su - postgres -c $'psql -c "GRANT ALL PRIVILEGES ON DATABASE zea2 to zea2_pgsql"'
sudo su - postgres -c $'psql -c "GRANT ALL PRIVILEGES ON DATABASE zea2_testing to zea2_pgsql"'

# migrate
sudo /usr/bin/vm-vagrant-migrate.sh

# migrations
sudo chmod a+x /support-files/vagrant-migrations/*

sudo /support-files/vagrant-migrations/000001_mongodb.sh
sudo /support-files/vagrant-migrations/000002_memcached.sh
sudo /support-files/vagrant-migrations/000003_apc.sh

# restart
sudo service mongod restart
sudo service postgresql restart
sudo service php7.1-fpm restart
sudo service nginx restart

sudo chown -R www-data:www-data /var/vm-project/storage

# vm-dev-reset

/usr/bin/vm-dev-reset.sh
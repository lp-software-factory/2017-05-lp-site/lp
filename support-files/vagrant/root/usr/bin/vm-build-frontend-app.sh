#!/usr/bin/env bash
cd /opt/vm-project/src/app/src/frontend/lp-app
pug src/
ng build --aot --prod --output-hashing none

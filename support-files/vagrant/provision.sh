#!/usr/bin/env bash
sudo cp -R /support-files/vagrant/root/usr/bin/* /usr/bin/
sudo sed -i 's/\r$//' /usr/bin/vm-*
sudo chmod a+x /usr/bin/vm-*
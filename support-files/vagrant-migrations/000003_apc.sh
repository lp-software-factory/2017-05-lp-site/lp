#!/usr/bin/env bash

cd /tmp
wget https://pecl.php.net/get/apcu-5.1.7.tgz
tar -xvf apcu-5.1.7.tgz
cd apcu-5.1.7
phpize7.1 ./configure --with-php-config=/usr/bin/php-config7.1
./configure
make
sudo make install
sudo ln -s /etc/php/7.1/mods-available/apcu.ini /etc/php/7.1/cli/conf.d/20-apcu.ini
sudo ln -s /etc/php/7.1/mods-available/apcu.ini /etc/php/7.1/fpm/conf.d/20-apcu.ini

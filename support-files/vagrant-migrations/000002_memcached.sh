#!/usr/bin/env bash

sudo apt-get install libmemcached-dev -y
cd /tmp
git clone https://github.com/php-memcached-dev/php-memcached.git
cd php-memcached
git checkout php7
phpize7.1 ./configure --with-php-config=/usr/bin/php-config7.1
./configure
make
sudo make install
sudo ln -s /etc/php/7.1/mods-available/memcached.ini /etc/php/7.1/cli/conf.d/20-memcached.ini
sudo ln -s /etc/php/7.1/mods-available/memcached.ini /etc/php/7.1/fpm/conf.d/20-memcached.ini
